package features.components.dependency;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import trainingDataGenerator.AnnotatedSentenceParser.EntityPairMention;
import trainingDataGenerator.AnnotatedSentenceParser.Range;
import trainingDataGenerator.AnnotatedSentenceParser.SemanticGraphContainer;
import trainingDataGenerator.AnnotatedSentenceParserAdapter;
import trainingDataGenerator.AnnotatedSentenceParserAdapter.AnnotationParsingException;
import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.trees.GrammaticalRelation;
import edu.stanford.nlp.trees.semgraph.SemanticGraph;

/**
 * This was an attempt to use SemanticGraph implementation by stanford
 * Does not work for what we need because there is no way of determinsitically finding head of 
 * entities
 * @author mhotan
 *
 */
public class DependencySemanticWrapper {

	public enum ENTITY {ONE, TWO};

	/**
	 * Dependency graph for all the results
	 */
	private final SemanticGraph mDepGraph;

	/**
	 * 
	 */
	private final IndexedWord[] mVertices; 

	private final DepEntityField mEntity1, mEntity2;

	public static DependencySemanticWrapper build(AnnotatedSentenceParserAdapter parser, 
			EntityPairMention pairMention) throws AnnotationParsingException{

		SemanticGraphContainer graphContainer = parser.getSemanticGraph();
		if (graphContainer == null) {
			throw new IllegalArgumentException("Unable to build Dependency graph because parser was unable to provide" +
					"Semantic Graph");
		}

		SemanticGraph g = graphContainer.graph;
		List<IndexedWord> verticeList = g.vertexListSorted();
		
		IndexedWord[] vertices = new IndexedWord[verticeList.size()];
		for (int i = 0; i < verticeList.size(); ++i){
			vertices[i] = verticeList.get(i);
		}
		
		int[] ent1VerticeIndexes = getVerticeIndexes(vertices, pairMention.tokenRange1);
		int[] ent2VerticeIndexes = getVerticeIndexes(vertices, pairMention.tokenRange2);

		StringBuffer b = new StringBuffer();
		b.append("Entity 1: ");
		for (int i: ent1VerticeIndexes){
			b.append(" ");
			b.append(vertices[i]);
		}
		System.out.println(b);
		
		b = new StringBuffer();
		b.append("Entity 2: ");
		for (int i: ent2VerticeIndexes){
			b.append(" ");
			b.append(vertices[i]);
		}
		System.out.println(b);
		
		// Do a breadth first search for all the roots to find
		List<IndexedWord> roots = new ArrayList<IndexedWord>(g.getRoots());
		Collections.sort(roots);

		if (roots.size() > 1) {
			System.out.println("Multiple Roots found for single sentence");
		}
		List<Integer> eh1s = new ArrayList<Integer>();
		List<Integer> eh2s = new ArrayList<Integer>();
		for (IndexedWord r: roots){
			// Get entity 1 head through BFS
			IndexedWord[] entity1Words = getIndexedWordsWithinRange(vertices, pairMention.tokenRange1);
			if (containsIndexWord(entity1Words, r))
				eh1s.add(r.index());
			else
				eh1s.add(BFS(g, r, entity1Words));
			
			// Get entity 2 head through BFS
			IndexedWord[] entity2Words = getIndexedWordsWithinRange(vertices, pairMention.tokenRange1);
			if (containsIndexWord(entity2Words, r))
				eh2s.add(r.index());
			else
				eh2s.add(BFS(g, r, entity2Words));
		}


		return null;
	}
	
	private static int[] getVerticeIndexes(IndexedWord[] allVertices, Range r){
		try{ // Easy way to check out of bounds
			IndexedWord i = allVertices[r.getEnd()-1];
			i = allVertices[r.getStart()];
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new IllegalArgumentException("Illegal range out of bounds of vertices: " + r + " " + e.getMessage());
		}
		
		// Indeces that point back into vertice list
		int[] correlatedIndeces = new int[r.getEnd()- r.getStart()];
		for (int i = 0; i < correlatedIndeces.length; ++i) {
			int targetSentIndex = r.getStart() + i;
			for (IndexedWord w: allVertices) {
				int sentIndex = w.sentIndex();
				if (sentIndex == targetSentIndex){
					correlatedIndeces[i] = w.index();
					break;
				}
			}
		}
		return correlatedIndeces;
	}

	private static IndexedWord[] getIndexedWordsWithinRange(IndexedWord[] vertices, Range r){
		try{ // Easy way to check out of bounds
			IndexedWord i = vertices[r.getEnd()-1];
			i = vertices[r.getStart()];
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new IllegalArgumentException("Illegal range out of bounds of vertices: " + r + " " + e.getMessage());
		}
		// Get range of tokens for the word
		int numSearches = r.getEnd() - r.getStart();
		IndexedWord[] subIndexWords = new IndexedWord[numSearches];
		// 
		for (int i = 0; i < numSearches; ++i) {
			subIndexWords[i] = vertices[r.getStart()+i];
		}
		return subIndexWords;
	}

	/**
	 * 
	 * @param g graph that contains Indexed words
	 * @param root Root to search in
	 * @param targets Targets to be searched for
	 * @param seen set of all seen elements.  This will help eliminate against a cycle
	 * @return -1 if targets cant be found, index of the indexed word other wise
	 */
	private static int BFS(SemanticGraph g, IndexedWord root,
			IndexedWord[] targets){
		// check if we have reached one of our targets
	
		Queue<IndexedWord> q = new LinkedList<IndexedWord>();
		Set<IndexedWord> marked = new HashSet<IndexedWord>();
		
		//Straight from wikipedia BFS search
		q.add(root);
		marked.add(root);
		while (!q.isEmpty()){
			IndexedWord t = q.remove();
			if (containsIndexWord(targets, t))
				return t.index();
			List<IndexedWord> children = g.getChildrenWithReln(t, GrammaticalRelation.DEPENDENT);
			Collections.sort(children);
			for (IndexedWord child : children){
				if (!marked.contains(child)) {
					marked.add(child);
					q.add(child);
				}
			}
		}
		//Got nothing
		return -1;
	}

	/**
	 * Checks for index equality with in
	 * @param keys
	 * @param query
	 * @return
	 */
	private static boolean containsIndexWord(IndexedWord[] keys, IndexedWord query){
		if (query == null) return false;
		for (IndexedWord w: keys)
			if (w.index() == query.index())
				return true;
		return false;
	}

	public IndexedWord getHeadOfEntity(ENTITY which){
		switch (which){
		case ONE: return mVertices[mEntity1.mHead];
		case TWO: return mVertices[mEntity2.mHead]; 
		default: return null;
		}
	}

	public IndexedWord getTailOfEntity(ENTITY which){
		switch (which){
		case ONE: return mVertices[mEntity1.mTail];
		case TWO: return mVertices[mEntity2.mTail]; 
		default: return null;
		}
	}

	private DependencySemanticWrapper(SemanticGraph g, IndexedWord[] tokens, 
			DepEntityField entity1, DepEntityField entity2){
		mDepGraph = g;
		mVertices = tokens;
		mEntity1 = entity1;
		mEntity2 = entity2;
		checkRep();
	}
	
	/**
	 * Container for vertex based indexes
	 * @author mhotan
	 */
	private class DepEntityField {
		final int mHead; // Vertice index
		final int mTail; // Vertice index
		// Each value points to the vertice index 
		// that relates to the sentence index
		private final int[] mVerticeIndexes;
		private final IndexedWord[] mAllVertices;
		/**
		 * All indexes are base off of allVertices. 
		 * @param headIndex Index that points to the vertex index
		 * @param tailIndex
		 * @param vertexIndexes
		 * @param allVertices 
		 */
		public DepEntityField(int headIndex, int tailIndex, 
				int[] vertexIndexes, IndexedWord[] AllVertices) {
			mHead = headIndex;
			mTail = tailIndex;
			mVerticeIndexes = vertexIndexes;
			mAllVertices = AllVertices;
			checkRep();
		}
		
		private void chechRep(){
			int size = mAllVertices.length;
			if (mHead < 0 || mHead >= size)
				throw new RuntimeException("Entity Head Index out of bounds, Index: " 
						+ mHead + " Num tokens: " +size );
			if (mTail < 0 || mTail >= size)
				throw new RuntimeException("Entity Tail Index out of bounds, Index: " 
						+ mTail + " Num tokens: " +size );
			if (mVerticeIndexes == null)
				throw new RuntimeException("Null Vertices Indexes");
			for (int vertexIndex: mVerticeIndexes) {
				if (vertexIndex < 0 || vertexIndex >= size)
					throw new RuntimeException("Vertex Index out of bounds, Index: " 
							+ vertexIndex + " Num tokens: " +size );
			}
		}
	}

	private void checkRep() {
		if (mDepGraph == null)
			throw new RuntimeException("Null Graph at build time");
		if (mVertices == null)
			throw new RuntimeException("Null Index word array at build time");
		
		
	}
}
