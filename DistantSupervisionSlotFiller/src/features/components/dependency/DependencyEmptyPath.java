package features.components.dependency;

import java.util.ArrayList;
/**
 * Hack to return a Dependency Path that represents a single node with no dependencies to anything else
 * @author mhotan
 *
 */
class DependencyEmptyPath extends DependencyPath {

	public DependencyEmptyPath(DependencyRelationGraph graph, DependencyNode alone) {
		super(graph, new ArrayList<DependencyNode>(), alone, null);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected String getPathString(PRINT_TYPE type){
		return NO_PATH;
	}
	
}
