package features.components.dependency;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import trainingDataGenerator.AnnotatedSentenceParser.DeptCCProcessed;
import trainingDataGenerator.AnnotatedSentenceParser.EntityPairMention;
import trainingDataGenerator.AnnotatedSentenceParser.GrammaticalDependency;
import trainingDataGenerator.AnnotatedSentenceParser.Stanfordlemma;
import trainingDataGenerator.AnnotatedSentenceParser.Stanfordner;
import trainingDataGenerator.AnnotatedSentenceParser.Stanfordpos;
import trainingDataGenerator.AnnotatedSentenceParser.Tokens;
import trainingDataGenerator.AnnotatedSentenceParserAdapter;
import trainingDataGenerator.AnnotatedSentenceParserAdapter.AnnotationParsingException;
import util.GraphConstructionException;
import util.Logger;
import features.components.Graph;
import features.components.RelationInstanceNode.ENTITY_POS;

/**
 * A graphical representation of grammatical dependencies of a sentence
 * 
 * @author mhotan 
 */
public class DependencyRelationGraph extends Graph<DependencyNode, DependencyEdge>{

	private static final String TAG = "DependencyRelationGraph";

	/**
	 * If no Semantic Graph is available use our own graph implementation 
	 */
	private DependencyNode arg1Head, arg2Head, arg1Tail, arg2Tail, mRoot;

	/**
	 * Create a grammatical dependency 
	 */
	private List<DependencyPath> mEntity1Dependencies, mEntity2Dependencies;
	private DependencyPath mPathBetweenEntities;
	/**
	 * ROOT node never changes per graph
	 */
	//	private static final DependencyNode ROOT = new DependencyNode(-1, 0, ENTITY_POS.NOT_ENTITY);


	/**
	 * Ensure no clients can make their own instance
	 */
	private DependencyRelationGraph(){
	}

	/**
	 * Build graph of dependency relations with out debugging
	 * @param parser
	 * @param pairMention
	 * @return
	 * @throws AnnotationParsingException 
	 */
	public static DependencyRelationGraph buildGraph(AnnotatedSentenceParserAdapter parser, 
			EntityPairMention pairMention) throws AnnotationParsingException{
		return buildGraph(parser, pairMention, false);
	}


	/**
	 * Build graph of dependency relations
	 * <b>In order to debug appropiately the annotations provided must be created by annotation generator
	 * @param annotations Mapping of annotation constants to stanford dependencies
	 * @param pairMention Information involving entity pair mention
	 * @param DEBUG Test if the roots found by find common governer method and annotation provided root are equal
	 * @return Full dependency graph of the sentences represented by annotations
	 * @throws AnnotationParsingException 
	 */
	public static DependencyRelationGraph buildGraph(AnnotatedSentenceParserAdapter parser, 
			EntityPairMention pairMention, boolean DEBUG) throws AnnotationParsingException {

		if (parser == null)
			throw new RuntimeException("Building SyntacticParseTree annotations argument is null");
		if (pairMention == null)
			throw new RuntimeException("Building SyntacticParseTree pairMention argument is null");

		// Create a graph to create node
		DependencyRelationGraph nGraph = new DependencyRelationGraph();
		// Add the root of this graph
		//		nGraph.addNode(ROOT);

		Tokens tokens = parser.getTokens();
		Stanfordlemma lemma = parser.getStanfordlemma();
		Stanfordpos pos = parser.getStanfordpos();
		Stanfordner ner = parser.getStanfordner();

		// Valid input check
		if (tokens == null || tokens.tokens == null 
				|| lemma == null || lemma.lemma == null
				|| pos == null || pos.pos == null
				|| ner == null || ner.ner == null)
			throw new GraphConstructionException(TAG + " Null annotation");

		assert (tokens.size() == lemma.size() &&
				lemma.size() == pos.size() &&
				pos.size() == ner.size()) : 
					"Size of tokens, lemma, pos, ner are not equal!";

		// Intermediate data structure to hold Nodes
		List<DependencyNode> nodeList = new ArrayList<DependencyNode>();	
		int size = tokens.size();

		List<DependencyNode> entity1 = new ArrayList<DependencyNode>();
		List<DependencyNode> entity2 = new ArrayList<DependencyNode>();
		for (int i = 0; i < size; ++i) {
			int whichArg = 0;
			ENTITY_POS position = ENTITY_POS.NOT_ENTITY;
			// Identify If this node is a part entity 1
			if ( i >= pairMention.tokenRange1.getStart()
					&& i < pairMention.tokenRange1.getEnd()) {
				whichArg = 1;
				position = ENTITY_POS.NOT_HEAD;
			}
			// Identify if this node is a part entity 2
			else if (i >= pairMention.tokenRange2.getStart()
					&& i < pairMention.tokenRange2.getEnd()) {
				whichArg = 2;
				position = ENTITY_POS.NOT_HEAD;
			}

			DependencyNode node = new DependencyNode(i, whichArg, position);

			// Add defining attributes
			node.setAttribute(new DependencyAttribute(
					pos.get(i), tokens.get(i), lemma.get(i), ner.get(i)));
			nodeList.add(node);

			// Check if is part of entity
			switch (whichArg) {
			case 1:  
				entity1.add(node);
				break;
			case 2: 
				entity2.add(node);
			default:
			}
		}

		// add the entirety of the node list to the graph
		for (DependencyNode node: nodeList)
			nGraph.addNode(node);

		// Actual dependency
		DeptCCProcessed deps = parser.getDeptCCProcessed();
		if (deps == null || deps.dependencies == null)
			throw new GraphConstructionException("No dependencies found in annotations");

		/// This is a debug reference to the root node
		// If we are using 
		DependencyNode testRoot = null;

		// For every dependency add an edge in the graph
		// Populate the graph full of edges connecting relations
		for (GrammaticalDependency dep : deps.dependencies) {

			// If we come across an index of -1 this identifies the
			// Stanford abstraction of the root relation.  Usually roots governer values of (-1) are 
			// related to a dependent of a token in the actual graph.  This token is the actual root
			// of the graph. 
			DependencyNode src = null;	
			if (dep.governor == -1 && withInList(dep.dependent, nodeList))// The Root is usually always represented as a governer
				testRoot = nodeList.get(dep.dependent);
			else if (withInList(dep.governor, nodeList)) // Check governener within bounds
				src = nodeList.get(dep.governor);
			DependencyNode dst = null;
			if (withInList(dep.dependent, nodeList)) // Check dependency within bounds 
				dst = nodeList.get(dep.dependent);

			if (dep.governor == -1){
//				Logger.write("Found root annotation but did not add to graph: " + dep +"\n", DEBUG);
				continue;
			} else if (src == null || dst == null){
				Logger.write("Unable to find src or dst nodes in list: " + dep +"\n", true);
				continue;
			}
			// Gets singleton edges
			// Create edges actually keeps a cache of unique edges and retrieves as nescesary 
			DependencyEdge governerToDependent = DependencyEdge.createEdge(dep.dependencyType, nGraph, false);
			DependencyEdge dependentToGoverner = DependencyEdge.createEdge(dep.dependencyType, nGraph, true);
			// Add edges that represent parent to child and child to parent relation
			// Governer to Dependent ==> parent to child
			// Dependent to Governer ==> child to Parent
			nGraph.addEdge(src, dst, governerToDependent);
			nGraph.addEdge(dst, src, dependentToGoverner);
		}

		// Get the closet root for both entities
		// the perculates up from an entity node
		nGraph.mRoot = nGraph.getCommonGoverner(entity1, entity2);
		// If we are debugging with default 
		if (DEBUG){
			if (testRoot != null){
				if (!testRoot.equals(nGraph.mRoot))
					throw new RuntimeException("[buildGraph] DEBUG Roots found by common governer and root in annotation " +
							"are not equal!");
			} 
		}

		// Have to cover the case where there exist a common root for the entities
		if (nGraph.mRoot != null){
			nGraph.arg1Head = nGraph.getHeadViaBFS(entity1);
			nGraph.arg2Head = nGraph.getHeadViaBFS(entity2);
		} else { // This indicates that there is no common Governer (Root) between any members of both entities
			// This will be done by finding the node with the most descendants
			nGraph.arg1Head = nGraph.getHeadViaMaxDepedents(entity1);
			nGraph.arg2Head = nGraph.getHeadViaMaxDepedents(entity2);
		}
		
		// Attempt to find the path between the two entities prioritizing the path between there heads
		DependencyPath potent = null;
		potent = nGraph.getDependencyPath(nGraph.arg1Head, nGraph.arg2Head);
		if (potent == null) // Ok lets try any node in 1 to the 2
			potent = nGraph.getAnyDependencyPath(entity1, entity2);
		if (potent == null) // Couldnt find a single path between the entities
			potent = new DependencyPath(nGraph, new ArrayList<DependencyNode>(), nGraph.arg1Head, nGraph.arg2Head);
		nGraph.mPathBetweenEntities = potent;

		// have to do some checking here because this shoudl
		if (nGraph.arg1Head == null)
			throw new IllegalStateException("Null Head for argument 1, Check number of tokens in entity 1 > 0");
		if (nGraph.arg2Head == null)
			throw new IllegalStateException("Null Head for argument 2, Check number of tokens in entity 2 > 0");

		// Attempt to find the tail of each entity
		// This can be done by attempting to find the with the longest length 
		// from the head to all the other components of the entity
		nGraph.arg1Tail = nGraph.getNodeWithLongestPathFrom(entity1, nGraph.arg1Head);
		nGraph.arg2Tail = nGraph.getNodeWithLongestPathFrom(entity2, nGraph.arg2Head);

		// have to do some checking here because this shoudl
		if (nGraph.arg1Tail == null)
			throw new IllegalStateException("Null Tail for argument 1, Check number of tokens in entity 1 > 0");
		if (nGraph.arg2Tail == null)
			throw new IllegalStateException("Null Tail for argument 2, Check number of tokens in entity 2 > 0");

		List<DependencyNode> pathDests = new ArrayList<DependencyNode>(entity1.size()-1);
		for (DependencyNode n: entity1){
			if (!n.equals(nGraph.arg1Head))
				pathDests.add(n);
		}	
		nGraph.mEntity1Dependencies = nGraph.getPathsTo(nGraph.arg1Head, pathDests);
		pathDests.clear();
		for (DependencyNode n: entity2){
			if (!n.equals(nGraph.arg2Head))
				pathDests.add(n);
		}
		nGraph.mEntity2Dependencies = nGraph.getPathsTo(nGraph.arg2Head, pathDests);

		return nGraph;
	}

	// Getters for this graph.. Lazy documentation
	// Should always have head and tails
	// Therefore never be null
	public DependencyNode getEntity1Head(){return arg1Head;}
	public DependencyNode getEntity2Head(){return arg2Head;}
	public DependencyNode getEntity1Tail(){return arg1Tail;}
	public DependencyNode getEntity2Tail(){return arg2Tail;}
	
	/**
	 * Can return null if no root exist
	 * @return
	 */
	public DependencyNode getRoot(){return mRoot;}

	/**
	 * For every node within dsts a path will be found from root to that member of the list.
	 * <b>If no path is found then an empty list will be added.
	 * <b>The list of paths returned will be the same size as the list passed in and index will correspond
	 * <b>	IE. Index 0 in return value will be the path from root to dsts.get(0)
	 * @param src Source of all the paths found
	 * @param dsts destinations to all the paths returned
	 * @return List of paths for all the destinations
	 */
	public List<DependencyPath> getPathsTo(DependencyNode src, List<DependencyNode> dsts){
		List<DependencyPath> result = new ArrayList<DependencyPath>(dsts.size());
		for (DependencyNode n: dsts){
			result.add(getDependencyPath(src, n));
		}
		return result;
	}
	
	/**
	 * Given a collection arguments attempts to find a Root that connects any Node in 1st collection to
	 * any node in Second collection
	 * @param arg1s List of node that will be attempted to find a root from
	 * @param arg2s List of node that will be attempted to find a root from
	 * @return common governer between them both, or null if non can be found
	 */
	public DependencyNode getCommonGoverner(Collection<DependencyNode> arg1s, Collection<DependencyNode> arg2s){
		DependencyNode result = null;
		for (DependencyNode arg1: arg1s){
			result = getCommonGoverner(arg2s, arg1);
			if (result != null)
				break;
		}
		return result;
	}

	/**
	 * Returns the common governer between to list of node and a single node
	 * If any node can be a root to any of the nodes in the list and the second argument
	 * it is returned
	 * @param arg1s List of node that will be attempted to find a root from
	 * @param arg2 2nd node
	 * @return comon governer between them both, or null if non can be found
	 */
	public DependencyNode getCommonGoverner(Collection<DependencyNode> arg1s, DependencyNode arg2){
		DependencyNode result = null;
		for (DependencyNode arg1: arg1s){
			result = getCommonGoverner(arg1, arg2);
			if (result != null)
				break;
		}
		return result;
	}

	/**
	 * Returns the common governer between to two arguments
	 * @param arg1 first node
	 * @param arg2 2nd node
	 * @return comon governer between them both, or null if non are found
	 */
	public DependencyNode getCommonGoverner(DependencyNode arg1, DependencyNode arg2){
		if (arg1.equals(arg2))  // Same node passed in
			return arg1;
		if (hasDependent(arg1, arg2)) // 
			return arg1;
		if (hasDependent(arg2, arg1))
			return arg2;
		HashSet<DependencyNode> seen = new HashSet<DependencyNode>();
		seen.add(arg1);
		return getCommonGovernerRecursive(arg1, arg2, seen);
	}

	/**
	 * For when we found that arg1 and 2 are not dependents of eachother
	 * @param arg1 node not dependent on 2
	 * @param arg2 node not dependent on 1
	 * @param seen keeps track of 
	 * @return
	 */
	private DependencyNode getCommonGovernerRecursive(DependencyNode arg1, 
			DependencyNode arg2, HashSet<DependencyNode> seen){
		for (DependencyNode gov : getGoverners(arg1)){
			if (!seen.add(gov)) continue;
			if (hasDependent(gov, arg2))
				return gov;
			DependencyNode n = getCommonGovernerRecursive(gov, arg2, seen);
			if (n != null) 
				return n;
		}
		return null;
	}

	/**
	 * From a given governer check if a it ever can point to a given dependent dep
	 * @param dep dependent to look for
	 * @param gov node to look from
	 * @return true if dependent exists or false otherwise
	 */
	public boolean hasDependent(DependencyNode gov, DependencyNode dep){
		if (gov.equals(dep)) return false;
		HashSet<DependencyNode> seen = new HashSet<DependencyNode>();
		// This can also be done by adding the governer to the set 
		for (DependencyNode subGov : getDependents(gov)){
			if (isDependent(subGov, dep, seen))
				return true;
		}
		return false;
	}

	/**
	 * For a given potential dependent and its governer checks if this dependent is actually a dependent of 
	 * the governer. Should call hasDependent to cover the case where the two nodes our equal
	 * @param childOfGoverner should be a child of the governer
	 * @param dep dependent to check
	 * @param seen a set of nodes that have already been seen
	 * @return true if a child or its descendants is a dependent of the governer
	 */
	private boolean isDependent(DependencyNode childOfGoverner, DependencyNode dep, HashSet<DependencyNode> seen){
		if (seen == null)
			seen = new HashSet<DependencyNode>();
		if (!seen.add(childOfGoverner)) // Failed to add to seen because already exists
			return false; // Reached a cycle so return false
		if (childOfGoverner.equals(dep))
			return true;
		for (DependencyNode subGov : getDependents(childOfGoverner)){
			if (isDependent(subGov, dep, seen))
				return true;
		}
		return false;
	}

	/**
	 * Returns all the nodes that is a governer to this node
	 * @param n node 
	 * @return empty list if non exist or list of governeners other wise
	 */
	public List<DependencyNode> getGoverners(DependencyNode n){
		List<DependencyNode> nbrs = getNeighorsOf(n);
		List<DependencyNode> governers = new ArrayList<DependencyNode>();
		if (nbrs == null || nbrs.isEmpty()) return governers;
		for (DependencyNode nbr: nbrs){
			DependencyEdge e = getFirstEdge(n, nbr);
			if (e.isDepToGov()){
				governers.add(nbr);
			}
		}
		return governers;
	}

	/**
	 * Returns all the dependents of this node
	 * @param n node that exists in this graph
	 * @return empty list if non exist or list of dependents other wise
	 */
	public List<DependencyNode> getDependents(DependencyNode n){
		List<DependencyNode> nbrs = getNeighorsOf(n);
		List<DependencyNode> dependents = new ArrayList<DependencyNode>();
		if (nbrs == null || nbrs.isEmpty()) return dependents;
		for (DependencyNode nbr: nbrs){
			DependencyEdge e = getFirstEdge(n, nbr);
			if (e.isGovToDep()){
				dependents.add(nbr);
			}
		}
		return dependents;
	}

	/**
	 * Get the shortest dependency path between the two inputted entities
	 * Path includes entities 
	 * @return A list of dependency Nodes that lead to entity 1 to entity 2
	 */
	public DependencyPath getDependencyPathForEntities(){
		return  new DependencyPath(mPathBetweenEntities);
	}

	/**
	 * Get a list of all the paths from entity head to 
	 * rest of entity tokens
	 * @return complete dependency path for entity 1
	 */
	public List<DependencyPath> getEntity1Paths(){
		return new ArrayList<DependencyPath>(mEntity1Dependencies);
	}

	/**
	 * Get a list of all the paths from entity head to 
	 * rest of entity tokens
	 * @return complete dependency path for entity 2
	 */
	public List<DependencyPath> getEntity2Paths(){
		return new ArrayList<DependencyPath>(mEntity2Dependencies);
	}

	/**
	 * @return if there exist a path from 1 to 2. 
	 */
	public boolean hasPath(DependencyNode n1, DependencyNode n2) {
		return !getDependencyPath(n1, n2).isEmpty();
	}

//	/**
//	 * Given a path between node in a graph returns the associated size
//	 * @param path path to find size of
//	 * @return Length of the path or -1 if path is empty
//	 */
//	public static int getPathSize(DependencyPath path){
//		return path.size()-1;
//	}
	
	/**
	 * Returns the dependency path bewtween src and dst
	 * @param src Source node o start
	 * @param dst Destination node to end at
	 * @return DependencyPath
	 */
	public DependencyPath getDependencyPath(DependencyNode src, DependencyNode dst){
		List<DependencyNode> path = getPathWithLeastEdges(src, dst);
		return new DependencyPath(this, path, src, dst);
	}

	/**
	 * Finds the head of the first entity via Breadth first search
	 * @param entity composition of list 
	 * @return the Dependency node of an entity that represents its head
	 */
	private DependencyNode getHeadViaBFS(List<DependencyNode> entity){
		// If there is no root return null
		if (mRoot == null)
			throw new IllegalStateException("The Root of this graph is still node. Please find root first");
		Queue<DependencyNode> q = new LinkedList<DependencyNode>();
		Set<DependencyNode> marked = new HashSet<DependencyNode>();

		//Straight from wikipedia BFS search
		q.add(mRoot);
		marked.add(mRoot);
		while (!q.isEmpty()){
			DependencyNode t = q.remove();
			if (containsIndexWord(entity, t))
				return t;
			List<DependencyNode> children = getNeighorsOf(t);
			for (DependencyNode child : children){
				if (!marked.contains(child)) {
					marked.add(child);
					q.add(child);
				}
			}
		}
		//Got nothing
		return null;
	}
	
	/**
	 * Attempts to find any path from any node in list 1 to any node in list 2
	 * @param l1 list 1 Source nodes
	 * @param l2 list 2 Destination nodes
	 * @return null if no path is found or a path that connects one element in list 1 to one element in list 2
	 */
	private DependencyPath getAnyDependencyPath(List<DependencyNode> l1, List<DependencyNode> l2){
		for (DependencyNode n1: l1){
			for (DependencyNode n2: l2){
				DependencyPath path = getDependencyPath(n1, n2);
				if (!path.isEmpty())
					return path;
			}
		}
		return null; // Got nothing
	}

	/**
	 * NOTE: Should only be used when there is no root
	 * connecting the list of node to another.  If such a root exists or is unknown call
	 * getHeadViaBFS()  This uses the Root as its 
	 * <b>This does not guarantee there exist a path that connects all the nodes of entity nor does it assume so
	 * 
	 * @param entity Given an entity composed of a list of tokens
	 * @return a array of two values.  The first [0] will be considered the head.  The 2nd [1] will be considered the tail.
It is possible to have both be the same for an entity with one element.  null will be returned on failure to find head or tail (IE Empty list)
	 */
	private DependencyNode getHeadViaMaxDepedents(List<DependencyNode> entity){
		if (entity.isEmpty()) return null;

		// Local to store out results
		DependencyNode result = null;
		// Set our default to -1 so by default our first node will be assigned as head
		int maxSeen = -1;
		// Think of every node as a potential head
		for (DependencyNode potHead: entity){
			int depCnt = 0;
			// Then check if it has a dependent relation to the rest of the nodes
			// NOTE: hasDependent(A,A) will return false
			for (DependencyNode other: entity){
				if (hasDependent(potHead, other))
					depCnt++;
			}
			// Update our head to be whatever value found
			if (depCnt > maxSeen)
				result = potHead;
			// After one iteration we can assert our new head will not be null.
			assert result != null: "Head null in getHeadAndTailViaMaxDepedents";
		}
		return result;
	}

	/**
	 * Checks for index equality where any elements in the keys list 
	 * is equals to the query
	 * @param keys list of keys to be checked
	 * @param query node to be found
	 * @return true if found false otherwise
	 */
	public static boolean containsIndexWord(List<DependencyNode> keys, DependencyNode query){
		if (query == null) return false;
		for (DependencyNode n: keys)
			if (n.equals(query))
				return true;
		return false;
	}

	/**
	 * Attempts to find out of the potential Nodes the longest path node from root
	 * IF root is contained in potentialLongest then it is guaranteed that this will 
	 * return a non null value.  If not there is possibly a chance where that all the 
	 * potential nodes are disconnected from room.  Therefore root will be null.
	 * @param potentialLongest list of nodes to search for
	 * @param root Root to find distance from
	 * @return one of the potentialLongest nodes that has the longest root, null if list is empty or no path every found
	 */
	public DependencyNode getNodeWithLongestPathFrom(
			List<DependencyNode> potentialLongest, DependencyNode root){
		if (potentialLongest == null || potentialLongest.isEmpty())
			return null; // No path exist if it is empty

		DependencyNode result = null;
		// We want the path with the longest length
		int maxLength = -1; 
		for (DependencyNode n: potentialLongest){
			// Iterate through all the node including the head itself
			// find a path.  We know that a path to itself is guaranteed
			// to return a path size of 0.  Therefore we are guaranteed to 
			// get a ndoe as the tail.
			List<DependencyNode> path = getPathWithLeastEdges(root, n);
			int currPathSize = path.size();
			if (currPathSize > maxLength) {
				result = n;
				maxLength = currPathSize;
			}	
		}

		if (potentialLongest.contains(root) && result == null)
			throw new RuntimeException("Given the root is contained in the potential list, " +
					"Failed pass it back as a default tail when no other could be found");

		// Null if bodies are disconnected
		// root if root was included in list 
		return result;
	}

	/**
	 * Checks if a given index falls with the list
	 * @param ind
	 * @param list
	 * @return
	 */
	private static boolean withInList(int ind, List<?> list){
		return ind >= 0 && ind < list.size();
	}
}
