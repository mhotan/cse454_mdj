package features.components.dependency;

public class DependencyAttribute implements features.components.Attribute {
	private final static String TAG = "[SequenceAttribute]";

	/**
	 * This represents POS tag for a given node
	 */
	public final String pos;
	public final String token;
	public final String lemma;
	public final nlp.StanfordNerTag ner;

	/**
	 * 
	 * @param pos POS tag
	 * @param token token for this node
	 * @param lemma lemm
	 * @param tag NER Tag
	 */
	public DependencyAttribute(String pos, String token, 
			String lemma, nlp.StanfordNerTag tag){
		this.pos = pos;
		this.token = token;
		this.lemma = lemma;
		this.ner = tag;
		checkRep();
	}

	@Override
	public String toDiscernableString() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean equals(Object o){
		if (o == null) return false;
		if (!o.getClass().equals(this.getClass())) return false;
		DependencyAttribute a = (DependencyAttribute)o;
		return this.pos.equals(a.pos) && this.token.equals(a.token) &&
				this.lemma.equals(a.lemma) && this.ner == a.ner;
	}
	@Override 
	public int hashCode(){
		return pos.hashCode() + 13 * token.hashCode() + 7 * lemma.hashCode() + ner.hashCode();
	}
	
	public String toString(){
		return "[ POS: " + pos + " TOKEN: " + token + " LEMMA: " + lemma + " NER: " + ner + " ]"  ;
	}

	/**
	 * representation Checker
	 */
	private void checkRep(){
		assert pos != null: TAG + " NULL POS";
		assert token != null: TAG + " NULL TOKEN" ;
		assert lemma != null: TAG + " NULL LEMMA";
		assert ner != null: TAG + " NULL NER";
	}
}
