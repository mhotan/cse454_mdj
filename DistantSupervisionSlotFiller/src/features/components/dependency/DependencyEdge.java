package features.components.dependency;

import java.util.HashMap;
import java.util.Map;

import features.components.RelationInstanceEdge;

/**
 * Class defines a dependency edge
 * @author mhotan
 */
public class DependencyEdge extends RelationInstanceEdge implements Comparable<DependencyEdge> {
	
	/**
	 * Mapping of names of edges that already exist to the edges
	 * Helps reduce redundant repeated edges 
	 */
	private static Map<Boolean, HashMap<String, DependencyEdge>> mKnownTypes;
	
	private final String mType;
	
	/**
	 * Need nodes that point to parents
	 * For route finding
	 */
	private final boolean mDependentToGoverner;

	public boolean isGovToDep(){
		return !mDependentToGoverner;
	}
	
	public boolean isDepToGov(){
		return mDependentToGoverner;
	}
	
	/**
	 * @param type of dependency relation
	 */
	private DependencyEdge(String type, boolean pointsToParent){
		mDependentToGoverner = pointsToParent;
		mType = type;
		checkRep();
	}

	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (! o.getClass().equals(getClass()))
			return false;
		DependencyEdge e = (DependencyEdge)o;
		return e.mType.equals(mType);
	}

	private static final String DEP_TO_GOV = "<-";
	private static final String GOV_TO_DEP = "->";
	
	@Override
	public String toString(){
		if (mDependentToGoverner) return DEP_TO_GOV + mType + DEP_TO_GOV;
		return GOV_TO_DEP + mType + GOV_TO_DEP;
	}
	
	@Override
	public int hashCode(){
		return mType.hashCode();
	}

	private void checkRep(){
		assert mType != null: "Null dependency type!";
	}
	
	/**
	 * @param type can't be null, is type of dependency between governer and dependent
	 * @param graph Graph that can produce a DependencyEdge
	 * @return Edge
	 */
	public static DependencyEdge createEdge(String type, DependencyRelationGraph graph, boolean pointsToParent) {
		if (type == null) return null;
		// Initialize the map 
		if(mKnownTypes == null) {
			mKnownTypes = new HashMap<Boolean, HashMap<String, DependencyEdge>>();
			mKnownTypes.put(Boolean.TRUE, new HashMap<String, DependencyEdge>());
			mKnownTypes.put(Boolean.FALSE, new HashMap<String, DependencyEdge>());
		}

		HashMap<String, DependencyEdge> booleanMap = mKnownTypes.get(Boolean.valueOf(pointsToParent));
		
		if (booleanMap.containsKey(type))
			return booleanMap.get(type);
		
		DependencyEdge nEdge = new DependencyEdge(type, pointsToParent);
		booleanMap.put(type, nEdge);
		return nEdge;
	}

	@Override
	public int compareTo(DependencyEdge o) {
		return this.mType.compareTo(o.mType);
	}
}
