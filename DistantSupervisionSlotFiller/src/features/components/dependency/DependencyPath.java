package features.components.dependency;

import java.util.ArrayList;
import java.util.List;

import features.components.Path;

/**
 * A Path representation of a sequence of nodes for easy feature generation
 * 
 * @author mhotan
 */
public class DependencyPath extends Path implements Comparable<DependencyPath>{
	private final DependencyRelationGraph mGraph;
	private final List<DependencyNode> mPath;
	protected final DependencyNode mSrc, mDest;
	
	public static DependencyPath getNoPathRep(DependencyRelationGraph g, DependencyNode alone){
		return new DependencyEmptyPath(g, alone);
	}
	
	/**
	 * This represents a collection of features for a dependency path.
	 * @param g
	 * @param path
	 * @param src
	 * @param dst
	 */
	public DependencyPath(DependencyRelationGraph g, List<DependencyNode> path, DependencyNode src, DependencyNode dst){
		mGraph = g;
		mPath = path;
		mSrc = src;
		mDest = dst;
	}

	/**
	 * Create a Path copying the values from the inputted path
	 * @param p
	 */
	public DependencyPath(DependencyPath p){
		mGraph = p.mGraph;
		mSrc = p.mSrc;
		mDest = p.mDest;
		mPath = new ArrayList<DependencyNode>(p.mPath.size());
		mPath.addAll(p.mPath);
	}
	
	public String getNERPath(){
		return getPathString(PRINT_TYPE.NER);
	}

	public String getTokenPath(){
		return getPathString(PRINT_TYPE.TOKEN);
	}

	public String getLemmaPath(){
		return getPathString(PRINT_TYPE.LEMMA);
	}

	public String getPosPath(){
		return getPathString(PRINT_TYPE.POS);
	}

	@Override
	public boolean isEmpty(){
		return mPath.isEmpty();
	}
	
	@Override
	public String toString(){
		return getTokenPath() + "\n" + getLemmaPath() + "\n" + getNERPath() + "\n" + getPosPath();
	}

	@Override
	public int compareTo(DependencyPath o) {
		return o.mPath.size() - this.mPath.size();
	}

	protected String getPathString(PRINT_TYPE type){
		
		if (mPath.isEmpty()) {
			String srcValue = null;
			String dstValue = null;
			DependencyAttribute srcA = mSrc.getAttribute();
			DependencyAttribute dstA = mDest.getAttribute();
			switch (type){
			case TOKEN:
				srcValue = srcA.token;
				dstValue = dstA.token;
				break;
			case LEMMA:
				srcValue = srcA.lemma;
				dstValue = dstA.lemma;
				break;
			case POS:
				srcValue = srcA.pos;
				dstValue = dstA.pos;
				break;
			default:
				srcValue = srcA.ner.toString();
				dstValue = dstA.ner.toString();
			}
			return srcValue + "-" + NO_PATH + "-" + dstValue;
		}
		
		// IF no path exist

		DependencyNode curr = mPath.get(0);
		DependencyAttribute a = curr.getAttribute();

		StringBuffer b = new StringBuffer();
		b.append(OPEN_SQUARE_BRACE);
		switch (type){
		case TOKEN:
			b.append(a.token);
			break;
		case LEMMA:
			b.append(a.lemma);
			break;
		case POS:
			b.append(a.pos);
			break;
		default:
			b.append(a.ner.toString());
		}
		b.append(CLOSE_SQUARE_BRACE);

		for (int i = 1; i < mPath.size(); ++i){
			DependencyNode next = mPath.get(i);
			// Add string 
			DependencyEdge e = mGraph.getFirstEdge(curr, next);
			a = next.getAttribute();
			b.append(e.toString());
			b.append(OPEN_SQUARE_BRACE);
			switch (type){
			case TOKEN:
				b.append(a.token);
				break;
			case LEMMA:
				b.append(a.lemma);
				break;
			case POS:
				b.append(a.pos);
				break;
			default:
				b.append(a.ner.toString());
			}
			b.append(CLOSE_SQUARE_BRACE);
			curr = next;
		}
		return b.toString();
	}

	@Override
	public int getSize() {
		return mPath.size()-1;
	}

}
