package features.components.dependency;

import features.components.RelationInstanceNode;

/**
 * Defines a Node that exists in a dependency graph
 * @author mhotan 
 */
public class DependencyNode extends RelationInstanceNode<DependencyAttribute> {
	
	/**
	 * Creates a dependency Node that has knowledge to its index 
	 * and identifier
	 * @param index of the node 
	 * @param argIdentifier Defines how many arguments are related to this
	 */
	public DependencyNode(int index, int numArgs, ENTITY_POS
			 pos) {
		super(index, RelationInstanceNode.getArgumentIdentifier(numArgs), pos);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (! o.getClass().equals(getClass()))
			return false;
		DependencyNode n = (DependencyNode) o;
		// Check if the entire list are equal
		// They must be equal in size
		boolean argsEqual = true;
		if (mAttributes == null)
			argsEqual &= n.mAttributes == null;
		else 
			argsEqual &= mAttributes.equals(n.mAttributes);

		return mIndex == n.mIndex &&
				mArgTag == n.mArgTag && 
				argsEqual &&
				mArgPos == n.mArgPos;
	}
	
	@Override
	public int hashCode(){
		int code = mIndex * 13;
		if (mAttributes != null)
			code += 7*mAttributes.hashCode();
		code += 17*mArgPos.hashCode() + 31*mArgTag.hashCode();
		return code;
	}
	
}
