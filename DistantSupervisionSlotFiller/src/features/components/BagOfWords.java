package features.components;

import java.util.HashMap;
import java.util.Map;

/**
 * A global object that is able to track all the tokens seen and when they were seen
 * relative to each other word
 * 
 * IE a word A with index 0 appears before B if B's Integer value is greater then 0
 *  
 * @author mhotan
 */
public class BagOfWords {

	private BagOfWords(){
		currentUnusedIndex = 0;
		mSeenWords = new HashMap<String, Integer>();
	}

	/**
	 * The next available index to assign to new string
	 */
	private int currentUnusedIndex;

	/**
	 * Complete dictionary to track global seen words.
	 * Integer value is the number in which orders the occurence they appeared in
	 */
	private Map<String, Integer> mSeenWords;
	
	private static BagOfWords mInstance;

	public static BagOfWords getInstance(){
		if (mInstance == null)
			mInstance = new BagOfWords();
		return mInstance;
	}
	
	/**
	 * @param token
	 * @return if token has been seen already
	 */
	public boolean contains(String token){
		return mSeenWords.containsKey(token);
	}

	/**
	 * If token already exist or is null nothing is added.  Else global token tracker is 
	 * altered adding new value
	 * @param name
	 */
	public int add(String token){
		if (token == null)
			throw new IllegalArgumentException("Should not attempt to add NULL token");
		if (!contains(token)) {
			// Add it doesnt exist an adjust the global counter
			mSeenWords.put(token, currentUnusedIndex);
			currentUnusedIndex++;
		}
		return mSeenWords.get(token);
	}

	/**
	 * 
	 * @param token token to get index of
	 * @return null if token does not exist in mapping
	 */
	public Integer getIndex(String token){
		return mSeenWords.get(token);
	}
}
