package features.components.syntactic;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.stanford.nlp.trees.Tree;
import features.components.RelationInstanceNode.ARGUMENT_IDENTIFIER;
import features.components.RelationInstanceNode.ENTITY_POS;

class SyntacticNodeBuilder {
	
	private static final SyntacticEdge parentToChild = new SyntacticEdge(true);
	private static final SyntacticEdge childToParent = new SyntacticEdge(false);
	
	public static final int NOT_TOKEN = -1;
	
	/**
	 * index of where the token is in the sentence
	 * Nodes without any tokens will have this value be NOT_TOKEN
	 */
	public final int sentenceIndex;
	
	/**
	 * Level within the tree
	 */
	public final int mlevel;
	public final int mChildIndex;
	
	private final Tree tree;
	private final List<SyntacticNode> potentialChildren;
	private ARGUMENT_IDENTIFIER idt;
	private ENTITY_POS pos;
	
	private String mValue;

	/**
	 * Creates a node adds it to the graph and adds parentToChild and child to parent
	 * relations with all 
	 * @param tree
	 * @return
	 */
	public SyntacticNode build(SyntacticParseTree tree){
		SyntacticNode n;
		if (sentenceIndex == NOT_TOKEN) { // Represents a non leaf
			n = new SyntacticNode(this.idt, this.tree, this.mlevel, this.mChildIndex);
			// add regular attributes
			n.setAttribute(new DefaultSyntacticAttribute(this.tree.value()));
		} else { // Leaf node that pertains to an index
			n = new SyntacticNode(this.sentenceIndex, this.idt, this.pos, this.tree,
					this.mlevel, this.mChildIndex);
			// Add special Attribute
			n.setAttribute(new ValueSyntacticAttribute(
					this.mValue, this.tree.value()));
		}
		// Add node to graph
		if (!tree.containsNode(n))
			tree.addNode(n);
		tree.toString();
		// add relation to all the children
		for (SyntacticNode child : this.potentialChildren) {
			if (!tree.containsNode(child))
				tree.addNode(child);
			tree.addEdge(n, child, parentToChild);
			tree.addEdge(child, n, childToParent);
		}

		return n;
	}

	/**
	 * Cannot accept leaves as nodes
	 * @param tree
	 */
	public SyntacticNodeBuilder(Tree tree, int level, int childIndex){
		if (tree == null || tree.isLeaf())
			throw new IllegalArgumentException("Illegal tree: " + tree);

		this.mChildIndex = childIndex;
		this.mlevel = level;
		this.tree = tree;
		this.idt = ARGUMENT_IDENTIFIER.NO_ARGUMENTS;
		this.pos = ENTITY_POS.NOT_ENTITY;
		potentialChildren = new LinkedList<SyntacticNode>();

		if (this.tree.isPreTerminal()) {
			Tree leaf = this.tree.getChild(0);
			this.sentenceIndex = getLabelIndex(leaf);
			this.mValue = leaf.value();
//			removeLeaves(this.tree);
		} else
			this.sentenceIndex = NOT_TOKEN;
	}
	
	private static void removeLeaves(Tree t){
		if (t.isPreTerminal()){
			for (int i = 0; i < t.numChildren(); ++i)
				t.removeChild(0);
		} else
			for (Tree child: t.getChildrenAsList())
				removeLeaves(child);
	}

	public boolean isLeaf(){
		return this.sentenceIndex != NOT_TOKEN;
	}

	public void updateWhichArgument(ARGUMENT_IDENTIFIER idt) {
		this.idt = idt;
	}

	public void updateEntityPosition(ENTITY_POS pos) {
		this.pos = pos;
	}

	public void addChild(SyntacticNode node){
		if (node == null) return;
		if (potentialChildren.contains(node)) return;
		this.potentialChildren.add(node);
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) return false;
		if (o.getClass() != this.getClass()) return false;
		SyntacticNodeBuilder nb = (SyntacticNodeBuilder)o;
		return this.tree.equals(nb.tree);
	}

	@Override
	public int hashCode(){
		return 7+13*tree.hashCode();
	}

	private void checkRep() {
		if (sentenceIndex > -1) assert tree.isPreTerminal();
		assert potentialChildren != null;
	}

	
	/**
	 * Only intended to 
	 * Does a ad hoc way of parsing out the index of from the label and then
	 * decrementing the index by 1 to make is base 0
	 * @param label Label that is to be parsed.
	 * @return base 0 index of the label, or -1 if is not Leaf 
	 */
	private static int getLabelIndex(Tree leaf){
		if (!leaf.isLeaf()) throw new IllegalArgumentException("Tree is not leaf");
		Pattern pattern = Pattern.compile("IndexAnnotation=[1-9][0-9]*");
		Matcher matcher = pattern.matcher(leaf.label().toString());
		if (!matcher.find()) return -1;
		String s = matcher.group();
		String[] array = s.split("=");
		// Should be of size two
		// Last element should number
		return Integer.parseInt(array[array.length-1]) - 1;
	}
}
