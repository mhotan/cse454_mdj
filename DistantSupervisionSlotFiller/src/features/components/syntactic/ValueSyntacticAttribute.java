package features.components.syntactic;
/**
 * Used for acutal value annotation
 * @author mhotan
 */
public class ValueSyntacticAttribute extends DefaultSyntacticAttribute {
	/**
	 * Token value or name of this
	 */
	public final String token;

	public ValueSyntacticAttribute(String token, String posTag) {
		super(posTag);
		this.token = token;
	}

	@Override
	public String toDiscernableString() {
		// TODO 
		return null;
	}
	
	@Override 
	public String toString(){
		return "[ POS: " + this.posTag + " TOKEN: " + token + " ]"  ;
	}
}
