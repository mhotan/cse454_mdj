package features.components.syntactic;

import features.components.RelationInstanceEdge;

/**
 * Genereic Edge that connect parents to their children in the tree
 * or children to their parents
 * @author mhotan
 */
class SyntacticEdge extends RelationInstanceEdge implements Comparable<SyntacticEdge>{

	enum SYN_EDGE_DIR {
		LEFT_CHILD("<-"),
		RIGHT_CHILD("<-"),
		PARENT("P");
		
		private final String mText;
		
		SYN_EDGE_DIR(String text){
			mText = text;
		}
		
		public String toString(){
			return mText;
		}
	}
	
	//Defines if the edge points to parent or child
	private final boolean mPointsToChild;

	public SyntacticEdge(boolean pointsToChild){
		mPointsToChild = pointsToChild;
	}

	public boolean pointsToChild(){
		return mPointsToChild;
	}

	public boolean pointsToParent(){
		return !mPointsToChild;
	}

	@Override
	public int compareTo(SyntacticEdge o) {
		// Not comparable
		return 0;
	}
}
 
