package features.components.syntactic;

import edu.stanford.nlp.trees.Tree;
import features.components.RelationInstanceNode;

/**
 * Default Syntactic Node 
 * @author mhotan
 */
public class SyntacticNode extends RelationInstanceNode<DefaultSyntacticAttribute> implements Comparable<SyntacticNode>{
	/**
	 * Reference the Stanford tree root that it corresponds to
	 */
	private final Tree stanfordTree;

	/**
	 * Level within the tree
	 */
	public final int mLevel;

	/**
	 * Root is the only one that has child Index of 0
	 * Identifies which child it is with respect to its parent
	 */
	public final int mChildIndex;

	/**
	 * Create
	 * @param idt Identity type of this node
	 * @param stanfordTree Stanford tree root that corresponds with this node
	 * @param level level within the tree this node is on
	 * @param childIndex Which child it is with respect to its parents
	 */
	protected SyntacticNode(ARGUMENT_IDENTIFIER idt, Tree stanfordTree, int level, int childIndex) {
		super(-1, idt, ENTITY_POS.NOT_ENTITY);
		this.stanfordTree = stanfordTree;
		this.mLevel = level;
		this.mChildIndex = childIndex;
		this.checkRep();
	}

	protected SyntacticNode(int index, ARGUMENT_IDENTIFIER idt, 
			ENTITY_POS entityPos ,Tree stanfordTree, int level, int childIndex) {
		super(index, idt, entityPos);
		this.stanfordTree = stanfordTree;
		this.mLevel = level;
		this.mChildIndex = childIndex;
		this.checkRep();
	}

	public Tree getTree(){
		return this.stanfordTree;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null) return false;
		if (o.getClass() != this.getClass()) return false;
		SyntacticNode nb = (SyntacticNode)o;
		return this.stanfordTree.equals(nb.stanfordTree);
	}

	@Override
	public int hashCode(){
		return 7+13*stanfordTree.hashCode();
	}


	private void checkRep(){
		assert this.stanfordTree != null : "Null associated stanford tree";
	}

	@Override
	public int compareTo(SyntacticNode o) {
		if (this.mLevel == o.mLevel) 
			return this.mChildIndex - o.mChildIndex;
		else 
			return this.mLevel - o.mLevel;
	}
}
