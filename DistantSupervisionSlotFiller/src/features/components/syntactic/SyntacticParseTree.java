package features.components.syntactic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import trainingDataGenerator.AnnotatedSentenceParser.CJ;
import trainingDataGenerator.AnnotatedSentenceParser.EntityPairMention;
import trainingDataGenerator.AnnotatedSentenceParser.Range;
import trainingDataGenerator.AnnotatedSentenceParserAdapter;
import trainingDataGenerator.AnnotatedSentenceParserAdapter.AnnotationParsingException;
import edu.stanford.nlp.trees.Tree;
import features.components.Graph;
import features.components.RelationInstanceNode;
import features.components.RelationInstanceNode.ARGUMENT_IDENTIFIER;

/**
 * Wrapper class around stanford parse tree to make it easier to
 * use systematic approach
 * @author mhotan
 */
public class SyntacticParseTree extends Graph<SyntacticNode, SyntacticEdge> {

	/*
	 * Note: there is a lot of Symmetry with all the different graphs
	 * I tried to leave it as verbose as possible so its easy to 
	 * add additional attributes to features
	 * 
	 * Creating a graph where children have edges pointing to their parents
	 */

	/**
	 * Represents the root of this syntactic tree
	 * Maintain the root for all the entities
	 */
	private SyntacticNode mRoot, mRootEntity1, mRootEntity2;

	/**
	 * Lets keep data structure where the index of the leaves
	 * corresponds to the natural ordering of where the tokens appear 
	 * in the sentence
	 */
	private List<SyntacticNode> mLeaves;

	private SyntacticParseTree() {
		mLeaves = new ArrayList<>();
	}

	/**
	 * 
	 * @param annotations
	 * @param pairMention
	 * @return Syntactic Parse Tree of the sentence
	 * @throws AnnotationParsingException 
	 */
	public static SyntacticParseTree buildTree(AnnotatedSentenceParserAdapter parser, 
			EntityPairMention pairMention) throws AnnotationParsingException{
		if (parser == null)
			throw new RuntimeException("Building SyntacticParseTree annotations argument is null");
		if (pairMention == null)
			throw new RuntimeException("Building SyntacticParseTree pairMention argument is null");

		// Create a default edge that 
		CJ stanfordTree = parser.getCJ();

		if (stanfordTree == null)
			throw new RuntimeException("BuildingSyntactic parse tree failed: Unable to find Syntact " +
					"tree using stanford Core NLP");


		Tree stanfordRoot = stanfordTree.parseTree;
		//Create a tree to build
		SyntacticParseTree nTree = new SyntacticParseTree();
		List<SyntacticNode> leaves = new ArrayList<SyntacticNode>();

		//		Tree original = stanfordRoot.deepCopy(); For debug

		// Build the tree and return the root
		nTree.mRoot = recursiveBuild(stanfordRoot, pairMention, nTree, leaves, 0, 0);
		nTree.mLeaves = leaves;
		nTree.mRootEntity1 = nTree.getRootForSentenceRange(pairMention.tokenRange1);
		nTree.mRootEntity2 = nTree.getRootForSentenceRange(pairMention.tokenRange2);
		nTree.removeTokens();
		return nTree;
	}

	/**
	 * Remove all the tokens that lie at the leaves. Because tokens are represented
	 * as leaves in the tree we can just trim off all the nodes
	 * @param root
	 */
	private void removeTokens(){
		if (mLeaves == null){
			throw new RuntimeException("Ensure leaves are intialized and put in list");
		}
		for (SyntacticNode n: mLeaves){
			Tree t = n.getTree();
			List<Tree> children = t.getChildrenAsList();
			int size = children.size();
			//			System.out.println("For Node: " + t + " assert preterminal: " + t.isPreTerminal() );
			for (int i = 0; i < size; ++i){
				//				System.out.println("Trimming leaf: " + t.getChild(0));
				t.removeChild(0);
			}
			//			System.out.println("Post Effect: " + t + " assert preterminal and is leaf: " + (!t.isPreTerminal() && t.isLeaf()) );
		}
	}

	/**
	 * Recursively buils our tree by traversing to the leafs and building on the way up
	 * needs on average log()
	 * @param stanfordTree
	 * @param mention
	 * @param leaves An empty list that will hold all the leaves at the end of build
	 * @param syntacticTree non null (preferrably empty tree)
	 * @return root if new tree
	 */
	private static SyntacticNode recursiveBuild(Tree stanfordTree, final EntityPairMention mention,
			final SyntacticParseTree syntacticTree, final List<SyntacticNode> leaves,
			int level, int childIndex) {
		assert mention != null;
		assert syntacticTree != null;
		assert leaves != null;

		// Creating a node builder does the computate
		SyntacticNodeBuilder builder = new SyntacticNodeBuilder(stanfordTree, level, childIndex);
		boolean isLeaf = builder.isLeaf();
		// Base case - Reach preterminal Node
		if (isLeaf){
			// Have to adjust the position of the entity index (if its the head of an argument, or after, or non)
			builder.updateEntityPosition(RelationInstanceNode.getPositionWithinEntity(builder.sentenceIndex, mention));
			builder.updateWhichArgument(RelationInstanceNode.getArgIdentifier(builder.sentenceIndex, mention));

		} else {	// Recursive case	
			// Create a node for this tree root

			// Boolean to keep track of if an argument token is
			// in a below descendant
			boolean seenArg1 = false;
			boolean seenArg2 = false;	

			// Increment the level of for the children
			level++;

			// Iterate through one level of children
			List<Tree> children = stanfordTree.getChildrenAsList();
			for (int i = 0; i < children.size(); ++i){
				// Tracking which ones have an Argument identity of one or more arguments

				SyntacticNode childNode = recursiveBuild(children.get(i), mention, 
						syntacticTree, leaves, level, i+1);
				builder.addChild(childNode);
				// check if any of the arguments are in any of this descendants
				if (childNode.mArgTag == ARGUMENT_IDENTIFIER.ARGUMENT_1 ||
						childNode.mArgTag == ARGUMENT_IDENTIFIER.BOTH_ARGUMENTS)
					seenArg1 = true;
				if (childNode.mArgTag == ARGUMENT_IDENTIFIER.ARGUMENT_2 || 
						childNode.mArgTag == ARGUMENT_IDENTIFIER.BOTH_ARGUMENTS)
					seenArg2 = true;
			}

			// Adjust this current builders identity with respect to its children
			if (seenArg1 && seenArg2)
				builder.updateWhichArgument(ARGUMENT_IDENTIFIER.BOTH_ARGUMENTS);
			else if (seenArg1)
				builder.updateWhichArgument(ARGUMENT_IDENTIFIER.ARGUMENT_1);
			else if (seenArg2) 
				builder.updateWhichArgument(ARGUMENT_IDENTIFIER.ARGUMENT_2);
		}

		// Create the node and add the leaf
		SyntacticNode toReturn = builder.build(syntacticTree);
		if (isLeaf)
			leaves.add(toReturn);

		return toReturn;
	}

	//////////////////////////////////////////////////////////
	//	Gettes
	//////////////////////////////////////////////////////////

	/**
	 * For any verb found on the path from entity one to two
	 * Copy a new tree from that node remove any occurences of one or two
	 * @return list subtrees rooted at verbs excluding isntances of enitities
	 */
	public List<Tree> getVerbOnPathSubtrees(){
		List<Tree> result = new ArrayList<Tree>();
		
		Tree ent1 = mRootEntity1.getTree();
		Tree ent2 = mRootEntity2.getTree();
		List<Tree> shortestPath = mRoot.getTree().pathNodeToNode(ent1, ent2);
		// Find all the verbs along the path
		for (Tree pos: shortestPath){
			if (isVerb(pos.value())){ // Verb Found
				Tree verbCopy = pos.deepCopy();
				removeOccurencesOfEntity(verbCopy, ent1);
				removeOccurencesOfEntity(verbCopy, ent2);
				result.add(verbCopy);
			}
		}
		return result;
	}

	/**
	 * Attempt to find all occurrences of target and remove it from
	 * the root and all children of root
	 * @param root Root of tree to remove target from
	 * @param target target tree to remove
	 */
	private void removeOccurencesOfEntity(Tree root, Tree target){
		// FInd target tree
		if (target == null)
			return;
		
		List<Tree> children = root.getChildrenAsList();
		int toRemove = -1;
	
		for (int i = 0; i < children.size(); ++i){
			// Search for child to remove
			if (target.equals(children.get(i))){
				toRemove= i;
				break;
			} else // Recursive search for children
				removeOccurencesOfEntity(children.get(i), target);
		}
		// If we found an occurence
		if (toRemove != -1){
			root.removeChild(toRemove);
		}
	}

	public static boolean isVerb(String posTag){
		if (posTag == null) return false;
		if (posTag.length() == 0) return false;
		return posTag.charAt(0) == 'V';
	}

	/**
	 * Get the list of all subtrees inside Entity 1 by returning a tree rooted at each node. 
	 * These are not copies, but all share structure. The tree is regarded as a subtree of itself.
	 * Note: If you only want to form this Collection so that you can iterate over it, 
	 * it is more efficient to simply use the Tree class's own iterator() method. 
	 * This will iterate over the exact same elements (but perhaps/probably in a different order).
	 * @return the List of all subtrees in the Entity 1
	 */
	public List<Tree> getEntity1Subtrees(){
		return mRootEntity1.getTree().subTreeList();
	}

	/**
	 * Get the list of all subtrees inside Entity 2 by returning a tree rooted at each node. 
	 * These are not copies, but all share structure. The tree is regarded as a subtree of itself.
	 * Note: If you only want to form this Collection so that you can iterate over it, 
	 * it is more efficient to simply use the Tree class's own iterator() method. 
	 * This will iterate over the exact same elements (but perhaps/probably in a different order).
	 * @return the List of all subtrees in the Entity 2
	 */
	public List<Tree> getEntity2Subtrees(){
		return mRootEntity2.getTree().subTreeList();
	}

	
	
	
	/**
	 * Returns the shortest path from the head of argument 1 and 2
	 * Direction invariant
	 * @param src source node
	 * @param dst destination
	 * @return
	 */
	public List<String> getShortestPath(SyntacticNode src, SyntacticNode dst){
		List<Tree> lst = getShortestPathTrees(src, dst);
		if (lst == null) return null;
		List<String> result = new ArrayList<String>(lst.size());
		for (Tree t: lst){
			result.add(t.value());
		}
		return result;
	}

	/**
	 * 
	 * @param src
	 * @param dst
	 * @return
	 */
	private List<Tree> getShortestPathTrees(SyntacticNode src, SyntacticNode dst){
		return mRoot.getTree().pathNodeToNode(
				mRootEntity1.getTree(), mRootEntity2.getTree());
	}

	/**
	 * @return Root of this tree
	 */
	public SyntacticNode getRoot(){
		return mRoot;
	}

	/**
	 * @return the head for entity 1
	 */
	public SyntacticNode getRootForEntity1(){
		return mRootEntity1;
	}

	/**
	 * @return the head for entity 2
	 */
	public SyntacticNode getRootForEntity2(){
		return mRootEntity2;
	}

	/**
	 * Returns the children of this root in the order that they were added
	 * @param root The root in which all its children will be found
	 * @return Sorted list of all children of root.  Order in 
	 */
	public List<SyntacticNode> getChildren(SyntacticNode root){
		List<SyntacticNode> nbrs = getNeighorsOf(root);	
		List<SyntacticNode> children = new ArrayList<SyntacticNode>(nbrs.size()-1);
		for (SyntacticNode n : nbrs){
			SyntacticEdge edge = getFirstEdge(root, n);
			if (edge.pointsToChild())
				children.add(n);
		}
		Collections.sort(children);
		return children;
	}

	/**
	 * Returns the parent of this node or null if parent does not exist
	 * @param root Root of the node to find parent of
	 * @return Parent of root
	 */
	public SyntacticNode getParent(SyntacticNode root){
		List<SyntacticNode> nbrs = getNeighorsOf(root);	
		for (SyntacticNode n : nbrs){
			SyntacticEdge edge = getFirstEdge(root, n);
			if (edge.pointsToParent())
				return n;
		}
		return null; // Is the root
	}

	/**
	 * Returns whether root contains the query
	 * @param root Root to search from
	 * @param query Query node to search for
	 * @return Whether Root contains query node
	 */
	public boolean containsNode(SyntacticNode root, SyntacticNode query){
		if (!containsNode(root))
			throw new IllegalArgumentException("Root not in tree");
		if (!containsNode(query))
			throw new IllegalArgumentException("Query not in tree");

		Tree rootTree = root.getTree();
		Tree queryTree = query.getTree();
		// never found the node
		return rootTree.dominates(queryTree);
	}

	/**
	 * Given a start inclusive and end index inclusive obtain the upper 
	 * most root of the those sentence nodes.
	 * @param start Node to start from
	 * @param end Node to end at
	 * @return The root to the sub tree where the lowest index is start and highest index is end 
	 */
	public SyntacticNode getRootForSentenceRange(Range r){
		int start = r.getStart();
		int end = r.getEnd();
		if (start > end)
			throw new IllegalArgumentException("Start index: " + start + " is greater then end index: " + end);
		if (start < 0)
			throw new IllegalArgumentException("Start index: " + start + " cant be negative");
		if (end > mLeaves.size())
			throw new IllegalArgumentException("End index: " + end + " " +
					"cant be greater then  number of leaves: " + mLeaves.size());
		SyntacticNode startNode = mLeaves.get(start);
		SyntacticNode endNode = mLeaves.get(end-1);

		SyntacticNode root = startNode;
		while (root != null && !containsNode(root, endNode)){
			root = getParent(root);
		}

		if (root == null)
			throw new RuntimeException("Unable to find root for some reason: " +
					"Check containsNode(root, query) method");
		return root;
	}

	public boolean isLeaf(SyntacticNode root){
		return getChildren(root).size() == 0;
	}

	@Override
	public String toString(){
		if (mRoot != null)
			return mRoot.getTree().toString();
		else
			return "Building Graph...";
	}


	//
	//	private String recursiveToString2(StringBuffer b, SyntacticNode node){
	//		if (b == null)
	//			b = new StringBuffer();
	//		boolean isLeaf = isLeaf(node);
	//		if (node == null) return "GRAPH NOT COMPLETE";
	//		if (!isLeaf)
	//			b.append(OPEN_PARENTH);
	//		DefaultSyntacticAttribute a = node.getAttribute();
	//		b.append(node.getAttribute().posTag);			
	//		for (SyntacticNode child: getChildren(node)){
	//			b.append(SPACE);
	//			recursiveToString2(b,child);
	//		}
	//		if (!isLeaf)
	//			b.append(CLOSE_PARENTH);
	//		return b.toString();
	//	}
	//
	//	private String recursiveToString3(StringBuffer b, SyntacticNode node){
	//		if (node == null) return super.toString();
	//		if (b == null)
	//			b = new StringBuffer();
	//		b.append(OPEN_PARENTH);
	//		DefaultSyntacticAttribute a = node.getAttribute();
	//		if (a != null){
	//			b.append(node.getAttribute().posTag);
	//			if (a instanceof ValueSyntacticAttribute)
	//				b.append(((ValueSyntacticAttribute)a).token);
	//		}
	//		List<SyntacticNode> children = null;
	//		try {
	//			children = getChildren(node);
	//		} catch (IllegalArgumentException e) {
	//			System.out.println("Node: " + node + e.getMessage());
	//		}
	//		for (SyntacticNode child: children){
	//			b.append(SPACE);
	//			recursiveToString3(b,child);
	//		}
	//		b.append(CLOSE_PARENTH);
	//		return b.toString();
	//	}
}
