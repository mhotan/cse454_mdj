package features.components.syntactic;

import features.components.Attribute;

/**
 * Attribute that does not correspond to a lemma or token value
 * @author mhotan
 */
public class DefaultSyntacticAttribute implements Attribute {
	/**
	 * This represents POS tag for a given node
	 */
	public final String posTag;

	//TODO Create more attributes that define a node in a syntactic parse
	// tree

	public DefaultSyntacticAttribute(String pos) {
		posTag = pos;
		checkRep();
	}

	/**
	 * representation Checker
	 */
	private void checkRep(){
		assert posTag != null: "POS tag is null";
	}

	@Override
	public String toDiscernableString() {
		return posTag;
	}
	
	@Override 
	public String toString(){
		return "[ POS: " + posTag + " ]"  ;
	}
}
