package features.components;

public abstract class Path {

	protected static final String OPEN_SQUARE_BRACE = "[";
	protected static final String CLOSE_SQUARE_BRACE = "]";
	protected static final String NO_PATH = "-NO_PATH-";
	
	protected enum PRINT_TYPE {TOKEN, NER, POS, LEMMA}
	
	public abstract int getSize();
	
	public abstract boolean isEmpty();
}
