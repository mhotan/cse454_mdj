package features.components.sequential;

import features.components.Attribute;
/**
 * Right now only attribute in sequence graph is its post tag
 * @author mhotan
 */
public class SequenceAttribute implements Attribute {

	private final static String TAG = SequenceAttribute.class.getSimpleName();
	
	/**
	 * Represents attributes of a sentence token feature
	 */
	public final String pos;
	public final String token;
	public final String lemma;
	public final nlp.StanfordNerTag ner;

	/**
	 * Represents an attribute for this
	 * @param token
	 */
	public SequenceAttribute(String pos, String token, 
			String lemma, nlp.StanfordNerTag tag) {
		this.pos = pos;
		this.token = token;
		this.lemma = lemma;
		this.ner = tag;
		checkRep();
	}
	
	@Override
	public String toDiscernableString() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override 
	public String toString(){
		return "[ POS: " + pos + " TOKEN: " + token + " LEMMA: " + lemma + " NER: " + ner + " ]"  ;
	}

	/**
	 * representation Checker
	 */
	private void checkRep(){
		// Dont know why i chose assertions here
		// Maybe because sometimes we  forget to run the program with assertions on
		assert pos != null: TAG + " NULL POS";
		assert token != null: TAG + " NULL TOKEN" ;
		assert lemma != null: TAG + " NULL LEMMA";
		assert ner != null: TAG + " NULL NER";
	}
	
}
