package features.components.sequential;

import java.util.ArrayList;
import java.util.List;

import util.POSIdentifier;

import features.components.Path;

public class SequentialPath extends Path implements Comparable<SequentialPath>{

	private final List<SequentialNode> mPath;
	
	/**
	 * Extracts the path end to end from the graph
	 * @param graph
	 */
	public SequentialPath(SequenceRelationGraph graph){
		int size = graph.getNumOfNodes();
		mPath = new ArrayList<SequentialNode>(size);
		// Adds all the node from this to a list
		for (SequentialNode n: graph){
			mPath.add(n);	
		}
	}
	
	/**
	 * Creates a sequential path treating a list of nodes as a literal path
	 * first element is considered the start of the path while the last is at the end
	 * of the list
	 * @param path
	 */
	public SequentialPath(List<SequentialNode> path){
		if (path == null) throw new IllegalArgumentException("Paths cannot be null");
		mPath = new ArrayList<SequentialNode>(path);
	}
	
	/**
	 * Returns sub path of only verbs and content
	 * @return
	 */
	public SequentialPath getJustVerbs(){
		List<SequentialNode> nPath = new ArrayList<SequentialNode>();
		for (SequentialNode n: mPath){
			if (POSIdentifier.isVerb(n.getAttribute().pos))
				nPath.add(n);
		}
		return new SequentialPath(nPath);
	}
	
	public SequentialPath getJustContentWord(){
		List<SequentialNode> nPath = new ArrayList<SequentialNode>();
		for (SequentialNode n: mPath){
			if (POSIdentifier.isContentWord(n.getAttribute().pos))
				nPath.add(n);
		}
		return new SequentialPath(nPath);
	}
	
	@Override
	public int compareTo(SequentialPath o) {
		return o.getSize() - this.getSize();
	}

	@Override
	public int getSize() {
		return mPath.size()-1;
	}

	@Override
	public boolean isEmpty() {
		return mPath.isEmpty();
	}
	
	public String getNERPath(){
		return getPathString(mPath.size(), PRINT_TYPE.NER, false);
	}
	
	public String getTokenPath(){
		return getPathString(mPath.size(), PRINT_TYPE.TOKEN, false);
	}
	
	public String getLemmaPath(){
		return getPathString(mPath.size(), PRINT_TYPE.LEMMA, false);
	}
	
	public String getPosPath(){
		return getPathString(mPath.size(), PRINT_TYPE.POS, false);
	}
	
	public String getNERPath(int window){
		return getPathString(window, PRINT_TYPE.NER, false);
	}

	public String getTokenPath(int window){
		return getPathString(window, PRINT_TYPE.TOKEN, false);
	}

	public String getLemmaPath(int window){
		return getPathString(window, PRINT_TYPE.LEMMA, false);
	}

	public String getPosPath(int window){
		return getPathString(window, PRINT_TYPE.POS, false);
	}
	
	public String getNERPathReverse(int window){
		return getPathString(window, PRINT_TYPE.NER, true);
	}

	public String getTokenPathReverse(int window){
		return getPathString(window, PRINT_TYPE.TOKEN, true);
	}

	public String getLemmaPathReverse(int window){
		return getPathString(window, PRINT_TYPE.LEMMA, true);
	}

	public String getPosPathReverse(int window){
		return getPathString(window, PRINT_TYPE.POS, true);
	}
	
	private static final String SEPERATOR = "-";
	private static final String BLANK = "_NA_";
	private static final String SPACE = " ";
	/**
	 * Print string in reverse order if specied
	 * @param type
	 * @param reverse
	 * @return
	 */
	protected String getPathString(int windowSize, PRINT_TYPE type, boolean reverse){
		StringBuffer b = new StringBuffer();
		int cnt = 0;
		if (reverse){
			for (int i = mPath.size()-1; i >= 0 && cnt < windowSize; i--){
				SequentialNode n = mPath.get(i);
				b.insert(0, getType(n, type));// Insert to front
				if (i != 0) // If not end
					b.insert(0, SEPERATOR);
				cnt++;
			}
			
			//Append remaining with blank annotationss
			for (int i = cnt; i < windowSize; i++){
				b.insert(0, (i-cnt+1)+BLANK+SPACE);
			}
		} else { // in order traversal
			for (int i = 0; i < mPath.size() && cnt < windowSize; i++){
				SequentialNode n = mPath.get(i);
				b.append(getType(n, type));
				if (i != mPath.size()-1)
					b.append(SEPERATOR);
				cnt++;
			}
			
			// Append remaining with blank annotations
			for (int i = cnt; i < windowSize; i++){
				b.append(SPACE+BLANK+(i-cnt+1));
			}
		}
		return b.toString();
	}

	/**
	 * Quick extractor for 
	 * Arguements cant be null
	 * @param n
	 * @param type
	 * @return
	 */
	private static String getType(SequentialNode n, PRINT_TYPE type){
		SequenceAttribute a = n.getAttribute();
		switch (type){
		case LEMMA: return a.lemma;
		case NER: return a.ner.toString();
		case POS: return a.pos;
		default: // has to be token
			return a.token;
		}
	}
	
	@Override
	public String toString(){
		StringBuffer b = new StringBuffer();
		for (SequentialNode n: mPath){
			b.append(n.getAttribute().token);
			b.append(SPACE);
		}
		return b.toString();
	}
}
