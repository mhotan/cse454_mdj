package features.components.sequential;

import java.util.Iterator;
import java.util.List;

import trainingDataGenerator.AnnotatedSentenceParser.EntityPairMention;
import trainingDataGenerator.AnnotatedSentenceParser.Stanfordlemma;
import trainingDataGenerator.AnnotatedSentenceParser.Stanfordner;
import trainingDataGenerator.AnnotatedSentenceParser.Stanfordpos;
import trainingDataGenerator.AnnotatedSentenceParser.Tokens;
import trainingDataGenerator.AnnotatedSentenceParserAdapter;
import trainingDataGenerator.AnnotatedSentenceParserAdapter.AnnotationParsingException;
import features.components.Graph;
import features.components.RelationInstanceNode;
import features.components.RelationInstanceNode.ARGUMENT_IDENTIFIER;
import features.components.RelationInstanceNode.ENTITY_POS;
import features.components.sequential.SequentialEdge.EDGE_DIRECTION;

/**
 * Seqeunce Representation of a sentence or sequence of tokens.
 * <b> Nodes are connected to neighbor if and only if the neighbor follows the current 
 * node in order it is presented in the sentence.  
 * @author mhotan
 */
public class SequenceRelationGraph extends Graph<SequentialNode, SequentialEdge> implements Iterable<SequentialNode> {

	/////////////////////////////////////////////////////////////////////
	////  Unique components of the graph
	/////////////////////////////////////////////////////////////////////

	/*
	 * A node is represented as a combination of a tokens literal value and its POS tag
	 * An edge is identified as a backward or forward running edge
	 * 
	 * A neighbor can be any node to the left(backward edge) or right(forward edge) of the 
	 * node in question
	 * 
	 * Every node can have up to two neighbors.
	 * 
	 * Every node can have up to one forward node and one backward node
	 *  
	 */
	
	
	/*
	 * Abstract Representation:
	 * For a given instance graph G,
	 * 	G = {V,E,A,B} as defined by Jing Jiang and ChengXiang Zhai
	 * V => Represents all nodes in G [v1, vi, ]
	 * E => All edges connecting nodes in G
	 * A => Function of all v such that A(v) produces a collection of all attributes
	 * B => Function distinguished argument nodes between non argument nodes
	 */

	/**
	 * Because our graph can be represented as a single branch tree
	 * Our first node can also be seen as our first token
	 * its 
	 */
	private SequentialNode mFirstNode;
	private SequentialNode mLastNode;

	/**
	 * Points to the head of the first entity
	 */
	private SequentialNode mFirstEntity;

	/**
	 * Points to the head of the second entity
	 */
	private SequentialNode mSecondEntity;

	private static final SequentialEdge forwardEdge = new SequentialEdge(EDGE_DIRECTION.FORWARD);
	private static final SequentialEdge backwardEdge = new SequentialEdge(EDGE_DIRECTION.BACKWARD);

	/**
	 * 
	 * @param sentence should be a single English sentence,  If String contains more then one
	 * sentence then only the first sentence in the String will be used 
	 * @param Map of string constant that define a type of feature descriptor to its corresponding value 
	 * of the sentece
	 * @return Sequential graph of the sentence
	 * @throws AnnotationParsingException 
	 */
	public static SequenceRelationGraph buildGraph(AnnotatedSentenceParserAdapter parser, 
			EntityPairMention pairMention) throws AnnotationParsingException{
		assert parser != null: "Null attributes";

		// In this graph we need to know which node can be seen as a entity
		// entity can span multiple tokens
		SequenceRelationGraph nGraph = new SequenceRelationGraph();

		Tokens tokens = parser.getTokens();
		Stanfordlemma lemma = parser.getStanfordlemma();
		Stanfordpos pos = parser.getStanfordpos();
		Stanfordner ner = parser.getStanfordner();

		// Group all the tokens that represent the entity into a single node
		// Within this node it is possible to have a smaller data structure inside

		assert (tokens.size() == lemma.size() &&
				lemma.size() == pos.size() &&
				pos.size() == ner.size()) : 
					"Size of tokens, lemma, pos, ner are not equal!";

		// Sentence size determined by number of tokens
		int size = tokens.tokens.size();

		// NOTE: As of right now there is 


		// root is root of the graph or in this case a tree,
		// current is the current node being 				
		SequentialNode previous = null;
		for (int i = 0; i < size; ++i) {

			ARGUMENT_IDENTIFIER idt = RelationInstanceNode.getArgIdentifier(i, pairMention);
			ENTITY_POS position = RelationInstanceNode.getPositionWithinEntity(i, pairMention);

			SequentialNode node = new SequentialNode(i, idt, position);

			// Check if we are pointing to the head of either argument
			if (position == ENTITY_POS.HEAD){
				if (idt == ARGUMENT_IDENTIFIER.ARGUMENT_1)
					nGraph.mFirstEntity = node;
				else if (idt == ARGUMENT_IDENTIFIER.ARGUMENT_2)
					nGraph.mSecondEntity = node;
				else 
					throw new RuntimeException("Fail to build graph: " +
							"Could not distuingish which entity a head node belongs to. Pairmention = " + pairMention.serialize() + 
							", annots =  " +parser.toString());
			}


			// Add attributes to this node that will define this node
			node.setAttribute(new SequenceAttribute(
					pos.get(i), tokens.get(i), lemma.get(i), ner.get(i)));

			// Add node to graph
			nGraph.addNode(node);

			// Check if we are looking at the first/root node
			if (previous != null) {
				nGraph.addEdge(previous, node);
			} else // If current is still null then it means we are first token
				nGraph.mFirstNode = node;

			// Assign the last node
			if (i == size-1)
				nGraph.mLastNode = node;

			previous = node; // Update the current 
		}

		nGraph.checkRep();
		return nGraph;
	}

	//////////////////////////////////////////////////////////////////////
	//// Sequential specific related methods
	//////////////////////////////////////////////////////////////////////

	/**
	 * Creates a token order relation with two specific Edges
	 * @param src
	 * @param dst
	 */
	private void addEdge(SequentialNode src, SequentialNode dst){
		if (src == null || dst == null) return; // Dont add edges to null values
		super.addEdge(src, dst, forwardEdge);
		super.addEdge(dst, src, backwardEdge);
	}

	/**
	 * Can return null if this is not a complete graph
	 * @return
	 */
	public SequentialNode getHeadOfEntity1(){
		return mFirstEntity;
	}

	/**
	 * Can return null if not a complete graph
	 * @return
	 */
	public SequentialNode getHeadOfEntity2(){
		return mSecondEntity;
	}

	/**
	 * @return null if no entity in this graph, Tail node of the first entity otherwise
	 */
	public SequentialNode getTailOfEntity1(){
		if (mFirstEntity == null) return null; 
		return getTailOf(getHeadOfEntity1());
	}

	public SequentialNode getTailOfEntity2(){
		if (mFirstEntity == null) return null; 
		return getTailOf(getHeadOfEntity2());
	}

	/**
	 * 
	 * @param entityHead Head of entity 
	 * @return
	 */
	private SequentialNode getTailOf(SequentialNode entityHead) {
		SequentialNode end = entityHead;
		SequentialIterator iter = getIteratorAt(entityHead);
		ARGUMENT_IDENTIFIER idt = end.mArgTag;
		while (iter.hasNext()){
			SequentialNode temp = iter.next();
			boolean endOfSentence = temp == null; // Reached end of sentence
			boolean notSameEntity = temp.mArgPos.equals(ENTITY_POS.NOT_ENTITY) ;// If reached a non entity word
			boolean otherEntity = (temp.mArgPos.equals(ENTITY_POS.HEAD) // If reached next entity before non entity
					&& idt != temp.mArgTag);
			if (endOfSentence || notSameEntity || otherEntity)
				break; // end1 now stores exclusive node for subgraph
			end = temp;
		}
		return end;
	}

	public SequentialNode getFirstNode(){
		return mFirstNode;
	}

	public SequentialNode getLastNode(){
		return mLastNode;
	}

	/**
	 * Returns a graph from the start node(inclusive) to the end node(exclusive)
	 * Contracts: 
	 *  Start node must be in graph if null else empty graph is returned
	 *  End node can exist in the graph or can be null.  if null up until the end of the graph is copied
	 *  the start argument must preceed the end argument
	 * @param start preceeding node to start subgraph
	 * @param end node to stop short of or null to complete to the end of this graph
	 * @return subgraph that starts at start and ends at end
	 */
	public SequenceRelationGraph getSubGraph(SequentialNode start, SequentialNode end) {
		if (start == null)
			return new SequenceRelationGraph();

		if (!containsNode(start))
			throw new IllegalArgumentException("Starting node: " + start + " not in this graph");
		if (end != null && !containsNode(end))
			throw new IllegalArgumentException("Ending node: " + end + " not in this graph");
		if (end != null && start.mIndex > end.mIndex)
			throw new IllegalArgumentException("Starting node MUST preceed ending node");

		SequenceRelationGraph nGraph = new SequenceRelationGraph();

		SequentialNode prev = null;
		SequentialIterator iter = new SequentialIterator(start);
		while (iter.hasNext()){
			SequentialNode next = iter.next();
			// If we reached end of our subgraph or reached end of the containing graph
			if (next.equals(end)){
				nGraph.mLastNode = prev;
				break;
			} else if (next.equals(mLastNode)){ // Edge case where Last node in the outer graph will be the same for subgraph
				nGraph.mLastNode = next;
			}

			nGraph.addNode(next);
			if (prev == null){ // Check if we are looking at first node
				nGraph.mFirstNode = next;
			} else // Add edge if there was a proceeding node
				nGraph.addEdge(prev, next);

			prev = next;
		}

		return nGraph;
	}

	public SequentialNode getNodeBefore(SequentialNode node){
		if (!containsNode(node)) return null;
		if (node.equals(mFirstNode)) return null;
		List<SequentialNode> lst = getNeighorsOf(node);
		if (lst.isEmpty())
			return null;
		for (SequentialNode nbr : lst){
			SequentialEdge edge = getFirstEdge(node, nbr);
			if (edge.isPointingToPrevious())
				return nbr;
		}
		// If it reaches this point then something terribly wrong happen
		// The graph is broken
		throw new RuntimeException("Could not find a node before the argument node: " + node.toString());
	}

	/**
	 * Gets node after
	 * @param node
	 * @return null if Node is not in this graph or This is the last node in the sequence in the graph, Following node 
	 * otherwise
	 */
	public SequentialNode getNodeAfter(SequentialNode node){
		if (!containsNode(node)) return null;
		if (node.equals(mLastNode)) return null;
		List<SequentialNode> lst = getNeighorsOf(node);
		if (lst.isEmpty())
			return null;
		for (SequentialNode nbr : lst){
			SequentialEdge edge = getFirstEdge(node, nbr);
			if (edge.isPointingToNext()) // Should only be one node
				return nbr;
		}
		// If it reaches this point then something terribly wrong happen
		// The graph is broken
		throw new RuntimeException("Could not find a node before the argument node: " + node.toString());
	}

	/**
	 * Runs in O(n) time so use only when have to
	 * @return Number of nodes in this graphs
	 */
	public int getSize(){
		// Counting the exact number of nodes is the only way to maintain
		// deterministic approach
		return this.getNumOfNodes();
	}

	@Override
	public String toString(){
		StringBuffer b = new StringBuffer();
		b.append("[");
		for (SequentialNode node : this){
			b.append(node.toString());
			if (!node.equals(mLastNode))
				b.append(", ");
		}
		b.append("]");
		return b.toString();
	}

	public SequentialIterator getIteratorAt(SequentialNode node){
		if (!containsNode(node))
			throw new IllegalArgumentException("Node: "+node+" to start iterator not in this graph");
		return new SequentialIterator(node);
	}
	
	public ReverseSequentialIterator getReverseIterator(){
		return new ReverseSequentialIterator();
	} 

	@Override
	protected boolean checkRep(){
		super.checkRep();
		if (mFirstNode == null) 
			throw new RuntimeException("First node can't be null");
		if (mFirstEntity == null)
			throw new RuntimeException("Null first entity node");
		if (mSecondEntity == null)
			throw new RuntimeException("Null second entity node");
		for (SequentialNode node : this)
			if (node.getAttribute() == null)
				throw new RuntimeException("Null Attribute found with node: " + node);
		return true;
	}

	/**
	 * Guaranteed to touch every node in order of presentation in sentence
	 */
	@Override
	public Iterator<SequentialNode> iterator() {
		return new SequentialIterator(mFirstNode);
	}

	public class SequentialIterator implements Iterator<SequentialNode>{

		private SequentialNode mCurrent;

		/**
		 * Create an iterator for this 
		 * @param firstNode
		 */
		public SequentialIterator(SequentialNode node){
			mCurrent = node;
		}

		@Override
		public boolean hasNext() {
			return mCurrent != null;
		}

		@Override
		public SequentialNode next() {
			SequentialNode previous = mCurrent;
			mCurrent = getNodeAfter(previous);
			return previous;
		}

		@Override
		public void remove() {
//			if (mCurrent == null)
//				return;
//			SequentialNode previous  = getNodeBefore(mCurrent);
//			SequentialNode after = getNodeAfter(mCurrent);
//			deleteNode(mCurrent);
//			addEdge(previous, after);
//			mCurrent = after;
		}
	}
	
	public class ReverseSequentialIterator implements Iterator<SequentialNode>{

		/**
		 * Current node to return
		 */
		private SequentialNode mCurrent;
		
		public ReverseSequentialIterator(){
			if (getSize() != 0 && mLastNode == null)
				throw new RuntimeException("ReverseSequentialIterator: Last node is null");
			mCurrent = mLastNode;
		}
		
		@Override
		public boolean hasNext() {
			return mCurrent != null;
		}

		@Override
		public SequentialNode next() {
			SequentialNode previous = mCurrent;
			mCurrent = getNodeBefore(previous);
			return previous;
		}

		@Override
		public void remove() {
//			if (mCurrent == null)
//				return;
//			SequentialNode previous  = getNodeBefore(mCurrent);
//			SequentialNode after = getNodeAfter(mCurrent);
//			deleteNode(mCurrent);
//			addEdge(previous, after);
//			mCurrent = after;
		}
		
	}
}
