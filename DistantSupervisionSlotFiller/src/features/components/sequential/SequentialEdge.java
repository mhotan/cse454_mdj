package features.components.sequential;

import features.components.RelationInstanceEdge;

/**
 * Represents an edge connecting to Sequential Nodes.
 * <b>I.E. If there exist SequentialNode A and B  that represents two entities
 * where B follows  
 * @author mhotan
 */
public class SequentialEdge extends RelationInstanceEdge implements Comparable<SequentialEdge>{

	/**
	 * Dictactes the edge direction of one node to the other
	 * If forward then the destination node appear in a later sequence 
	 * @author mhotan
	 */
	public enum EDGE_DIRECTION {
		FORWARD("->"),
		BACKWARD("<-")
		;

		private final String text;

		private EDGE_DIRECTION(String name){
			this.text = name;
		}

		@Override
		public String toString(){
			return text;
		}
	}

	public final EDGE_DIRECTION mDirection;		

	public SequentialEdge(EDGE_DIRECTION direction){
		if (direction == null)
			throw new IllegalArgumentException("Direction of an edge can't be null");
		mDirection = direction;
	}

	@Override
	public String toString(){
		return mDirection.toString();
	}

	@Override
	public boolean equals(Object o){
		if (o == null) return false;
		if (!o.getClass().equals(this.getClass())) return false;
		SequentialEdge edge = (SequentialEdge)o;
		return this.mDirection == edge.mDirection;
	}

	@Override
	public int hashCode(){
		return mDirection.hashCode();
	}

	public boolean isPointingToPrevious(){
		return mDirection == EDGE_DIRECTION.BACKWARD;
	}

	public boolean isPointingToNext(){
		return mDirection == EDGE_DIRECTION.FORWARD;
	}

	@Override
	public int compareTo(SequentialEdge o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
