package features.components;

import trainingDataGenerator.AnnotatedSentenceParser.EntityPairMention;


public class RelationInstanceNode<T extends Attribute> {

	private final static String TAG = RelationInstanceNode.class.getSimpleName();

	/*
	 * Abstract Representation:
	 * Represents a Node in a relation instance graph.
	 * 
	 * Abstract Function
	 * mArgTag => Defines how many of the arguments this node represents 
	 * mAttributes => 
	 * Representation 
	 */

	/**
	 * Defines if node is related to which argument
	 * @author mhotan
	 */
	public enum ARGUMENT_IDENTIFIER {
		NO_ARGUMENTS("Not An Argument"), 
		ARGUMENT_1("1st Argument"), 
		ARGUMENT_2("2nd Argument"), 
		BOTH_ARGUMENTS("Both Arguments")
		;

		private final String text;

		private ARGUMENT_IDENTIFIER(String name){
			this.text = name;
		}

		@Override
		public String toString(){
			return text;
		}
	}

	/**
	 * For a given node if it a node that represents a ENTITY or ARGUMENT
	 * @author mhotan
	 */
	public static enum ENTITY_POS { 
		HEAD("HEAD"), 
		NOT_HEAD("Not HEAD"), 
		NOT_ENTITY("Not Entity") 
		;

		private final String text;

		private ENTITY_POS(String name){
			this.text = name;
		}

		@Override
		public String toString(){
			return text;
		}
	}

	/**
	 * Correlates number of arguments to A
	 * @param i, 0-3 number of arguments
	 * @return ARGUMENT_IDENTIFIER 
	 */
	public static ARGUMENT_IDENTIFIER getArgumentIdentifier(int i){
		switch (i){
		case 0: return ARGUMENT_IDENTIFIER.NO_ARGUMENTS;
		case 1: return ARGUMENT_IDENTIFIER.ARGUMENT_1;
		case 2: return ARGUMENT_IDENTIFIER.ARGUMENT_2;
		case 3: return ARGUMENT_IDENTIFIER.BOTH_ARGUMENTS;
		default: return null;
		}
	}

	/**
	 * For a given entity pair mention there is a range of tokens for the first argument
	 * and the second argument.  Frequently you have to assign if a node in a graph falls within these 
	 * ranges.  This method computes the simple logic and returns where a certain node would lie just based 
	 * off the index.  This method does not make any assumptions about the graph.  It strictly looks at index values
	 * 
	 * mention can't be null
	 * 
	 * @param tokenIndex Index of the position in question
	 * @param mention contains the information regarding the index of the 
	 * entities that relate to a specific sentence
	 * @return An identifier that expresses if specific index falls within entity range
	 */
	public static ARGUMENT_IDENTIFIER getArgIdentifier(int tokenIndex, EntityPairMention mention) {
		ARGUMENT_IDENTIFIER identifier = ARGUMENT_IDENTIFIER.NO_ARGUMENTS;
		// the first entity
		if (tokenIndex >= mention.tokenRange1.getStart()
				&& tokenIndex < mention.tokenRange1.getEnd()) {
			identifier = ARGUMENT_IDENTIFIER.ARGUMENT_1;
		}

		// Check if its the second entity
		if (tokenIndex >= mention.tokenRange2.getStart() 
				&& tokenIndex < mention.tokenRange2.getEnd()) {
			if (identifier == ARGUMENT_IDENTIFIER.ARGUMENT_1)
				identifier = ARGUMENT_IDENTIFIER.BOTH_ARGUMENTS;
			else
				identifier = ARGUMENT_IDENTIFIER.ARGUMENT_2;
		}
		return identifier;
	}

	/**
	 * Returns whether the index, which should relate to an index of a certain node,
	 * falls at the head of an entity, middle or end, or not at all.
	 * @param tokenIndex Target index that should reference a token
	 * @param mention Mention of the pair of entities pertaining to target sentence
	 * @return The Entity position if the index reference HEAD, After head (but still entity), or Not an entity at all
	 */
	public static ENTITY_POS getPositionWithinEntity(int tokenIndex, EntityPairMention mention){
		ENTITY_POS position = ENTITY_POS.NOT_ENTITY;
		if (tokenIndex == mention.tokenRange1.getStart())
			position = ENTITY_POS.HEAD;
		else if (tokenIndex > mention.tokenRange1.getStart()
				&& tokenIndex < mention.tokenRange1.getEnd()) {
			position = ENTITY_POS.NOT_HEAD;
		} else if (tokenIndex == mention.tokenRange2.getStart())
			position = ENTITY_POS.HEAD;
		else if (tokenIndex > mention.tokenRange2.getStart() && 
				tokenIndex < mention.tokenRange2.getEnd())
			position = ENTITY_POS.NOT_HEAD;
		return position;
	}

	/**
	 * Collection of all relevant attributes of this 
	 * specific feature instance
	 */
	protected T mAttributes;

	/**
	 * Defines the number of arguments in the feature node
	 */
	public final ARGUMENT_IDENTIFIER mArgTag;

	/**
	 * 
	 */
	public ENTITY_POS mArgPos;

	/**
	 * Represents the assigned index of this node
	 * Index value and meaning depends on the implementation
	 * of the subclass of t
	 */
	public final int mIndex;

	/**
	 * Creates a node assigning it an identifier
	 * @param argIdentifier
	 */
	public RelationInstanceNode(int index, ARGUMENT_IDENTIFIER argIdentifier, ENTITY_POS pos){
		mArgTag = argIdentifier;
		mAttributes = null;
		mIndex = index;
		mArgPos = pos;
		//		checkRep();
	}

	/**
	 * Add a definitive attribute to this system.  If Node
	 * already has this attribute then it is not added.
	 * Check a.equals.
	 * <b>If a is null then it is not added
	 * @param a attribute to be associated with this node
	 */
	public void setAttribute(T a){
		if (a == null) return;
		if (mAttributes == null || !mAttributes.equals(a))
			mAttributes = a;
	}

	/**
	 * If Node contains this attribute then
	 * it is removed from this Node
	 * @param a Attribute to be removed
	 */
	public void removeAttribute(){
		mAttributes = null;
	} 
	
	/**
	 * 
	 * @return null if no attributes exist
	 */
	public T getAttribute(){
		return mAttributes;
	}

	// This didnt feel right doing

	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (! o.getClass().equals(getClass()))
			return false;
		RelationInstanceNode<T> n = (RelationInstanceNode<T>) o;
		// Check if the entire list are equal
		// They must be equal in size
		boolean argsEqual = true;
		if (mAttributes == null)
			argsEqual &= n.mAttributes == null;
		else 
			argsEqual &= mAttributes.equals(n.mAttributes);

		return mIndex == n.mIndex &&
				mArgTag == n.mArgTag && 
				argsEqual &&
				mArgPos == n.mArgPos;
	}

	@Override
	public int hashCode(){
		int code = mIndex * 13;
		if (mAttributes != null)
			code += 7*mAttributes.hashCode();
		code += 17*mArgPos.hashCode() + 31*mArgTag.hashCode();
		return code;
	}

	/**
	 * Checks representation of 
	 */
	private void checkRep(){
		if (mArgTag == null)
			throw new RuntimeException(TAG+": Null Argument Tag");
		if (mArgPos == null)
			throw new RuntimeException(TAG+": Null Position Tag");
		if (mIndex < 0)
			throw new RuntimeException(TAG+": Invalid Index: " + mIndex );
	}

	@Override
	public String toString(){
		StringBuffer b = new StringBuffer();
		b.append("[");
		b.append(" INDEX: ");
		b.append(mIndex);
		if (mAttributes != null) {
			b.append(" ATTRIBUTES: ");
			b.append(mAttributes.toString());
		}
		b.append(" TYPE: ");
		b.append(mArgTag);
		if (mArgPos != ENTITY_POS.NOT_ENTITY){
			b.append(" POSITION: ");
			b.append(mArgPos);
		}
		b.append("]");
		return b.toString();
	}
}
