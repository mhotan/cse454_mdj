 package features.components;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
/**
 * Implements The Labeled Directional Graph ADT. This class is mutable however there are limitations.
 * Ever Method that returns an Object either returns an immutable group of Objects or and Mutable Copy. <p> 
 * 
 * This graph is a collection of Nodes and edges.  Nodes represent stationary objects
 * within the graph.  Edges are directional, which is represented by a path that connects two distinct Nodes, where one Node is the "source" while the other 
 * is the "Destination".  Edges begin at sources and end at Destination.  A path is represented by a sequence of edges and intermediate Nodes the connects a source Node and A 
 * Destination Node.  <p>  
 * 
 * One Node Can be a source of multiple Edges from the same Nodes and Other Distinct Nodes
 * <br>One Node Can be a destination of multiple Edges from the same Nodes and Other Distinct Nodes
 * <br>A Node can have no edges, one edge, or multiple edges
 * <br>Every Edge has exactly one source and exactly one destination 
 * <br>An Edge can connect the same Node to itself
 *
 *<br>Commenting Representation
 *<br>A : B , A exists in B
 *<br>A !: B , A not exists in B
 *<br> Edge E = [A,B] , E is an edge that has A as Source and B as Destination
 * 
 *@author Michael Hotan
 *
 *@param <NIT>  Node Identity Type, data type holds the identity of the Node.  It is used to to establish a distinct Node that can be compared to other Nodes.
 *@param <EDT> Edge Type that Must be used through out the life duration of the instance of the graph 
 *
 */
public class Graph<NIT, EDT extends Comparable<EDT>> {

	/**
	 * Structure to control and Maintain all the Nodes within the graph
	 */
	private HashMap<NIT, Node<NIT,EDT>> nodesMap;

	/*
	 * Abstract Definition
	 * Let Graph g, 
	 * 	be a collection of Nodes [n1,n2,...,nk]
	 *  Nodes are connected be edges e, st e' = <n1,n2> is a an edge with n1 as its source and n2 as its destination 
	 * 
	 * *Abstraction Function: 
	 * 
	 * For an Empty graph 
	 * 		* g[] => nodesMap.size() = 0 || nodesMap is Empty
	 * For a non Empty graph
	 * 		* for all 1 <= i <= k where k is the amount Nodes in Graph
	 * 			 Node Ni where Ni is in graph => nodesMap.containsKey(ni.identity) && nodesMap.containsValue(ni) 
	 * For any Edge List E if where 1 <= i <= d, where d is number of edges Ei = [n1,n2] where n1,n2 are Nodes
	 * 		*  E = n1.get(n2)
	 * 		*  List of edges E [E1...Ei...Ed], Ei = E.get(i)
	 * 		// In other words: Node n1 obtains the edge List that connects it to Node n2, Edges are then maintianed in order of i
	 * 
	 * *Representation Invariant:
	 * 
	 * this.getSize() >= 0
	 * nodesMap != null 
	 * !nodesMap.containkey(null)
	 * (nodesmap.get(k) != null -> k != null or nodesMap.get(null) == null)
	 * Every Node within nodesMap has Unique identity by NIT(Enforced by HashMap ADT)
	 *  
	 *  In other Words:
	 *  	Graph can't Have negative size
	 *  	nodesMap != null
	 *  	Can't contain null identity
	 *  	If node value is not null then node key is not null 
	 *  	Every Node identity within Nodes map is unique (Enforced using HashMap map) 
	 * 
	 * */

	/*-------------------Creator-------------------*/

	/**
	 * Constructor for Graph.  Creates an empty Graph with No Nodes and No Edges
	 */
	public Graph(){
		nodesMap = new HashMap<NIT, Node<NIT,EDT>>();
		assert checkRep();
	}

	/*-------------------Observers-----------------*/

	/**
	 * Returns the number of Nodes in the Graph
	 * 
	 * @return Amount of Nodes within the graph
	 */
	public int getNumOfNodes(){
		return nodesMap.size();
	}

	/**
	 * Returns whether there are no Nodes in the graph.
	 * 
	 * @return boolean value == the graph has no Nodes
	 */
	public boolean isEmpty(){
		return nodesMap.isEmpty();
	}

	/**
	 * Returns whether there exist an edge that connect source and dest exist edge E st E : [source,dest] or E : [dest, source]
	 *  
	 * @requires source != null && dest != null
	 * @param source Node identity to be checked for an edge to dest
	 * @param dest Node identity to be checked for an edge to source
	 * @return boolean value whether there exist a path between source and dest
	 * @throws IllegalArgumentException If either source or dest or edge is null
	 */
	public boolean containsEdge(NIT src, NIT dest, EDT edge) throws IllegalArgumentException{
		if(src == null || dest == null)
			throw new IllegalArgumentException("Null Identities");
		Node<NIT,EDT> srcNode = getPrivateNode(src); //GetNode Calls IllegalArgumentException
		Node<NIT,EDT> destNode = getPrivateNode(dest);
		return srcNode.hasDistinctEdge(destNode, edge);
	}

	/**
	 * Returns whether there exist a Node n of type nType where the name of nType equals the identity of the prospective element in the graph
	 * 
	 * @requires n != null
	 * @param nType The identity contained in Node to be found
	 * @return whether graph contains a node with the same name as n
	 */
	public boolean containsNode(NIT nType){
		return nodesMap.get(nType) != null;
	}

	/**
	 * Returns boolean value determined whether there exist a path(collection of edges) from Node src to Node dest. 
	 * <br>That is src is a neighbor of dest.
	 * 
	 * @requires src != null && dest != null && src : this && dest : this
	 * @param src Node to begin from
	 * @param dest Node to be found with a sequence of edges
	 * @return whether there exists a path from src to dest
	 * @throws IllegalArgumentException If either source or dest Node is null
	 */
	public boolean hasPath(NIT src, NIT dest){
		return getShortestPathLength(src, dest) >= 1;  //throws Exception if preconditions not met
	}

	/**
	 * Returns whether there exist an edge that connect source and dest.  
	 *  <br>Exist edge E st E : <source,dest> or E : [dest, source]. 
	 *  <br>False Could be returned if Node src or dest do not exist in this
	 * 
	 * @requires source != null && dest != null
	 * @param source Node identity to be checked for an edge to dest
	 * @param dest Node identity to be checked for an edge to source
	 * @return boolean value whether there exist a path between source and dest
	 * @throws IllegalArgumentException If either source or dest is null 
	 */
	public boolean areNeighbors(NIT src, NIT dest) throws IllegalArgumentException{
		if(src == null || dest == null)
			throw new IllegalArgumentException("Null Identities");
		if(!nodesMap.containsKey(src))
			throw new IllegalArgumentException("Node not exist");
		Node<NIT,EDT> srcNode = getPrivateNode(src); //GetNode Calls IllegalArgumentException
		Node<NIT,EDT> destNode = getPrivateNode(dest);
		return srcNode.hasNeighbor(destNode);
	}

	/*-----------Getters-------------------*/

	/**
	 * Returns a List of all the Nodes in this. 
	 *  <br>Any changes to the Nodes in the List will effect the state of this. 
	 *  <br>Does not make copies of Nodes. Changes will reflect in Graph state
	 * 
	 * @effects Allows Access to all Nods of this
	 * @return List of all the Node references in this
	 */
	public List<NIT> getNodes(){
		Collection<Node<NIT,EDT>> nodes = nodesMap.values();
		List<NIT> ans = new ArrayList<NIT>();
		for (Node<NIT,EDT> node : nodes){
			ans.add(node.getNodeIdentity());
		}
		return Collections.unmodifiableList(ans);	
	}


	/**
	 * Returns the length of the smallest path of that Connects src and dest. 
	 *  <br>If src == dest returns 0. 
	 *  <br>If no path exist from src to dest then returns -1. 
	 * 
	 * @requires src != null && dest != null && src : this && dest : this
	 * @param src Start Point of Path
	 * @param dest End Point of Path
	 * @return length of Path connecting src and dest, returns 0 if src == dest, returns -1 if no path exists
	 * @throws IllegalArgumentException If either source or dest Node is null
	 */
	public int getShortestPathLength(NIT src, NIT dest) throws IllegalArgumentException{
		List<NIT> list = getPathWithLeastEdges(src, dest); //throws Exception if preconditions not met
		return list.size()-1;
	}

	/**
	 * Returns a List of all the Nodes beginning at the src Node at index 0 and ends at dest if such a list exists. 
	 *  <br>Nodes at index(i) has atleast one edge to index(i+1). 
	 *  <br>Combination of Node and Edge relationship represent a path from one node to the other.  
	 *  <br>Returns single element List if src == dest. 
	 *  <br>if no path exists return Empty List. 
	 *  
	 * 
	 *  <p>Any Alteration to Nodes on this list will translate to the original states of the Nodes. 
	 * 
	 * @requires src != null && dest != null && src : this && dest : this
	 * @param src Source Node of path 
	 * @param dest Destination Node of path
	 * @return list of Nodes that represent the path from src to dest, or empty list if src == dest or no path exist,
	 * @throws IllegalArgumentException If either source or dest Node is null
	 */
	public List<NIT> getPathWithLeastEdges(NIT src, NIT dest) throws IllegalArgumentException{
		if (src == null || dest == null)
			throw new IllegalArgumentException("Null Identities");
		if (!nodesMap.containsKey(src) || !nodesMap.containsKey(dest))
			throw new IllegalArgumentException("Nodes don't Exist");

		Node<NIT,EDT> srcNode = getPrivateNode(src); //Could throw IllegalArgument Exception
		Node<NIT,EDT> target = getPrivateNode(dest);

		//If src == dest 
		if (srcNode == target){
			List<NIT> singleElemPath = new ArrayList<NIT>();
			singleElemPath.add(src);
			return singleElemPath;
		}
		//Map holds the Nodes already checked
		HashMap<Node<NIT,EDT>, List<Node<NIT,EDT>>> map = new HashMap<Node<NIT,EDT>, List<Node<NIT,EDT>>>();
		//queue holds the Nodes to be visited
		LinkedList<Node<NIT,EDT>> queue = new LinkedList<Node<NIT,EDT>>();

		List<Node<NIT, EDT>> immNeighbors;

		//Create List of Nodes to hold current path between original node and current subneighbor
		List<Node<NIT,EDT>> currentPath = new LinkedList<Node<NIT,EDT>>();

		queue.add(srcNode); // add initial to Q
		map.put(srcNode, new LinkedList<Node<NIT,EDT>>()); // Add Empty Path to List
		while(!queue.isEmpty()){
			Node<NIT,EDT> node = queue.removeFirst();
			if(node.equals(target)){
				currentPath = map.get(node);
				break;
			}
			immNeighbors = getNeighbors(node);
			for (int i = 0; i < immNeighbors.size(); i++){
				Node<NIT,EDT> edgedNode = immNeighbors.get(i); //m represent node which exist edge E<node,m>
				//If Node m has not been visited yet
				if (!map.containsKey(edgedNode)){
					queue.add(edgedNode);
					List<Node<NIT,EDT>> tempPath = new LinkedList<Node<NIT,EDT>>(map.get(node));
					tempPath.add(edgedNode);
					map.put(edgedNode, tempPath);
				}
			}
		}
		//If No path exist return empty List
		if (currentPath.size() == 0 )
			return new ArrayList<NIT>();

		currentPath.add(0 , srcNode);
		List<NIT> correctPath = new ArrayList<NIT>(currentPath.size());
		//If No path was found and src is the only element
		//Convert Node to List of Node identity s
		Node<NIT, EDT> nodeTemp;
		for (int i = 0; i < currentPath.size() ; i++){
			nodeTemp = currentPath.get(i);
			correctPath.add(i, nodeTemp.getNodeIdentity());
		}
		return Collections.unmodifiableList(correctPath);
	}

	/**
	 * Returns a list of neighbor Nodes connected to src
	 * <br> That is returns all Nodes N' st all Edge E that exist st [src , N'] 
	 * <br> Duplicate neighbors are not allowed therefore list is unique
	 * 
	 * @requires src != null && node with identity src is in this
	 * @param src identity of Node to be found 
	 * @return Unmodifiable List of Nodes neighbors of src,
	 * @throws IllegalArgumentException when src is null, or Node with identity src is not in this
	 */
	public List<NIT> getNeighorsOf(NIT src) throws IllegalArgumentException{
		Node<NIT,EDT> srcNode = getPrivateNode(src);
		if (!containsNode(srcNode))
			throw new IllegalArgumentException("Graph Does not contain Node");	
		return Collections.unmodifiableList(srcNode.getNeighbors());	
	}

	/**
	 * Returns the Edge Data Type of the Edge that originates at src and ends at dest. 
	 *  <br>Throws Exception if no such direct edge exists or src or edge not exist in graph
	 * 
	 * @requires src != null && dest != null && src and dest are neighbors
	 * @param src source Node of the edge to be found
	 * @param dest destination Node of the edge to be found
	 * @return Unmodifiable List of edges that connects from src to dest, 
	 * @throws IllegalArgumentException if src or dest == null or src and dst are not neighbors 
	 */
	public List<EDT> getEdges(NIT src, NIT dest) throws IllegalArgumentException{
		if (src == null || dest == null)
			throw new IllegalArgumentException("Null identities");
		Node<NIT,EDT> srcNode = getPrivateNode(src); // throws Exception
		Node<NIT,EDT> destNode = getPrivateNode(dest);
		if (!srcNode.hasNeighbor(destNode) || !containsNode(srcNode))
			throw new IllegalArgumentException("Not Neighbors");
		return Collections.unmodifiableList(srcNode.getAllEdgesTo(destNode));
	}

	/**
	 * If to Nodes have multiple edge then this method returns the first edge by order of insertion. 
	 *  <br>Returns null if no such direct edge exists or src or edge not exist in graph
	 *  <br>Does not make copies of Edge. Changes will reflect in Graph state
	 * 
	 * @requires src != null && dest != null && src and dest are neighbors
	 * @param src source Node of the edge to be found
	 * @param dest destination Node of the edge to be found
	 * @return first edge that connects from src to dest, 
	 */
	public EDT getFirstEdge(NIT src, NIT dest) throws IllegalArgumentException{
		return getEdges(src, dest).get(0); // throws Exception
	}

	/**
	 * Returns A string representation of this.  Includes every Node in this graph
	 * 
	 * @return String representation of this 
	 */
	public String toString(){
		String ans = "{";
		List<NIT> nodes = getNodes();
		for (int i = 0; i < nodes.size(); i++){
			ans += nodes.get(i).toString() + "\n";
		}
		ans += "}";
		return ans;
	}


	/*-------------------Mutators--------------------*/

	/**
	 * Attempts to Add a Node of type identity to this. 
	 *  <br>Node will be created with identity.  If Node with identity already exists in graph then new Node will not be added. 
	 *  <br>If Node with "identity" does not exist in the graph then a new node will be added. 
	 *  <br>If Node does exist the state of the graph will not be changed. 
	 * 
	 * @requires identity != null
	 * @modifies this 
	 * @effects Node with type identity will exist in a Node in this
	 * @param identity New Node's key feature that identifies its distinction among other Nodes
	 * @return Whether Node successfully added
	 * @throws IllegalArgumentException if null identity is passed in
	 */
	public boolean addNode(NIT identity) throws IllegalArgumentException{
		if (identity == null)
			throw new IllegalArgumentException("Null identity");
		//If identity of Node is not contained in graph or the node mapped to by identity is null
		//Creates a new Node in place of the null one
		if (!nodesMap.containsKey(identity) || nodesMap.get(identity) == null){
			Node<NIT, EDT> newNode = new Node<NIT, EDT>(identity);
			nodesMap.put(identity, newNode);
			assert checkRep();
			return true;
		}
		return false;
	}

	/**
	 * Attempts to Create a group of Nodes of type identities and add to graph
	 * <br>For each identity object that is not null in the collection of "identitys" a Node will attempted to be created and added to this
	 * <br>If any of the identity already exist in this a new copy will not be added 
	 * <br>If Nodes do exist with any of types identitys the state of the graph will not be changed
	 * <br>No null elements of Collection will be added to this
	 * <br>If all identities are added returns true,
	 *
	 * @requires identitys != null 
	 * @modifies this 
	 * @effects Node's with corresponding identities will exists in this
	 * @param identitys collection of identity to be added as Nodes to this
	 * @return Whether all Nodes was successfully added
	 * @throws IllegalArgumentException if identitys == null
	 */
	public boolean addNodesbyIdts(Collection<NIT> identitys){
		boolean allAdded = true;
		Iterator<NIT> it = identitys.iterator();
		//Iterate through all identities only adding non null
		while(it.hasNext()){
			NIT temp = it.next();
			if (temp != null)
				allAdded &= addNode(temp);
			else
				allAdded = false;
		}
		return allAdded;
	}

	/**
	 * Replace identity of Old Nodes labeled with oldData with newData
	 * <br>If oldData Does not exist then new Node is created and add a new Node to this
	 * <br>If oldIdt is null a new node will be added of identity newIdt
	 * <br>If newIdt already exist creation an IllegalArgumentException will be thrown
	 * 
	 * @requires newIdt != null new Idt not currently in this
	 * @modifies this, Node with identity oldIdt
	 * @effects newIdt Node has take the place of oldIdt Node
	 * @param oldData identity to be updated
	 * @param newData new updated identity
	 * @return true if Node was successfully updated, else return false then New Node is created with newIdt
	 * @throws IllegalArgumentException if newIDt == null or newIdt Already Exist in graph
	 */
	public void replaceNodeNewIdt(NIT oldIdt, NIT newIdt) throws IllegalArgumentException{
		if (newIdt == null )
			throw new IllegalArgumentException("new identity null");
		if (containsNode(newIdt) )
			throw new IllegalArgumentException("new identity already exist must delete newIdt first");

		Node<NIT,EDT> oldNode = getPrivateNode(oldIdt);

		//Map does not contain oldData or oldIdt == null
		//Add Node with new Identity and return
		if (oldNode == null){
			addNode(newIdt);
			return;
		}
		Node<NIT,EDT> newNode = oldNode.dupNodeNewIdt(newIdt);
		addNode(newNode);

		//For Every Node in graph that has edge directed toward old Data must add new Data
		List<NIT> nodes = getNodes();
		List<EDT> edges;
		Node<NIT,EDT> temp;

		//For all nodes in graph go through and delete traces of Old Node While Adding new Node
		for (int i = 0 ; i < nodes.size(); i++){
			temp = getPrivateNode(nodes.get(i));
			if (temp.hasNeighbor(oldNode)){
				edges = temp.getAllEdgesTo(oldNode);
				temp.deleteNeighbor(oldNode);
				for(int j=0 ; j < edges.size(); j++){
					temp.addEdgeToNode(newNode, edges.get(j));
				}
			}
		}

		//Finally Remove oldData after newData abstractly taken its place
		deleteNode(oldIdt);
		assert checkRep();
	}


	/**
	 * Attempts to add edge from Node src to Node dest
	 * <br>If No edge exist from src to dest new edge "edge" will be added.
	 * <br>No edge that already exist will not be effected.
	 * <br>Once edge has been added it cannot be changed,  Replacal of an Edge is the only way
	 * 
	 * @requires src != null && dest != null && edge != null && src : this && dest : this 
	 * @modifies this 
	 * @effects new edge containing "edge" will be added connection src to dest
	 * @param src Source of the edge to be added
	 * @param dest Destination to be added
	 * @param edge Edge identity to be added between src and dest
	 * @throws IllegalArgumentException if src, dest, or edge are not sufficient to create edge between two Nodes, src or dest or edeg == null
	 */
	public void addEdge(NIT src, NIT dest, EDT edge) throws IllegalArgumentException{
		final EDT immEdge = edge; //Immutable Edge
		Node<NIT,EDT> srcNode = getPrivateNode(src); //Throws Illegal Argument if null
		Node<NIT,EDT> destNode = getPrivateNode(dest);
		if(srcNode == null || destNode == null)
			throw new IllegalArgumentException("Insufficient Input Null Nodes");
		srcNode.addEdgeToNode(destNode, immEdge);
		assert checkRep();
	}

	/**
	 * Attempts to add a group of edges from Node src to Node dest
	 * <br>If No edge exist from src to dest new edge "edge" will be added.
	 * <br>No edge that already exist will not be effected.
	 * <br>null edges in the Collection will be not be added
	 * 
	 * @requires src != null && dest != null && edges != null && src : this && dest : this 
	 * @modifies this 
	 * @effects new edges contained in "edges" will be added connection src to dest
	 * @param src Source of the edge to be added
	 * @param dest Destination to be added
	 * @param edge Edges identity to be added between src and dest
	 * @throws IllegalArgumentException if  src or dest or edges == null
	 */
	public void addEdges(NIT src, NIT dest, Collection<EDT> edges) throws IllegalArgumentException{
		if (edges == null){
			throw new IllegalArgumentException("Null Collection of Edges");
		}
		Iterator<EDT> edgeIt = edges.iterator();
		EDT temp;
		while (edgeIt.hasNext()){
			temp = edgeIt.next();
			if (temp != null)
				addEdge(src, dest, temp);
		}
	}

	/**
	 * Attempts to add an edge from Node src toa group of Nodes with identies dests
	 * <br>If No edge exist from src to dest new edge "edge" will be added.
	 * <br>Edges to repeated neighbors is dest will not be added
	 * <br>null destinations in the Collection will be not be added to
	 * 
	 * @requires src != null && dests != null && edges != null && src : this && all elements in dests are in Graph
	 * @modifies this 
	 * @effects new edge "edge" will be added from src to ever Node with identies of dests
	 * @param src Source of the edge to be added
	 * @param dests Destinations to be added to
	 * @param edge Edge identity to be added between src and the destination
	 * @throws IllegalArgumentException if src, dests, or edge are not sufficient to create edge between two Nodes, src or dest or edeg == null
	 */
	public void addEdges(NIT src, Collection<NIT> dests, EDT edge) throws IllegalArgumentException{
		Iterator<NIT> destIt = dests.iterator();
		NIT temp;
		while (destIt.hasNext()){
			temp = destIt.next();
			if (temp != null)
				addEdge(src, temp, edge); //Throws Illegal Argument if null
		} 
	}

	/**
	 * Deletes all the edge connecting src and dest
	 * <br>If no edges exist between src and dest there will be no effect
	 *
	 * @requires src != null && dest != null && src : this && dest : this
	 * @modifies this 
	 * @effects Any edge that connect src to dst will be deleted
	 * @param src Source of the Edge to be deleted
	 * @param dest Destination of the Edge to be deleted
	 * @throws IllegalArgumentException  src or dest or edeg == null
	 */
	public void deleteAllEdges(NIT src, NIT dest) throws IllegalArgumentException{
		Node<NIT,EDT> srcNode = getPrivateNode(src); //Throws Illegal Argument if null
		Node<NIT,EDT> destNode = getPrivateNode(dest);
		if(srcNode == null || destNode == null)
			throw new IllegalArgumentException("No node by src and dest exist in Grap");
		srcNode.deleteNeighbor(destNode);
		assert checkRep();
	}

	/**
	 * Deletes the the distinct edge that has a Source at src and Destination at dest and returns true
	 * <br>If no such edge exists then there is no effect and returns false
	 *
	 * @requires src != null && dest != null && edge != null && src : this && dest : src && edge = <src,dest>
	 * @modifies this 
	 * @effects Distinct Edge "edge" is deleted
	 * @param src Source of the Edge to be deleted
	 * @param edge Edge to be deleted
	 * @return Whether edge existed before method call, 
	 * @throws IllegalArgumentException  src or dest or edeg == null
	 */
	public boolean deleteEdge(NIT src, NIT dest, EDT edge) throws IllegalArgumentException{
		Node<NIT,EDT> srcNode = getPrivateNode(src); //Throws Illegal Argument if null
		Node<NIT,EDT> destNode = getPrivateNode(dest);
		if(srcNode == null || destNode == null)	
			throw new IllegalArgumentException("Source node or Edge is Null");
		assert checkRep();
		return srcNode.deleteEdgeTo(destNode, edge); //throws IllegalArgument Exception is edge is null
	}

	/**
	 * Deletes a Node from this
	 * <br>All Edges that use nodeIdt as its Destination or Source would be deleted
	 * <br>If Node nodeIdt does not exists there is no over effect on this
	 * 
	 * @requires nodeIdt != null && nodeIdt: this 
	 * @modifies this 
	 * @effects Node nodeIdt !: this, Node with identity nodeIdt does not exist in this
	 * @param nodeIdt Node to be deleted
	 * @return boolean value whether node was successfully found and deleted,  If node did not exist the return false
	 * @throws IllegalArgumentException nodeIdt == null
	 */
	public boolean deleteNode(NIT nodeIdt)  throws IllegalArgumentException{
		Node<NIT,EDT> toDel = getPrivateNode(nodeIdt);//Throws Illegal Argument if null
		if (toDel == null) //Node does not exist
			return false;
		Set<NIT> nodeIdtKeys = new HashSet<NIT>(nodesMap.keySet());
		Iterator<NIT> it = nodeIdtKeys.iterator();
		Node<NIT,EDT> temp;
		//traverse every nodeIdt in graph an erase all signature of nodeIdt
		while(it.hasNext()){
			temp = nodesMap.get(it.next());
			temp.deleteNeighbor(toDel);
		}
		//Finally remove nodeIdt
		nodesMap.remove(nodeIdt);
		assert checkRep();
		return true;
	}

	/**
	 * Clears the entire graph of all Nodes and Edges
	 * 
	 * @modifies this 
	 * @effects this contains no Nodes or Edges
	 * @param n Node to be deleted
	 */
	public void clear(){
		nodesMap.clear();
		assert checkRep();
	}

	/*Private Helper Methods*/

	/**
	 * returns whether there exist a Node n.
	 * <br>Where the name of n equals the identity of the prospective element in the graph
	 * 
	 * @requires n != null
	 * @param n Node to be checked within the 
	 * @return whether graph contains a node with the same name as n
	 */
	private boolean containsNode(Node<NIT,EDT> n){
		return nodesMap.containsKey(n.getNodeIdentity());
	}

	/**
	 * Attempts to Add a Node to this. 
	 *  <br>If a node does has a identity that is unique and is not contained in the list already it will be added and true will be returned. 
	 *  <br>If node with the same identity already exists in graph then new Node will not be added.  
	 *  <br>If Node does exist the state of the graph will not be changed. 
	 * 
	 * @requires node != null
	 * @modifies this 
	 * @effects Node "node" will exist in a Node in this, "node' identity will be unique amongst others
	 * @param node New Node to be added
	 * @return Whether Node was successfully added
	 * @throws IllegalArgumentException if null node is passed in
	 */
	private void addNode(Node<NIT,EDT> node) throws IllegalArgumentException{
		assert node != null: "Node added == null"; 
		//If identity of Node is not contained in graph or the node mapped to by identity is null
		//Creates a new Node in place of the null one
		assert !nodesMap.containsKey(node.getNodeIdentity()) : "New node identity already contained in Map";
		nodesMap.put(node.getNodeIdentity(), node);
		assert checkRep();
		//add only if identity is not existent in map, demands use of removal and replace node
	}

	/**
	 * Returns the set of all neighbors of n
	 * <br>Any alterations with the set of Nodes returned translates to the state of n
	 * <br>If n has no neighbors the empty set is returned
	 * 
	 * @requires n != null 
	 * @param n Root Node in which neighbors are found
	 * @return a set of Nodes in which every Node is the neighbor of n
	 */
	private List<Node<NIT, EDT>> getNeighbors(Node<NIT,EDT> n){
		List<NIT> nbrIdts = n.getNeighbors();
		List<Node<NIT, EDT>> nbrNodes = new LinkedList<Node<NIT,EDT>>();
		for (NIT idt : nbrIdts){
			nbrNodes.add(getPrivateNode(idt));
		}
		return nbrNodes;
	}

	/**
	 * Returns Actual Node with identity that equals "identity" 
	 * 
	 * @requires identity != null && node with identity exists in this
	 * @param identity identity of Node to be retrieved
	 * @return Node that contains identity, returns null if Node is Not found
	 * @throws IllegalArgumentException if identity is null or Node with identity does not exist in this
	 */
	private Node<NIT,EDT> getPrivateNode(NIT identity){
		assert identity != null: "Node Identity == null"; 
		assert nodesMap.containsKey(identity): "Node Identity not exist in map";
		return nodesMap.get(identity);
	}

	/**
	 * Checks representaion invariant with assertions of this
	 */
	protected boolean checkRep(){
		assert nodesMap != null;
		assert nodesMap.size() >= 0;
		assert !nodesMap.containsKey(null);
		assert nodesMap.get(null) == null;  //If null key returns object then fail
		return true;
	}

	/**
	 * Class that implements the general purpose of a graph Node. This class has mutable and imutable characteristics.  Once A Nodes identity
	 *  has been set it cannot be changed.  However classes within the same package can created instance of this class and maniuplate its characteristics to to contain data 
	 *  about neighboring Nodes.
	 *  
	 * <br>Node contains the knowledge about every edge outgoing from it.  
	 * <br>Node also has knowledge of all of its surrounding neighbors
	 * <br>For every Node, where it is the source of a group of edges, that Node is aware of but not accountable for the destinations of that group of edges
	 * <br>Nodes maintain a set of unique edges per neighbor
	 * <br>Nodes are allowed to be a source and destination for a distinct edge, that is A Node is allowed to be its own neighbor
	 * <br>Nodes are never neighbored to another Unique Node Object with two or more different groups of Edges
	 * <br>Nodes are unique depending on there Node Identity Type NIT
	 * 
	 * <p>Once A Nodes unique Identity is set it can never be changed. Its identity is Immutable, however its groups of neighbors are not
	 * 
	 * <p>IE:
	 * <br>if NIT is of type Point, Then for any to points p = new Point(0,0) and p' = new Point(0,0)
	 * <br>then p equals p' but p != p.setLocation(1,0);
	 * 
	 * @author Michael Hotan
	 *
	 * @param <NIT> Node Identity Type, data type holds the identity of the Node.  It is used to to establish a distinct Node that can be compared to other Nodes.
	 * @param <EDT> Edge Data Type, holds the state of any edge that connects a Node to another one.
	 */
	public class Node<NIT, EDT extends Comparable<EDT>> {

		/** Holds identity to ensure uniqueness of this Node */
		private final NIT identity;

		/** Holds all the neighbors and edges of this  */
		private AbstractMap<Node<NIT, EDT>, LinkedHashSet<EDT>> neighbors;
		//Incorporating a LinkeHashSet as structure to hold edges allows implicit uniqueness 
		//and general sorting by insertion order

		/*
		 * *For Simple Written Representation
		 * An labeled directional edge E is labeled from its source and to its destination Node by: E : <source, destination> 
		 * Relative Node : N
		 * Neighbors to Relative Node: N'c ,  where c is arbitrary integer and used for distinction of neighbors
		 * 
		 * *Abstraction Function: 
		 * 
		 * For Any Node N:
		 * N.getNodeIdentity() = n.identity
		 * 
		 * If N has no Neighbors:
		 * NOT exist Any N' or E' st E': <N, N'>  = neighbors.isEmpty()
		 * 
		 * If N'1, N'2, ..., N'k are neighbors of N st Exist E : <N , N'1> E2 : <N , N'2>  ... Ek : <N, N'k>
		 * for all 0 < i <= k  
		 *   neighbors.containsKey(N'i) = true
		 *   neighbors.getValue(N'i) = List of edges Ei <N, N'i> , List size >= 1
		 *   
		 * If there exist Node N and Node N' and multiple Edges E1, E2, ..., Ek 
		 * st for all 1 <= i <= k Ei = <N , N'>
		 * Exists LinkedList L = L.containsAll(Ei)
		 * L = neighbors.getValue(N');
		 * 
		 * In Other words:
		 * If Arbitrary Node N 
		 * has no neighbors then neighbors HashMap is empty
		 * has neighbors with atleast 1 edge to each then all the neighbors exist as keys and a List of Edges(not Empty) as values in the Hashmap   
		 * exists a neighbor with multiple edges then exists a list as a value in the HashMap containing all those edges.
		 * 
		 * *Representation Invariant: 
		 * 
		 * identity != null
		 * neighbors != null 
		 * neighbors.size() >= 0 
		 * for all non null Nodes in neighbors their List<EDT>() != null 
		 * List<EDT>.size() >= 1
		 * Edges Are Unique and distinct per neighbor relationship (Enforce by LinkedHashedSet)
		 * 
		 * In Other Words:
		 * * A Node is not allowed to have null identities
		 * * A Node is allowed to have no neighbors 
		 * * If there exist a key in neighbors there must be a non null LabeledEdge List for its value
		 * 
		 * It is Client responsibility to ensure that null node objects to do not exist in neighborsMap,
		 * 
		 */
		/*------------------Creators-------------------*/

		/**
		 * Constructor that creates Node with a distinct identity of type NodeDataT
		 * 
		 * @requires identity != null
		 * @modifies this
		 * @effects updates identity to Idty, creates structure to hold neighbors
		 * @param Idty identity type to be stored within Node
		 * @throws IllegalArgumentException if Idty == null
		 */
		public Node(NIT Idty){
			if (Idty == null)
				throw new IllegalArgumentException("Cant have null Identity");
			identity = Idty;
			neighbors = new HashMap< Node<NIT, EDT>, LinkedHashSet<EDT>>();
			assert checkRep();
		}

		/*------------------Observers / Getters------------------*/


		/**
		 * Returns the identity stored in this
		 * 
		 * @return the identity stored in this, of type NodeDataT
		 */
		public NIT getNodeIdentity(){
			return identity;
		}

		/**
		 * Returns the Nth Edge that connects this and dest
		 * <br>nth is defined by N edges where the earliest added edge is the 1st while the latest added edge is the Nth 
		 * <br>Zero based index where first element is at 0
		 * <br>If index surpasses amount of elements available returns the first edge
		 * <br> Copy is not made so posterior changes will translate
		 * 
		 * @requires dest != null && dest is neighbor of this
		 * @param dest the neighbors of this to be returned
		 * @return the Nth Edge that connects this and dest, if index surpasses the  
		 * @throws IllegalArgumentException when dest is not a neighbor of this
		 */
		public EDT getNthEdge(Node<NIT, EDT> dest, int index) throws IllegalArgumentException{
			List<EDT> edgeList = getAllEdgesTo(dest); //Calls Exception if preconditions aren't met
			if (edgeList.size() <= index)
				return getFirstEdge(dest);
			//assert checkRep();
			return edgeList.get(index);
		}

		/**
		 * Returns the First Edge that connects this and dest
		 * <br> first edge is generally defined as order of insertion but is not enforce and cannot be guaranteed
		 * <br> Copy is not made so posterior changes will translate
		 * 
		 * @requires dest != null && dest is neighbor of this
		 * @param dest the neighbors of this to be returned
		 * @return the Edge that connects this and dest 
		 * @throws IllegalArgumentException when dest is not a neighbor of this
		 */
		public EDT getFirstEdge(Node<NIT, EDT> dest) throws IllegalArgumentException{
			List<EDT> edgeList = getAllEdgesTo(dest); //Calls Exception if preconditions aren't met
			return edgeList.get(0);
		}

		/**
		 * Returns All the edges that connects this and dest
		 * <br> Copy is not made so posterior changes will translate
		 * 
		 * @requires dest != null && dest is neighbor of this
		 * @param dest the neighbors of this to be returned
		 * @return List of all Edges that connects this and dest,
		 * @throws IllegalArgumentException when dest is not a neighbor of this or dest == null
		 */
		public List<EDT> getAllEdgesTo(Node<NIT, EDT> dest) throws IllegalArgumentException{
			if (!neighbors.containsKey(dest) || dest == null)
				throw new IllegalArgumentException("Dest is not a neighbor");
			List<EDT> edges = new ArrayList<EDT>(neighbors.get(dest));
			return edges;
		}

		/**
		 * Returns the amount of edges that originate at this and end at dest
		 * 
		 * @param dest Node that edges point to;
		 * @return number of edges with this as source and dest as destination, returns 0 if dest is not a neighbor
		 */
		public int getNumOfEdges(Node<NIT, EDT> dest){
			if (!neighbors.containsKey(dest))
				return 0;
			return neighbors.get(dest).size();
		}

		/**
		 * Returns a List of Nodes that are Neighbors to this
		 * <br>Removal or addition from this List will not remove Neighbors from this
		 * <br> Copy is not made so posterior changes will translate
		 * 
		 * @modifies Potentially allow alteration of distinct neighbor nodes
		 * @return a Set of Nodes which are neighbors of this
		 */
		public List<NIT> getNeighbors(){
			HashSet<Node<NIT, EDT>> nSet = new HashSet<Node<NIT, EDT>>(neighbors.keySet());
			List <Node<NIT, EDT>> nlist = new ArrayList<Node<NIT, EDT>>(nSet);
			List <NIT> identityList = new ArrayList<NIT>();
			for (Node<NIT, EDT> nbr: nlist){
				identityList.add(nbr.getNodeIdentity());
			}
			return identityList;
		}

		/**
		 * Returns whether there is a distinct edge that originates at this and ends at dest
		 * if dest is not a neighbor false is returned 
		 * 
		 * @param dest Node to to be checked for connection
		 * @param edge distinct edge to be to checked for 
		 * @return whether distinct edge exists from this to dest
		 */
		public boolean hasDistinctEdge(Node<NIT, EDT> dest , EDT edge){
			if (!neighbors.containsKey(dest))
				return false;
			return neighbors.get(dest).contains(edge);
		}

		/**
		 * Returns the amount of neighbors that this has
		 * 
		 * @return Amount of neighbors
		 */
		public int getAmountOfNeighbors(){
			return neighbors.size();
		}

		/**
		 * Returns the boolean value whether Node n is neighbor of this
		 * <br>that is: There Exist Edge that begins at this and ends at n
		 * 
		 * @param n potential Node neighbor to be checked as neighbor
		 * @return Whether node n is neighbor of this
		 */
		public boolean hasNeighbor(Node<NIT, EDT> neighbor){
			return neighbors.get(neighbor) != null;
		}

		/**
		 * Returns the boolean value whether Node n has any neighbors
		 * <br>that is: There Exist any Edge that connects to any node
		 * 
		 * @return Whether node n has neighbors
		 */
		public boolean hasAnyNeighbors(){
			return !neighbors.isEmpty();
		}

		/**
		 * {@inheritDoc}
		 */
		public int hashCode(){
			return this.identity.hashCode()*31;
		}

		/**
		 * Specialized for Node comparison.  Is equals if identity is equals
		 * <br>{@inheritDoc}
		 */
		public boolean equals(Node<NIT, EDT> n){
			return this.identity.equals(n.identity);
		}

		/**
		 * Returns String representation of this and its neighbors
		 */
		public String toString(){
			String ans = "[Node Data: " + this.identity.toString();
			Set<Node<NIT,EDT>> neighborSet = neighbors.keySet();
			if(neighborSet.size() > 0) {	
				Iterator<Node<NIT, EDT>> neighbIt = neighborSet.iterator();
				Node<NIT, EDT> tempNode;
				HashSet<EDT> tempEdges; 
				while (neighbIt.hasNext()){
					tempNode = neighbIt.next();
					ans += " ; Neighbor: " + tempNode.getNodeIdentity().toString() + " ; Edges:";
					tempEdges = neighbors.get(tempNode);
					Iterator<EDT> edgeIt = tempEdges.iterator();
					while (edgeIt.hasNext()){
						ans += " " + edgeIt.next().toString();
					}
				}
			}
			ans += "]";
			return ans;
		}

		/**
		 * Created a new Node with the same Neighbors as this 
		 * <br>If newIdt is null then identity does not change
		 * 
		 * @requires newIdt != null
		 * @param newIdt New identity which the new node will be created with
		 * @return New Node with same properties of this but with new identy
		 * @throws  IllegalArgumentException If null new identity is passed in 
		 */
		public Node<NIT,EDT> dupNodeNewIdt(NIT newIdt){
			if (newIdt == null)
				throw new IllegalArgumentException("");
			Node<NIT,EDT> newNode = new Node<NIT,EDT>(newIdt);
			newNode.neighbors = new HashMap<Node<NIT, EDT>, LinkedHashSet<EDT>>(this.neighbors);
			assert checkRep();
			return newNode;
		}

		/*------------------Mutators--------------------*/

		/**
		 * Adds an Edge "edge" to Node "n".
		 * <br>n becomes a neighbor of this
		 * <br>If already exists a an edge that is equals to parameter "edge" then the new edge will not be added
		 * 
		 * @requires addedNode != null, edge != null
		 * @modifies this
		 * @effects adds a neighbor if n does not already exist, replaces edge if n already exists
		 * @param n Node to be added as neighbor
		 * @param edge to be used to connect this and Node n
		 * @return true if the Node did not already contain that specified edge
		 * @throws IllegalArgumentException if edge == null or addedNode == null 
		 */
		public boolean addEdgeToNode(Node<NIT, EDT> addedNode, EDT edge) throws IllegalArgumentException{
			if (edge == null || addedNode == null){
				throw new IllegalArgumentException("Null Edge or Node to be added");
			}
			//Check if addedNode is already a neighbor
			boolean added;
			LinkedHashSet<EDT> edges;
			// Can't Add Edge to Self, current implementation spec which is subject to change if necessary 
			/*if (this.equals(addedNode)) 
				return false;*/
			if (neighbors.containsKey(addedNode)){
				edges = neighbors.get(addedNode);
				added = edges.add(edge);
			} else { // addedNode becomes new neighbor
				edges = new LinkedHashSet<EDT>();
				added = edges.add(edge);
				neighbors.put(addedNode, edges);
			}
			assert checkRep();
			return added;
		}

		/**
		 * Deletes the distinct edge that connects this and dest
		 * <br>That is delete LabeledEdge E, if before is neighbors : [E<this,dest>] , then after is  neighbors : [] 
		 * <br>If no such edge exist then there is no effect
		 * <br>Returns true if dest was successfully found and deleted
		 * <br>Returns false if "edge" did not exist or dest is not a neighbor of this
		 * 
		 * @requires dest != null dest is neighbor of this
		 * @modifies this
		 * @effects removes edge to neighbor dest
		 * @param dest neighbor in which edge to is to be removed
		 * @return if Edge exist between this and dest then was deleted
		 * @throws IllegalArgumentException if dest == null 
		 */
		public boolean deleteEdgeTo(Node<NIT, EDT> dest, EDT edge) throws IllegalArgumentException{
			//If contained in neighbors remove key and Value
			if (!neighbors.containsKey(dest))
				throw new IllegalArgumentException("Dest is not a neighbor of");
			LinkedHashSet<EDT> eList = neighbors.get(dest);
			boolean deleted = eList.remove(edge);
			//If No more edges exist dest remove as neighbor
			if (eList.size() == 0)
				deleteNeighbor(dest);
			assert checkRep();
			return deleted;
		}

		/**
		 * Deletes neighbor if it exists
		 * <br>If it does not exist then there no effect this
		 * <br>If dest == null there is no effect on this
		 * <br>IE: if neighbors : [E[this,N1] , E[this,N2], ... , E[this,Nk]] , then after delete N1 this's neighbors : [ E[this,N2], ... , E[this,Nk]]
		 * 
		 * @modifies this
		 * @effects Removes all edges from this to dest
		 * @param dest Node to be removed if possible
		 */
		public void deleteNeighbor(Node<NIT, EDT> dest){
			neighbors.remove(dest);
			assert checkRep();
		}

		/**
		 * Deletes all edges outgoing from this Node
		 * <br>That is delete Edge E, if before this's neighbors : [E[this,N1] , E[this,N2], ... , E[this,Nk]] , then after this's neighbors : [] 
		 * 
		 * @modifies this
		 * @effects removes all edges to any neighbor 
		 */
		public void deleteAllNeighbors(){
			neighbors.clear();
			assert checkRep();
		}

		/**
		 * Private Method to check internal Representation Invariants
		 */
		private boolean checkRep(){
			assert identity!= null;
			assert neighbors!=null;
			assert neighbors.size() >= 0;
			assert !neighbors.containsKey(null);
			assert !neighbors.containsValue(null);
			//Linked HashSetEnforce not repeated edges
			return true;
		}

	}
}
