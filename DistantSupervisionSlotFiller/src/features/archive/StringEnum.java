package features.archive;

/**
 * Quick class to make readable enums for debugging
 * @author mhotan
 */
public enum StringEnum {
	// No enum types just intended for inheritence
	;
	private final String mReadableName;
	
	StringEnum(String name){
		mReadableName = name;
	}
	@Override
	public String toString(){
		return mReadableName;
	}
}
