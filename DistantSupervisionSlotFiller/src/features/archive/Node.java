package features.archive;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
/**
 * Class that implements the general purpose of a graph Node. This class has mutable and imutable characteristics.  Once A Nodes identity
 *  has been set it cannot be changed.  However classes within the same package can created instance of this class and maniuplate its characteristics to to contain data 
 *  about neighboring Nodes.
 *  
 * <br>Node contains the knowledge about every edge outgoing from it.  
 * <br>Node also has knowledge of all of its surrounding neighbors
 * <br>For every Node, where it is the source of a group of edges, that Node is aware of but not accountable for the destinations of that group of edges
 * <br>Nodes maintain a set of unique edges per neighbor
 * <br>Nodes are allowed to be a source and destination for a distinct edge, that is A Node is allowed to be its own neighbor
 * <br>Nodes are never neighbored to another Unique Node Object with two or more different groups of Edges
 * <br>Nodes are unique depending on there Node Identity Type NIT
 * 
 * <p>Once A Nodes unique Identity is set it can never be changed. Its identity is Immutable, however its groups of neighbors are not
 * 
 * <p>IE:
 * <br>if NIT is of type Point, Then for any to points p = new Point(0,0) and p' = new Point(0,0)
 * <br>then p equals p' but p != p.setLocation(1,0);
 * 
 * @author Michael Hotan
 *
 * @param <NIT> Node Identity Type, data type holds the identity of the Node.  It is used to to establish a distinct Node that can be compared to other Nodes.
 * @param <EDT> Edge Data Type, holds the state of any edge that connects a Node to another one.
 */
public class Node<NIT, EDT extends Comparable<EDT>> {
	
	/** Holds identity to ensure uniqueness of this Node */
	private final NIT identity;
	
	/** Holds all the neighbors and edges of this  */
	private AbstractMap<Node<NIT, EDT>, LinkedHashSet<EDT>> neighbors;
	//Incorporating a LinkeHashSet as structure to hold edges allows implicit uniqueness 
	//and general sorting by insertion order
	
	/*
	 * *For Simple Written Representation
	 * An labeled directional edge E is labeled from its source and to its destination Node by: E : <source, destination> 
	 * Relative Node : N
	 * Neighbors to Relative Node: N'c ,  where c is arbitrary integer and used for distinction of neighbors
	 * 
	 * *Abstraction Function: 
	 * 
	 * For Any Node N:
	 * N.getNodeIdentity() = n.identity
	 * 
	 * If N has no Neighbors:
	 * NOT exist Any N' or E' st E': <N, N'>  = neighbors.isEmpty()
	 * 
	 * If N'1, N'2, ..., N'k are neighbors of N st Exist E : <N , N'1> E2 : <N , N'2>  ... Ek : <N, N'k>
	 * for all 0 < i <= k  
	 *   neighbors.containsKey(N'i) = true
	 *   neighbors.getValue(N'i) = List of edges Ei <N, N'i> , List size >= 1
	 *   
	 * If there exist Node N and Node N' and multiple Edges E1, E2, ..., Ek 
	 * st for all 1 <= i <= k Ei = <N , N'>
	 * Exists LinkedList L = L.containsAll(Ei)
	 * L = neighbors.getValue(N');
	 * 
	 * In Other words:
	 * If Arbitrary Node N 
	 * has no neighbors then neighbors HashMap is empty
	 * has neighbors with atleast 1 edge to each then all the neighbors exist as keys and a List of Edges(not Empty) as values in the Hashmap   
	 * exists a neighbor with multiple edges then exists a list as a value in the HashMap containing all those edges.
	 * 
	 * *Representation Invariant: 
	 * 
	 * identity != null
	 * neighbors != null 
	 * neighbors.size() >= 0 
	 * for all non null Nodes in neighbors their List<EDT>() != null 
	 * List<EDT>.size() >= 1
	 * Edges Are Unique and distinct per neighbor relationship (Enforce by LinkedHashedSet)
	 * 
	 * In Other Words:
	 * * A Node is not allowed to have null identities
	 * * A Node is allowed to have no neighbors 
	 * * If there exist a key in neighbors there must be a non null LabeledEdge List for its value
	 * 
	 * It is Client responsibility to ensure that null node objects to do not exist in neighborsMap,
	 * 
	 */
	/*------------------Creators-------------------*/
	
	/**
	 * Constructor that creates Node with a distinct identity of type NodeDataT
	 * 
	 * @requires identity != null
	 * @modifies this
	 * @effects updates identity to Idty, creates structure to hold neighbors
	 * @param Idty identity type to be stored within Node
	 * @throws IllegalArgumentException if Idty == null
	 */
	public Node(NIT Idty){
		if (Idty == null)
			throw new IllegalArgumentException("Cant have null Identity");
		identity = Idty;
		neighbors = new HashMap< Node<NIT, EDT>, LinkedHashSet<EDT>>();
		assert checkRep();
	}
	
	/*------------------Observers / Getters------------------*/

	
	/**
	 * Returns the identity stored in this
	 * 
	 * @return the identity stored in this, of type NodeDataT
	 */
	public NIT getNodeIdentity(){
		return identity;
	}
	
	/**
	 * Returns the Nth Edge that connects this and dest
	 * <br>nth is defined by N edges where the earliest added edge is the 1st while the latest added edge is the Nth 
	 * <br>Zero based index where first element is at 0
	 * <br>If index surpasses amount of elements available returns the first edge
	 * <br> Copy is not made so posterior changes will translate
	 * 
	 * @requires dest != null && dest is neighbor of this
	 * @param dest the neighbors of this to be returned
	 * @return the Nth Edge that connects this and dest, if index surpasses the  
	 * @throws IllegalArgumentException when dest is not a neighbor of this
	 */
	public EDT getNthEdge(Node<NIT, EDT> dest, int index) throws IllegalArgumentException{
		List<EDT> edgeList = getAllEdgesTo(dest); //Calls Exception if preconditions aren't met
		if (edgeList.size() <= index)
			return getFirstEdge(dest);
		//assert checkRep();
		return edgeList.get(index);
	}
	
	/**
	 * Returns the First Edge that connects this and dest
	 * <br> first edge is generally defined as order of insertion but is not enforce and cannot be guaranteed
	 * <br> Copy is not made so posterior changes will translate
	 * 
	 * @requires dest != null && dest is neighbor of this
	 * @param dest the neighbors of this to be returned
	 * @return the Edge that connects this and dest 
	 * @throws IllegalArgumentException when dest is not a neighbor of this
	 */
	public EDT getFirstEdge(Node<NIT, EDT> dest) throws IllegalArgumentException{
		List<EDT> edgeList = getAllEdgesTo(dest); //Calls Exception if preconditions aren't met
		return edgeList.get(0);
	}
	
	/**
	 * Returns All the edges that connects this and dest
	 * <br> Copy is not made so posterior changes will translate
	 * 
	 * @requires dest != null && dest is neighbor of this
	 * @param dest the neighbors of this to be returned
	 * @return List of all Edges that connects this and dest,
	 * @throws IllegalArgumentException when dest is not a neighbor of this or dest == null
	 */
	public List<EDT> getAllEdgesTo(Node<NIT, EDT> dest) throws IllegalArgumentException{
		if (!neighbors.containsKey(dest) || dest == null)
			throw new IllegalArgumentException("Dest is not a neighbor");
		List<EDT> edges = new ArrayList<EDT>(neighbors.get(dest));
		return edges;
	}
	
	/**
	 * Returns the amount of edges that originate at this and end at dest
	 * 
	 * @param dest Node that edges point to;
	 * @return number of edges with this as source and dest as destination, returns 0 if dest is not a neighbor
	 */
	public int getNumOfEdges(Node<NIT, EDT> dest){
		if (!neighbors.containsKey(dest))
			return 0;
		return neighbors.get(dest).size();
	}
	
	/**
	 * Returns a List of Nodes that are Neighbors to this
	 * <br>Removal or addition from this List will not remove Neighbors from this
	 * <br> Copy is not made so posterior changes will translate
	 * 
	 * @modifies Potentially allow alteration of distinct neighbor nodes
	 * @return a Set of Nodes which are neighbors of this
	 */
	public List<NIT> getNeighbors(){
		HashSet<Node<NIT, EDT>> nSet = new HashSet<Node<NIT, EDT>>(neighbors.keySet());
		List <Node<NIT, EDT>> nlist = new ArrayList<Node<NIT, EDT>>(nSet);
		List <NIT> identityList = new ArrayList<NIT>();
		for (Node<NIT, EDT> nbr: nlist){
			identityList.add(nbr.getNodeIdentity());
		}
		return identityList;
	}
	
	/**
	 * Returns whether there is a distinct edge that originates at this and ends at dest
	 * if dest is not a neighbor false is returned 
	 * 
	 * @param dest Node to to be checked for connection
	 * @param edge distinct edge to be to checked for 
	 * @return whether distinct edge exists from this to dest
	 */
	public boolean hasDistinctEdge(Node<NIT, EDT> dest , EDT edge){
		if (!neighbors.containsKey(dest))
			return false;
		return neighbors.get(dest).contains(edge);
	}
	
	/**
	 * Returns the amount of neighbors that this has
	 * 
	 * @return Amount of neighbors
	 */
	public int getAmountOfNeighbors(){
		return neighbors.size();
	}
	
	/**
	 * Returns the boolean value whether Node n is neighbor of this
	 * <br>that is: There Exist Edge that begins at this and ends at n
	 * 
	 * @param n potential Node neighbor to be checked as neighbor
	 * @return Whether node n is neighbor of this
	 */
	public boolean hasNeighbor(Node<NIT, EDT> neighbor){
		return neighbors.get(neighbor) != null;
	}
	
	/**
	 * Returns the boolean value whether Node n has any neighbors
	 * <br>that is: There Exist any Edge that connects to any node
	 * 
	 * @return Whether node n has neighbors
	 */
	public boolean hasAnyNeighbors(){
		return !neighbors.isEmpty();
	}
	
	/**
	 * {@inheritDoc}
	 */
	public int hashCode(){
		return this.identity.hashCode()*31;
	}
	
	/**
	 * Specialized for Node comparison.  Is equals if identity is equals
	 * <br>{@inheritDoc}
	 */
	public boolean equals(Node<NIT, EDT> n){
		return this.identity.equals(n.identity);
	}
	
	/**
	 * Returns String representation of this and its neighbors
	 */
	public String toString(){
		String ans = "[Node Data: " + this.identity.toString();
		Set<Node<NIT,EDT>> neighborSet = neighbors.keySet();
		if(neighborSet.size() > 0) {	
			Iterator<Node<NIT, EDT>> neighbIt = neighborSet.iterator();
			Node<NIT, EDT> tempNode;
			HashSet<EDT> tempEdges; 
			while (neighbIt.hasNext()){
				tempNode = neighbIt.next();
				ans += " ; Neighbor: " + tempNode.getNodeIdentity().toString() + " ; Edges:";
				tempEdges = neighbors.get(tempNode);
				Iterator<EDT> edgeIt = tempEdges.iterator();
				while (edgeIt.hasNext()){
					ans += " " + edgeIt.next().toString();
				}
			}
		}
		ans += "]";
		return ans;
	}
	
	/**
	 * Created a new Node with the same Neighbors as this 
	 * <br>If newIdt is null then identity does not change
	 * 
	 * @requires newIdt != null
	 * @param newIdt New identity which the new node will be created with
	 * @return New Node with same properties of this but with new identy
	 * @throws  IllegalArgumentException If null new identity is passed in 
	 */
	public Node<NIT,EDT> dupNodeNewIdt(NIT newIdt){
		if (newIdt == null)
			throw new IllegalArgumentException("");
		Node<NIT,EDT> newNode = new Node<NIT,EDT>(newIdt);
		newNode.neighbors = new HashMap<Node<NIT, EDT>, LinkedHashSet<EDT>>(this.neighbors);
		assert checkRep();
		return newNode;
	}
	
	/*------------------Mutators--------------------*/
	
	/**
	 * Adds an Edge "edge" to Node "n".
	 * <br>n becomes a neighbor of this
	 * <br>If already exists a an edge that is equals to parameter "edge" then the new edge will not be added
	 * 
	 * @requires addedNode != null, edge != null
	 * @modifies this
	 * @effects adds a neighbor if n does not already exist, replaces edge if n already exists
	 * @param n Node to be added as neighbor
	 * @param edge to be used to connect this and Node n
	 * @return true if the Node did not already contain that specified edge
	 * @throws IllegalArgumentException if edge == null or addedNode == null 
	 */
	public boolean addEdgeToNode(Node<NIT, EDT> addedNode, EDT edge) throws IllegalArgumentException{
		if (edge == null || addedNode == null){
			throw new IllegalArgumentException("Null Edge or Node to be added");
		}
		//Check if addedNode is already a neighbor
		boolean added;
		LinkedHashSet<EDT> edges;
		// Can't Add Edge to Self, current implementation spec which is subject to change if necessary 
		/*if (this.equals(addedNode)) 
			return false;*/
		if (neighbors.containsKey(addedNode)){
			edges = neighbors.get(addedNode);
			added = edges.add(edge);
		} else { // addedNode becomes new neighbor
			edges = new LinkedHashSet<EDT>();
			added = edges.add(edge);
			neighbors.put(addedNode, edges);
		}
		assert checkRep();
		return added;
	}
	
	/**
	 * Deletes the distinct edge that connects this and dest
	 * <br>That is delete LabeledEdge E, if before is neighbors : [E<this,dest>] , then after is  neighbors : [] 
	 * <br>If no such edge exist then there is no effect
	 * <br>Returns true if dest was successfully found and deleted
	 * <br>Returns false if "edge" did not exist or dest is not a neighbor of this
	 * 
	 * @requires dest != null dest is neighbor of this
	 * @modifies this
	 * @effects removes edge to neighbor dest
	 * @param dest neighbor in which edge to is to be removed
	 * @return if Edge exist between this and dest then was deleted
	 * @throws IllegalArgumentException if dest == null 
	 */
	public boolean deleteEdgeTo(Node<NIT, EDT> dest, EDT edge) throws IllegalArgumentException{
		//If contained in neighbors remove key and Value
		if (!neighbors.containsKey(dest))
			throw new IllegalArgumentException("Dest is not a neighbor of");
		LinkedHashSet<EDT> eList = neighbors.get(dest);
		boolean deleted = eList.remove(edge);
		//If No more edges exist dest remove as neighbor
		if (eList.size() == 0)
			deleteNeighbor(dest);
		assert checkRep();
		return deleted;
	}
	
	/**
	 * Deletes neighbor if it exists
	 * <br>If it does not exist then there no effect this
	 * <br>If dest == null there is no effect on this
	 * <br>IE: if neighbors : [E[this,N1] , E[this,N2], ... , E[this,Nk]] , then after delete N1 this's neighbors : [ E[this,N2], ... , E[this,Nk]]
	 * 
	 * @modifies this
	 * @effects Removes all edges from this to dest
	 * @param dest Node to be removed if possible
	 */
	public void deleteNeighbor(Node<NIT, EDT> dest){
		neighbors.remove(dest);
		assert checkRep();
	}
	
	/**
	 * Deletes all edges outgoing from this Node
	 * <br>That is delete Edge E, if before this's neighbors : [E[this,N1] , E[this,N2], ... , E[this,Nk]] , then after this's neighbors : [] 
	 * 
	 * @modifies this
	 * @effects removes all edges to any neighbor 
	 */
	public void deleteAllNeighbors(){
		neighbors.clear();
		assert checkRep();
	}
	
	/**
	 * Private Method to check internal Representation Invariants
	 */
	private boolean checkRep(){
		assert identity!= null;
		assert neighbors!=null;
		assert neighbors.size() >= 0;
		assert !neighbors.containsKey(null);
		assert !neighbors.containsValue(null);
		//Linked HashSetEnforce not repeated edges
		return true;
	}
	
}
