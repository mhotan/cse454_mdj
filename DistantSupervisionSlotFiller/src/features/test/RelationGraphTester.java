package features.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import knowledgeBase.Entity;
import knowledgeBase.RelationInstance;
import knowledgeBase.Relationship;
import nlp.StanfordNerTag;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import trainingDataGenerator.AnnotatedSentenceParser.EntityPairMention;
import trainingDataGenerator.AnnotatedSentenceParser.Range;
import trainingDataGenerator.AnnotatedSentenceParserAdapter;
import trainingDataGenerator.AnnotatedSentenceParserAdapter.AnnotationParsingException;
import trainingDataGenerator.AnnotationConstant;
import edu.stanford.nlp.trees.Tree;
import features.components.RelationInstanceNode.ENTITY_POS;
import features.components.dependency.DependencyAttribute;
import features.components.dependency.DependencyNode;
import features.extractor.DependencyFeatureExtractor;
import features.extractor.OverallFeatureExtractor;
import features.extractor.SequenceFeatureExtractor;
import features.extractor.SyntacticFeatureExtractor;

public class RelationGraphTester {

	protected static String sent1, sent2, sent3, sent4, sent5, sent6, sent;
	protected static EntityPairMention p1, p2 , p3, p4, p5, p6, p;
	protected static Map<AnnotationConstant, String> ann1, ann2, ann3, ann4, ann5, ann6, ann;
	protected static AnnotatedSentenceParserAdapter adapt1, adapt2, adapt3, adapt4, adapt5, adapt6, adapt;
	
	private static SyntacticFeatureExtractor mSynExtractor1, mSynExtractor2, mSynExtractor3,
	mSynExtractor4, mSynExtractor5, mSynExtractor6;
	private static List<SyntacticFeatureExtractor>  SynExtractList;
	private static SequenceFeatureExtractor mSeqExtractor1, mSeqExtractor2, mSeqExtractor3, 
	mSeqExtractor4, mSeqExtractor5, mSeqExtractor6;
	private static List<SequenceFeatureExtractor>  SeqExtractList;
	private static DependencyFeatureExtractor mDepExtractor1, mDepExtractor2, mDepExtractor3, 
	mDepExtractor4, mDepExtractor5,mDepExtractor6;
	private static List<DependencyFeatureExtractor>  DepExtractList;
	private static OverallFeatureExtractor mOverExtractor1, mOverExtractor2, mOverExtractor3,
	mOverExtractor4, mOverExtractor5, mOverExtractor6;
	private static List<OverallFeatureExtractor>  OverExtractList;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		sent1 = "hundreds of Palestinians converged on the square";
		sent2 = "Astronomer Edwin Hubble was born in Marshfield, Missouri";
		// Similar sentences
		sent3 = "Cat born in the Hat";
		sent4 = "Dog born in the bar";
		
		SynExtractList = new ArrayList<SyntacticFeatureExtractor>();
		SeqExtractList = new ArrayList<SequenceFeatureExtractor>();
		DepExtractList = new ArrayList<DependencyFeatureExtractor>();
		OverExtractList = new ArrayList<OverallFeatureExtractor>();
		
		Range ent1 = new Range(2,3);
		Range ent2 = new Range(6,7);
		p1 = new EntityPairMention("1", new RelationInstance(
				Entity.getEntityFromMid("Palestinians"), Entity.getEntityFromMid("square"), Relationship.NA),  ent1, ent2);
		ann1 = trainingDataGenerator.AnnotationGenerator.generateAnnotation(sent1);
		adapt1 = new AnnotatedSentenceParserAdapter(ann1);
		
		ent1 = new Range(1,3);
		ent2 = new Range(6,9);
		p2 = new EntityPairMention("2", new RelationInstance(
				Entity.getEntityFromMid("Edwin Hubble"), Entity.getEntityFromMid("Marshfield, Missouri"), Relationship.PLACE_OF_BIRTH),  ent1, ent2);
		ann2 = trainingDataGenerator.AnnotationGenerator.generateAnnotation(sent2);
		adapt2 = new AnnotatedSentenceParserAdapter(ann2);
		
		Range entR1 = new Range(0,1);
		Range entR2 = new Range(4,5);
		Entity ent3_1 = Entity.getEntityFromMid("Cat");
		Entity ent3_2 = Entity.getEntityFromMid("Hat");
		Entity ent4_1 = Entity.getEntityFromMid("Dog");
		Entity ent4_2 = Entity.getEntityFromMid("bar");
		p3 = new EntityPairMention("3", new RelationInstance(ent3_1, ent3_2, Relationship.PLACE_OF_BIRTH), entR1, entR2);
		p4 = new EntityPairMention("4", new RelationInstance(ent4_1, ent4_2, Relationship.PLACE_OF_BIRTH), entR1, entR2);
		ann3 = trainingDataGenerator.AnnotationGenerator.generateAnnotation(sent3);
		ann4 = trainingDataGenerator.AnnotationGenerator.generateAnnotation(sent4);
		adapt3 = new AnnotatedSentenceParserAdapter(ann3);
		adapt4 = new AnnotatedSentenceParserAdapter(ann4);
	
		sent5 = "KBR Inc (formerly Kellogg Brown & Root) is an American engineering, " +
				"construction and private military contracting company, formerly a subsidiary of Halliburton, headquartered in Houston, Texas";
		ann5 = trainingDataGenerator.AnnotationGenerator.generateAnnotation(sent5);
		adapt5 = new AnnotatedSentenceParserAdapter(ann5);
		List<String> tokens = adapt5.getTokens().tokens;	
		ent1 = new Range(0, 2);
		ent2 = new Range(tokens.size()-3, tokens.size());
		Entity ent5_1 = Entity.getEntityFromMid("KBR Inc");
		Entity ent5_2 = Entity.getEntityFromMid("Houston, Texas");
		Relationship r = Relationship.PLACE_OF_DEATH;
		p5 = new EntityPairMention("5", new RelationInstance(ent5_1, ent5_2, r), ent1, ent2);
		
		sent6 = "A brand new and bigger Air China was launched in Beijing on Monday.";
		ann6 = trainingDataGenerator.AnnotationGenerator.generateAnnotation(sent6);
		adapt6 = new AnnotatedSentenceParserAdapter(ann6);
		ent1 = new Range(5,7);
		ent2 = new Range(10,11);
		Entity ent6_1 = Entity.getEntityFromMid("Air China");
		Entity ent6_2 = Entity.getEntityFromMid("Beijing");
		Relationship r6 = Relationship.PLACE_OF_BIRTH;
		p6 = new EntityPairMention("6", new RelationInstance(ent6_1, ent6_2, r6), ent1, ent2);
		
		
		mSynExtractor1 = new SyntacticFeatureExtractor(ann1, p1);
		mSynExtractor2 = new SyntacticFeatureExtractor(ann2, p2);
		mSynExtractor3 = new SyntacticFeatureExtractor(ann3, p3);
		mSynExtractor4 = new SyntacticFeatureExtractor(ann4, p4);
		mSynExtractor5 = new SyntacticFeatureExtractor(ann5, p5);
		mSynExtractor6 = new SyntacticFeatureExtractor(ann6, p6);
		
		mSeqExtractor1 = new SequenceFeatureExtractor(ann1, p1);
		mSeqExtractor2 = new SequenceFeatureExtractor(ann2, p2);
		mSeqExtractor3 = new SequenceFeatureExtractor(ann3, p3);
		mSeqExtractor4 = new SequenceFeatureExtractor(ann4, p4);
		mSeqExtractor5 = new SequenceFeatureExtractor(ann5, p5);
		mSeqExtractor6 = new SequenceFeatureExtractor(ann6, p6);
		
		mDepExtractor1 = new DependencyFeatureExtractor(ann1, p1, true);
		mDepExtractor2 = new DependencyFeatureExtractor(ann2, p2, true);
		mDepExtractor3 = new DependencyFeatureExtractor(ann3, p3, true);
		mDepExtractor4 = new DependencyFeatureExtractor(ann4, p4, true);
		mDepExtractor5 = new DependencyFeatureExtractor(ann5, p5, true);
		mDepExtractor6 = new DependencyFeatureExtractor(ann6, p6, true);
		
		mOverExtractor1 = new OverallFeatureExtractor(ann1, p1);
		mOverExtractor2 = new OverallFeatureExtractor(ann2, p2);
		mOverExtractor3 = new OverallFeatureExtractor(ann3, p3);
		mOverExtractor4 = new OverallFeatureExtractor(ann4, p4);
		mOverExtractor5 = new OverallFeatureExtractor(ann5, p5);
		mOverExtractor6 = new OverallFeatureExtractor(ann6, p6);
	}

	@Before
	public void setUp() throws Exception {
		
	}
	
	@Test 
	public void testTreeEquality(){
		Tree t1 = mSynExtractor3.getTree().getRoot().getTree();
		Tree t2 = mSynExtractor4.getTree().getRoot().getTree();
		assertEquals("Equality between Tree: "+ t1 + " and Tree:" + t2, t1.toString(), t2.toString());
	}
	@Test
	public void testSyntacticExtractor(){
		List<String> r1 = mSynExtractor1.getFeatures();
		List<String> r2 = mSynExtractor2.getFeatures();
		List<String> r3 = mSynExtractor3.getFeatures();
		List<String> r4 = mSynExtractor4.getFeatures();
		assertNotNull(r1);
		assertNotNull(r2);
		assertNotNull(r3);
		assertNotNull(r4);
	}
	
	@Test
	public void testVerbTrees(){
		List<String> t1 = mSynExtractor1.getAllVerbConstructsBetween1and2();
		List<String> t2 = mSynExtractor1.getAllVerbConstructsBetween1and2();	
	}
	
	@Test
	public void testVerbTrees2(){
		List<String> t1 = mSynExtractor3.getAllVerbConstructsBetween1and2();
		List<String> t2 = mSynExtractor3.getAllVerbConstructsBetween1and2();	
	}
	
	@Test 
	public void equalityTesting(){
		DependencyNode n1 = new DependencyNode(0,1,ENTITY_POS.HEAD);
		assertEquals("Same instance node should be equal", n1, n1);
		assertEquals("Same instance node hash code should be equal", n1.hashCode(), n1.hashCode());
		DependencyNode n2 = new DependencyNode(0,1,ENTITY_POS.HEAD);
		assertEquals("Same node should be equal", n1, n2);
		assertEquals("Same node hash code should be equal", n1.hashCode(), n2.hashCode());
		assertEquals("Same node should be equal bidirectional", n2, n1);
		assertEquals("Same node hash code should be equal bidirectional", n2.hashCode(), n1.hashCode());
		}
	
	@Test 
	public void equalityTestingAttribute(){
		DependencyNode n1 = new DependencyNode(0,1,ENTITY_POS.HEAD);
		DependencyNode n2 = new DependencyNode(0,1,ENTITY_POS.HEAD);
		DependencyAttribute a = new DependencyAttribute("a", "b", "c", StanfordNerTag.DATE);
		n1.setAttribute(a);
		assertFalse("Node inequality post set attribute", n1.equals(n2));
		assertFalse("Node inequality post set attribute", n2.equals(n1));
		assertFalse("Node inequality post set attribute", n2.hashCode()==n1.hashCode());
		n1.removeAttribute();
		assertEquals("Same node should be equal", n1, n2);
		assertEquals("Same node hash code should be equal", n1.hashCode(), n2.hashCode());
		
	}
	
	@Test 
	public void equalityTestingAttribute2(){
		DependencyNode n1 = new DependencyNode(0,1,ENTITY_POS.HEAD);
		DependencyNode n2 = new DependencyNode(0,1,ENTITY_POS.HEAD);
		DependencyAttribute a = new DependencyAttribute("a", "b", "c", StanfordNerTag.DATE);
		n1.setAttribute(a);
		n2.setAttribute(a);
		assertEquals("Same node should be equal", n1, n2);
		assertEquals("Same node hash code should be equal", n1.hashCode(), n2.hashCode());
	}
	
	@Test 
	public void equalityTestingAttribute3(){
		DependencyNode n1 = new DependencyNode(0,1,ENTITY_POS.HEAD);
		DependencyNode n2 = new DependencyNode(0,1,ENTITY_POS.HEAD);
		DependencyAttribute a = new DependencyAttribute("a", "b", "c", StanfordNerTag.DATE);
		DependencyAttribute a2 = new DependencyAttribute("a", "b", "c", StanfordNerTag.DATE);
		n1.setAttribute(a);
		n2.setAttribute(a2);
		assertEquals("Same node should be equal", n1, n2);
		assertEquals("Same node hash code should be equal", n1.hashCode(), n2.hashCode());
	}
	
	@Test 
	public void equalityTestingAttribute4(){
		DependencyNode n1 = new DependencyNode(0,1,ENTITY_POS.HEAD);
		DependencyNode n2 = new DependencyNode(0,1,ENTITY_POS.HEAD);
		DependencyAttribute a = new DependencyAttribute("a", "b", "c", StanfordNerTag.DATE);
		DependencyAttribute a2 = new DependencyAttribute("a", "B", "c", StanfordNerTag.DATE);
		n1.setAttribute(a);
		n2.setAttribute(a2);
		assertFalse("Same node should be equal", n1.equals(n2));
		assertFalse("Same node hash code should be equal", n1.hashCode()==n2.hashCode());
	}
	
	@Test
	public void dependencyPathTest(){
		assertEquals("Dependency path length",4,mDepExtractor5.getPathLengthBetweenEntities());
	}
	
	@Test
	public void dependencyGetFeatures(){
		List<String> features5 = mDepExtractor5.getFeatures();
		List<String> features3 = mDepExtractor3.getFeatures();
		assertNotNull(features3);
		assertNotNull(features5);
	}
	
	@Test
	public void SeqGetFeatures(){
		List<String> features5 = mSeqExtractor5.getFeatures();
		List<String> features3 = mSeqExtractor3.getFeatures();
		assertNotNull(features3);
		assertNotNull(features5);
	}
	
	@Test
	public void SynGetFeatures(){
		List<String> features5 = mSynExtractor5.getFeatures();
		List<String> features3 = mSynExtractor3.getFeatures();
		assertNotNull(features3);
		assertNotNull(features5);
	}
	
	@Test
	public void OverralGetFeatures(){
		List<String> features1 = mOverExtractor1.getFeatures();
		List<String> features2 = mOverExtractor2.getFeatures();
		List<String> features3 = mOverExtractor3.getFeatures();
		List<String> features4 = mOverExtractor4.getFeatures();
		List<String> features5 = mOverExtractor5.getFeatures();
		assertNotNull(features1);
		assertNotNull(features2);
		assertNotNull(features3);
		assertNotNull(features4);
		assertNotNull(features5);
		assertTrue("Size of feature set 1 greater then 0", features1.size() > 0);
		assertTrue("Size of feature set 2 greater then 0", features2.size() > 0);
		assertTrue("Size of feature set 3 greater then 0", features3.size() > 0);
		assertTrue("Size of feature set 4 greater then 0", features4.size() > 0);
		assertTrue("Size of feature set 5 greater then 0", features5.size() > 0);
		
	}
	
	// This bug occured march 5th during corpus analysis
	/*
	 * This bug was produced by assuming that there is always a gramatical depency between tokens of an entity.
	 * Which is not always the case.  I also started a recusive parent search to look for the a root node that had both the
	 * first token entity.  Instead I found that tokens withiin the entity are disconnect or not connected through dependency
	 * Instead I found the head of the dependencies  
	 */
	@Test
	public void TestMarch5Bug() throws AnnotationParsingException{
		String sent = "Malaysia's team of 50 lawyers is headed by the International Affairs Adviser to the Prime Minister Abdul Kadir Mohamed.";
		Range r1 = new Range(0,1);
		Range r2 = new Range(1,3);
		TestAllExtractorsBuild(sent, r1, r2);
		TestAllExtractorsGraphComponents(sent, r1, r2);
	}
	
	/*
	 * Bug was produced because entities did not have any path connecting them.
	 * This caused the root to be null and failure to build the graph
	 * The graph was changed to handle root nodes and 
	 * 
	 */
	@Test
	public void TestMarch5Bug2() throws AnnotationParsingException{
		String sent = "XI ' AN, Feb. 14 (Xinhua)";
		Range r1 = new Range(7,8);
		Range r2 = new Range(1,2);
		TestAllExtractorsBuild(sent, r1, r2);
		TestAllExtractorsGraphComponents(sent, r1, r2);
	}
	
	@Test
	public void TestMarch5Bug2PrintFeatures() throws AnnotationParsingException{
		String sent = "XI ' AN, Feb. 14 (Xinhua)";
		Range r1 = new Range(7,8);
		Range r2 = new Range(1,2);
		Entity ent1 = Entity.getEntityFromMid("Entity1_Build");
		Entity ent2 = Entity.getEntityFromMid("Entity2_Build");
		EntityPairMention p = new EntityPairMention("_NA_Build_", new RelationInstance(ent1, ent2, null), r1, r2);
		Map<AnnotationConstant, String> ann = trainingDataGenerator.AnnotationGenerator.generateAnnotation(sent);
		AnnotatedSentenceParserAdapter adapt = new AnnotatedSentenceParserAdapter(ann);
		SequenceFeatureExtractor sfe = new SequenceFeatureExtractor(adapt, p);
		SyntacticFeatureExtractor syfe = new SyntacticFeatureExtractor(adapt,p);
		DependencyFeatureExtractor dfe = new DependencyFeatureExtractor(adapt,p);
		OverallFeatureExtractor fe = new OverallFeatureExtractor(adapt, p);
		System.out.println("Sequencial");
		List<String> seqFeatures = sfe.getFeatures();
		List<String> synFeatures = syfe.getFeatures();
		List<String> depFeatures = dfe.getFeatures();
		for (String f: seqFeatures)
			System.out.println(f);
		System.out.println("Syntactic");
		for (String f: synFeatures)
			System.out.println(f);
		System.out.println("Dependency");
		for (String f: depFeatures)
			System.out.println(f);
	}
	
	private void TestAllExtractorsBuild(String sentence, Range r1, Range r2) throws AnnotationParsingException{
		Entity ent1 = Entity.getEntityFromMid("Entity1_Build");
		Entity ent2 = Entity.getEntityFromMid("Entity2_Build");
		EntityPairMention p = new EntityPairMention("_NA_Build_", new RelationInstance(ent1, ent2, null), r1, r2);
		
		Map<AnnotationConstant, String> ann = trainingDataGenerator.AnnotationGenerator.generateAnnotation(sentence);
		AnnotatedSentenceParserAdapter adapt = new AnnotatedSentenceParserAdapter(ann);
		
		SequenceFeatureExtractor seqExt = new SequenceFeatureExtractor(adapt, p);
		SyntacticFeatureExtractor synExt = new SyntacticFeatureExtractor(adapt, p);
		DependencyFeatureExtractor depExt = new DependencyFeatureExtractor(adapt, p);
		
		assertNotNull("Sequence Extractor not null", seqExt);
		assertNotNull("Syntactic Extractor not null", synExt);
		assertNotNull("Dependency Extractor not null", depExt);
		
		assertNotNull("Sequence Complete graph null", seqExt.mGraphCompPath);
		assertNotNull("Sequence before first entity graph null", seqExt.mGraphBeforePath);
		assertNotNull("Sequence after secodn entity graph null", seqExt.mGraphAfterPath);
		assertNotNull("Sequence entity 1 graph null", seqExt.mGraphNER1Path);
		assertNotNull("Sequence entity 2 graph null", seqExt.mGraphNER2Path);
		assertNotNull("Syntactic Tree Extractor not null", synExt.mTree);
		assertNotNull("Dependency graph not null", depExt.mGraph);
		// Test if we can build Each grap	
	}
	
	private void TestAllExtractorsGraphComponents(String sentence, Range r1, Range r2) throws AnnotationParsingException{
		Entity ent1 = Entity.getEntityFromMid("Entity1_Graph_Components");
		Entity ent2 = Entity.getEntityFromMid("Entity2_Graph_Components");
		EntityPairMention p = new EntityPairMention("_NA_Graph_Components_", new RelationInstance(ent1, ent2, null), r1, r2);
		
		Map<AnnotationConstant, String> ann = trainingDataGenerator.AnnotationGenerator.generateAnnotation(sentence);
		AnnotatedSentenceParserAdapter adapt = new AnnotatedSentenceParserAdapter(ann);
		
		SequenceFeatureExtractor seqExt = new SequenceFeatureExtractor(adapt, p);
		SyntacticFeatureExtractor synExt = new SyntacticFeatureExtractor(adapt, p);
		DependencyFeatureExtractor depExt = new DependencyFeatureExtractor(adapt, p);
		
//		assertNotNull("Sequence graph 1st node null", seqExt.mGraphComplete.getFirstNode());
//		assertNotNull("Sequence graph last node null", seqExt.mGraphComplete.getLastNode());
//		assertNotNull("Sequence graph head of 1st null", seqExt.mGraphComplete.getHeadOfEntity1());
//		assertNotNull("Sequence graph head of 2nd null", seqExt.mGraphComplete.getHeadOfEntity2());
		assertNotNull("Syntactic Tree Extractor Root not null", synExt.mTree.getRoot());
		assertNotNull("Syntactic Tree Extractor Root of entity 1 not null", synExt.mTree.getRootForEntity1());
		assertNotNull("Syntactic Tree Extractor Root of entity 2 not null", synExt.mTree.getRootForEntity2());
//		assertNotNull("Dependency graph root not null", depExt.mGraph.getRoot());
		assertNotNull("Dependency graph entity 1 Head not null", depExt.mGraph.getEntity1Head());
		assertNotNull("Dependency graph entity 2 Head not null", depExt.mGraph.getEntity2Head());
		assertNotNull("Dependency graph entity 1 Tail not null", depExt.mGraph.getEntity1Tail());
		assertNotNull("Dependency graph entity 2 Tail not null", depExt.mGraph.getEntity2Tail());
		
		if (depExt.mGraph.getEntity1Paths().isEmpty()){
			assertEquals("If there exist dependency path for 1 then that should infer that head and tails are the same"
					,depExt.mGraph.getEntity1Head(), depExt.mGraph.getEntity1Tail());
		}
		if (depExt.mGraph.getEntity2Paths().isEmpty()){
			assertEquals("If there exist dependency path for 2 then that should infer that head and tails are the same"
					,depExt.mGraph.getEntity2Head(), depExt.mGraph.getEntity2Tail());
		}
		if (depExt.mGraph.getRoot() != null)
			assertTrue("Dependency path between entities is not null if and only if the root is not null" +
					"", !depExt.mGraph.getDependencyPathForEntities().isEmpty());
	}
}
