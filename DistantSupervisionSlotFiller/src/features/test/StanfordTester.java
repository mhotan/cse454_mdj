package features.test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import trainingDataGenerator.AnnotatedSentenceParserAdapter;
import trainingDataGenerator.AnnotatedSentenceParserAdapter.AnnotationParsingException;
import edu.stanford.nlp.trees.Tree;

public class StanfordTester {

	private static String sent3, sent4;
	static AnnotatedSentenceParserAdapter ann3, ann4;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		sent3 = "Cat in the Hat";
		sent4 = "Dog in the bar";
		ann3 = new AnnotatedSentenceParserAdapter(trainingDataGenerator.AnnotationGenerator.generateAnnotation(sent3));
		ann4 = new AnnotatedSentenceParserAdapter(trainingDataGenerator.AnnotationGenerator.generateAnnotation(sent4));
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() throws AnnotationParsingException {
		Tree t1 = ann3.getCJ().parseTree;
		Tree t2 = ann4.getCJ().parseTree;
		
		removeLeaves(t1);
		removeLeaves(t2);
		
		assertEquals("Syntactic Tree equality", t1, t2);
	}
	
	private static void removeLeaves(Tree t){
		if (t.isPreTerminal()){
			for (int i = 0; i < t.numChildren(); ++i)
				t.removeChild(0);
		} else
			for (Tree child: t.getChildrenAsList())
				removeLeaves(child);
	}

}
