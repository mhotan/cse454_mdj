package features.extractor;

/**
 * Clas to help contain feature information about features for two entities
 * @author mhotan
 */
public class EntityFeaturePair {

	private final String mFeatureForEntity1, mFeatureForEntity2;
	
	public String get1(){return mFeatureForEntity1;}
	
	public String get2(){return mFeatureForEntity2;}
	
	public EntityFeaturePair(String entity1Feature, String entity2Feature){
		mFeatureForEntity1 = entity1Feature;
		mFeatureForEntity2 = entity2Feature;
	}
}
