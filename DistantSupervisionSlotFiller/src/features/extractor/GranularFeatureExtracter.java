package features.extractor;

import java.util.List;
import java.util.Map;

import trainingDataGenerator.AnnotatedSentenceParser.EntityPairMention;
import trainingDataGenerator.AnnotatedSentenceParser.Range;
import trainingDataGenerator.AnnotatedSentenceParserAdapter;
import trainingDataGenerator.AnnotationConstant;

/**
 * General super class for all Feature Extractors that can produce 
 * Granular features.  However the definition of Granularity can very between subclasses
 * @author mhotan
 */
public abstract class GranularFeatureExtracter implements FeatureExtractable {

	/**
	 * For a single feature 
	 */
	protected static final String ARG_SPLITTER = "|";

	/**
	 * Range of available granularity ranges for all subclasses
	 */
	private static final Range GRANULARITY_RANGE = new Range(1,5);	

	/**
	 * Default value for the granularity level
	 */
	protected static final int DEFAULT_LEVEL = GRANULARITY_RANGE.getEnd();

	/**
	 * Mappings of all the annotations of a particular sentence
	 */
	protected final AnnotatedSentenceParserAdapter mParser;

	/**
	 * CONTRACT: All subclasses should right this value to non null
	 * Prefix string for all subclasses
	 */
	protected String PREFIX;

	/**
	 * Returns a list of features at a particular granularity level
	 * @param level Level of granularity to find features
	 * @return List of features at granularity level 
	 */
	public abstract List<String> getFeatures(int level);

	/**
	 * labels if the entities are inverted in appearance in the sentence
	 * That is if entity 2 appears before 1 then it is inverted
	 */
	protected final boolean invertedEntities;
	protected final String INV_STRING;

	/**
	 * Initializes the Parser field
	 * @param annotations
	 * @param pair
	 */
	protected GranularFeatureExtracter(AnnotatedSentenceParserAdapter adapter,
			EntityPairMention pair){
		mParser = adapter;
		// The direction of where the second entity is relative to the first
		// have to account for entities appearing in reverse order
		// That is in some situation entity 1 might appear before 2
		// and others 2 before 1
		invertedEntities = pair.tokenRange1.getStart() > pair.tokenRange2.getStart();
		// Entity Appearance Order
		INV_STRING = invertedEntities ? "inverse_true" : "inverse_false";
	}

	/**
	 * Initializes the Parser field
	 * @param annotations
	 * @param pair
	 */
	protected GranularFeatureExtracter(Map<AnnotationConstant, String> annotations,
			EntityPairMention pair){
		this(new AnnotatedSentenceParserAdapter(annotations), pair);
	}

	/**
	 * Granularity can only be found at particular ranges
	 * @return Range of all possible granularity levels
	 */
	public Range getGranularityRange(){
		return new Range(GRANULARITY_RANGE.getStart(), GRANULARITY_RANGE.getEnd());
	}

	/**
	 * Normalize the level to fall between granularity ranges
	 * @param level prospective level client might want to use
	 * @return
	 */
	protected int filterLevel(int level){
		return Math.max(GRANULARITY_RANGE.getStart(), Math.min(GRANULARITY_RANGE.getEnd(), level));
	}

	/**
	 * Returns the prefix identitifier for this type of Feature space extractor
	 * @return Prefix string literal
	 */
	public String getUniquePrefix(){return PREFIX;}

	/**
	 * assertions inside should never occur
	 * Checks representation of this particular extractor
	 */
	protected void checkRep(){
		if (PREFIX == null) 
			throw new RuntimeException("Null prefix: All subclasses should set inherited PREFIX string to " +
					" class specific prefix");
		if (mParser == null) {
			throw new RuntimeException("Null annotations");
		}
	}
}
