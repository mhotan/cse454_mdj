package features.extractor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import trainingDataGenerator.AnnotatedSentenceParser.EntityPairMention;
import trainingDataGenerator.AnnotatedSentenceParserAdapter;
import trainingDataGenerator.AnnotatedSentenceParserAdapter.AnnotationParsingException;
import trainingDataGenerator.AnnotationConstant;
import util.POSIdentifier;
import edu.stanford.nlp.trees.Tree;
import features.components.syntactic.SyntacticParseTree;

/**
 * Extracts features based on Syntactic parse tree for specific levels of
 * granularity
 * 
 * @author mhotan
 */
public class SyntacticFeatureExtractor extends GranularFeatureExtracter {

	
	private static final boolean USE_VERB_PERMUTATIONS = true;
	
	private static final int TREE_THRESHOLD = 3;

	/**
	 * Create a field for the tree
	 * Assigned public visibility for testing purposes
	 * TODO: Assign private again
	 */
	public final SyntacticParseTree mTree;

	private final TreeComparator mComparator;
	
	/**
	 * Creates a feature extractor based off this a syntatic parse tree
	 * 
	 * @param sentence 
	 * @param pair
	 * @throws AnnotationParsingException 
	 */
	public SyntacticFeatureExtractor(String sentence, EntityPairMention pair) throws AnnotationParsingException {
		this(trainingDataGenerator.AnnotationGenerator
				.generateAnnotation(sentence), pair);
	}

	/**
	 * TODO:
	 * @param annotations
	 * @param pair
	 * @throws AnnotationParsingException 
	 */
	public SyntacticFeatureExtractor(Map<AnnotationConstant, String> annotations, EntityPairMention pair) throws AnnotationParsingException{
		this(new AnnotatedSentenceParserAdapter(annotations), pair);
	}
	
	/**
	 * TODO
	 * @param adapt
	 * @param pair
	 * @throws AnnotationParsingException 
	 */
	public SyntacticFeatureExtractor(
			AnnotatedSentenceParserAdapter adapt, EntityPairMention pair) throws AnnotationParsingException {
		super(adapt, pair);
		mTree = SyntacticParseTree.buildTree(mParser, pair);
		mComparator = new TreeComparator();
		PREFIX = "SYN" + ARG_SPLITTER;
	}

	@Override
	public List<String> getFeatures() {
		// TODO Auto-generated method stub
		return getFeatures(getGranularityRange().getEnd());
	}

	@Override
	public List<String> getFeatures(int level) {

		// //////////////////////////////////////////////////
		// /// Level Independent (Features that are included at all levels)

		String inv = INV_STRING;
		
		List<String> middle = new ArrayList<String>();
		// Syntactic path from head of argument 1 head to argument 2 head
		List<String> completePath1to2 = getPathFrom1to2();
		// Use only Content words (aka non functional word)
		List<String> contentWordsMiddle = new ArrayList<String>();
		List<String> verbsMiddle = new ArrayList<String>();
		for (String postag: completePath1to2){
			if (POSIdentifier.isVerb(postag))
				verbsMiddle.add(postag);
			if (POSIdentifier.isContentWord(postag))
				contentWordsMiddle.add(postag);
		}
		middle.add(pathToString(completePath1to2));
		middle.add(pathToString(contentWordsMiddle));
		middle.add(pathToString(verbsMiddle));
		
		// Added MH
		// From the root node that contains the named entity
		//		IE Astronomer Edwin Hubble 
		//			(Entity: Edwin Hubble | Additional: Astronomer)
		//			(Entity: NNP NNP | Additional: NNP)
		// This is done by retrieving all subtrees of each entity
		// Then extracting the biggest subtrees to based on which granularity level
		List<String> ent1Subtrees = getAllEntity1Subtree(level);
		List<String> ent2Subtrees = getAllEntity2Subtree(level);
		//		List<String> verbStructures = 

		List<String> verbSubTrees = getAllVerbConstructsBetween1and2();

		List<String> results = new ArrayList<>(2*(ent1Subtrees.size() +
				ent2Subtrees.size()) + verbSubTrees.size());
		
		// For every permutation of entity 1 and 2 and a concatonation feature
		for (String ent1: ent1Subtrees) {
			for (String ent2: ent2Subtrees) {
				for (String midStr: middle){
					String result = inv + ARG_SPLITTER + ent1 + ARG_SPLITTER + midStr + ARG_SPLITTER + ent2;
					// Only reference the verb subtrees if they exist
					//  If there are no verbs then just what we have  
					if (verbSubTrees.isEmpty()) {
						results.add(result);
					} else {
						switch (FeatureExtractorVariables.VERB_USE) {
						case USE_VERBS:
							for (String verbPerm: verbSubTrees){ // For every verb permutation
								results.add(result + ARG_SPLITTER + verbPerm);
							}
							break;
						case NO_VERBS_USED:
							results.add(result);
						}
					}
				}
			}
		}
		return results;
	}

	public SyntacticParseTree getTree(){
		return mTree;
	}

	public List<String> getPathFrom1to2(){
		return mTree.getShortestPath(mTree.getRootForEntity1(), mTree.getRootForEntity2());
	}

	//	public String getPathFrom1to2Alt(){
	//		List<String> path = mTree.getShortestPathOther(mTree.getRootForEntity1(), mTree.getRootForEntity2());
	//		return pathToString(path);
	//	}

	public List<String> getAllEntity1Subtree(int granularity){
		// Max granularity means we will use all the trees
		// Threshold is set to have minimum number of subtrees
		List<Tree> trees = mTree.getEntity1Subtrees();
		return getNumTrees(trees, granularity);
	}

	public List<String> getAllEntity2Subtree(int granularity){
		List<Tree> trees = mTree.getEntity2Subtrees();
		return getNumTrees(trees, granularity);
	}
	
	public List<String> getAllVerbConstructsBetween1and2(){
		List<Tree> verbTrees = mTree.getVerbOnPathSubtrees();
//		List<Tree> trees = new ArrayList<Tree>();
//		for (Tree verbTree: verbTrees) {
////			List<Tree> subtrees = verbTree.subTreeList();
////			for (Tree t: subtrees) // For every subtree in verb tree
////				if (t.size() > 1) // Add all trees wiht size greater 
////					trees.add(t);
//			trees.addAll(verbTree.subTreeList());
//		}
		
		List<String> verbsAsStrings = new ArrayList<String>();
		for (Tree t: verbTrees)
			verbsAsStrings.add(t.toString());
		return verbsAsStrings;
	}

	/**
	 * 
	 * @param trees 
	 * @param granularity specific granularity
	 * @return List of trees based of granularity High granularity returns more trees
	 */
	private List<String> getNumTrees(List<Tree> trees, int granularity){
		return getNumTrees(trees, granularity, TREE_THRESHOLD);
	}
	
	/**
	 * Given a list of trees and a granularity, Produce a new list of 
	 * that has the largest k trees.  k is a value relative the granularity
	 * Larger the granularity the more trees
	 * @param trees trees to be sorted and filtered
	 * @param granularity Level of granularity desired
	 * @
	 * @return new list
	 */
	private List<String> getNumTrees(List<Tree> trees, int granularity, int threshold){
		// threshhold must be positive but maxed at the list size
		threshold = Math.min(Math.max(threshold, 1), trees.size());
		// Sort in order from most trees to least
		// Value the subtrees with largest size
		Collections.sort(trees, mComparator);
		
		// This way if we have 
		granularity = super.filterLevel(granularity);
		double scaleFactor = (double) granularity / getGranularityRange().getEnd();
		int numTrees = (int)(trees.size() * scaleFactor); // Always be less then size
		if (numTrees < threshold)
			numTrees = threshold;

		if (numTrees > trees.size())
			throw new RuntimeException("U messed up getting trees mike");

		List<String> biggestTrees = new ArrayList<String>();
		for (int i = 0; i < numTrees; i++) 
			biggestTrees.add(trees.get(i).toString());
		return biggestTrees;
	}

	/**
	 * Comparator that returns the tree with greatest size in highest priority
	 * @author mhotan
	 */
	private class TreeComparator implements Comparator<Tree> {

		@Override
		public int compare(Tree o1, Tree o2) {
			return o2.size() - o1.size();
		}

	}
	// //////////////////////////////////////////////////
	// /// Requires Pre Processing (Looking at all the training data sentences
	// before hand)
	//
	// NA


	// ////////////////////////////////////////////////////
	// // Type of Features (Represented as Sub Graphs)
	// ////////////////////////////////////////////////////

	// //////////////////////////////////////////////////
	// /// Level Dependent (Features quantities that vary at every level)

	// Sub trees of containing the each argument
	// For the lowest level of granularity we only compare the syntactic
	// relations between tokens of each argument
	// For every higher level take the sub tree rooted at the next parent
	// Max granularity will be taking the entire syntactic subtree at the root
	// of the entire tree That is for the entire relationship
	// This can be done by grabbing the internal Stanford tree and producing an
	// iterator
	// Then print out the individual values of the iterator.
	// Or create my own iterator that maintains some order (prefix, infix,
	// postfix)
	// this guarantees that nodes are visited in the same order
	//

	/**
	 * Converts a list of string names into a single path
	 * @param path
	 * @return
	 */
	public static String pathToString(List<String> path){
		StringBuffer b = new StringBuffer();
		for (int i = 0; i < path.size(); i++){
			b.append(path.get(i));
			if (i < path.size()-1)
				b.append("-");
		}
		return b.toString();
	}

	@Override
	protected void checkRep(){
		super.checkRep();
		if (mTree == null)
			throw new RuntimeException("Null graph");
	}

}
