package features.extractor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;
import java.util.TreeSet;

import nlp.StanfordNerTag;
import trainingDataGenerator.AnnotatedSentenceParser.EntityPairMention;
import trainingDataGenerator.AnnotatedSentenceParserAdapter;
import trainingDataGenerator.AnnotatedSentenceParserAdapter.AnnotationParsingException;
import trainingDataGenerator.AnnotationConstant;
import features.components.BagOfWords;
import features.components.RelationInstanceNode.ARGUMENT_IDENTIFIER;
import features.components.RelationInstanceNode.ENTITY_POS;
import features.components.sequential.SequenceRelationGraph;
import features.components.sequential.SequentialNode;
import features.components.sequential.SequentialPath;
import features.extractor.FeatureExtractorVariables.EXTERNAL_NODES_USE_TYPE;

/**
 * Derives Sequence based features at multiple granularities
 * @author mhotan
 */
public class SequenceFeatureExtractor extends GranularFeatureExtracter {

	public static final String NEXT = "->";
	public static final String NUM_MIDDLE_TOKENS = "NumMiddleTokens:";
	public static final String NUM_NER1_TOKENS = "NumNER1Tokens:";
	public static final String NUM_NER2_TOKENS = "NumNER2Tokens:";
	public static final String NUM_BEFORE_TOKENS = "NumBeforeTokens:"; // Tokens before first entity
	public static final String NUM_AFTER_TOKENS = "NumAfterTokens:"; // Tokens after last entity
	public static final String BAGOFWORDS = "BAGOFWORD_VECTOR:"; // Tokens after last entity
	public static final String BAGOFWORDS_ALL = "ALL:" + BAGOFWORDS;
	public static final String BAGOFWORDS_ARGUMENTS = "ARGUMENTS:" + BAGOFWORDS;
	public static final String BAGOFWORDS_NONARGUMENTS = "NONARGUMENTS:" + BAGOFWORDS;
	public static final String BAGOFWORDS_BEFORE = "BEFORE:" + BAGOFWORDS; // Before first occured entity
	public static final String BAGOFWORDS_AFTER = "AFTER:" + BAGOFWORDS; // Before first occured entity
	public static final String BAGOFWORDS_MIDDLE = "MIDDLE:" + BAGOFWORDS; // Before first occured entity
	public static final String BAGOFWORDS_NER1 = "NER1:" + BAGOFWORDS; // Before first occured entity
	public static final String BAGOFWORDS_NER2 = "NER2:" + BAGOFWORDS; // Before first occured entity

	public final SequentialPath mGraphCompPath, mGraphBeforePath, mGraphNER1Path, 
	mGraphMiddlePath, mGraphNER2Path, mGraphAfterPath;

	/**
	 * MH bag of words found not to be valuable
	 * A "Global mapping" of all the words that have appeared 
	 * during the training or testing session
	 */
//	private static final BagOfWords mBagOfWords = BagOfWords.getInstance();

	/**
	 * Provides a list of window ranges in descending order
	 */
	private final ArrayList<Integer> mWindowRanges;
	
	/**
	 * Creates A simpel sequence extractor
	 * @param sentence sentence to extract features
	 * @param pair Entity pair for this sentence
	 * @param wordsSeen Is a Dictionary of unique tokens to the global index of when it occured 
	 * @throws AnnotationParsingException 
	 */
	public SequenceFeatureExtractor(String sentence, EntityPairMention pair) throws AnnotationParsingException {
		this(trainingDataGenerator.AnnotationGenerator.generateAnnotation(sentence), pair);
	}

	/**
	 * 
	 * @param annotations
	 * @param pair
	 * @throws AnnotationParsingException 
	 */
	public SequenceFeatureExtractor(Map<AnnotationConstant, String> annotations,
			EntityPairMention pair) throws AnnotationParsingException{
		this(new AnnotatedSentenceParserAdapter(annotations), pair);
	}

	/**
	 * Dev Note: I create 6 complete graphs to play with.  It is really not necessary but
	 * is a robust way to search individual subgraphs
	 * @param annotations annotations extracted of a given sentence.
	 * @param pair Entity pair that contains information about the entities within the annotations
	 * @param wordsSeen Is a Dictionary of unique tokens to the global index of when it occured 
	 * @throws AnnotationParsingException 
	 */
	public SequenceFeatureExtractor(AnnotatedSentenceParserAdapter adapt,
			EntityPairMention pair) throws AnnotationParsingException{
		super(adapt, pair);

		SequenceRelationGraph mGraphComplete = SequenceRelationGraph.buildGraph(mParser, pair);
		mGraphCompPath = new SequentialPath(mGraphComplete);

		// Get the individual entity graphs
		// We needs to get the inclusive ends to these graphs
		SequentialNode excEnd1 = mGraphComplete.getNodeAfter(mGraphComplete.getTailOfEntity1());
		SequentialNode excEnd2 = mGraphComplete.getNodeAfter(mGraphComplete.getTailOfEntity2());

		// Lets break down the graph into 5 pieces
		// AAA NER BBB NER CCC
		// AAA or BBB or CCC can possibly contain no nodes
		// NER can be either 1 or 2, but both 1 or 2 are included in the appear exactly
		// once for context of the EntityPairMention pair

		SequenceRelationGraph mGraphNER1 = mGraphComplete.getSubGraph(mGraphComplete.getHeadOfEntity1(), excEnd1);
		SequenceRelationGraph mGraphNER2 = mGraphComplete.getSubGraph(mGraphComplete.getHeadOfEntity2(), excEnd2);
		mGraphNER1Path = new SequentialPath(mGraphNER1);
		mGraphNER2Path = new SequentialPath(mGraphNER2);

		SequenceRelationGraph mGraphBeforeFirstEntity;
		SequenceRelationGraph mGraphInbetweenNERs;
		SequenceRelationGraph mGraphAfterSecondEntity;
		if (invertedEntities) { // NER 2 appears before NER 1
			mGraphBeforeFirstEntity = mGraphComplete.getSubGraph(mGraphComplete.getFirstNode(), mGraphComplete.getHeadOfEntity2());
			mGraphInbetweenNERs = mGraphComplete.getSubGraph(excEnd2, mGraphComplete.getHeadOfEntity1());
			mGraphAfterSecondEntity = mGraphComplete.getSubGraph(excEnd1, null); //Empty graph 
		} else {
			mGraphBeforeFirstEntity = mGraphComplete.getSubGraph(mGraphComplete.getFirstNode(), mGraphComplete.getHeadOfEntity1());
			mGraphInbetweenNERs = mGraphComplete.getSubGraph(excEnd1, mGraphComplete.getHeadOfEntity2());
			mGraphAfterSecondEntity = mGraphComplete.getSubGraph(excEnd2, null); 
		}

		mGraphBeforePath = new SequentialPath(mGraphBeforeFirstEntity);
		mGraphMiddlePath = new SequentialPath(mGraphInbetweenNERs);
		mGraphAfterPath = new SequentialPath(mGraphAfterSecondEntity);

		// Store in order of ... 5, 4, 3, 2, 1 ...
		mWindowRanges = new ArrayList<Integer>();
		for (int i = getGranularityRange().getEnd(); i >= getGranularityRange().getStart(); i--){
			mWindowRanges.add(i);
		}
		
		// Prefix for type of feature
		// SEQ Rerpresents sequential
		PREFIX = "SEQ" + ARG_SPLITTER;
	}

	@Override
	public List<String> getFeatures() {
		// This is the same as getting features at max granularity
		return getFeatures(getGranularityRange().getEnd());
	}

	@Override
	public List<String> getFeatures(int level) {
		// Adjust level to be within range
		level = super.filterLevel(level);
		// Further filter the window size instead 

		// AAA NER BBB NER CCC

		// Get a list of which words occur in the graphs
		List<String> result = new ArrayList<String>();
		//		List<String> bagOfWords = getBagOfWords();

		// NOTE: Num tokens cluttered the feature space
		// Number of tokens between for all sub portions of the graph
		// NOTE: Num tokens features is not specific enough 
		//		String numTokensBefore = NUM_BEFORE_TOKENS + mGraphBeforeFirstEntity.getSize();
		//		String numTokensAfter = NUM_AFTER_TOKENS + mGraphAfterSecondEntity.getSize();
		//		String numTokensMiddle = NUM_MIDDLE_TOKENS + mGraphInbetweenNERs.getSize();
		//		String numTokensNER1 = NUM_NER1_TOKENS + mGraphNER1.getSize();
		//		String numTokensNER2  = NUM_NER2_TOKENS + mGraphNER2.getSize();
		//		result.add(INV_STRING + ARG_SPLITTER + numTokensBefore + ARG_SPLITTER +
		//				numTokensMiddle + ARG_SPLITTER + numTokensAfter);
		//		result.add(numTokensNER1 + ARG_SPLITTER + numTokensNER2);

		// Combinations of attributes of both arguments
		// These can be conjunct features
		// Entity NER types for argument 1 and 2
		EntityFeaturePair entityNERTypes = getEntityNERFeatures();
		EntityFeaturePair entityPOSTypes = getEntityPOSTagFeatures();
		List<EntityFeaturePair> entities = new ArrayList<EntityFeaturePair>(2);
		entities.add(entityNERTypes);
		entities.add(entityPOSTypes);

		// Maintain a list for all the paths we want to use
		List<SequentialPath> pathsBefore = new ArrayList<SequentialPath>();
		List<SequentialPath> pathsMiddle = new ArrayList<SequentialPath>();
		List<SequentialPath> pathsAfter = new ArrayList<SequentialPath>();
		
		switch (FeatureExtractorVariables.EXTERNAL_NODE_USE) {
		case EVERYTHING:
			pathsBefore.add(mGraphBeforePath);
			pathsMiddle.add(mGraphMiddlePath);
			pathsAfter.add(mGraphAfterPath);
			break;
		case CONTENTWORDS:
			pathsBefore.add(mGraphBeforePath.getJustContentWord());
			pathsMiddle.add(mGraphMiddlePath.getJustContentWord());
			pathsAfter.add(mGraphAfterPath.getJustContentWord());
			break;
		case VERBS:
			pathsBefore.add(mGraphBeforePath.getJustVerbs());	
			pathsMiddle.add(mGraphMiddlePath.getJustVerbs());
			pathsAfter.add(mGraphAfterPath.getJustVerbs());
			break;
		}

		// The amount of paths should correlate which as a result should have the 
		// same size
		assert pathsBefore.size() == pathsMiddle.size() && pathsMiddle.size() == pathsAfter.size();
		int size = pathsAfter.size();

		List<String> before = new ArrayList<String>(size*level);
		List<String> middle = new ArrayList<String>(size*level);
		List<String> after = new ArrayList<String>(size*level);

		// Using a queue will dequeue in right order
		Queue<Integer> windowQ = new LinkedList<Integer>(mWindowRanges);
		int index = 0;
		// Remove window size in sequential descending order
		while (index < level){
			// Attempt to extract next window size
			Integer i = windowQ.poll();
			if (i== null) break;
			// Get a window size for each region 
			// Ensure the windows are reversed
			for (SequentialPath beforePath: pathsBefore){
				before.add(beforePath.getLemmaPathReverse(i));
				before.add(beforePath.getNERPathReverse(i));
				before.add(beforePath.getPosPathReverse(i));
			}
			for (SequentialPath middlePath: pathsMiddle) {
				middle.add(middlePath.getLemmaPath());
				middle.add(middlePath.getNERPath());
				middle.add(middlePath.getPosPath());
			}
			for (SequentialPath afterPath: pathsAfter){
				after.add(afterPath.getLemmaPath(i));
				after.add(afterPath.getNERPath(i));
				after.add(afterPath.getPosPath(i));
			}
			// After increment which window we are on
			index++;
		}

		assert before.size() == middle.size() && middle.size() == after.size();
		size = before.size();

		for (int i = 0; i < size; i++){
			String beforeStr = before.get(i);
			String middleStr = middle.get(i);
			String afterStr = after.get(i);
			for (EntityFeaturePair e: entities){
				result.add(INV_STRING + ARG_SPLITTER + beforeStr + ARG_SPLITTER + e.get1() + ARG_SPLITTER 
						+ middleStr + ARG_SPLITTER + e.get2() + ARG_SPLITTER + afterStr);
			}
		}

		return result;
	}

	/**
	 * Gets the POS tag representation
	 * @return features for both entities relating there POS tag.
	 */
	private EntityFeaturePair getEntityPOSTagFeatures(){
		String entity1 = mGraphNER1Path.getPosPath();
		String entity2 = mGraphNER2Path.getPosPath();
		return new EntityFeaturePair(entity1, entity2);

	}

	/**
	 * Returns Structure of the NER tags of both Entities
	 * @return Enitity 1 first in the list Entity 2 in the list
	 */
	private EntityFeaturePair getEntityNERFeatures(){
		String entity1 = mGraphNER1Path.getNERPath();
		String entity2 = mGraphNER2Path.getNERPath();
		return new EntityFeaturePair(entity1, entity2);
	}	
}

