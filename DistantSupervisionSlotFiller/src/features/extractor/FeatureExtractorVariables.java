package features.extractor;

/**
 * Global variables that determine certain feature traits for feature extractors
 * @author mhotan
 */
public class FeatureExtractorVariables {

	// Sequence Settings

	/**
	 * Mutable Reference that can very the 
	 */
	public static EXTERNAL_NODES_USE_TYPE EXTERNAL_NODE_USE = EXTERNAL_NODES_USE_TYPE.EVERYTHING;
	
	/**
	 * Determines the types of path nodes to exlude
	 * @author mhotan
	 */
	public enum EXTERNAL_NODES_USE_TYPE { EVERYTHING("Seq-AllWords",0), 
		VERBS("Seq-Verbs",1), CONTENTWORDS("Seq-ContentWords",2); 

	private final String mText;
	private final int mIndex;
	
	private EXTERNAL_NODES_USE_TYPE(String text, int index){
		mText = text;
		mIndex = index;
	}

	public String toString(){
		return mText;
	}
	
	public int getIndex(){
		return mIndex;
	}
	
	public static EXTERNAL_NODES_USE_TYPE valueOf(int i){
		switch (i){
		case 1: return VERBS;
		case 2: return CONTENTWORDS;
		default: return EVERYTHING;
		}
	}
	}

	/**
	 * Simple to 
	 */
	public static final EXTERNAL_NODES_USE_TYPE[] SEQUENTIAL_USE_TYPES = {
		EXTERNAL_NODES_USE_TYPE.EVERYTHING, 
		EXTERNAL_NODES_USE_TYPE.CONTENTWORDS,
		EXTERNAL_NODES_USE_TYPE.VERBS };


	// Syntactic Variations
	      
	/**
	 * Determines the inclusion of occlusion of verbs 
	 */
	public static USE_VERB_VARIABLE VERB_USE = USE_VERB_VARIABLE.NO_VERBS_USED;
	
	public enum USE_VERB_VARIABLE { USE_VERBS("Syn-Verbs",1), NO_VERBS_USED("Syn-NoVerbs",0);

	private final String mText;
	private final int mIndex;
	
	private USE_VERB_VARIABLE(String text, int index){
		mText = text;
		mIndex = index;
	}

	public String toString(){
		return mText;
	}
	
	public int getIndex(){
		return mIndex;
	}
	
	public static USE_VERB_VARIABLE valueOf(int i){
		switch (i){
		case 1: return USE_VERBS;
		default: return NO_VERBS_USED;
		}
	}
	}
	
	public static final USE_VERB_VARIABLE[] USE_VERB_VARIABLE_TYPES = {
		USE_VERB_VARIABLE.USE_VERBS, USE_VERB_VARIABLE.NO_VERBS_USED};

}
