package features.extractor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import trainingDataGenerator.AnnotatedSentenceParser.EntityPairMention;
import trainingDataGenerator.AnnotatedSentenceParserAdapter;
import trainingDataGenerator.AnnotatedSentenceParserAdapter.AnnotationParsingException;
import trainingDataGenerator.AnnotationConstant;
import features.components.dependency.DependencyNode;
import features.components.dependency.DependencyPath;
import features.components.dependency.DependencyRelationGraph;

/**
 * Base class to extract grammatical dependency relations.
 * 
 */
public class DependencyFeatureExtractor extends GranularFeatureExtracter {

	private static final String TAG = DependencyFeatureExtractor.class.getSimpleName();
	
	/**
	 * Signifies if the use of the actual word is intended to be used
	 */
	private static final boolean USE_LEMMA = true;
	
	/**
	 * 
	 */
	private static final String PREFIX_TAG = "DEP";

	/**
	 * Create fields for all individual
	 * Assigned public visibility for testing purposes
	 * TODO: Assign private again
	 */
	public final DependencyRelationGraph mGraph;

	private enum WHICH_ENTITY {ONE, TWO}

	/**
	 * Extracts the annotations with to create feature extractor
	 * @param setnence
	 * @param mention
	 * @throws AnnotationParsingException 
	 */
	public DependencyFeatureExtractor(String sentence, EntityPairMention mention) throws AnnotationParsingException{
		this(trainingDataGenerator.AnnotationGenerator.generateAnnotation(sentence), mention);
	}
	
	public DependencyFeatureExtractor(Map<AnnotationConstant, String> annotations, 
			EntityPairMention mention, boolean debug) throws AnnotationParsingException {
		this(new AnnotatedSentenceParserAdapter(annotations), mention, debug);
	}
	
	public DependencyFeatureExtractor(Map<AnnotationConstant, String> annotations, 
			EntityPairMention mention) throws AnnotationParsingException {
		this(new AnnotatedSentenceParserAdapter(annotations), mention);
	}
	
	public DependencyFeatureExtractor(AnnotatedSentenceParserAdapter adapt, 
			EntityPairMention mention) throws AnnotationParsingException {
		this(adapt, mention, false);
	}
	
	/**
	 * Given a mapping of annotations for a given sentence and an entity pair mention 
	 * this constructs a feature extractor for dependency feature occurences
	 * @param annotations
	 * @param mention
	 * @throws AnnotationParsingException 
	 */
	public DependencyFeatureExtractor(AnnotatedSentenceParserAdapter adapt, 
			EntityPairMention mention, boolean DEBUG) throws AnnotationParsingException {
		super(adapt, mention);
		mGraph = DependencyRelationGraph.buildGraph(mParser, mention, DEBUG);
		PREFIX = PREFIX_TAG + ARG_SPLITTER;
	}

	@Override
	public List<String> getFeatures() {
		return getFeatures(getGranularityRange().getEnd());
	}

	@Override
	public List<String> getFeatures(int level) {

		String inv = INV_STRING;
		////////////////////////////////////////////////////
		///// Level Independent (Features that are included at all levels)

		
		// Different types of Dependency Path between argument 1 and 2
		// And the paths from the root to to the heads
		// If none exist mark as NA
		DependencyPath pathBetweenEntities = getPathBetweenEntities();
		List<DependencyPath> middle = new ArrayList<DependencyPath>();
		middle.add(pathBetweenEntities);
		
		/*
		 * NOTE: For the middle path represetnation for the conjunction 
		 * I decided to only include the complete path between 1 and 2 
		 * if one exists.  Further experimentation should be conducted to see if 
		 * subpartitions of the path would be valuable
		 * 
		 * As of right now I left it out for simplicity
		 */
//		DependencyNode root = mGraph.getRoot();
//		if (root != null){
//			// Get the individual Dependency Routes from middle to 1 and 2
//			DependencyNode e1Head = mGraph.getEntity1Head();
//			DependencyNode e2Head = mGraph.getEntity2Head();
//			DependencyPath RootTo1 = mGraph.getDependencyPath(root, e1Head);
//			DependencyPath RootTo2 = mGraph.getDependencyPath(root, e2Head);
//			middle.add(RootTo1);
//			middle.add(RootTo2);
//		}
		
		////////////////////////////////////////////////////
		///// Level Dependent (Features quantities that vary at every level)

		// getGranularPathsFor will return atleast the path from the head of one to the tail
		// If no such path exist it will return the largest it can
		// Therefore no extra steps are needed to find those
		
		// The type of dependencies 
		// Windows to the right of the farthest right entity
		List<DependencyPath> granular1 = getGranularPathsFor(level, WHICH_ENTITY.ONE);
		// Windows of the left entity
		List<DependencyPath> granular2 = getGranularPathsFor(level, WHICH_ENTITY.TWO);
		
		// Container for all the results
		List<String> result = new ArrayList<String>();
		
		// For every permutation which describes
		for (DependencyPath e1: granular1){
			for (DependencyPath e2: granular2){
				for (DependencyPath m: middle){
					if (USE_LEMMA){
						result.add(inv + ARG_SPLITTER + e1.getNERPath() + ARG_SPLITTER + m.getLemmaPath() + ARG_SPLITTER + e2.getNERPath());
						result.add(inv + ARG_SPLITTER + e1.getPosPath() + ARG_SPLITTER + m.getLemmaPath()  + ARG_SPLITTER + e2.getPosPath());
					}
					result.add(inv + ARG_SPLITTER + e1.getNERPath() + ARG_SPLITTER + m.getNERPath() + ARG_SPLITTER + e2.getNERPath());
					result.add(inv + ARG_SPLITTER + e1.getPosPath() + ARG_SPLITTER + m.getPosPath() + ARG_SPLITTER + e2.getPosPath());
					result.add(inv + ARG_SPLITTER + e1.getPosPath() + ARG_SPLITTER + m.getNERPath() + ARG_SPLITTER + e2.getPosPath());
				}
			}
		}
		
		return result;
	}

	/**
	 * Based on the argument level of granularity return a list of dependency paths that is represents 
	 * the entity approaching the root.  If level of granularity is to large 
	 * @param level Level of granularity desired. Granularity max means grabbing all the paths and sub paths 
	 * from the entity to the root.
	 * @return list of paths
	 */
	public List<DependencyPath> getGranularPathsFor(int granularity, WHICH_ENTITY w){
		// This way if we have 
		granularity = super.filterLevel(granularity);
		List<DependencyPath> result = new ArrayList<DependencyPath>();
		// Strategy
		// Each entity has a X number of dependency paths which originate at the head 
		// Where X is >= 1
		// For both entities 
		// Find the variable amount of paths to use
		// Where a granularity of 10 will use all the sizes and
		// And a granularity of 1 will use the path with the largest size
		
		List<DependencyPath> ent;
		switch (w) { 
		case ONE: ent = mGraph.getEntity1Paths();
		break;
		case TWO: 
		default:
			ent = mGraph.getEntity2Paths();
		}
		
		// Sort the Paths putting max on top
		Collections.sort(ent, new Comparator<DependencyPath>() {
			@Override
			public int compare(DependencyPath o1, DependencyPath o2) {
				// Large dependency paths are prioritized
				return o2.getSize() - o1.getSize();
			}
		});
		
		// Find out whats the maximum number of paths to return
		// Find out what the minimum 
		// Return a factor equal to or between based on granularity
		int choices = ent.size() - 1; 
		// choices should never be less then 0 but we can put a check in there regardless
		if (choices<0){ // There was onyl one element in the graph
			result.addAll(ent);
//			util.Logger.write(TAG+" No paths found for entity: "+w+ "\n", true);
			return result;
		}

		// Get the scale factor based off number of choices and absolute range of granularities
		int rangeMagnitude = getGranularityRange().getEnd() - getGranularityRange().getStart();
		float scaleFactor = choices / (float)rangeMagnitude;
		float amountToUseFlt = scaleFactor*granularity + 1; // Must use atleast one element
		int toUse = Math.round(amountToUseFlt);
		
		for (int i = 0; i < toUse && i < ent.size(); ++i){
			result.add(ent.get(i));
		}
		
		return result;
	}

	/**
	 * @return entity2 dependency path
	 */
	public List<DependencyPath> getEntity2Dependencies(){
		return new ArrayList<DependencyPath>(mGraph.getEntity2Paths());
	}

	/**
	 * @return entity1 dependency path
	 */
	public List<DependencyPath> getEntity1Dependencies(){
		return new ArrayList<DependencyPath>(mGraph.getEntity1Paths());
	}

	/**
	 * @return Dependency Path that lie inbetween entities
	 */
	public DependencyPath getPathBetweenEntities(){
		return mGraph.getDependencyPathForEntities();
	}

	/**
	 * For two entities within a sentence it is possible to have a grammatical relationship
	 * path between them.  This method returns a size for the path between the heads
	 * of two entities in a sentence.  IE if the entities resided at the same head word, then 
	 * the path would have one element but its size would be 0.
	 * @return the dependency path length between two entities, -1 if non exist
	 */
	public int getPathLengthBetweenEntities(){
		return mGraph.getDependencyPathForEntities().getSize();
	}

	////////////////////////////////////////////////////
	///// Requires Pre Processing (Looking at all the training data sentences before hand)
	//	
	//  NA

	//////////////////////////////////////////////////////
	////  Type of Features (Represented as Sub Graphs)
	//////////////////////////////////////////////////////






}
