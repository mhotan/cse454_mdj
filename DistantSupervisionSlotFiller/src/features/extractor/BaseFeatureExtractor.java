package features.extractor;

/**
 * Feature Extractor template that other classes an use to  
 * @author mhotan
 */
public abstract class BaseFeatureExtractor {

	/**
	 * Given a sequence of tokens returns in the form of a string extract all the features 
	 * and return to the user
	 * @param seqTokens Sequence of tokens to 
	 * @return All the features of this class
	 */
	public abstract Features extractFeatures(String seqTokens);
	
	/**
	 * Class that is used to represent all the features of a 
	 * given sequence of tokens
	 * @author mhotan
	 */
	protected class Features {
		
	}
	
}
