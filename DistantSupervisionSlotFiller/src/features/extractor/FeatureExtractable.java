package features.extractor;

import java.util.List;

/**
 * Simple interface for extracting features
 * @author mhotan
 */
public interface FeatureExtractable {

	public List<String> getFeatures();
	
}
