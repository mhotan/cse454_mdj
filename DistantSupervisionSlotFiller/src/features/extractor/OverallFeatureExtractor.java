package features.extractor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import trainingDataGenerator.AnnotatedSentenceParser.EntityPairMention;
import trainingDataGenerator.AnnotatedSentenceParserAdapter;
import trainingDataGenerator.AnnotatedSentenceParserAdapter.AnnotationParsingException;
import trainingDataGenerator.AnnotationConstant;
import features.oldstuff.FeatureExtractor;
import features.oldstuff.GranularExtractorWrapper;
import features.oldstuff.ModdedRelationECML;

/**
 * Combines all three feature extractor (Sequence, Syntactic, Dependencies)
 * Extracts all the features with or without a level for all three feature spaces
 * @author mhotan
 */
public class OverallFeatureExtractor extends GranularFeatureExtracter {

	/**
	 * Create a new feature extractor based on the provided command
	 * format: 1. If use Congle's features, command = "ecml"
	 *    2. If use Mike's awesome features,  command = "mdj_1_2_3", where 1,2 and 3
	 *    	correspond to syntacticLvl, sequentialLvl, and dependencyLvl respectively
	 */
	public static FeatureExtractor getFeatureExtractor(String command){
		try{
			String[] tokens = command.split("_");
			if(tokens[0].trim().equals("ecml")){
				return new ModdedRelationECML();
			}else if(tokens[0].equals("mdj")){
				int syn = Integer.parseInt(tokens[1]);
				int seq = Integer.parseInt(tokens[2]);
				int dep = Integer.parseInt(tokens[3]);
				if (tokens.length > 4 && tokens.length <= 6){
					FeatureExtractorVariables.EXTERNAL_NODE_USE = 
							FeatureExtractorVariables.EXTERNAL_NODES_USE_TYPE.valueOf(Integer.parseInt(tokens[4]));
					FeatureExtractorVariables.VERB_USE = 
							FeatureExtractorVariables.USE_VERB_VARIABLE.valueOf(Integer.parseInt(tokens[5]));
				}
				return new GranularExtractorWrapper(syn, seq, dep);
			}
			throw new IllegalArgumentException("Command is of the wrong form : " + command);
			
		}catch(Exception e){
			System.out.println("Usage : 1. If use Congle's features, command = \"ecml\""
					+  "  2. If use Mike's awesome features,  command = \"mdj_1_2_3\", where 1,2 and 3"
					+ " correspond to syntacticLvl, sequentialLvl, and dependencyLvl respectively ");
			throw e;
		}
	}

	private final SequenceFeatureExtractor mSeqExtractor;
	private final SyntacticFeatureExtractor mSynExtractor;
	private final DependencyFeatureExtractor mDepExtractor;

	/**
	 * Given the pre constructed annotations prepares the contained extractors
	 * @param annotations annotation that has all the stanford data
	 * @param pair Entities that are contained in the argument annotations
	 * @throws AnnotationParsingException 
	 */
	public OverallFeatureExtractor(AnnotatedSentenceParserAdapter adpater, EntityPairMention pair) throws AnnotationParsingException{
		super(adpater, pair);
		mSeqExtractor = new SequenceFeatureExtractor(mParser, pair);
		mSynExtractor = new SyntacticFeatureExtractor(mParser, pair);
		mDepExtractor = new DependencyFeatureExtractor(mParser, pair);
		PREFIX = "";
	}

	/**
	 * Given a sentence the annotations are generated with
	 * trainingDataGenerator.AnnotationGenerator.generateAnnotation and 
	 * @param sentence sentence to be parsed and the used for extraction
	 * @param pair Entities that are contained in the argument sentence
	 * @throws AnnotationParsingException 
	 */
	public OverallFeatureExtractor(String sentence, EntityPairMention pair) throws AnnotationParsingException{
		this(trainingDataGenerator.AnnotationGenerator.generateAnnotation(sentence), pair);
	}

	/**
	 * Given the pre constructed annotations prepares the contained extractors
	 * @param annotations annotation that has all the stanford data
	 * @param pair Entities that are contained in the argument annotations
	 * @throws AnnotationParsingException 
	 */
	public OverallFeatureExtractor(Map<AnnotationConstant, String> annotations,
			EntityPairMention pair) throws AnnotationParsingException{
		super(annotations, pair);
		mSeqExtractor = new SequenceFeatureExtractor(annotations, pair);
		mSynExtractor = new SyntacticFeatureExtractor(annotations, pair);
		mDepExtractor = new DependencyFeatureExtractor(annotations, pair);
		PREFIX = "";
	}



	@Override
	public List<String> getFeatures() {
		return getFeatures(getGranularityRange().getEnd());
	}

	@Override
	public List<String> getFeatures(int level) {
		return getFeatures(level, level, level);
	}

	/**
	 * Allows the use of specific levels of granularity for specific extractors
	 * @param syntacticLvl level of granularity desired for syntactic extractor
	 * @param sequentialLvl level of granularity desired for sequential extractor
	 * @param dependencyLvl level of granularity desired for dependency extractor
	 * @return Collective features from all extractors
	 */
	public List<String> getFeatures(int syntacticLvl, int sequentialLvl, int dependencyLvl){
		List<String> result = new ArrayList<String>();
		result.addAll(mSeqExtractor.getFeatures(sequentialLvl));
		result.addAll(mSynExtractor.getFeatures(syntacticLvl));
		result.addAll(mDepExtractor.getFeatures(dependencyLvl));
		return result;
	}

}
