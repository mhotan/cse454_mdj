package features.oldstuff;

import java.util.List;

import nlp.StanfordNerTag;
import trainingDataGenerator.AnnotatedSentenceParser;
import trainingDataGenerator.AnnotatedSentenceParser.GrammaticalDependency;
import trainingDataGenerator.AnnotatedSentenceParser.Range;
import trainingDataGenerator.AnnotatedSentenceParser.Tokens;
import trainingDataGenerator.AnnotatedSentenceParserAdapter;
import trainingDataGenerator.AnnotatedSentenceParserAdapter.AnnotationParsingException;

/**
 * a wrapper on top of Congle's RelationECML feature extractor class
 * to have a different interface, which should hopefully
 * be propagated to the other feature classes.
 * The goal of this is to  of using StanfordNLP at all
 * @author sjonany
 */
public class ModdedRelationECML implements FeatureExtractor{
	private RelationECML baseFeatureExtractor;
	
	public ModdedRelationECML(){
		baseFeatureExtractor = new RelationECML();
	}
	
	/**
	 * @param annotations everything linguistic you need to know about the sentence should be here
	 * @see AnnotatedSentenceParser for an idea of the available annotation values
	 * @return
	 * @throws AnnotationParsingException 
	 */
	public List<String> getFeatures(AnnotatedSentenceParserAdapter annotations, Range range1, Range range2) throws AnnotationParsingException{
		//sentence id
		Tokens tokensParsed = annotations.getTokens();
		int sentenceId = Integer.parseInt(tokensParsed.globalSentenceId);
		
		//tokens[]
		String[] tokens = tokensParsed.tokens.toArray(new String[0]);
		
		//postags[]
		String[] postags = annotations.getStanfordpos().pos.toArray(new String[0]);
		
		//arg1pos[] and arg2pos[]-> btw this should really be of type 'Range'
		//instead of just an array of size 2 
		int[] arg1Pos = new int[2];
		int[] arg2Pos = new int[2];
		
		arg1Pos[0] = range1.getStart();
		arg1Pos[1] = range1.getEnd();		
		arg2Pos[0] = range2.getStart();
		arg2Pos[1] = range2.getEnd();
		
		//arg1ner and arg2ner
		List<StanfordNerTag> ner = annotations.getStanfordner().ner;
		
		//TODO: sometimes token span has different ner's. how to resolve?
		String arg1ner = ner.get(range1.getEnd()-1).toString();
		String arg2ner = ner.get(range2.getEnd()-1).toString();
		
		//For the below two fields, Congle's representation is incomplete.
		//int[] depParents,
		//String[] depTypes
		//this assumes that the kth token only depends on another, single token
		//but xiao ling's representation is more dependable, because it takes
		//into account that the first token can depend on the second token, 
		//and also the sixth token at the same time.
		List<GrammaticalDependency> dependencies = annotations.getDeptCCProcessed().dependencies;
		int[] depParents = new int[tokens.length];
		String[] depTypes = new String[tokens.length];
		
		for(int i=0; i<depParents.length; i++){
			//default value = depends on ROOT, aka, no dependency
			depParents[i] = -1;
			depTypes[i] = "ROOT";
		}
		
		//again, same caveat as the above chunk of text,
		//there might be more than one dependencies occuring on the same token
		//so things might get overridden here
		for(GrammaticalDependency dep : dependencies){
			depParents[dep.dependent] = dep.governor;
			depTypes[dep.dependent] = dep.dependencyType;
		}
		
		return baseFeatureExtractor.getFeatures(sentenceId, tokens, postags, depParents, depTypes, arg1Pos, arg2Pos, arg1ner, arg2ner);
	}
}
