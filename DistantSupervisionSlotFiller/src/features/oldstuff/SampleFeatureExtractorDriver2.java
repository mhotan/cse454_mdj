package features.oldstuff;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import knowledgeBase.Entity;
import knowledgeBase.RelationInstance;
import knowledgeBase.Relationship;
import trainingDataGenerator.AnnotatedSentenceParser.EntityPairMention;
import trainingDataGenerator.AnnotatedSentenceParser.Range;
import trainingDataGenerator.AnnotatedSentenceParserAdapter;
import trainingDataGenerator.AnnotatedSentenceParserAdapter.AnnotationParsingException;
import edu.stanford.nlp.ling.Label;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.IntPair;

public class SampleFeatureExtractorDriver2 {

	/**
	 * @param args
	 * @throws AnnotationParsingException 
	 */
	public static void main(String[] args) throws AnnotationParsingException {

		ModdedRelationECML featureExtractor = new ModdedRelationECML();
		String sentence = "KBR Inc (formerly Kellogg Brown & Root) is an American engineering, " +
				"construction and private military contracting company, formerly a subsidiary of Halliburton, headquartered in Houston, Texas";
		
		String sentence2 = "hundreds of Palestinians converged on the square";
		
		AnnotatedSentenceParserAdapter adapter = new AnnotatedSentenceParserAdapter(sentence);
		List<String> tokens = adapter.getTokens().tokens;
		
		Range range1 = new Range(0, 2);
		Range range2 = new Range(tokens.size()-3, tokens.size());
		Entity entity1 = Entity.getEntityFromMid("KBR Inc");
		Entity entity2 = Entity.getEntityFromMid("Houston, Texas");
		Relationship r = Relationship.PLACE_OF_DEATH;

		EntityPairMention mention = new EntityPairMention();
		mention.tokenRange1 = range1;
		mention.tokenRange2 = range2;
		mention.relInstance = new RelationInstance(entity1, entity2, r);

		System.out.println("Generating annotations...");

		
		Tree tree = adapter.getCJ().parseTree;
		tree.indentedXMLPrint();
		printWithLabels(tree);
//		adapter.getCJ().parseTree.indentedXMLPrint();
//		adapter.getCJ().parseTree.indentedListPrint();
//		
//		Tree t = adapter.getCJ().parseTree;
//		t.indexLeaves();
//		printLeaves(t);
//		
//		DeptCCProcessed deps = adapter.getDeptCCProcessed();
//		deps.dependencies
//		
//		
//		System.out.println("Text: " + adapter.getText());
////		System.out.println("Meta: " + adapter.getMeta());
//		System.out.println("Tokens: " + adapter.getTokens());
//		System.out.println("Stanford Lemma: " + adapter.getStanfordlemma());
////		System.out.println("TokenSpans: " + adapter.getTokenSpans());
//		System.out.println("Stanfordpos: " + adapter.getStanfordpos());
//		System.out.println("Stanfordner: " + adapter.getStanfordner());
////		System.out.println("Wikification: " + adapter.getWikification());
//		
//		System.out.println("CJ: " + adapter.getCJ());
////		System.out.println("EntityPairMention: " + adapter.getEntityPairMention());
//		System.out.println("DeptCCProcessed: " + adapter.getDeptCCProcessed());
	}

	public static void printLeaves(Tree tree) {
		int i = 0;
		for (Tree leaf: tree.getLeaves()) {
			System.out.println(i + ". " + leaf.value() + " Num Parents children: ");
			++i;
		}
	}
	
	/**
	 * Trying to figure what the F is going on in Stanford CJ parser
	 * @param tree
	 */
	public static void printTree(Tree tree){
		recPrintTree(tree, 0);
	}
	
	public static void printWithLabels(Tree tree){
		recPrintTree2(tree, 0, "Root:");
	}
	
	private static void recPrintTree2(Tree tree, int level, String prefix) {
		if (tree == null) return;
		StringBuffer b = new StringBuffer();
		for (int i = 0; i < level; i++)
			b.append("  ");
		b.append(prefix);
		b.append("<");
		b.append(tree.value());
		b.append(">");
		System.out.println(b.toString());
		List<Tree> children = tree.getChildrenAsList();
		for (int i = 0; i < children.size(); i++){
			Tree child = children.get(i);
			if (i == 0)
				recPrintTree2(child, level+1, "L:");
			else if (i == 1)
				recPrintTree2(child, level+1, "R:");
			else 
				recPrintTree2(child, level+1, "Unknown:");
		}
	}

	/**
	 * 
	 * @param tree
	 * @return
	 */
	public static void recPrintTree(Tree tree, int depth) {
		if (tree == null) return;
		String labels = "(";
		for (Label l: tree.labels()) {
			labels += l.value() + ", ";
		}
		if (!tree.labels().isEmpty())
			labels = labels.substring(0, labels.length()-2);
		labels += ")";
		
//		String booleanVals = "(";
//		booleanVals += "Is Leaf:" + tree.isLeaf();
//		booleanVals += ", Is Phrasal:" + tree.isPhrasal();
//		booleanVals += ", Is Pre-pre-terminal:" + tree.isPrePreTerminal();
//		booleanVals += ", Is Preterminal:" + tree.isPreTerminal();
//		booleanVals += "Is Rewrite:" + tree.isUnaryRewrite();
//		booleanVals += ")";
//		
		
		IntPair span = tree.getSpan();
		
		if (tree.isPreTerminal()) {
			System.out.println("Preterminal: " + tree.label() + 
					"Sentence Index: " + getLabelIndex(tree));
		}
		
//		tree.is
//		if(tree.isLeaf()){
//			System.out.println("Label: " + tree.label() + 
//					"Sentence Index: " + getLabelIndex(tree));
//		}
		
//		System.out.println("[Node depth: " + depth + " label:" + tree.label() + 
//				" value:" + tree.value() + booleanVals + " It Span:" + span + "]");
		for (Tree t: tree.getChildrenAsList())
			recPrintTree(t, depth+1);
	}
	
	/**
	 * Only intended to 
	 * Does a ad hoc way of parsing out the index of from the label and then
	 * decrementing the index by 1 to make is base 0
	 * @param label Label that is to be parsed.
	 * @return base 0 index of the label, or -1 if is not Leaf 
	 */
	private static int getLabelIndex(Tree leaf){
		if (!leaf.isLeaf()) throw new IllegalArgumentException("Tree is not leaf");
		Pattern pattern = Pattern.compile("IndexAnnotation=[1-9][0-9]*");
		Matcher matcher = pattern.matcher(leaf.label().toString());
		if (!matcher.find()) return -1;
		String s = matcher.group();
		String[] array = s.split("=");
		// Should be of size two
		// Last element should number
		return Integer.parseInt(array[array.length-1]) - 1;
	}
	
}
