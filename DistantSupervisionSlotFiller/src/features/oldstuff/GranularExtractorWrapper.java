package features.oldstuff;

import java.util.List;

import features.extractor.OverallFeatureExtractor;

import trainingDataGenerator.AnnotatedSentenceParser.EntityPairMention;
import trainingDataGenerator.AnnotatedSentenceParser.Range;
import trainingDataGenerator.AnnotatedSentenceParserAdapter;
import trainingDataGenerator.AnnotatedSentenceParserAdapter.AnnotationParsingException;

/**
 * Wrapper on top of mike's feature extractors
 * @author sjonany
 */
public class GranularExtractorWrapper implements FeatureExtractor{
	private int syntacticLvl;
	private int sequentialLvl;
	private int dependencyLvl;
	
	public GranularExtractorWrapper(int syntacticLvl, int sequentialLvl, int dependencyLvl){
		this.sequentialLvl = sequentialLvl;
		this.syntacticLvl = syntacticLvl;
		this.dependencyLvl = dependencyLvl;
	}
	
	@Override
	public List<String> getFeatures(AnnotatedSentenceParserAdapter annotations,
			Range range1, Range range2) throws AnnotationParsingException {
		EntityPairMention pair = new EntityPairMention(null, null, range1, range2);
		OverallFeatureExtractor extractor = new OverallFeatureExtractor(annotations, pair);
		return extractor.getFeatures(syntacticLvl, sequentialLvl, dependencyLvl);
	}
}
