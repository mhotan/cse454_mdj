package features.oldstuff;

import java.util.List;

import trainingDataGenerator.AnnotatedSentenceParser;
import trainingDataGenerator.AnnotatedSentenceParserAdapter;
import trainingDataGenerator.AnnotatedSentenceParser.Range;
import trainingDataGenerator.AnnotatedSentenceParserAdapter.AnnotationParsingException;

public interface FeatureExtractor {	
	/**
	 * @param annotations everything linguistic you need to know about the sentence should be here
	 * @see AnnotatedSentenceParser for an idea of the available annotation values
	 * @return
	 */
	public List<String> getFeatures(AnnotatedSentenceParserAdapter annotations, Range range1, Range range2) throws AnnotationParsingException;
}
