package features.oldstuff;

import java.util.List;
import java.util.Map;
import java.util.Scanner;

import trainingDataGenerator.AnnotatedSentenceParser.Range;
import trainingDataGenerator.AnnotatedSentenceParserAdapter;
import trainingDataGenerator.AnnotatedSentenceParserAdapter.AnnotationParsingException;
import trainingDataGenerator.AnnotationConstant;

/**
 * A sample interactive driver for extracting features in a sentence
 * @author sjonany
 */

/*
 sample input to copy paste:
 
 
 
 ALAN E. SHAMEER Senior Industry Economist Rinfret Associates New York, Dec. 9, 1986
 0:5
 8:10
 
 This is what Congle's annotation looks like for the above sentence
    [ALAN, E., SHAMEER, Senior, Industry, Economist, Rinfret, Associates, New, York, ,, Dec., 9, ,, 1986]
	[NNP, NNP, NNP, NNP, NNP, NNP, NNP, NNPS, NNP, NNP, ,, NNP, CD, ,, CD]
	[4, 4, 4, 4, 7, 7, 7, 9, 9, -1, 9, 14, 11, 14, 9]
	[NAME, NAME, NAME, NAME, NAME, NAME, NAME, NAME, NAME, ROOT, P, NMOD, NMOD, P, APPO]
	[0, 5] ORGANIZATION
	[8, 10] LOCATION
	4
	9
	
The AnnotationGenerator combined with ModdedRelationECML gives
[ALAN, E., SHAMEER, Senior, Industry, Economist, Rinfret, Associates, New, York, ,, Dec., 9, ,, 1986]
[NNP, NNP, NNP, NNP, NNP, NNP, NNP, NNPS, NNP, NNP, ,, NNP, CD, ,, CD]
[4, 4, 4, 4, -1, 8, 8, 8, 4, 11, -1, 4, 11, -1, 11]
[nn, nn, nn, nn, root, nn, nn, nn, dep, nn, ROOT, dep, num, ROOT, num]
[0, 5] PERSON
[8, 10] LOCATION

 */
public class SampleFeatureExtractorDriver {
	public static void main(String[] args) throws AnnotationParsingException{
		//TODO: change this to whatever feature extractor you want
		ModdedRelationECML featureExtractor = new ModdedRelationECML();
		
		Scanner reader = new Scanner(System.in);
		System.out.println("Please input a sentence to extract features from: ");
		String sentence = reader.nextLine();
		System.out.println("Please input the token range of the first argument (0 based, e.g. 0:3) :");
		Range range1 = Range.fromString(reader.nextLine().trim());
		System.out.println("Please input the token range of the second argument (0 based, e.g. 0:3) :");
		Range range2 = Range.fromString(reader.nextLine().trim());
		
		System.out.println("Generating annotations...");
		Map<AnnotationConstant, String> annotations = trainingDataGenerator.AnnotationGenerator.generateAnnotation(sentence);
		
		System.out.println("Extracting features...");
		List<String> features = featureExtractor.getFeatures(new AnnotatedSentenceParserAdapter(annotations), range1 ,range2);
		
		System.out.println("Here are the features of this sentence:");
		for(String f : features){
			System.out.println(f);
		}
		
		
		
		reader.close();
	}
}
