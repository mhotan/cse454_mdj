package benchmark;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import knowledgeBase.KnowledgeBaseConstant;
import knowledgeBase.OfflineWikiFreebaseMapper;
import knowledgeBase.WikiFreebaseMapper;
import sf.SFConstants;
import sf.SFEntity;
import sf.SFEntity.Reasoning;
import sf.SFEntity.SingleAnswer;
import sf.eval.SFScore;
import sf.filler.Filler;
import sf.filler.distantsupervision.CauseOfDeathFiller;
import sf.filler.distantsupervision.CityOfBirth;
import sf.filler.distantsupervision.CityOfDeath;
import sf.filler.distantsupervision.CountryOfBirth;
import sf.filler.distantsupervision.Religion;
import sf.filler.distantsupervision.StateProvinceOfBirth;
import sf.filler.distantsupervision.StateProvinceOfDeath;
import tackbp.KbpConstants;
import trainingDataGenerator.AnnotatedCorpusReader;
import trainingDataGenerator.AnnotationConstant;
import trainingDataGenerator.TrainingConfig;
import util.FileUtil;
import classifier.Classifier;
import classifier.Result;
import features.extractor.OverallFeatureExtractor;
import features.oldstuff.FeatureExtractor;

/**
 * Modified Assignment1 for distant supervision based slot filler
 * 
 * @author Denny
 */

public class BenchmarkDriver {
	private static final boolean DEBUG = true;
	private boolean mRun;
	private List<Filler> fillers;

	public static void main(String[] args) throws Exception {
		if (args.length != 2) {
			printUsage();
			return;
		}

		BenchmarkDriver bd = new BenchmarkDriver(args);
		// bd.runWithSpecifiedBrain("/Users/antoniusharijanto/CSE_454/brain4",
		// "ecml");
		// bd.runWithSpecifiedBrain("/Users/antoniusharijanto/CSE_454/brain4",
		// "mdj_1_2_3");
		bd.runWithSpecifiedBrain();
	}

	private static void printUsage() {
		System.out.println("Usage: brainDir featureArgs");
	}

	/**
	 * @param args
	 *            command-line arguments
	 * @throws Exception
	 */
	public BenchmarkDriver(String[] args) throws Exception {
		String pathToBrain = args[0];
		String featureArgs = args[1];
		// init stuff needed by fillers
		Set<String> setOfCityMID = initializeSetOfCityMID();
		Set<String> setOfStateProvinceMID = initializeSetOfStateProvinceMID();
		Set<String> setOfCountryMID = initializeSetOfCountryMID();
		Set<String> setOfReligionMID = initializeSetOfReligionMID();

		Set<String> causeOfDeathMID = initSetOfCauseOfDeathMID();
		Classifier classifier = Classifier.BuildClassifierFromDir(pathToBrain);
		FeatureExtractor featureExtractor = OverallFeatureExtractor
				.getFeatureExtractor(featureArgs);
		WikiFreebaseMapper wikiFreebaseMapper = new OfflineWikiFreebaseMapper();

		// initialize the fillers
		fillers = new ArrayList<Filler>();
		fillers.add(new CauseOfDeathFiller(classifier, featureExtractor,
				wikiFreebaseMapper, causeOfDeathMID));
		
		// TODO: add province/state, country, placeOfDeath, religion
		
		// Place of Birth
		fillers.add(new CityOfBirth(classifier, featureExtractor,
				wikiFreebaseMapper, setOfCityMID));
		fillers.add(new StateProvinceOfBirth(classifier, featureExtractor,
				wikiFreebaseMapper, setOfStateProvinceMID));
		fillers.add(new CountryOfBirth(classifier, featureExtractor,
				wikiFreebaseMapper, setOfCountryMID));
		
		// Place of Death
		fillers.add(new CityOfDeath(classifier, featureExtractor,
				wikiFreebaseMapper, setOfCityMID));
		fillers.add(new StateProvinceOfDeath(classifier, featureExtractor,
				wikiFreebaseMapper, setOfStateProvinceMID));
		fillers.add(new CountryOfBirth(classifier, featureExtractor,
				wikiFreebaseMapper, setOfCountryMID));
		
		// Religion
		fillers.add(new Religion(classifier, featureExtractor,
				wikiFreebaseMapper, setOfReligionMID));
		
	}


	/**
	 * Run the benchmark with the brain specified in the parameter
	 * 
	 * @param pathToBrain
	 *            path to the classifier brain
	 * @throws Exception
	 */
	public void runWithSpecifiedBrain() throws Exception {
		// if the first 2 steps are run
		mRun = true;
		// if the 3rd step is run
		boolean eval = true;

		// The slot filling pipeline
		if (mRun) {
			// read the queries
			// Query is like this:
			/*
			 * <query id="SF207"> <name>Charles Wuorinen</name>
			 * <docid>eng-WL-109-174581-12951116</docid> <enttype>PER</enttype>
			 * <nodeid>NIL00008</nodeid> </query>
			 */
			sf.query.QueryReader queryReader = new sf.query.QueryReader();
			queryReader.readFrom(SFConstants.queryFile);

			StringBuilder answersString = new StringBuilder();
			// initialize the corpus
			// FIXME replace the list by a generic class with an input of slot
			// name and an output of all the relevant files from the answer file
			AnnotatedCorpusReader corpus;

			try {
				corpus = new AnnotatedCorpusReader(
						KbpConstants.processedDocPath,
						TrainingConfig.INPUT_CONSTANTS);

				Map<AnnotationConstant, String> annotations = null;
				int c = 0;
				while (corpus.hasNext()) {
					annotations = corpus.next();
					if (c++ % 100000 == 0) {
						System.err.print("finished reading " + c + " lines\r");
					}

					// cache to ensure that the same range pair is not
					// classified more than once
					// key = pair of ranges (order matters), value = result of
					// classifying the pair of ranges under the current
					// annotation
					Map<RangePair, Result> classifierCache = new HashMap<RangePair, Result>();
					// for each query, find out if the slot can be filled
					for (SFEntity query : queryReader.queryList) {
						// apply the filler to the sentences with its
						// annotations
						for (Filler filler : fillers) {
							filler.predict(query, annotations, classifierCache);
						}
					}
				}
				// for each query, print out the answer, or NIL if nothing is
				// found

				for (Filler filler : fillers) {
					for (SFEntity query : queryReader.queryList) {
						if (query.answers.containsKey(filler.slotName)) {
							// The output file format
							// Column 1: query id
							// Column 2: the slot name
							// Column 3: a unique run id for the submission
							// Column 4: NIL, if the system believes no
							// information is learnable for this slot. Or,
							// a single docid which supports the slot value
							// Column 5: a slot value
							SingleAnswer ans = (SingleAnswer) query.answers
									.get(filler.slotName);
							answersString.append(String.format(
									"%s\t%s\t%s\t%s\t%s\n", query.queryId,
									filler.slotName, filler.getClass()
											.getName(), ans.doc, ans.answer));
						} else {
							answersString.append(String.format(
									"%s\t%s\t%s\t%s\t%s\n", query.queryId,
									filler.slotName, filler.getClass()
											.getName(), "NIL", ""));
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			FileUtil.writeTextToFile(answersString.toString(),
					SFConstants.outFile);

			// output for manual judgment. Only useful for measuring precision.
			// Dan said it's ok
			for (Filler filler : fillers) {
				PrintWriter evalWriter = new PrintWriter(new BufferedWriter(
						new FileWriter(
								SFConstants.getReasoningFile(filler.slotName))));
				for (SFEntity query : queryReader.queryList) {
					if (query.answers.containsKey(filler.slotName)) {
						SingleAnswer ans = (SingleAnswer) query.answers
								.get(filler.slotName);
						Reasoning r = ans.reasoning;
						evalWriter.write(r.relationship + "\t" + r.e1 + "\t"
								+ r.e2 + "\t" + r.sentence + "\n");
					}
				}
				evalWriter.close();
			}
		}
		if (eval) {
			// Evaluate against the gold standard labels
			// The label file format (11 fields):
			// 1. NA
			// 2. query id
			// 3. NA
			// 4. slot name
			// 5. from which doc
			// 6., 7., 8. NA
			// 9. answer string
			// 10. equiv. class for the answer in different strings
			// 11. judgement. Correct ones are labeled as 1.
			try {
				SFScore.main(new String[] { SFConstants.outFile,
						SFConstants.labelFile });
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	protected static Set<String> initSetOfCauseOfDeathMID() {
		return initializeMIDSet(KnowledgeBaseConstant.CAUSE_OF_DEATH_MIDS);
	}

	/**
	 * @return Set of MIDs that corresponds to city/town entity
	 */
	protected static Set<String> initializeSetOfCityMID() {
		return initializeMIDSet(KnowledgeBaseConstant.PATH_CITY_TOWN_MIDS); 
	}

	/**
	 * @return Set of MIDs that corresponds to state/province entity
	 */
	protected static Set<String> initializeSetOfStateProvinceMID() {
		return initializeMIDSet(KnowledgeBaseConstant.PATH_STATE_PROVINCE_MIDS);
	}
	

	/**
	 * @return Set of MIDs that corresponds to country entity
	 */
	protected static Set<String> initializeSetOfCountryMID() {
		return initializeMIDSet(KnowledgeBaseConstant.PATH_COUNTRY_MIDS);
	}
	
	/**
	 * @return Set of MIDs that corresponds to religion entity
	 */
	protected static Set<String> initializeSetOfReligionMID() {
		return initializeMIDSet(KnowledgeBaseConstant.PATH_RELIGION_MIDS);
	}
	
	/**
	 * @param path the path to a file where the first token of each line is an MID
	 * @return the set that contains all the MIDs on the file
	 */
	protected static Set<String> initializeMIDSet(String path){
		Set<String> result = new HashSet<String>();
		try {
			// Path to the text file that contains the MIDs of all cities
			File input_file = new File(path);
			Scanner s;
			s = new Scanner(input_file);
			while (s.hasNextLine()) {
				String line = s.nextLine();
				Scanner lineScanner = new Scanner(line);
				// MID is the first token
				result.add(lineScanner.next());
				lineScanner.close();
			}
			s.close();
		} catch (FileNotFoundException e) {
			System.err.println("Couldn't create scanner of from path: "
					+ KnowledgeBaseConstant.PATH_COUNTRY_MIDS);
			e.printStackTrace();
		}
		if (DEBUG) {
			System.out.println("SetOfCountryMID Set size: " + result.size());
		}
		return result;
	}
}
