package benchmark;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.Set;

import knowledgeBase.Entity;
import knowledgeBase.KnowledgeBaseConstant;
import knowledgeBase.OfflineWikiFreebaseMapper;
import knowledgeBase.WikiFreebaseMapper;
import sf.SFConstants;
import sf.SFEntity;
import sf.SFEntity.Reasoning;
import sf.SFEntity.SingleAnswer;
import sf.filler.Filler;
import sf.filler.distantsupervision.CauseOfDeathFiller;
import sf.filler.distantsupervision.CityOfBirth;
import sf.filler.distantsupervision.CityOfDeath;
import sf.filler.distantsupervision.CountryOfBirth;
import sf.filler.distantsupervision.CountryOfDeath;
import sf.filler.distantsupervision.Religion;
import sf.filler.distantsupervision.StateProvinceOfBirth;
import sf.filler.distantsupervision.StateProvinceOfDeath;
import tackbp.KbEntity.EntityType;
import trainingDataGenerator.AnnotatedCorpusReader;
import trainingDataGenerator.AnnotatedSentenceParser.Wikification.WikificationInfo;
import trainingDataGenerator.AnnotatedSentenceParserAdapter;
import trainingDataGenerator.AnnotationConstant;
import trainingDataGenerator.TrainingConfig;
import util.FillerHelper;
import classifier.Classifier;
import classifier.Result;
import features.extractor.OverallFeatureExtractor;
import features.oldstuff.FeatureExtractor;

/**
 * generate our own query set on the fly over the corpus, since the benchmark
 * driver has a very limited number of queries The outputs can then be manually
 * checked through manual precision driver
 * 
 * @author sjonany
 */
public class ManualBenchmarkDriver {
	private static final int NUM_REASONING_PER_SLOT = 25;
	public static void main(String[] args) throws Exception {
		if (args.length != 4) {
			System.out.println("Usage: brainDir featureArgs pathToCorpus outputPath");
			return;
		}

		String pathToBrain = args[0];
		String featureArgs = args[1];
		String corpusPath = args[2];
		String outputPath = args[3].endsWith("" + File.separatorChar) ? args[3]
				: args[3] + File.separatorChar;

		// initialize fillers
		List<Filler> fillers = new ArrayList<Filler>();
		// init stuff needed by fillers

		Set<String> setOfCityMID = BenchmarkDriver.initializeSetOfCityMID();
		Set<String> setOfStateProvinceMID = BenchmarkDriver
				.initializeSetOfStateProvinceMID();
		Set<String> setOfCountryMID = BenchmarkDriver
				.initializeSetOfCountryMID();

		Set<String> setOfCauseOfDeathMID = BenchmarkDriver
				.initSetOfCauseOfDeathMID();
		Set<String> setOfReligionMID = BenchmarkDriver
				.initializeSetOfReligionMID();

		Set<String> peopleMids = initPeopleMID();
		Classifier classifier = Classifier.BuildClassifierFromDir(pathToBrain);
		FeatureExtractor featureExtractor = OverallFeatureExtractor
				.getFeatureExtractor(featureArgs);
		WikiFreebaseMapper wikiFreebaseMapper = new OfflineWikiFreebaseMapper();

		// initialize the fillers
		fillers = new ArrayList<Filler>();
		fillers.add(new CauseOfDeathFiller(classifier, featureExtractor,
				wikiFreebaseMapper, setOfCauseOfDeathMID));
		// Place of Birth
		fillers.add(new CityOfBirth(classifier, featureExtractor,
				wikiFreebaseMapper, setOfCityMID));
		fillers.add(new StateProvinceOfBirth(classifier, featureExtractor,
				wikiFreebaseMapper, setOfStateProvinceMID));
		fillers.add(new CountryOfBirth(classifier, featureExtractor,
				wikiFreebaseMapper, setOfCountryMID));

		// Place of Death
		fillers.add(new CityOfDeath(classifier, featureExtractor,
				wikiFreebaseMapper, setOfCityMID));
		fillers.add(new StateProvinceOfDeath(classifier, featureExtractor,
				wikiFreebaseMapper, setOfStateProvinceMID));
		fillers.add(new CountryOfDeath(classifier, featureExtractor,
				wikiFreebaseMapper, setOfCountryMID));

		// Religion
		fillers.add(new Religion(classifier, featureExtractor,
				wikiFreebaseMapper, setOfReligionMID));

		// key = e1MID, value = query+answer related to e1MID
		Map<String, SFEntity> queryTable = new HashMap<String, SFEntity>();
		// key = slotname, value = number of reasonings related to the
		// relationship that has been found
		Map<String, Integer> reasoningCount = new HashMap<String, Integer>();
		for(Filler filler: fillers){
			reasoningCount.put(filler.slotName, 0);
		}
		
		int countCompletedSlots = 0;
		
		// name and an output of all the relevant files from the answer file
		AnnotatedCorpusReader corpus = new AnnotatedCorpusReader(corpusPath,
				TrainingConfig.INPUT_CONSTANTS);

		int c = 0;
		while (corpus.hasNext() && countCompletedSlots < fillers.size()) {
			Map<AnnotationConstant, String> annotations = corpus.next();
			if (c++ % 100000 == 0) {
				System.err.print("finished reading " + c + " lines\r");
			}

			// queries pertaining to this sentence
			Set<MidNamePair> peopleMid = getPeopleMid(annotations, peopleMids,
					wikiFreebaseMapper);
			List<SFEntity> relatedQueries = new ArrayList<SFEntity>();
			for (MidNamePair person : peopleMid) {
				if (queryTable.containsKey(person.mid)) {
					// get the previous result, but replace with current mention
					// name, which is what is exactly used in this sentence
					SFEntity query = queryTable.get(person.mid);
					query.mentionString = person.name;
					relatedQueries.add(query);
				} else {
					// no query pertaining to this person has been asked
					SFEntity newQuery = new SFEntity();
					newQuery.mentionString = person.name;
					newQuery.entityType = EntityType.PER;
					relatedQueries.add(newQuery);
					queryTable.put(person.mid, newQuery);
				}
			}

			// cache to ensure that the same range pair is not classified more
			// than once
			// key = pair of ranges (order matters), value = result of
			// classifying the pair of ranges under the current annotation
			Map<RangePair, Result> classifierCache = new HashMap<RangePair, Result>();
			// for each query, find out if the slot can be filled
			for (SFEntity query : relatedQueries) {
				// apply the filler to the sentences with its
				// annotations
				for (Filler filler : fillers) {
					boolean isEmptyAtFirst = query.answers.get(filler.slotName) == null;
					filler.predict(query, annotations, classifierCache);
					boolean isFilledNow = query.answers.get(filler.slotName) != null;
					if(isEmptyAtFirst && isFilledNow){
						int prevCount = reasoningCount.get(filler.slotName);
						reasoningCount.put(filler.slotName, prevCount + 1);
						if(prevCount == NUM_REASONING_PER_SLOT - 1){
							countCompletedSlots++;
						}
					}
				}
			}
		}

		// key = slotname, value = print writer that writes the reasoning file
		// for the slotname
		Map<String, PrintWriter> writers = new HashMap<String, PrintWriter>();

		for (Filler filler : fillers) {
			writers.put(
					filler.slotName,
					new PrintWriter(
							new BufferedWriter(
									new FileWriter(
											outputPath
											+ SFConstants
											.getReasoningFileName(filler.slotName)))));
		}

		for (SFEntity query : queryTable.values()) {
			for (Filler filler : fillers) {
				String slotName = filler.slotName;
				SFEntity.Answer answer = query.answers.get(slotName);
				if (answer != null) {
					int prevCount = reasoningCount.get(filler.slotName);
					if(prevCount >= 0){
						Reasoning r = ((SingleAnswer) answer).reasoning;
						writers.get(slotName).write(
								r.relationship + "\t" + r.e1 + "\t" + r.e2 + "\t"
										+ r.sentence + "\n");

						reasoningCount.put(filler.slotName, prevCount - 1);
					}
					
				}
			}
		}

		for (Filler filler : fillers) {
			writers.get(filler.slotName).close();
		}
	}

	/**
	 * return the mid's of people who are in this sentence and also the name
	 * corresponding to the mid that is used in the sentence
	 * 
	 * @param annot
	 * @return
	 * @throws Exception
	 */
	private static Set<MidNamePair> getPeopleMid(
			Map<AnnotationConstant, String> annot, Set<String> peopleMids,
			WikiFreebaseMapper wikiMapper) throws Exception {
		AnnotatedSentenceParserAdapter annotation = new AnnotatedSentenceParserAdapter(
				annot);
		Set<MidNamePair> midNamePairs = new HashSet<MidNamePair>();
		for (WikificationInfo wiki : annotation.getWikification().wiki) {
			Entity e = wikiMapper.convertToEntity(wiki.wikiArticleName);
			if (e != null && peopleMids.contains(e.getMid())) {
				MidNamePair pair = new MidNamePair();
				pair.mid = e.getMid();
				pair.name = FillerHelper.getCombinedTokens(annotation,
						wiki.range);
				midNamePairs.add(pair);
			}
		}
		return midNamePairs;
	}

	/**
	 * get all the people mids for the relationships we care about.
	 */
	public static Set<String> initPeopleMID() throws Exception {
		Set<String> mids = new HashSet<String>();
		Scanner sc = new Scanner(new File(KnowledgeBaseConstant.PEOPLE_MIDS));
		while (sc.hasNext()) {
			mids.add(sc.nextLine().trim());
		}
		sc.close();
		return mids;
	}

	public static class MidNamePair {
		public String mid;
		public String name;
	}
}
