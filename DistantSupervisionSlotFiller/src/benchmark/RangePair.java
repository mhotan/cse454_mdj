package benchmark;

import trainingDataGenerator.AnnotatedSentenceParser.Range;

public class RangePair {
	private Range range1;
	private Range range2;

	public RangePair(Range range1, Range range2) {
		this.range1 = range1;
		this.range2 = range2;
	}

	@Override
	public int hashCode() {
		int hashCode = 0;

		try {
			hashCode = 37 * range1.hashCode() + range2.hashCode();
		} catch (Exception e) {
			if (range1 == null) {
				System.err.println("RangePair.hashcode() : range1 is null");
			}
			if (range2 == null) {
				System.err.println("RangePair.hashcode() : range2 is null");
			}
			e.printStackTrace();
		}
		return hashCode;
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof RangePair)) {
			return false;
		}

		RangePair other = (RangePair) o;
		return other.range1.equals(this.range1)
				&& other.range2.equals(this.range2);
	}
}