package benchmark;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import sf.SFConstants;

/**
 * Manually judge each of the predictions based on the reasoning to get the precision
 * Run this after BenchmarkDriver 
 * @author sjonany
 */
public class ManualPrecisionDriver {
	public static void main(String[] args) throws IOException{
		if(args.length != 2){
			System.out.println("Usage: reasoningDir outDir");
			return;
		}

		Map<String, Score> scoreTable = new HashMap<String, Score>();
		String reasoningDirName = args[0].endsWith("" + File.separatorChar) ? args[0] : args[0] + File.separatorChar;
		String outDir = args[1].endsWith("" + File.separatorChar) ? args[1] : args[1] + File.separatorChar;

		File reasoningDir = new File(reasoningDirName);
		File[] reasonFiles = reasoningDir.listFiles();

		Scanner userIn = new Scanner(System.in);
		int numReasonFileGraded = 0;
		for(File reasonFile : reasonFiles){
			System.out.println("You have graded " + numReasonFileGraded + " out of " + reasonFiles.length + "\n");
			System.out.println("Now grading " + reasonFile.getName() + "\n");
			numReasonFileGraded++;
			String name = reasonFile.getName();
			String slotName = name.substring(name.indexOf("per:"), name.indexOf(".reasoning"));

			PrintWriter judgWriter = new PrintWriter(new BufferedWriter(new FileWriter(outDir + SFConstants.getJudgmentFileName(slotName))));
			Scanner sc = new Scanner(reasonFile);
			int tp = 0;
			int fp = 0;
			boolean isDone = false;
			int lineCount = 1;
			
			while(!isDone && sc.hasNextLine()){
				String line = sc.nextLine();
				boolean isInvalidInput = true;
				while(isInvalidInput){
					System.out.println(lineCount + ". " + line);
					System.out.print("Judgment (t/f/done):");
					String input = userIn.nextLine();
					isInvalidInput = false;
					if(input.equals("t")){
						line += "\tt";
						tp++;
					}else if(input.equals("f")){
						line +="\tf";
						fp++;
					}else if(input.equals("done")){
						isDone = true;
					}else{
						System.out.println("Unrecognized command, please try again");
						isInvalidInput = true;
					}
				}
				judgWriter.write(line + "\n");
				lineCount++;
			}

			double precision = 1.0 * tp / (tp + fp);
			judgWriter.write("TP = " + tp + "\n");
			judgWriter.write("FP = " + fp + "\n");
			judgWriter.write("Precision = " + precision + "\n");
			System.out.println("Precision = " + precision + "\n");
			scoreTable.put(slotName, new Score(tp, fp));
			judgWriter.close();
			sc.close();
		}
		userIn.close();
		PrintWriter summaryWriter = new PrintWriter(new BufferedWriter(new FileWriter(outDir + "summary.txt")));
		int totalTP = 0;
		int totalFP = 0;
		summaryWriter.write("List of precisions for reasoning files found in " + reasoningDir + "\n");
		for(Entry<String, Score> stats : scoreTable.entrySet()){
			Score score = stats.getValue();
			summaryWriter.write(stats.getKey() + " : " + score.calcPrecision() + "\n");
			totalTP += score.TP;
			totalFP += score.FP;
		}
		
		summaryWriter.write("Overall : " + new Score(totalTP, totalFP).calcPrecision() + "\n");
		summaryWriter.close();
	}
	
	public static class Score{
		public int TP;
		public int FP;
		public Score(int TP, int FP){
			this.TP = TP;
			this.FP = FP;
		}
		
		public double calcPrecision(){
			return  1.0 * TP / (TP + FP);
		}
	}
}
