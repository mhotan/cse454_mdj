package nlp;

import java.util.ArrayList;
import java.util.List;

import nlp.PreprocessedSentence.SentenceMetaInfo;
import nlp.PreprocessedSentence.TokenSpan;
import edu.stanford.nlp.ling.CoreAnnotations.CharacterOffsetBeginAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.CharacterOffsetEndAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.NamedEntityTagAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

/**
 * handles preprocessing of a single document
 * @author sjonany
 *
 */
public class DocumentPreprocessor {
	
	/**
	 * preprocess the entire document
	 * @param corpus
	 * @return
	 */
	//TODO: how should a corpus be represented? should save its meta info too
	public static List<PreprocessedSentence> preprocessCorpus(String document){
		StanfordCoreNLP nlpProcessor = CoreNLP.getInstance();
	    String documentText = document;
	    Annotation annotatedDoc = new Annotation(documentText);
	    nlpProcessor.annotate(annotatedDoc);
	    List<CoreMap> sentences = annotatedDoc.get(SentencesAnnotation.class);
	    
	    List<PreprocessedSentence> preprocessedCorpus = new ArrayList<PreprocessedSentence>();
	    for(CoreMap sentence : sentences){
	    	//TODO: sentence meta info how.
	    	SentenceMetaInfo sentenceMetaInfo = null;
	    	preprocessedCorpus.add(preprocessSentence(sentence, sentenceMetaInfo));
	    }
	    
	    return preprocessedCorpus;
	}
	
	/**
	 * preprocess a single sentence, chunking named entities into a single token 
	 * the chunking code is manually coded because CRFClassifier only works on PERSON or LOCATION
	 * http://stackoverflow.com/questions/13765349/multi-term-named-entities-in-stanford-named-entity-recognizer
	 * @param sentence an annotated sentence 
	 * @param sentenceMetaInfo
	 */
	public static PreprocessedSentence preprocessSentence(CoreMap sentence, 
			SentenceMetaInfo sentenceMetaInfo){
		List<String> chunks = new ArrayList<String>();
		List<String> nerTags = new ArrayList<String>();
		List<TokenSpan> chunkSpans = new ArrayList<TokenSpan>();
		
		//the information of the current chunk we are building
		String curChunkEntityTag = null;
		String curChunk = null;
		int curChunkStartOffset = -1;
		int curChunkEndOffset = -1;
		
		for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
			String word = token.get(TextAnnotation.class);
			String ne = token.get(NamedEntityTagAnnotation.class);    
			//note: the startOffset is inclusive, endOffset is one more than where it really ends
			int startOffset = token.get(CharacterOffsetBeginAnnotation.class);
			int endOffset = token.get(CharacterOffsetEndAnnotation.class);
			
			//check if current chunk should end here
			//a paper mentioned that chunks should not spread across subtrees. However, I can't think of a case
			//where an NE chunk will spread across subtrees, since if they are separate NE, there should be a token in
			//between like a punctuation or a word. So this case is not checked
			if(!ne.equals(curChunkEntityTag) || !StanfordNerTag.isNamedEntity(ne)){
				if(curChunk != null){
					//flush the old chunk
					chunks.add(curChunk);
					nerTags.add(curChunkEntityTag);
					chunkSpans.add(new TokenSpan(curChunkStartOffset, curChunkEndOffset));
				}
				
				//start a new chunk
				curChunkEntityTag = ne;
				curChunk = word;
				curChunkStartOffset = startOffset;
				curChunkEndOffset = endOffset;
			}else{
				
				//continue the old chunk
				curChunkEndOffset = endOffset;
				//can't just add word, we might need information from spacing/delimiter
				curChunk = sentence.toString().substring(curChunkStartOffset, curChunkEndOffset);
			}
		}
		
		//flush the last chunk
		chunks.add(curChunk);
		nerTags.add(curChunkEntityTag);
		chunkSpans.add(new TokenSpan(curChunkStartOffset, curChunkEndOffset));
		
		return new PreprocessedSentence(sentenceMetaInfo, sentence.toString(),
				chunks, nerTags, chunkSpans);
	}
}
