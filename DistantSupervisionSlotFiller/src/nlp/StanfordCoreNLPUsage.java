package nlp;

import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

import edu.stanford.nlp.ling.CoreAnnotations.CharacterOffsetBeginAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.CharacterOffsetEndAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.NamedEntityTagAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeCoreAnnotations.TreeAnnotation;
import edu.stanford.nlp.trees.semgraph.SemanticGraph;
import edu.stanford.nlp.trees.semgraph.SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation;
import edu.stanford.nlp.util.CoreMap;

/**
 * Joe's exampel code for standford Core NLP
 * @author mhotan
 *
 */
public class StanfordCoreNLPUsage {
	public static void main(String[] args) throws ClassCastException, ClassNotFoundException, IOException {
		/*
		String text = "Michael William Jackson lives in Ohio," + 
				" and has been a member of the Wildlife Foundation since July 1, 2013, " + 
				"and he is 32 years old, has $32.23 in his bank account, and danced at 5 pm.";
		 */
		StanfordCoreNLP nlp = CoreNLP.getInstance();
		Scanner reader = new Scanner(System.in);
		while(true){
			System.out.println("Please enter a sentence to process: ");
			String text = reader.nextLine();
			// read some text in the text variable
	
			// create an empty Annotation just with the given text
			Annotation document = new Annotation(text);
	
			// run all Annotators on this text
			nlp.annotate(document);
	
			// these are all the sentences in this document
			// a CoreMap is essentially a Map that uses class objects as keys and has values with custom types
			List<CoreMap> sentences = document.get(SentencesAnnotation.class);
	
			for(CoreMap sentence: sentences) {
				System.out.println("Sentence : " + sentence);
				// traversing the words in the current sentence
				// a CoreLabel is a CoreMap with additional token-specific methods
				for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
					// this is the text of the token
					String word = token.get(TextAnnotation.class);
					System.out.println("Word: " + word);
					// this is the POS tag of the token
					//String pos = token.get(PartOfSpeechAnnotation.class);
					//System.out.println("Pos: " + pos);
					// this is the NER label of the token
					
					String lemma = token.get(LemmaAnnotation.class);
					System.out.println("Lemma: " + lemma);

					/*
					String ne = token.get(NamedEntityTagAnnotation.class);    
					System.out.println("Ne: " + ne);   
					int startOffset = token.get(CharacterOffsetBeginAnnotation.class);
					int endOffset = token.get(CharacterOffsetEndAnnotation.class);
					System.out.println(startOffset + "-" + endOffset);
					*/
					System.out.println();
				}
	
				/*
				// this is the parse tree of the current sentence
				Tree tree = sentence.get(TreeAnnotation.class);
				
	
				// this is the Stanford dependency graph of the current sentence
				SemanticGraph dep = sentence.get(CollapsedCCProcessedDependenciesAnnotation.class);
				Scanner reader = new Scanner(System.in);
				
				while(true){
					System.out.println("Which indices of words to see if they share the common ancestor?");
					int first = reader.nextInt();
					int second = reader.nextInt();
					IndexedWord firstWord = dep.getNodeByIndex(first);
					IndexedWord secondWord = dep.getNodeByIndex(second);
					boolean isSameAncestor = dep.getParent(firstWord).equals(dep.getParent(secondWord));
				}
				*/
			}
		}
	}
}
