package nlp;

/**
 * All the possible NER tags produced by StanfordCoreNLP (says Xiao Ling anyways) 
 * There does not seem to be an enum provided in the library?
 * @author sjonany
 */
public enum StanfordNerTag {
	//names
	LOCATION("LOCATION"),
	PERSON("PERSON"),
	ORGANIZATION("ORGANIZATION"),
	//numerical entities
	DATE("DATE"),
	TIME("TIME"),
	DURATION("DURATION"),
	MONEY("MONEY"),
	NUMBER("NUMBER"),
	ORDINAL("ORDINAL"),
	PERCENT("PERCENT"),
	SET("SET"),
	MISC("MISC"),
	//not a named entity
	NOT_NAMED_ENTITY("O");
	
	//the string produced by StanfordCoreNLP NamedEntityTagAnnotation
	private String nerStr;
	private StanfordNerTag(String nerStr){
		this.nerStr = nerStr;
	}
	
	@Override
	public String toString(){
		return this.nerStr;
	}
	
	/**
	 * use this INSTEAD of valueOf()
	 * @param text
	 */
	public static StanfordNerTag getEnum(String text){
		for(StanfordNerTag tag : StanfordNerTag.values()){
			if(tag.nerStr.equals(text)){
				return tag;
			}
		}
		
		return null;
	}
	
	/**
	 * checks if the given nerTag corresponds to a named entity
	 */
	public static boolean isNamedEntity(String nerTag){
		return !NOT_NAMED_ENTITY.toString().equals(nerTag);
	}
}
