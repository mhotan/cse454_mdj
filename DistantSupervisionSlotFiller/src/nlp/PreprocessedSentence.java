package nlp;

import java.util.List;

import util.DebugConfig;

/**
 * A sentence which has been annotated 
 * @author sjonany
 */
public class PreprocessedSentence {
	//@see constructor
	private SentenceMetaInfo sentenceMetaInfo;
	private String sentence;
	private List<String> chunks;
	private List<String> nerTags;
	private List<TokenSpan> chunkSpans;
	
	/**
	 * @param sentenceMetaInfo meta info of this sentence
	 * @param sentence the raw unprocessed sentence itself
	 * @param chunks a series of contiguous words sharing the same name entity tag and
	 *     do not cross the dependency tree
	 *     the chunks concatenated together with separators will form a sentence
	 * @param nerTags the NER tags corresponding to each chunk 
	 * 	   e.g. the chunk "The united states" might have NER tag of "location"
	 * @param chunkSpans the indices based on the original sentence which indicate 
	 *     where each chunk starts and ends
	 */
	
	public PreprocessedSentence(SentenceMetaInfo sentenceMetaInfo, String sentence,
			List<String> chunks, List<String> nerTags, List<TokenSpan> chunkSpans){
		this.sentenceMetaInfo = sentenceMetaInfo;
		this.sentence = sentence;
		this.chunks = chunks;
		this.nerTags = nerTags;
		this.chunkSpans = chunkSpans;
		
		if(DebugConfig.IS_DEBUG){
			checkRep();
		}
	}
	
	public SentenceMetaInfo getSentenceMetaInfo(){
		return sentenceMetaInfo;
	}
	
	public String getSentence(){
		return sentence;
	}
	
	public List<String> getChunks(){
		return chunks;
	}
	
	public List<String> getNerTags(){
		return nerTags;
	}
	
	public List<TokenSpan> getTokenSpans(){
		return chunkSpans;
	}
	
	private void checkRep(){
		if(chunks.size() != chunkSpans.size() || 
				chunks.size() != nerTags.size()){
			throw new IllegalArgumentException("Chunk spans do not correspond to chunks' length!");
		}
	}
	
	/**
	 * stores how far a token spans in a sentence
	 * @author sjonany
	 */
	public static class TokenSpan{
		//all indices inclusive
		private int startIndex;
		private int endIndex;
		
		public TokenSpan(int startIndex, int endIndex){
			this.startIndex = startIndex;
			this.endIndex = endIndex;
		}
		
		public int getStartIndex(){
			return startIndex;
		}
		
		public int getEndIndex(){
			return endIndex;
		}
		
		@Override
		public String toString(){
			return startIndex + "-" + endIndex;
		}
	}

	//TODO: Do we decide on globalSentenceID ourselves or is it from an online source?
	public static class SentenceMetaInfo{
		private String globalSentenceID;
		private String fileName;
		private int lineNumber;
		private int startCharOffset;
		private int endCharOffset;
		
		public SentenceMetaInfo(String globalSentenceID, String fileName, int lineNumber,
				int startCharOffset, int endCharOffset){
			this.globalSentenceID = globalSentenceID;
			this.fileName = fileName;
			this.lineNumber = lineNumber;
			this.startCharOffset = startCharOffset;
			this.endCharOffset = endCharOffset;
		}

		public String getGlobalSentenceID() {
			return globalSentenceID;
		}

		public String getFileName() {
			return fileName;
		}

		public int getLineNumber() {
			return lineNumber;
		}

		public int getStartCharOffset() {
			return startCharOffset;
		}

		public int getEndCharOffset() {
			return endCharOffset;
		}
		
		@Override
		public boolean equals(Object o){
			return globalSentenceID.equals(((SentenceMetaInfo) o));
		}
		
	}
}
