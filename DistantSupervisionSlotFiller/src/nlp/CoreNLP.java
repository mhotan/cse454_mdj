package nlp;

import java.util.Properties;

import edu.stanford.nlp.pipeline.StanfordCoreNLP;

/**
 * the main class which does all the nlp related stuff
 * @author sjonany
 */
public class CoreNLP {
	private static StanfordCoreNLP nlpProcessor;
	
	public static synchronized StanfordCoreNLP getInstance(){
		if(nlpProcessor == null){
			Properties props = new Properties();
			//props.put("annotators", "tokenize, ssplit, pos, lemma, ner, parse, dcoref");
			props.put("annotators", "tokenize, ssplit, pos, lemma, ner, parse");
			nlpProcessor = new StanfordCoreNLP(props);
		}
		
		return nlpProcessor;
	}
}
