package util;

public class POSIdentifier {

	/**
	 * Returns whether the passed in postag is interpretted as a Noun
	 * IE 
	 * @param postag
	 * @return
	 */
	public static boolean isNoun(String postag){
		if (postag == null) return false;
		String pos = postag.toUpperCase();
		boolean isNoun = false;
		//TODO
		isNoun |= pos.charAt(0) == 'N';
		return isNoun;
	}

	public static boolean isVerb(String postag){
		if (postag == null) return false;
		String pos = postag.toUpperCase();
		boolean isVerb = false;
		//TODO
		isVerb |= pos.equals("MD");
		isVerb |= pos.charAt(0) == 'V';
		return isVerb;
	}

	public static boolean isAdjective(String postag){
		if (postag == null) return false;
		String pos = postag.toUpperCase();
		boolean isAdj = false;
		isAdj |= pos.charAt(0) == 'J';
		return isAdj;
	}

	public static boolean isAdverb(String postag){
		if (postag == null) return false;
		// Check for WH Adverb IE: how, why, where, etc
		String pos = postag.toUpperCase();
		if (pos.equals("WRB"))
			return true;
		return pos.charAt(0) == 'R';
	}

	/**
	 * Return whether this Pos tag represents a postag pronoun
	 * @param postag
	 * @return
	 */
//	public static boolean isPronoun(String postag){
//		if (postag == null) return false;
//		String pos = postag.toUpperCase();
//		return pos.equals("PRP") || pos.equals("PRP$") 
//				|| pos.equals("WP") || pos.equals("WP$");
//	}

	/**
	 * If pos tag represents a foreign word 
	 * @param postag
	 * @return
	 */
	public static boolean isForeignWord(String postag){
		if (postag == null) return false;
		return postag.toUpperCase().equals("FW");
	}
	
	/**
	 * Returns whether this part of speech tag is a content word
	 * <b>IE nouns, verbs, adjectives, and adverbs
	 * @param postag Pennbank annotated Part of Speech Tag
	 * @return whether is Content word
	 */
	public static boolean isContentWord(String postag){
		return isNoun(postag) || isVerb(postag) || isAdjective(postag) || isAdverb(postag);
	}
	
	/**
	 * 
	 * @param postag 
	 * @return pos tag not content word
	 */
	public static boolean isFunctionalWord(String postag){
		return !isContentWord(postag);
	}
}
