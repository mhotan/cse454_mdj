package util;

/**
 * for debugging purposes only
 * @author sjonany
 */
public class DebugConfig {
	//if set to true, all classes will use their checkRep()
	public static boolean IS_DEBUG = true;
}
