package util;

import java.util.List;

import trainingDataGenerator.AnnotatedSentenceParserAdapter;
import trainingDataGenerator.AnnotatedSentenceParser.Range;
import trainingDataGenerator.AnnotatedSentenceParserAdapter.AnnotationParsingException;

/**
 * Helper class for slot fillers - various string utilities
 */
public class FillerHelper {
	/**
	 * @return range wrt to source tokens where target tokens are first found
	 * 	null if target tokens can't be found in sourceTokens.
	 */
	public static Range getTokenRangeForSubset(List<String> sourceTokens, List<String> targetTokens){
		for(int i=0; i< sourceTokens.size()-targetTokens.size()+1; i++){
			boolean isRightIndex = true;
			for(int j=0; j<targetTokens.size(); j++){
				if(!sourceTokens.get(i+j).equalsIgnoreCase(targetTokens.get(j))){
					isRightIndex = false;
					break;
				}
			}
			if(isRightIndex){
				//remember, end is exclusive
				return new Range(i, i+targetTokens.size());
			}
		}
		return null;
	}
	

	public static String getCombinedTokens(AnnotatedSentenceParserAdapter annots, Range range) throws AnnotationParsingException{
		List<String> tokens = annots.getTokens().tokens;
		String result = "";
		for (int i = range.getStart(); i < range.getEnd(); i++) {
			result += tokens.get(i).trim();
			if (i != (range.getEnd() - 1)) {
				result += " ";
			}
		}
		return result;
	}

}
