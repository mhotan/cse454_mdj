package util;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import nlp.CoreNLP;
import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

//Driver for getting lemmas out strings
public class LemmaDriver {
	public static void main(String[] args){
		StanfordCoreNLP nlp = CoreNLP.getInstance();
		String[] arr= new String[]{
				"president",
				"birth",
				"birthplace",
				"born",
				"childhood",
				"child",
				"raised",
				"grew",
				"origin",
				"home",
				"native",
				"minister"};
		Set<String> lemmas = new HashSet<String>();

		String text = "";
		for(String s : arr){
			text += s + " ";
		}
		// create an empty Annotation just with the given text
		Annotation document = new Annotation(text);

		// run all Annotators on this text
		nlp.annotate(document);

		// these are all the sentences in this document
		// a CoreMap is essentially a Map that uses class objects as keys and has values with custom types
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);

		for(CoreMap sentence: sentences) {
			for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
				String lemma = token.get(LemmaAnnotation.class);
				lemmas.add(lemma);
			}
			break;
		}
		
		for(String s: lemmas){
			System.out.print("\"" + s + "\", ");
		}
	}
}
