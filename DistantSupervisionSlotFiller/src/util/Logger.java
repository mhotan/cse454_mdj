package util;


/**
 * any console output in any source code should be replaced by this
 * global logger used to write to log files certain states of the system when we are not watching
 * @author sjonany
 */
public class Logger {

	private static final boolean DEBUG = true; 

	//	private static Logger mInstance;
	//	
	//	public static Logger getInstance(){
	//		if (mInstance == null)
	//			mInstance = new Logger();
	//		return mInstance;
	//	}
	//	
	//	private Logger(){}

	public static void write(String s){
		//TODO: replace by writing to disk when we care about this
		System.out.print(s);
	}

	public static void write(String s, boolean forDebug){
		//TODO: replace by writing to disk when we care about this
		if (DEBUG && forDebug){
			System.out.print(s);
		}
	}

	public static void writeLine(String s){
		//TODO: replace by writing to disk when we care about this
		System.out.println(s);
	}

	public static void writeLine(String s, boolean forDebug){
		//TODO: replace by writing to disk when we care about this
		if (DEBUG && forDebug){
			System.out.println(s);
		}
	}
}
