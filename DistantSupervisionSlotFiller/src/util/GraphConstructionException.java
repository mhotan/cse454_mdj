package util;

public class GraphConstructionException extends RuntimeException {

	public GraphConstructionException(){
		super("Unable to construct graph");
	}
	
	public GraphConstructionException(String s){
		super("Failed to construct graph: " + s);
	}
	
}
