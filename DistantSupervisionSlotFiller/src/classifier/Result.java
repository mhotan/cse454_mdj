package classifier;

/**
 * stores the result of a classification process
 * @author sjonany
 */
public class Result {
	//the score used by Multi-R's Parser.score
	private double score;
	private String relationship;
	
	public Result(String relationship, double score){
		this.relationship = relationship;
		this.score = score;
	}
	
	public String getRelationship(){
		return relationship;
	}
	
	public double getScore(){
		return score;
	}
	
	@Override
	public String toString(){
		return  "relationship = " + relationship + " ,Score = " + score;
	}
}
