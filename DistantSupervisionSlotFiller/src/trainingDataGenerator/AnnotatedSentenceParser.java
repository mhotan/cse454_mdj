package trainingDataGenerator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import knowledgeBase.RelationInstance;
import knowledgeBase.Relationship;
import trainingDataGenerator.AnnotatedSentenceParser.Wikification.WikificationInfo;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.semgraph.SemanticGraph;
import edu.stanford.nlp.trees.semgraph.SemanticGraphCoreAnnotations;

/**
 * parses annotations for a single sentence
 * such a sentence is from AnnotatedCorpusReader.readNextAnnotatedSentence.get(some key)
 * each method is a parser to a single AnnotationConstant
 * 
 * MH added override toString to help visualize the data types
 * while debugging
 * 
 * @author sjonany
 */
public class AnnotatedSentenceParser {
	private static final String DELIMITER = "\t";

	// I used buffer to conserve memory space
	// I figured toString coud be called a lot at one point
	private static final StringBuffer mSB = new StringBuffer();

	////////////////////////////////////////////////////////////////////////
	// Retrieval methods for a feature objects
	// Input:  String Setence Output: StanfordNLP feature object
	////////////////////////////////////////////////////////////////////////

	/**
	 * 
	 * @param text String representations as defined
	 * @return
	 */
	public static Text parseText(String text){
		String[] tokens = text.split(DELIMITER);
		Text result = new Text();
		result.globalSentenceId = tokens[0];
		result.sentence = tokens[1];

		return result;
	}

	public static Meta parseMeta(String text){
		String[] tokens = text.split(DELIMITER);
		Meta meta = new Meta();
		meta.globalSentenceId = tokens[0];
		meta.lineNumber = Integer.parseInt(tokens[1]);
		meta.originalFileName = tokens[2];
		meta.beginCharOffset = Integer.parseInt(tokens[3]);
		meta.endCharOffset = Integer.parseInt(tokens[4]);

		return meta;
	}

	public static  Tokens parseTokens(String text){
		String[] tokens = text.split(DELIMITER);
		Tokens result = new Tokens();
		result.globalSentenceId = tokens[0];
		result.tokens = new ArrayList<String>();

		String[] textTokens = tokens[1].split(" ");
		for(String s: textTokens){
			result.tokens.add(s);
		}

		return result;
	}

	public static Stanfordlemma parseStanfordlemma(String text){
		String[] tokens = text.split(DELIMITER);
		Stanfordlemma result = new Stanfordlemma();
		result.globalSentenceId = tokens[0];
		result.lemma = new ArrayList<String>();

		String[] textTokens = tokens[1].split(" ");
		for(String s: textTokens){
			result.lemma.add(s);
		}

		return result;
	}

	public static TokenSpans parseTokenSpans(String text){
		String[] tokens = text.split(DELIMITER);	
		TokenSpans result = new TokenSpans();
		result.globalSentenceId = tokens[0];
		result.tokenSpans = new ArrayList<Range>();

		String[] textTokens = tokens[1].split(" ");
		for(String s: textTokens){
			result.tokenSpans.add(Range.fromString(s));
		}

		return result;
	}	

	public static  Stanfordpos parseStanfordpos(String text){
		String[] tokens = text.split(DELIMITER);	
		Stanfordpos result = new Stanfordpos();
		result.globalSentenceId = tokens[0];
		result.pos = new ArrayList<String>();

		String[] textTokens = tokens[1].split(" ");
		for(String s: textTokens){
			result.pos.add(s);
		}

		return result;
	}

	public static Stanfordner parseStanfordner(String text){
		String[] tokens = text.split(DELIMITER);	
		Stanfordner result = new Stanfordner();
		result.globalSentenceId = tokens[0];
		result.ner = new ArrayList<nlp.StanfordNerTag>();

		String[] textTokens = tokens[1].split(" ");
		for(String s: textTokens){
			result.ner.add(nlp.StanfordNerTag.getEnum(s));
		}

		return result;
	}

	public static  Wikification parseWikification(String text){
		Wikification result = new Wikification();
		String[] tokens = text.split(DELIMITER);	
		result.globalSentenceId = tokens[0];
		result.wiki = new ArrayList<Wikification.WikificationInfo>();

		for(int i=1; i<tokens.length; i++){
			String[] wikiTokens = tokens[i].split(" ");
			WikificationInfo info = new WikificationInfo();
			info.range = new Range(Integer.parseInt(wikiTokens[0]), Integer.parseInt(wikiTokens[1]));
			info.wikiArticleName = wikiTokens[2];
			info.confidence = Double.parseDouble(wikiTokens[3]);
			result.wiki.add(info);
		}

		return result;
	}

	public static CJ parseCJ(String text){
		CJ result = new CJ();
		String[] tokens = text.split(DELIMITER);	
		result.globalSentenceId = tokens[0];
		result.parseTree = Tree.valueOf(tokens[1]);
		return result;
	}

	public static EntityPairMention parseEntityPairMention(String text){
		String[] tokens = text.split(DELIMITER);
		EntityPairMention result = new EntityPairMention();
		result.globalSentenceId = tokens[0];
		result.relInstance = new RelationInstance(
				knowledgeBase.Entity.getEntityFromMid(tokens[1]),
				knowledgeBase.Entity.getEntityFromMid(tokens[2]),
				Relationship.valueOf(tokens[3]));
		result.tokenRange1 = Range.fromString(tokens[4]);
		result.tokenRange2 = Range.fromString(tokens[5]);

		return result;
	}

	/**
	 * Creates a Dependency tree with collapsed dependencies with
	 * propagation of conjunct dependencies
	 * 
	 * In the an example of 
	 * "Bell, a company which is based in L.A, 
	 * makes and distributes computer products."
	 * 
	 * an example of collapsed would be basic dependencies as 
	 * prep(based-7, in-8) // 1 based index
	 * pobj(in-8, LA-9)
	 * 
	 * becomes
	 * prep_in(based-7, LA-9)
	 * 
	 * Propagation of conjunction example (still problems)
	 * conjunction between makes and distributes should result in the propagation
	 * of subject object relations that exist on the first conjunct
	 * nsubj(distributes-13, Bell-1)
	 * dobj(distributes-13, products-15)
	 * 
	 * However current parser is unable to generate the second relation
	 * 
	 * @param text 
	 * @return
	 */
	public static DeptCCProcessed parseDeptCCProcessed(String text){
		
		DeptCCProcessed result = new DeptCCProcessed();
		String[] tokens = text.split("\t");	
		result.globalSentenceId = tokens[0];
		result.dependencies = new ArrayList<GrammaticalDependency>();

		String[] depTokens = tokens[1].split("\\|");
		for(int i=0; i<depTokens.length; i++){
			String[] gramTokens = depTokens[i].split(" ");
			GrammaticalDependency dep = new GrammaticalDependency();
			dep.governor = Integer.parseInt(gramTokens[0]);
			dep.dependencyType = gramTokens[1];
			dep.dependent = Integer.parseInt(gramTokens[2]);
			result.dependencies.add(dep);
		}

		return result;
	}
	
	public static SemanticGraphContainer parseDeptCCProcessedSemanticGraph(String text){
		SemanticGraphContainer result = new SemanticGraphContainer();
		String[] tokens = text.split("\t");	
		result.globalSentenceId = tokens[0];
		result.graph = SemanticGraph.valueOf(tokens[1]);
		return result;
	}

	//TODO: parser for articleIDs if we care enough to use them

	////////////////////////////////////////////////////////////////////////
	//Inner classes to contain all information of a single annotation type
	////////////////////////////////////////////////////////////////////////

	public static class Text{
		public String globalSentenceId;
		public String sentence;
		@Override
		public String toString(){
			LinkedList<String> l = new LinkedList<String>();
			l.add(sentence);
			return privateToString(globalSentenceId, "Sentence", l);
		}
	}

	public static class Meta{
		public String globalSentenceId;
		public int lineNumber;
		public String originalFileName;
		public int beginCharOffset;
		public int endCharOffset;
		
		@Override
		public String toString(){
			// Little wasteful but these 4 list are a necessary 
			// memory waste for scalable toString method
			// But it will all be released sometime shortly after
			// end of this block
			List<String> lineNumbers = new ArrayList<String>();
			List<String> originalFileNames = new ArrayList<String>();
			List<String> beginCharOffsets = new ArrayList<String>();
			List<String> endCharOffsets = new ArrayList<String>();
			
			// Add the values that correspond to meta data
			lineNumbers.add(""+lineNumber);
			originalFileNames.add(originalFileName);
			beginCharOffsets.add(""+beginCharOffset);
			endCharOffsets.add(""+endCharOffset);
			
			Map<String, List<String>> map = new HashMap<String, List<String>>();
			map.put("Line Number", lineNumbers);
			map.put("Original Filename", originalFileNames);
			map.put("Beginning Character Offset", beginCharOffsets);
			map.put("Ending Character Offset", endCharOffsets);
			
			return privateToString(globalSentenceId, map);
		}
	}

	public static class Tokens{
		public String globalSentenceId;
		public List<String> tokens;
		
		public int size(){
			return tokens.size();
		}
		
		/**
		 * Returns Token at position
		 * @param i index with in bounds 0 -> size()-1
		 * @return Token
		 */
		public String get(int i){
			return tokens.get(i);
		}
		
		@Override
		public String toString(){
			return privateToString(globalSentenceId, "Tokens", tokens);
		}
	}

	public static class Stanfordlemma{
		public String globalSentenceId;
		public List<String> lemma;

		public int size(){
			return lemma.size();
		}
		
		/**
		 * Returns Lemma at position
		 * @param i index with in bounds 0 -> size()-1
		 * @return Lemma
		 */
		public String get(int i){
			return lemma.get(i);
		}
		
		@Override
		public String toString(){
			return privateToString(globalSentenceId, "Lemmas", lemma);
		}
	}

	public static class TokenSpans{
		public String globalSentenceId;
		public List<Range> tokenSpans;

		public int size(){
			return tokenSpans.size();
		}
		
		/**
		 * Returns Lemma at position
		 * @param i index with in bounds 0 -> size()-1
		 * @return Range at given position
		 */
		public Range get(int i){
			return tokenSpans.get(i);
		}
		
		@Override
		public String toString(){
			List<String> list = new ArrayList<String>(tokenSpans.size());
			for (Range r : tokenSpans)
				list.add(r.toString());
			return privateToString(globalSentenceId, "Ranges", list);
		}
	}

	public static class Stanfordpos{
		public String globalSentenceId;
		public List<String> pos;

		public int size(){
			return pos.size();
		}
		
		/**
		 * Returns POS tag at position
		 * @param i index with in bounds 0 -> size()-1
		 * @return POS at given position
		 */
		public String get(int i){
			return pos.get(i);
		}
		
		@Override
		public String toString(){
			return privateToString(globalSentenceId, "POS TAGs", pos);
		}
	}

	public static class Stanfordner{
		public String globalSentenceId;
		public List<nlp.StanfordNerTag> ner;	

		public int size(){
			return ner.size();
		}
		
		/**
		 * Returns NER Tag tag at position
		 * @param i index with in bounds 0 -> size()-1
		 * @return NER Tag at given position
		 */
		public nlp.StanfordNerTag get(int i){
			return ner.get(i);
		}
		
		@Override
		public String toString(){
			List<String> list = new ArrayList<String>(ner.size());
			for (nlp.StanfordNerTag tag : ner)
				list.add(tag.toString());
			return privateToString(globalSentenceId, "NER TAGs", list);
		}
	}

	public static class Wikification{
		public String globalSentenceId;
		public List<WikificationInfo> wiki;

		public int size(){
			return wiki.size();
		}
		
		/**
		 * Returns NER Tag tag at position
		 * @param i index with in bounds 0 -> size()-1
		 * @return NER Tag at given position
		 */
		public WikificationInfo get(int i){
			return wiki.get(i);
		}
		
		public static class WikificationInfo{
			//this is the token range, NOT the character index
			//the start inclusive, end exclusive rule still applies
			public Range range;
			//note: the wiki article name with spaces are in underscores
			public String wikiArticleName;
			public double confidence;

			public WikificationInfo(){};

			public WikificationInfo(Range range, String wikiName, double conf){
				this.range = range;
				this.wikiArticleName = wikiName;
				this.confidence = conf;
			}

			@Override
			public boolean equals(Object other){
				if(!(other instanceof WikificationInfo)){
					return false;
				}
				WikificationInfo o = (WikificationInfo)other;
				return this.confidence == o.confidence &&
						this.range.equals(o.range) &&
						this.wikiArticleName.equals(o.wikiArticleName);
			}

			@Override
			public int hashCode(){
				int result = 37;
				result = 31 * result + range.hashCode();
				result = 31 * result + wikiArticleName.hashCode();
				result = 31 * result + new Double(confidence).hashCode();
				return result;
			}
		}
	}

	/**
	 * TODO Ask Joe if the token range corresponds to the entity respect to the 
	 * relation and not with respect to where it appears in the sentence.
	 */
	public static class EntityPairMention{
		//Warning: relInstance shouldn't be here. Because of my (Joe) bad design, this seems to hint that
		//relationInstance is always provided as an annotation. This is not true when we are doing slot filling,
		//where we are trying to find the relation instance itself. So don't depend on its availability!
		public RelationInstance relInstance;
		public Range tokenRange1;
		public Range tokenRange2;
		public String globalSentenceId;

		public EntityPairMention(){}
		public EntityPairMention(String globalSentenceId, RelationInstance relInstance, Range range1, Range range2){
			this.relInstance = relInstance;
			this.tokenRange1 = range1;
			this.tokenRange2 = range2;
			this.globalSentenceId = globalSentenceId;
		}

		public String serialize(){
			String relInstanceLine = "";
			if(relInstance != null && relInstance.getRelationship() != null){
				relInstanceLine = "\t" + relInstance.getEntity1() + "\t" + 
					relInstance.getEntity2() + "\t" + relInstance.getRelationship().name();
			}
			return globalSentenceId + relInstanceLine +
					"\t" + tokenRange1.toString() + "\t" + tokenRange2.toString();			
		}
		
		@Override
		public String toString(){
			return "[Sentence Id = " + globalSentenceId + ", Relation Instance: " + relInstance + " | " + relInstance.getEntity1() + " " + tokenRange1
					+ " | " + relInstance.getEntity2() + " " + tokenRange2 + "]";
		}

		public static EntityPairMention deserialize(String text){
			return parseEntityPairMention(text);
		}

		@Override
		public boolean equals(Object o){
			if(!(o instanceof EntityPairMention)){
				return false;
			}

			EntityPairMention other = (EntityPairMention) o;
			return other.relInstance.equals(this.relInstance) &&
					other.tokenRange1.equals(this.tokenRange1) &&
					other.tokenRange2.equals(this.tokenRange2) &&
					other.globalSentenceId.equals(this.globalSentenceId);
		}

		@Override
		public int hashCode(){
			int code = 37;
			code = 31*code + this.relInstance.hashCode();
			code = 31*code + this.tokenRange1.hashCode();
			code = 31* code + this.tokenRange2.hashCode();
			code = 31 * code + this.globalSentenceId.hashCode();
			return code;
		}

	}

	public static class CJ{
		public String globalSentenceId;
		public Tree parseTree;
	}

	public static class DeptCCProcessed{
		public String globalSentenceId;
		public List<GrammaticalDependency> dependencies;
		
		@Override
		public String toString(){
			List<String> l = new ArrayList<String>();
			for (GrammaticalDependency g : dependencies)
				l.add(g.toString());
			return privateToString(globalSentenceId, "Grammatical Dependencies", l);
		}
	}

	public static class SemanticGraphContainer {
		public String globalSentenceId;
		public SemanticGraph graph;
		
	}
	
	public static class GrammaticalDependency{
		//these are token indices that start at 0,
		//not 1, as opposed to the warning in Xiao's document
		//I manually reduced everything by 1, because Congle's
		//RelationECML code makes that assumption that it starts at 0 too
		public int governor;
		public int dependent;
		public String dependencyType;
		
		@Override
		public String toString(){
			return "(Governer: " + governor + " Dependent: "
		+ dependent + " Dependency Type: " + dependencyType + ")";
		}
		
	}

	/**
	 * start inclusive, end exclusive
	 * @author sjonany
	 */
	public static class Range{
		private int start;
		private int end;

		public Range(int start, int end){
			this.start = start;
			this.end = end;
		}

		public int getStart(){
			return start;
		}

		public int getEnd(){
			return end;
		}

		/**
		 * @return true if there is an integer in this range that overlaps with the other
		 * note that 'end' is exclusive and 'start' is inclusive
		 */
		public boolean isIntersect(Range other){
			return this.start < other.end && other.start < this.end;
		}

		@Override
		public boolean equals(Object o){
			if(!(o instanceof Range)){
				return false;
			}

			Range other = (Range) o;
			return (other.end == this.end && other.start == this.start);
		}

		@Override
		public int hashCode(){
			return start + 31 * end;
		}

		@Override
		public String toString(){
			return start + ":" + end;
		}

		public static Range fromString(String s){
			String[] splitTokenSpan = s.split(":");
			int start = Integer.parseInt(splitTokenSpan[0]);
			int end = Integer.parseInt(splitTokenSpan[1]);
			return new Range(start, end);
		}
	}


	/**
	 * Allows the same format String representation
	 *  for all similar objects
	 * @return String
	 */
	private static String privateToString(String ID, String listName, 
			List<String> values){
		mSB.delete(0, mSB.length()); // Clear the buffer
		mSB.append("[GlobalSentenceID: ");
		mSB.append(ID);
		mSB.append(" | ");
		mSB.append(listName);
		mSB.append(": ");
		// For loop with index lets me use the index value
		for (int i = 0; i < values.size(); ++i)
			mSB.append((i+1) + ". " + values.get(i) + " ");
		mSB.delete(mSB.length()-1, mSB.length()); // Delete excess space
		mSB.append("]");
		return mSB.toString();
	}

	private static String privateToString(String ID, Map<String, List<String>> values){
		mSB.delete(0, mSB.length());
		mSB.append("[GlobalSentenceID: ");
		mSB.append(ID);

		List<String> titles = new ArrayList<String>(values.keySet());
		Collections.sort(titles);
		for (String title : titles) {
			if (title == null) continue; // NUll check
			List<String> tValues = values.get(title);
			if (tValues == null) continue;
			mSB.append(" | ");
			mSB.append(title);
			mSB.append(": ");
			for (int i = 0; i < tValues.size(); ++i)
				mSB.append((i+1) + ". " + tValues.get(i) + " ");
		}
		mSB.append("]");
		return mSB.toString();
	}
}
