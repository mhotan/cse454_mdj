package trainingDataGenerator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import knowledgeBase.Entity;
import knowledgeBase.KnowledgeBase;
import knowledgeBase.OfflineWikiFreebaseMapper;
import knowledgeBase.RelationInstance;
import knowledgeBase.Relationship;
import knowledgeBase.WikiFreebaseMapper;
import trainingDataGenerator.AnnotatedSentenceParser.EntityPairMention;
import trainingDataGenerator.AnnotatedSentenceParser.Range;
import trainingDataGenerator.AnnotatedSentenceParser.Wikification;
import trainingDataGenerator.AnnotatedSentenceParser.Wikification.WikificationInfo;
import trainingDataGenerator.AnnotatedSentenceParserAdapter.AnnotationParsingException;
import trainingDataGenerator.TrainingCandidateFilter.FilterLevel;
import util.Logger;

/**
 * This is the FIRST step towards generating training data for Multi-R
 * This class is used to extract out entity pairs, the sentences containing them and the annotations.
 * The input is xiao's annotated sentences, output is a folder structure containing
 * a subset of xiao's annotated sentences with an extra annotation file each, which saves
 * the relation instance corresponding to a sentence.
 * The entity pair mentions that share the same relation instance are clumped together.
 * @author sjonany
 */
public class TrainingCandidateDriver {
	//any wikification entities with confidence lower than this will not be considered
	private static final double MIN_WIKI_ENTITY_CONFIDENCE = 0.5;
	
	public static void main(String[] args) throws Exception{
		if(args.length != 1){
			String choices = "";
			for(FilterLevel filter : FilterLevel.values()){
				choices += filter.name() + " ";
			}
			
			System.out.println("Usage = " + choices);
			System.out.println("The filter are cumulative. E.g. If choose dependency, then all filters will be used");
			return;
		}
				
		TrainingCandidateFilter trainingCandidateFilter = new TrainingCandidateFilter(FilterLevel.valueOf(args[0]));
		
		setupDirectoryStructure();
		
		KnowledgeBase knowledgeBase = KnowledgeBase.getInstance();
		AnnotatedCorpusReader corpus = new AnnotatedCorpusReader(
				TrainingConfig.INPUT_ANNOTATIONS_BASE_DIR, TrainingConfig.INPUT_CONSTANTS);
		TrainingCandidateGenerator trainer = new TrainingCandidateGenerator();
		WikiFreebaseMapper wikiFreebaseMapper = new OfflineWikiFreebaseMapper();
		
		Logger.writeLine("Begin reading from corpus");
		int lineCount = 0;
		while(corpus.hasNext()){
			lineCount++;
			if(lineCount % 100000 == 0){
				Logger.writeLine("Lines from corpus read: " + lineCount);
			}
			
			Map<AnnotationConstant, String> annotatedSentence = corpus.next();
			AnnotatedSentenceParserAdapter annotatedSentenceAdapter = new AnnotatedSentenceParserAdapter(annotatedSentence);
			
			Wikification wikiInfo = annotatedSentenceAdapter.getWikification();
			
			//sort in descending order of confidence
			Collections.sort(wikiInfo.wiki, new Comparator<WikificationInfo>(){
				@Override
				public int compare(WikificationInfo o1, WikificationInfo o2) {
					if(o1.confidence > o2.confidence){
						return -1;
					}else if(o1.confidence == o2.confidence){
						return 0;
					}
					return 1;
				}
				
			});

			//prune the entities by obtaining the maximal non-intersecting set,
			//greedily added according to its confidence
			//make sure that the two entities consist of different tokens
			//sometimes wikification spits out multiple entities with overlapping tokens

			List<Entity> entities = new ArrayList<Entity>();
			List<Range> entityTokenRange = new ArrayList<Range>();
			
			for(WikificationInfo wikiEntity : wikiInfo.wiki){
				//don't include  unconvincing results
				if(wikiEntity.confidence < MIN_WIKI_ENTITY_CONFIDENCE){
					break;
				}
				
				//check that the this new entity does not intersect any of the previous ones
				boolean isIntersect = false;
				for(Range r : entityTokenRange){
					if(r.isIntersect(wikiEntity.range)){
						isIntersect = true;
						break;
					}
				}
				
				if(!isIntersect){
					Entity knowledgeBaseEntity = wikiFreebaseMapper.convertToEntity(wikiEntity.wikiArticleName);
					//if the wikification entity is not recognized by the knowledge base, discard it
					if(knowledgeBaseEntity != null){
						entities.add(knowledgeBaseEntity);
						entityTokenRange.add(wikiEntity.range);
					}
				}
			}

			List<EntityPairMention> mentionPairs = new ArrayList<EntityPairMention>();
			
			//generate all the possible valid pairs (order matters) of entities
			//WARNING: assumes that all the entities are non-intersecting tokens
			for(int i=0; i<entities.size(); i++){
				for(int j=0; j<entities.size(); j++){
					if(i != j){
						EntityPairMention pairMention = new EntityPairMention();
						pairMention.globalSentenceId = wikiInfo.globalSentenceId;
						pairMention.tokenRange1 = entityTokenRange.get(i);
						pairMention.tokenRange2 = entityTokenRange.get(j);
						pairMention.relInstance = new RelationInstance(entities.get(i), entities.get(j), null);
						
						mentionPairs.add(pairMention);
					}
				}
			}

			//the pair mentions that will actually be used for training
			List<EntityPairMention> trainingPairs = new ArrayList<EntityPairMention>();
			//for each pair make it EITHER a list of positive training samples, with differing relationships, OR
			//a single negative training data
			for(EntityPairMention pair : mentionPairs){
				Entity e1 = pair.relInstance.getEntity1();
				Entity e2 = pair.relInstance.getEntity2();
				
				//what are the possible relationships for which training data can be generated given the sentence?
				Relationship[] possibleRelationships = new Relationship[0];
				
				try{
					possibleRelationships = trainingCandidateFilter.getPossibleRelationships(annotatedSentenceAdapter, pair);
				}catch(AnnotationParsingException e){
					Logger.writeLine(annotatedSentenceAdapter.toString());
					Logger.writeLine(pair.serialize());
				}
				
				boolean isPairRelated = false;
				for(Relationship r : possibleRelationships){	
					RelationInstance relationInstance = new RelationInstance(e1,e2,r);
					boolean isFact = knowledgeBase.isAssertionTrue(relationInstance);
					if(isFact){
						//positive training data
						EntityPairMention trainingPair = 
								new EntityPairMention(pair.globalSentenceId, relationInstance, pair.tokenRange1, pair.tokenRange2);
						trainingPairs.add(trainingPair);
						isPairRelated = true;
					}
				}

				//heuristic used by NYU2011 to reduce number of irrelevant NA samples
				boolean isPairCloseEnough = Math.abs(pair.tokenRange1.getStart()-pair.tokenRange2.getStart())<=12;
				if(!isPairRelated && isPairCloseEnough){
					//there is no relationship that connects these two entities.
					//possibility of a negative example
					
					//true iff for all relationships such that r<e1,*> exists, there exists a counter example
					boolean allValidRelationshipsHaveCounter = true;
					//exist <i,j'> where j' != j, I need this on top of the all valid boolean because I don't
					//want to include case where there are no valid relationships, which will cause allValid to be true
					boolean existsCounterexample = false;
					//is negative data only if there also exists <i,j'> where j' != j 
					//and this holds true for all relationships such that i is a valid first argument 
					for(Relationship r : possibleRelationships){
						boolean existCounter = knowledgeBase.getSatisfyingEntities(e1, r).size() != 0;
						existsCounterexample |= existCounter;
						if(knowledgeBase.isFirstArgumentEntity(e1, r) && !existCounter){
							allValidRelationshipsHaveCounter = false;
							break;
						}
					}
					
					if(allValidRelationshipsHaveCounter && existsCounterexample){
						RelationInstance relInstance = (RelationInstance)pair.relInstance.clone();
						relInstance.setRelationship(Relationship.NA);
						//negative raining data
						EntityPairMention trainingPair = 
								new EntityPairMention(pair.globalSentenceId, relInstance, pair.tokenRange1, pair.tokenRange2);
						trainingPairs.add(trainingPair);
					}
				}
			}
			if(trainingPairs.size() != 0){
				trainer.insertTrainingData(annotatedSentence, trainingPairs);
			}
		}
		
		corpus.close();
		trainer.flushEntityPairMentions();
		trainer.close();
	}

	/**
	 * set up a bare folder structure and empty files for the training candidate output
	 * @throws IOException 
	 */
	private static void setupDirectoryStructure() throws IOException{
		for(Relationship r : Relationship.values()){
			for(AnnotationConstant c : TrainingConfig.TRAINING_DATA_CONSTANTS){
				String path = TrainingConfig.getAnnotationPathForTrainingCandidateOutput(r, c);
				
				File f = new File(path);
				f.getParentFile().mkdirs();
				f.createNewFile();
			}
			
			String path = TrainingConfig.getAbsoluteAnnotationPathForEntityPairMention(r);
			
			File f = new File(path);
			f.getParentFile().mkdirs();
			f.createNewFile();
		}
	}
}
