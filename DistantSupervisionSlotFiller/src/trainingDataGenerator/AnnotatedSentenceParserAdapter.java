package trainingDataGenerator;

import java.util.Map;

import trainingDataGenerator.AnnotatedSentenceParser.CJ;
import trainingDataGenerator.AnnotatedSentenceParser.DeptCCProcessed;
import trainingDataGenerator.AnnotatedSentenceParser.Meta;
import trainingDataGenerator.AnnotatedSentenceParser.SemanticGraphContainer;
import trainingDataGenerator.AnnotatedSentenceParser.Stanfordlemma;
import trainingDataGenerator.AnnotatedSentenceParser.Stanfordner;
import trainingDataGenerator.AnnotatedSentenceParser.Stanfordpos;
import trainingDataGenerator.AnnotatedSentenceParser.Text;
import trainingDataGenerator.AnnotatedSentenceParser.TokenSpans;
import trainingDataGenerator.AnnotatedSentenceParser.Tokens;
import trainingDataGenerator.AnnotatedSentenceParser.Wikification;


/**
 * Wanted to create another layer of indirecton for convenience 
 * over Joes awesome parser.  Instead of handling the annot
 * @author mhotan
 */
public class AnnotatedSentenceParserAdapter {
	/**
	 * Annotations defined by StanfordNLP 
	 */
	private final Map<AnnotationConstant, String> mAnnotations;

	/**
	 * 
	 * @param annotations
	 */
	public AnnotatedSentenceParserAdapter(Map<AnnotationConstant, String> annotations){
		mAnnotations = annotations;
	}

	/**
	 * Creates a base class where simple queries can be made between wihtout direct interaction
	 * with the underlying annotations or Entity mention
	 * @param sentence Full sequence of tokens as presented in text
	 * @param mention information regarding the two entities and their relation
	 */
	public AnnotatedSentenceParserAdapter(String sentence){
		// Note can alter or update the annotation generator 
		// TODO add representation invariant check to ensure that all values are covered
		mAnnotations = trainingDataGenerator.AnnotationGenerator.generateAnnotation(sentence);
		checkRep();
	}

	public Map<AnnotationConstant, String> getUnderlyingAnnotations(){
		return mAnnotations;
	}

	//////////////////////////////////////////////////////////////////////////////////////
	///// Setters
	//////////////////////////////////////////////////////////////////////////////////////

	//  TODO: ideally we should never have to change an isntance of this
	//  It should be construct it let the generator do everything and black box it away

	//////////////////////////////////////////////////////////////////////////////////////
	///// retreivers
	//////////////////////////////////////////////////////////////////////////////////////

	/**
	 * @return Textual representation of this sentence, or null if not Text data is provided
	 * @throws AnnotationParsingException 
	 */
	public Text getText() throws AnnotationParsingException{
		try{
			if (mAnnotations.get(AnnotationConstant.TEXT) == null) return null;
			return AnnotatedSentenceParser.parseText(mAnnotations.get(AnnotationConstant.TEXT));
		}catch(Exception e){
			throw new AnnotationParsingException(e.getMessage());
		}
	}

	/**
	 * @return Meta data representation of this sentence, or null if no Meta data is provided
	 * @throws AnnotationParsingException 
	 */
	public Meta getMeta() throws AnnotationParsingException{
		try{
			if (mAnnotations.get(AnnotationConstant.META) == null) return null;
			return AnnotatedSentenceParser.parseMeta(mAnnotations.get(AnnotationConstant.META));

		}catch(Exception e){
			throw new AnnotationParsingException(e.getMessage());
		}
	}

	/**
	 * @return Tokens of the given sentence, or null if no Tokens exist (MH: This really should never happen)
	 * @throws AnnotationParsingException 
	 */
	public Tokens getTokens() throws AnnotationParsingException{
		try{
			if (mAnnotations.get(AnnotationConstant.TOKENS) == null) return null;
			return AnnotatedSentenceParser.parseTokens(mAnnotations.get(AnnotationConstant.TOKENS));
		}catch(Exception e){
			throw new AnnotationParsingException(e.getMessage());
		}
	}

	/**
	 * @return Stanford Lemma of the represented sentence, or null if non exist
	 * @throws AnnotationParsingException 
	 */
	public Stanfordlemma getStanfordlemma() throws AnnotationParsingException{
		try{
			if (mAnnotations.get(AnnotationConstant.STANFORDLEMMA) == null) return null;
			return AnnotatedSentenceParser.parseStanfordlemma(mAnnotations.get(AnnotationConstant.STANFORDLEMMA));
		}catch(Exception e){
			throw new AnnotationParsingException(e.getMessage());
		}
	}

	/**
	 * Token Span is the character index of the first token to the end of the last
	 * @return The token spans of each individual token in the sentence
	 */
	public TokenSpans getTokenSpans(){
		if (mAnnotations.get(AnnotationConstant.TOKEN_SPANS) == null) return null;
		return AnnotatedSentenceParser.parseTokenSpans(mAnnotations.get(AnnotationConstant.TOKEN_SPANS));
	}	

	/**
	 * POS Tags
	 * @return Stanford POS tags for the entire string, or null if non exists
	 * @throws AnnotationParsingException 
	 */
	public Stanfordpos getStanfordpos() throws AnnotationParsingException{
		try{
			if (mAnnotations.get(AnnotationConstant.STANFORDPOS) == null) return null;
			return AnnotatedSentenceParser.parseStanfordpos(mAnnotations.get(AnnotationConstant.STANFORDPOS));
		}catch(Exception e){
			throw new AnnotationParsingException(e.getMessage());
		}
	}

	/**
	 * 0 represents not a named entity
	 * @return Stanford Named entity tags for the given sentence, or null if non exists 
	 * @throws AnnotationParsingException 
	 */
	public Stanfordner getStanfordner() throws AnnotationParsingException{
		try{
			if (mAnnotations.get(AnnotationConstant.STANFORDNER) == null) return null;
			return AnnotatedSentenceParser.parseStanfordner(mAnnotations.get(AnnotationConstant.STANFORDNER));

		}catch(Exception e){
			throw new AnnotationParsingException(e.getMessage());
		}
	}

	/**
	 * @return Wikification of the with regards to the string, or null if non exists
	 * @throws AnnotationParsingException 
	 */
	public Wikification getWikification() throws AnnotationParsingException{
		try{
			if (mAnnotations.get(AnnotationConstant.WIKIFICATION) == null) return null;
			return AnnotatedSentenceParser.parseWikification(mAnnotations.get(AnnotationConstant.WIKIFICATION));
		}catch(Exception e){
			throw new AnnotationParsingException(e.getMessage());
		}
	}

	/**
	 * @return syntactic parse tree of this sentece, or null if non exist
	 * @throws AnnotationParsingException 
	 */
	public CJ getCJ() throws AnnotationParsingException{
		try{
			if (mAnnotations.get(AnnotationConstant.CJ) == null) return null;
			return AnnotatedSentenceParser.parseCJ(mAnnotations.get(AnnotationConstant.CJ));
		}catch(Exception e){
			throw new AnnotationParsingException(e.getMessage());
		}
	}

	/**
	 * This dependency parse tree is based off of Stanford Dependency Condensed Parse tree 
	 * @return dependency Processed or null if non exists
	 * @throws AnnotationParsingException 
	 */
	public DeptCCProcessed getDeptCCProcessed() throws AnnotationParsingException{
		try{
			if (mAnnotations.get(AnnotationConstant.DEPS_STANFORD_CC_PROCESSED) == null) return null;
			return AnnotatedSentenceParser.parseDeptCCProcessed(mAnnotations.get(AnnotationConstant.DEPS_STANFORD_CC_PROCESSED));
		}catch(Exception e){
			throw new AnnotationParsingException(e.getMessage());
		}
	}

	/**
	 * @return Container with semantic graph
	 * @throws AnnotationParsingException 
	 */
	public SemanticGraphContainer getSemanticGraph() throws AnnotationParsingException{
		try{
			if (mAnnotations.get(AnnotationConstant.DEPS_STANFORD_CC_PROCESSED_SEMANTIC_GRAPH) == null) return null;
			return AnnotatedSentenceParser.parseDeptCCProcessedSemanticGraph(mAnnotations.get(AnnotationConstant.DEPS_STANFORD_CC_PROCESSED_SEMANTIC_GRAPH));

		}catch(Exception e){
			throw new AnnotationParsingException(e.getMessage());
		}
	}

	@Override
	public String toString(){
		return mAnnotations.toString();
	}

	public static class AnnotationParsingException extends Exception{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public AnnotationParsingException(String msg){
			super(msg);
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////
	///// private helper methods for this class
	//////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Checks the internal represetion of this entity
	 * TODO: (Consult with Joe) It should be for every key of type
	 * AnnotationConstant in mAnnotations there is a non null value associated with it
	 * That be nice i think ideal.  It should be a do it once grab all the annotations
	 */
	private void checkRep() {
		// TODO The check should look something like this
		assert mAnnotations != null;
		assert mAnnotations.get(AnnotationConstant.TEXT) != null;
		assert mAnnotations.get(AnnotationConstant.META) != null;
		assert mAnnotations.get(AnnotationConstant.TOKENS) != null;
		assert mAnnotations.get(AnnotationConstant.STANFORDLEMMA) != null;
		assert mAnnotations.get(AnnotationConstant.TOKEN_SPANS) != null;
		assert mAnnotations.get(AnnotationConstant.STANFORDPOS) != null;
		assert mAnnotations.get(AnnotationConstant.STANFORDNER) != null;
		assert mAnnotations.get(AnnotationConstant.WIKIFICATION) != null;
		assert mAnnotations.get(AnnotationConstant.CJ) != null;
		assert mAnnotations.get(AnnotationConstant.DEPS_STANFORD_CC_PROCESSED) != null;
	}

}
