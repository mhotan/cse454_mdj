package trainingDataGenerator;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import knowledgeBase.RelationInstance;
import knowledgeBase.Relationship;
import trainingDataGenerator.AnnotatedSentenceParser.EntityPairMention;
import trainingDataGenerator.AnnotatedSentenceParser.Range;

/**
 * handles the IO and formatting for training candidate generation
 * called by TrainingCandidateDriver
 * Don't forget to call close() when you don't want to generate anymore training data
 * @author sjonany
 */
public class TrainingCandidateGenerator {
	//each <relationship, annotation constant> has its own writer
	private HashMap<TrainingType, PrintWriter> annotationWriters;
	
	//key = relationship, value = see below, all the relation instances which correspond to that relationship
	//key = <e1,e2,r>,  value = list of the locations where such relation instance is found
	//TODO: if not enough space, no need to partition by relationship
	private HashMap<Relationship, HashMap<RelationInstance, List<EntityPairLocation>>> relationInstanceIndices;
	
	public TrainingCandidateGenerator() throws IOException{
		relationInstanceIndices = new HashMap<Relationship, HashMap<RelationInstance, List<EntityPairLocation>>>();
		for(Relationship r : Relationship.values()){
			relationInstanceIndices.put(r, new HashMap<RelationInstance, List<EntityPairLocation>>());
		}
		
		//initialize the writers
		annotationWriters = new HashMap<TrainingType, PrintWriter>();
		for(Relationship r: Relationship.values()){
			for(AnnotationConstant c : TrainingConfig.TRAINING_DATA_CONSTANTS){
				String filename = TrainingConfig.getAnnotationPathForTrainingCandidateOutput(r, c);
				annotationWriters.put(new TrainingType(r, c),
						new PrintWriter(new BufferedWriter(new FileWriter(filename, true))));
			}
		}
	}
	
	//TODO: if there is a failure before relationInstanceIndices are flushed to disk,
	//all the work will be lost. maybe figure out a way to do partial backups?
	
	/**
	 * add a training data generated from a single sentence and write it to disk
	 * call this method ONCE per sentence in the corpus
	 * After the corpus has been read, invoke flushEntityPairMentions ONCE
	 * @param annotatedSentence the annotations corresponding to a single sentence
	 * @param mentionList the entity pair mentions that correspond to the annotated sentence.
	 * 		Contains both positive (relationship != NA) and and negative (r = NA) training data
	 * @throws IOException 
	 */
	public void insertTrainingData(Map<AnnotationConstant, String> annotatedSentence, List<EntityPairMention> mentionList) throws IOException{
		//group the input according to its relationship
		HashMap<Relationship, List<EntityPairMention>> groupedInput = new HashMap<Relationship, List<EntityPairMention>>();
		for(EntityPairMention mention : mentionList){
			Relationship relationship = mention.relInstance.getRelationship();
			List<EntityPairMention> mentions = groupedInput.get(relationship);
			if(mentions == null){
				mentions = new ArrayList<EntityPairMention>();
				groupedInput.put(relationship, mentions);
			}
			
			mentions.add(mention);
		}
		
		for(Relationship relationship : groupedInput.keySet()){
			//note: by doing this per relationship, and since this method is called once per sentence,
			//there won't be any duplicated annotation in each relationship folder
			//write down all the annotations corresponding to this sentence to disk
			for(AnnotationConstant c: TrainingConfig.TRAINING_DATA_CONSTANTS){
				TrainingType trainingType = new TrainingType(relationship, c);
				PrintWriter writer = annotationWriters.get(trainingType);
				
				//for the input constants, we just directly copy paste the exact same preprocessed tokens
				String serialization = annotatedSentence.get(c);
				if(serialization == null){
					throw new IllegalArgumentException("No annotation is provided for " + c.name());
				}
				writer.println(serialization);
			}
			//a subset of the training data that corresponds to a particular relationship
			List<EntityPairMention> mentionSubset = groupedInput.get(relationship);
			
			HashMap<RelationInstance, List<EntityPairLocation>> singleRelationshipIndices = relationInstanceIndices.get(relationship);
			//we need to group together for each relation instance, all the entity pair mentions in the corpus
			//this is NOT yet written to disk because we need to go through all the corpus first
			//flushEntityPairMentions() will dump this work last 
			for(EntityPairMention mention : mentionSubset){
				List<EntityPairLocation> collectedMentions = singleRelationshipIndices.get(mention.relInstance);
				if(collectedMentions == null){
					collectedMentions = new ArrayList<EntityPairLocation>();
					singleRelationshipIndices.put(mention.relInstance, collectedMentions);
				}
				collectedMentions.add(new EntityPairLocation(mention));
			}
		}
	}
	
	/**
	 * call this function AFTER there will be no more call to insertTrainingData
	 * This will flush all the entity pair mentions to disk
	 * This is done last because each relation instance must have its complete set of sentence annotations 
	 * @throws IOException 
	 */
	public void flushEntityPairMentions() throws IOException{
		for(Relationship r : relationInstanceIndices.keySet()){
			String filename = TrainingConfig.getAbsoluteAnnotationPathForEntityPairMention(r);
			PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(filename, true)));
			
			HashMap<RelationInstance, List<EntityPairLocation>> singleRelationshipIndices = relationInstanceIndices.get(r);
			
			//each line's format is 
			//[relationInstance dump] +  ('|' + [EntityPairMention dump])*
			for(RelationInstance relInstance: singleRelationshipIndices.keySet()){
				writer.write(relInstance.toStringDump());
				for(EntityPairLocation location : singleRelationshipIndices.get(relInstance)){
					writer.write( "|" + location.serialize());
				}
				writer.write("\n");
			}
			
			writer.close();
		}
	}
	
	public void close(){
		for(PrintWriter writer : annotationWriters.values()){
			writer.close();
		}
	}
	
	/**
	 * stores just the location of a relation instance, but nothing about the arguments themselves
	 * used to save memory usage and shortens dump
	 * @author sjonany
	 */
	public static class EntityPairLocation{
		private String globalSentenceId;
		private Range range1;
		private Range range2;
	
		private EntityPairLocation(){}
		public EntityPairLocation(EntityPairMention mention){
			this.globalSentenceId = mention.globalSentenceId;
			this.range1 = mention.tokenRange1;
			this.range2 = mention.tokenRange2;
		}
		
		public String getGlobalSentenceId(){
			return globalSentenceId;
		}
		
		public Range getRange1(){
			return range1;
		}
		
		public Range getRange2(){
			return range2;
		}
		
		public String serialize(){
			return globalSentenceId + "\t" + range1 + "\t" + range2;
		}
		
		public static EntityPairLocation deserialize(String text){
			String[] tokens = text.split("\t");
			EntityPairLocation result = new EntityPairLocation();
			
			result.globalSentenceId = tokens[0];
			result.range1 = Range.fromString(tokens[1]);
			result.range2 = Range.fromString(tokens[2]);
			
			return result;
		}
	}
	
	/**
	 * A single annotation file is tied to a single TrainingType.
	 * E.g. /.../date_of_birth/positive/sentences.tokens
	 */
	private static class TrainingType{
		private Relationship relationship;
		private AnnotationConstant annotationConstant;
		private boolean isHashcodeSet;
		private int hashcode;
		
		public TrainingType(Relationship r, AnnotationConstant c){
			this.relationship = r;
			this.annotationConstant = c;
			this.isHashcodeSet = false;
			hashcode = 0;
		}
		
		@Override
		public boolean equals(Object other){
			if(!(other instanceof TrainingType)){
				return false;
			}
			
			TrainingType o = (TrainingType) other;
			return  o.relationship.equals(this.relationship)&&
					o.annotationConstant.equals(this.annotationConstant);
		}
		
		@Override
		public int hashCode(){
			if(!isHashcodeSet){
				hashcode = 17;
				hashcode = 31 * hashcode + relationship.hashCode();
				hashcode = 31 * hashcode + annotationConstant.hashCode();
				
				isHashcodeSet = true;
			}
			
			return hashcode;
		}
	}
}
