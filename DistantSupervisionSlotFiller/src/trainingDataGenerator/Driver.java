package trainingDataGenerator;

import benchmark.BenchmarkDriver;
import benchmark.ManualBenchmarkDriver;
import benchmark.ManualPrecisionDriver;
import classifier.ClassifierBrainViewer;
import classifier.ClassifierDriver;
import classifier.ClassifierEvaluator;
import edu.uw.cs.multir.main.Main;

/**
 * Driver for training
 * because I am not fluent with running & setting cp thru command line :/
 * @author sjonany
 */
public class Driver {
	public static void main(String[] args) throws Exception{
		if(args.length == 0){
			printUsage();
			return;
		}
	
		String[] argsNext = new String[args.length-1];
		for(int i=0 ; i<argsNext.length; i++){
			argsNext[i] = args[i+1];
		}
		
		if(args[0].equals("candidate")){
			TrainingCandidateDriver.main(argsNext);
		}else if(args[0].equals("eval")){
			ClassifierEvaluator.main(argsNext);
		}else if(args[0].equals("multir")){
			Main.main(argsNext);
		}else if(args[0].equals("classifierdriver")){
			ClassifierDriver.main(argsNext);
		}else if(args[0].equals("brainviewer")){
			ClassifierBrainViewer.main(argsNext);
		}else if(args[0].equals("candidateToTrain")){
			PreprocessFromCandidate.main(argsNext);
		}else if(args[0].equals("auto")){
			BenchmarkDriver.main(argsNext);
		}else if(args[0].equals("manual")){
			ManualBenchmarkDriver.main(argsNext);
		}else if(args[0].equals("grading")){
			ManualPrecisionDriver.main(argsNext);
		}else{
			printUsage();
			return;
		}
	}
	
	private static void printUsage(){
		System.out.println("Usage: {candidate | candidateToTrain | multir | eval | classifierdriver | brainviewer | auto | manual | grading}");
	}
}
