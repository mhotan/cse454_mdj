package trainingDataGenerator;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import nlp.CoreNLP;
import trainingDataGenerator.AnnotationConstant;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.NamedEntityTagAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.LexedTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.process.WordToSentenceProcessor;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.PennTreebankLanguagePack;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import edu.stanford.nlp.trees.TypedDependency;
import edu.stanford.nlp.trees.TreeCoreAnnotations.TreeAnnotation;
import edu.stanford.nlp.trees.semgraph.SemanticGraph;
import edu.stanford.nlp.trees.semgraph.SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation;
import edu.stanford.nlp.util.CoreMap;
import features.components.Graph;

/**
 * generates annotations for a corpus
 * follows the convention used by Xiao Ling
 * WARNING: This is likely to be super slow, so only use this to annotate before hand
 * or just for debugging made up sentences
 * @author sjonany
 */
public class AnnotationGenerator {
	//short hack to get the formatting right
	private static int sentenceID = 0;
	
	/**
	 * Return the annotations pertaining to the given text
	 * note : everything in TrainingConfig.INPUT_CONSTANTS are provided except for META and WIKIFICATIONS
	 * you will have to hard code this for your example yourself
	 * sentence id is set to 1
	 * @param text a normal english sentence - no weird html stuff!
	 * @return annotations of the text
	 */
	public static Map<AnnotationConstant, String> generateAnnotation(String text){
		Map<AnnotationConstant, String> annotations = new HashMap<AnnotationConstant, String>();

		//text, tokens, tokenSpan
		extractSentencesAndTokenize(annotations, text);
		//CJ, dept cc processed
		parseCJ(annotations,text);
		//ner pos lemma
		parseNerPosLemma(annotations,text);
		
		return annotations;
	}

	//text, tokens, tokenSpan
	private static void extractSentencesAndTokenize (Map<AnnotationConstant, String> annotations, String text){
		String options = "invertible=true,ptb3Escaping=true";
		LexedTokenFactory<CoreLabel> ltf = new CoreLabelTokenFactory(true);

		WordToSentenceProcessor<CoreLabel> sen = new WordToSentenceProcessor<CoreLabel>();

		PTBTokenizer<CoreLabel> tok = new PTBTokenizer<CoreLabel>(
				new StringReader(text), ltf, options);
		List<CoreLabel> l = tok.tokenize();

		List<List<CoreLabel>> snts = sen.process(l);

		for (int s = 0; s < snts.size(); s++) {
			List<CoreLabel> snt = snts.get(s);

			StringBuilder sb2 = new StringBuilder();
			StringBuilder sb3 = new StringBuilder();

			int sntBegin = snt.get(0).beginPosition();
			for (CoreLabel cl : snt) {
				if (sb2.length() > 0)
					sb2.append(" ");
				// To avoid stanford corenlp group words
				// into a huge word between angle brackets,
				// which will confuse the cj parser (Xiao)
				if (cl.word().startsWith("<") && cl.word().endsWith(">")) {
					sb2.append("LONG_WORD");
				} else {
					sb2.append(cl.word());
				}

				if (sb3.length() > 0)
					sb3.append(" ");
				int from = cl.beginPosition() - sntBegin;
				int to = cl.endPosition() - sntBegin;
				sb3.append(from + ":" + to);
			}

			String rawSnt = text.substring(sntBegin, snt.get(snt.size() - 1)
					.endPosition());

			//w1.write(sentenceID + "\t" + rawSnt + "\n");
			annotations.put(AnnotationConstant.TEXT, sentenceID + "\t" + rawSnt);

			//w2.write(sentenceID + "\t" + sb2.toString() + "\n");
			annotations.put(AnnotationConstant.TOKENS, sentenceID + "\t" + sb2.toString());

			//w3.write(sentenceID + "\t" + sb3.toString() + "\n");
			annotations.put(AnnotationConstant.TOKEN_SPANS, sentenceID + "\t" + sb3.toString());
		}
	}
	
	//cj, depts cc processed
	private static void parseCJ (Map<AnnotationConstant, String> annotations, String text){
		//TODO: how to generate cj tree
		StanfordCoreNLP nlp = CoreNLP.getInstance();
		Annotation document = new Annotation(text);

		// run all Annotators on this text
		nlp.annotate(document);

		// these are all the sentences in this document
		// a CoreMap is essentially a Map that uses class objects as keys and has values with custom types
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);

		for(CoreMap sentence: sentences) {
			Tree tree = sentence.get(TreeAnnotation.class);
			annotations.put(AnnotationConstant.CJ, sentenceID + "\t" + tree.toString());

			TreebankLanguagePack tlp = new PennTreebankLanguagePack();
			GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();
			GrammaticalStructure gs = gsf.newGrammaticalStructure(tree);
			Collection<TypedDependency> tdl = null;
			try {
				tdl = /*gs.allTypedDependencies();*/ gs.typedDependenciesCCprocessed();
			} catch (NullPointerException e) {
				// there has to be a bug in EnglishGrammaticalStructure.collapseFlatMWP
				tdl = new ArrayList<TypedDependency>();
			}
			
			StringBuilder sb = new StringBuilder();
			List<TypedDependency> l = new ArrayList<TypedDependency>();
			l.addAll(tdl);
			for (int i=0; i < tdl.size(); i++) {
				TypedDependency td = l.get(i);
				String name = td.reln().getShortName();
				if (td.reln().getSpecific() != null)
					name += "-" + td.reln().getSpecific();				
				sb.append((td.gov().index()-1) + " ");
				sb.append(name + " ");
				sb.append((td.dep().index()-1));
				if (i < tdl.size()-1)
					sb.append("|");
			}
			
			//w.write(t[0] + "\t" + sb.toString() + "\n");
			annotations.put(AnnotationConstant.DEPS_STANFORD_CC_PROCESSED, sentenceID + "\t" + sb.toString());
			SemanticGraph semGraph = sentence.get(CollapsedCCProcessedDependenciesAnnotation.class);
			annotations.put(AnnotationConstant.DEPS_STANFORD_CC_PROCESSED_SEMANTIC_GRAPH, sentenceID + "\t" + semGraph.toCompactString(true));
			//remove duplicates from deps
			
			boolean KBEST = false; //true;
			String str = annotations.get(AnnotationConstant.DEPS_STANFORD_CC_PROCESSED);
			String[] t = str.split("\t");
			if ((KBEST && t.length != 3) ||
					(!KBEST && t.length != 2)) {
				annotations.put(AnnotationConstant.DEPS_STANFORD_CC_PROCESSED, str.trim());
				return;
			}
			String[] d = t[KBEST? 2 : 1].split("\\|");
			Set<String> hs = new HashSet<String>();
			for (int i=0; i < d.length; i++) hs.add(d[i]);
			
			//w.write(t[0] + "\t");
			str = t[0] + "\t";
			if (KBEST) str += (t[1] + "\t");			
			boolean first = true;
			for (String s : hs) {
				if (!first) str += ("|");
				str += s;
				first = false;
			}
			return;
		}
	}
	
	private static void parseNerPosLemma(Map<AnnotationConstant, String> annotations, String text){

		StanfordCoreNLP nlp = CoreNLP.getInstance();
		Annotation document = new Annotation(text);

		// run all Annotators on this text
		nlp.annotate(document);

		// these are all the sentences in this document
		// a CoreMap is essentially a Map that uses class objects as keys and has values with custom types
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		for (CoreMap sentence : sentences) {
			List<CoreLabel> tokens = sentence
					.get(CoreAnnotations.TokensAnnotation.class);

			// write pos
			String posText = "";
			posText += (sentenceID + "");
			posText += ("\t");
			for (int i = 0; i < tokens.size(); i++) {
				if (i > 0)
					posText += (" ");
				posText+=(tokens.get(i)
						.get(PartOfSpeechAnnotation.class));
			}
			
			// write lemma
			String lemmaText = "";
			lemmaText += (sentenceID + "");
			lemmaText += ("\t");
			for (int i = 0; i < tokens.size(); i++) {
				if (i > 0)
					lemmaText += (" ");
				lemmaText += (tokens.get(i).get(LemmaAnnotation.class));
			}
			lemmaText += ("\n");

			// write ner
			String nerText = "";
			nerText += (sentenceID + "");
			nerText += ("\t");
			for (int i = 0; i < tokens.size(); i++) {
				if (i > 0)
					nerText += (" ");
				String ner = tokens.get(i).get(
						NamedEntityTagAnnotation.class);
				if (ner == null) {
					//error
					break;
				}
				nerText += (ner);
			}
			
			annotations.put(AnnotationConstant.STANFORDPOS, posText.trim());
			annotations.put(AnnotationConstant.STANFORDNER, nerText.trim());
			annotations.put(AnnotationConstant.STANFORDLEMMA, lemmaText.trim());
			
			return;
		}	
	}
}
