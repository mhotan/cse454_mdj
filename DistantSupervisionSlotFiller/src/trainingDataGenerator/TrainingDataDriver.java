package trainingDataGenerator;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import knowledgeBase.RelationInstance;
import knowledgeBase.Relationship;
import trainingDataGenerator.TrainingCandidateGenerator.EntityPairLocation;
import util.Logger;
import cc.factorie.protobuf.DocumentProtos.Relation;
import cc.factorie.protobuf.DocumentProtos.Relation.RelationMentionRef;
import features.extractor.OverallFeatureExtractor;
import features.oldstuff.FeatureExtractor;

/**
 * NOTE: DEPECRATED
 * @see PreprocessFromCandidate to jump from candidate generation to combine multi'rs preprocess step without
 *       zipping/calling multi-r's preprocess 
 * generates training data for Multi-R input
 * This is the SECOND and final step to generating training data
 * The input is the files from TrainingCandidateDriver
 * The output is a train.pb, where if g-zipped, is exactly the train.pb.gz file
 * that is needed by Multi-R for training
 * @author sjonany
 */
public class TrainingDataDriver {
	//|NA| / |non-NA| 
	//The higher the ratio, the more likely classifier will say NA -> more false negatives
	//public static final double NA_TO_NON_NA_RATIO = 1.0;
	
	//limit to maximum number of samples for a single relation
	public static void main(String[] args) throws Exception{
		if(args.length != 3){
			System.out.println("Usage: NA_TO_NON_NA_RATIO(double) FeatureExtractorArgs candidatePath");
			return;
		}
	
		double NA_TO_NON_NA_RATIO = Double.parseDouble(args[0]);
		FeatureExtractor featureExtractor = OverallFeatureExtractor.getFeatureExtractor(args[1]);
		
		FileOutputStream out = new FileOutputStream(TrainingConfig.TRAINING_DATA_FINAL_OUTPUT_PATH, true);

		//Consume all training data for all relationships other than NA. For NA, we sample a max of |non-NA|*RATIO
		
		long totalNonNASamples = 0;
		//non-NA training data generation
		for(Relationship r : Relationship.values()){
			if(r.equals(Relationship.NA)){
				continue;
			}
			
			Logger.writeLine("Loading all annotations for " + r);
			Map<Integer, Map<AnnotationConstant, String>> annotationCache = initAnnotationCache(r);
			Logger.writeLine("Finished Loading all annotations for " + r);
			
			//for each mention, there is a feature vector
			//thus, it is more logical to undersample based on num mention, not num entity pair
			long mentionCount = 0;
			Logger.writeLine("Generating training data for relationship type : " + r);
			//read the entity pair mentions					
			BufferedReader relationInstanceReader = new BufferedReader(
					new InputStreamReader(new FileInputStream(TrainingConfig.getAbsoluteAnnotationPathForEntityPairMention(r)), "UTF-8"));
			String relInstanceLine  = relationInstanceReader.readLine();
			
			//for each relation instance, we look at all the mentions in the corpus,
			//and output relationship
			while(relInstanceLine != null){
				if(mentionCount %10000 == 0){
					Logger.writeLine("" + mentionCount);
				}
				//parse relInstanceLine
				String[] tokens = relInstanceLine.split("\\|");
				RelationInstance relationInstance = RelationInstance.fromStringDump(tokens[0]);
				List<EntityPairLocation> mentions = new ArrayList<EntityPairLocation>();
				//first token is relation instance, the rest are entity pair locations
				for(int i=1; i<tokens.length; i++){
					mentions.add(EntityPairLocation.deserialize(tokens[i]));
					totalNonNASamples++;
					mentionCount++;
				}
				
				writeRelationToDisk(relationInstance, mentions, out, annotationCache, featureExtractor);
				relInstanceLine = relationInstanceReader.readLine();
			}
			
			relationInstanceReader.close();
		}
		
		//NA training data generation
		long numNAToRead = (long)(totalNonNASamples * NA_TO_NON_NA_RATIO);
		//now for the NA samples, where |NA| =  ratio * (|non-NA|)
		long mentionCount = 0;
		
		Logger.writeLine("Loading all annotations for " + Relationship.NA);
		Map<Integer, Map<AnnotationConstant, String>> annotationCache = initAnnotationCache(Relationship.NA, numNAToRead, TrainingConfig.OUTPUT_ANNOTATIONS_BASE_DIR);
		Logger.writeLine("Finished Loading all annotations for " + Relationship.NA);

		Logger.writeLine("Generating training data for relationship type : " + Relationship.NA);
		//read the entity pair mentions					
		BufferedReader relationInstanceReader = new BufferedReader(
				new InputStreamReader(new FileInputStream(TrainingConfig.getAbsoluteAnnotationPathForEntityPairMention(Relationship.NA)), "UTF-8"));
		String relInstanceLine  = relationInstanceReader.readLine();
		
		//for each relation instance, we look at all the mentions in the corpus,
		//and output relationship
		while(relInstanceLine != null){
			if(mentionCount %10000 == 0){
				Logger.writeLine("" + mentionCount);
			}
			
			//undersample NA
			if(mentionCount> numNAToRead){
				break;
			}
			
			//parse relInstanceLine
			String[] tokens = relInstanceLine.split("\\|");
			RelationInstance relationInstance = RelationInstance.fromStringDump(tokens[0]);
			List<EntityPairLocation> mentions = new ArrayList<EntityPairLocation>();
			//first token is relation instance, the rest are entity pair locations
			for(int i=1; i<tokens.length; i++){
				mentions.add(EntityPairLocation.deserialize(tokens[i]));
				mentionCount++;
				if(mentionCount > numNAToRead){
					break;
				}
			}
			
			writeRelationToDisk(relationInstance, mentions, out, annotationCache, featureExtractor);
			relInstanceLine = relationInstanceReader.readLine();
		}
		
		relationInstanceReader.close();
	}
	
	
	
	/**
	 * load all the relevant annotations needed to train r but only barely enough to produce the required number of mentions
	 * useful for generating training data for NA, because there are too many of them, and we are undersampling
	 * @param numMention the number of mentions you need. With this, we don't need to load the entire annotation cache, but 
	 * just enough to satisfy the needs writeRelationToDisk for the selected mentions.
	 * @return annotationCache - key = sentence id, value = annotation related to sentence
	 * @throws Exception 
	 */
	public static Map<Integer, Map<AnnotationConstant, String>> initAnnotationCache(Relationship r, long numMention, String candidateDir) throws Exception{
		BufferedReader relationInstanceReader = new BufferedReader(
				new InputStreamReader(new FileInputStream(TrainingConfig.getAbsoluteAnnotationPathForEntityPairMention(r, candidateDir)), "UTF-8"));
		String relInstanceLine  = relationInstanceReader.readLine();
		
		//collect the set of sentence Ids that we need to satisfy the inputs for numMention feature vectors 
		TreeSet<Integer> sentenceIds = new TreeSet<Integer>();
		
		long mentionCount = 0;
		while(relInstanceLine != null){
			if(mentionCount > numMention){
				break;
			}
			//parse relInstanceLine
			String[] tokens = relInstanceLine.split("\\|");
			//first token is relation instance, the rest are entity pair locations
			for(int i=1; i<tokens.length; i++){
				Integer id = Integer.parseInt(EntityPairLocation.deserialize(tokens[i]).getGlobalSentenceId());
				sentenceIds.add(id);
				mentionCount++;
			}
			relInstanceLine = relationInstanceReader.readLine();
		}
		relationInstanceReader.close();
		
		Map<Integer, Map<AnnotationConstant, String>> annotCache = new HashMap<Integer, Map<AnnotationConstant, String>>();
		AnnotatedCorpusReader candidates = new AnnotatedCorpusReader(
				TrainingConfig.getAnnotationFolderForTrainingCandidateOutput(r, candidateDir)
				,TrainingConfig.TRAINING_DATA_CONSTANTS);

		//go through the candidates, and pick out annotations
		while(candidates.hasNext()){
			Map<AnnotationConstant, String> sentenceAnnot = candidates.next();
			AnnotatedSentenceParserAdapter annotations = new AnnotatedSentenceParserAdapter(sentenceAnnot);
			int sentenceId = Integer.parseInt(annotations.getText().globalSentenceId);
			
			if(sentenceIds.contains(sentenceId)){
				//WARNING: If I don't make this copy, values in hashtable will keep changing value!!
				Map<AnnotationConstant, String> shallowCopy = new HashMap<AnnotationConstant, String>();
				shallowCopy.putAll(sentenceAnnot);
				annotCache.put(sentenceId, shallowCopy);
			}

		}
		
		candidates.close();
		
		return annotCache;
	}
	
	/**
	 * load all the relevant annotations needed to train r to memory
	 * @return annotationCache - key = sentence id, value = annotation related to sentence
	 * @throws Exception 
	 */
	public static Map<Integer, Map<AnnotationConstant, String>> initAnnotationCache(Relationship r) throws Exception{
		Map<Integer, Map<AnnotationConstant, String>> annotCache = new HashMap<Integer, Map<AnnotationConstant, String>>();
		AnnotatedCorpusReader candidates = new AnnotatedCorpusReader(
				TrainingConfig.getAnnotationFolderForTrainingCandidateOutput(r)
				,TrainingConfig.TRAINING_DATA_CONSTANTS);

		//go through the candidates, and pick out annotations
		while(candidates.hasNext()){
			Map<AnnotationConstant, String> sentenceAnnot = candidates.next();
			AnnotatedSentenceParserAdapter annotations = new AnnotatedSentenceParserAdapter(sentenceAnnot);
			int sentenceId = Integer.parseInt(annotations.getText().globalSentenceId);
			
			//WARNING: If I don't make this copy, values in hashtable will keep changing value!!
			Map<AnnotationConstant, String> shallowCopy = new HashMap<AnnotationConstant, String>();

			shallowCopy.putAll(sentenceAnnot);
			annotCache.put(sentenceId, shallowCopy);
		}
		
		candidates.close();
		
		return annotCache;
	}
	
	/**
	 * write a single training block to disk
	 * a training block is a relation instance, with a list of <entity pair mention,features>
	 * @param relationInstance
	 * @param mentions
	 * @param out
	 * @param annotationCache - key = sentence id, value = annotation related to sentence
	 * @throws Exception 
	 */
	public static void writeRelationToDisk(RelationInstance relationInstance, List<EntityPairLocation> mentions, FileOutputStream out,
			Map<Integer, Map<AnnotationConstant,String>> annotationCache, FeatureExtractor featureExtractor) throws Exception{
		Relation.Builder relationBuilder = Relation.newBuilder();
		
		relationBuilder.setSourceGuid(relationInstance.getEntity1().getMid());
		relationBuilder.setDestGuid(relationInstance.getEntity2().getMid());
		relationBuilder.setRelType(relationInstance.getRelationship().toString());
		
		
		for(EntityPairLocation location : mentions){
			int sentenceId = Integer.parseInt(location.getGlobalSentenceId());
			if(!annotationCache.containsKey(sentenceId)){
				throw new IllegalArgumentException("Missing annotation for sentenceID = " + sentenceId + ", relation = " + relationInstance);
			}
			
			AnnotatedSentenceParserAdapter annotations = new AnnotatedSentenceParserAdapter(annotationCache.get(sentenceId));
			
			RelationMentionRef.Builder refMention = RelationMentionRef.newBuilder();
			refMention.setFilename(annotations.getMeta().originalFileName);
			//Congle said these id's do not matter
			refMention.setDestId(-123214);
			refMention.setSourceId(-12234314);
			refMention.setSentence(annotations.getText().sentence);
			
			try{
				for(String feature : getFeatures(annotations, relationInstance, location, featureExtractor)){
					refMention.addFeature(feature);
				}
			}catch(Exception e){
				//This is a short hack, just because there are some misformatting in the preprocessed annotations
				//I assume that any exception thrown here is because the annotations are misformatted, which renders this mention unusable
				continue;
			}
			
			relationBuilder.addMention(refMention);
		}
		
		if(relationBuilder.getMentionCount() != 0){
			relationBuilder.build().writeDelimitedTo(out);
		}
	}
	
	
	/**
	 * get the feature vectors corresponding to the given sentence and all its annotation
	 * @param annotations
	 * @return
	 * @throws Exception 
	 */
	private static List<String> getFeatures(AnnotatedSentenceParserAdapter annotations, 
			RelationInstance relInstance, EntityPairLocation location, FeatureExtractor featureExtractor) throws Exception{
		try{
			List<String> features =  featureExtractor.getFeatures(annotations, location.getRange1(), location.getRange2());
			return features;
		}catch(Exception e){
			Logger.writeLine(annotations.toString());
			Logger.writeLine(relInstance.toStringDump());
			Logger.writeLine(location.serialize());
			throw e;
		}
	}
}


