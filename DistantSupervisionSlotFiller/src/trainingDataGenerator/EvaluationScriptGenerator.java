package trainingDataGenerator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import features.extractor.FeatureExtractorVariables;

public class EvaluationScriptGenerator {

	private static final String SCRIPT_PATH = "/Users/mikehotan/Google Drive/AWS/evalScript";

	private static final String RESULT_DIR = "evals";
	private static final String CANDIDATE_TYPE = "candidatesDEPENDENCY";
	private static final int kSections = 5;

	public static void main(String[] args) throws FileNotFoundException{

		System.out.println("Creating Feature Extractors...");

		//a list of extractors that we want to try out.
		List<String> extractors = new ArrayList<String>();
		extractors.add("ecml");
		//		varies the params for mike's feature extractors
		int[] synLevels = new int[]{1,2,3};
		int[] seqLevels = new int[]{1,2,3};
		int[] depLevels = new int[]{1,2,3};

		for (FeatureExtractorVariables.EXTERNAL_NODES_USE_TYPE seqType: FeatureExtractorVariables.SEQUENTIAL_USE_TYPES){
			for (FeatureExtractorVariables.USE_VERB_VARIABLE synType: FeatureExtractorVariables.USE_VERB_VARIABLE_TYPES){
				for(int syn : synLevels){
					for(int seq: seqLevels){
						for(int dep : depLevels){
							extractors.add("mdj_" + syn + "_" + seq + "_" + dep + "_"
						+ seqType.getIndex() + "_" + synType.getIndex());
						}
					}
				}
			}
		}
		System.out.println("Writing...");


		try {
			writeEvaluations(extractors, kSections, .1, .1, .05, SCRIPT_PATH);
		} catch (IOException e) {
			System.err.println("Failed to write evaluations");
			e.printStackTrace();
			return;
		}


		System.out.println("Done!");

	}

	private static void writeEvaluations(List<String> featureExtractors, 
			int k, double startNARatio, double endNARatio, 
			double incNARatio, String scriptPath) throws IOException{

		// Create the file if it does not exist
		// Delete the old one
		File f = new File(scriptPath);
		if (f.exists()){
			f.delete();
		}
		f.createNewFile();
		PrintWriter writer = new PrintWriter(f);

		// Go to Home directory and create eval
		writer.write("#!/bin/bash" + "\n");
		writer.write("cd ~" + "\n");
		writer.write("mkdir " + RESULT_DIR + "\n");

		for(String extractor: featureExtractors){
			String outputDir = "./" + RESULT_DIR + "/" + extractor + ".txt";
			//generate train file from candidates
			writer.write("java -Djava.util.Arrays.useLegacyMergeSort=true -Xmx7000m -jar cse454.jar eval " + k + " " + startNARatio + " " + endNARatio 
					+ " " + incNARatio + " " + extractor + " " + "~/CSE_454/" + CANDIDATE_TYPE + " " + outputDir + "\n");
			writer.write("echo \"Completed eval wrote to: " + outputDir + "\"\n");
			writer.flush();
		}
		writer.close();
	}

}
