package trainingDataGenerator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * An iterator of sentences and their annotations from the corpus. Do not forget
 * to call close() after use.
 * 
 * @author xiaoling
 * --modified by sjonany to fit in my use
 */
public class AnnotatedCorpusReader {
	public AnnotatedCorpusReader(String annotationPath, List<AnnotationConstant> availableAnnotations) throws Exception {
		cacheReadConstant = availableAnnotations.get(0);
		annConstants = availableAnnotations;
		init(annotationPath, availableAnnotations);
	}

	protected Map<AnnotationConstant, BufferedReader> dataReaders;
	protected Map<AnnotationConstant, String> cur;
	
	// caching the line when hasNext is called.
	protected String cache = null;
	//one of the annotation constants is always read first for improving caching performance
	private AnnotationConstant cacheReadConstant;
	//the available annotations available for this corpus reader
	private List<AnnotationConstant> annConstants;

	/**
	 * @param annotationPath the path to the base folder in which all the annotation files are contained
	 * @param availableAnnotations the annotations available in this folder
	 * @throws Exception
	 */
	public void init(String annotationPath, List<AnnotationConstant> availableAnnotations) throws Exception {
		dataReaders = new HashMap<AnnotationConstant, BufferedReader>();
		cur = new HashMap<AnnotationConstant, String>();
		
		for (AnnotationConstant c : availableAnnotations) {
			try {
				String filename = TrainingConfig.getAbsoluteAnnotationPath(annotationPath, c);
				if (new File(filename).exists()) {
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(
									new FileInputStream(filename), "UTF-8"));
					dataReaders.put(c, reader);
					cur.put(c, null);
				} else {
					throw new Exception("The data type [" + c
							+ "] does not exist. filename = "+filename);
				}
			} catch (UnsupportedEncodingException | FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

	public void close() {
		for (BufferedReader reader : dataReaders.values()) {
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * read the next line from dataTypes[0], cache it for later use.
	 */
	public boolean hasNext() {
		if (cache != null) {
			return true;
		} else {
			try {
				cache = dataReaders.get(cacheReadConstant).readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (cache == null) {
				cur =null ;
				return false;
			} else {
				return true;
			}
		}
	}

	public Map<AnnotationConstant, String> next() {
		if (cur==null) {
			return cur;
		}
		if (cache != null) {
			cur.put(cacheReadConstant, cache);
			cache = null;
		} else {
			try {
				cur.put(cacheReadConstant, dataReaders.get(cacheReadConstant).readLine());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		for (AnnotationConstant c : annConstants) {
			if(!c.equals(cacheReadConstant)){
				try {
					cur.put(c, dataReaders.get(c).readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return cur;
 	}
}
