package trainingDataGenerator;

/**
 * All the available annotation constants which correspond to the
 * currently available preprocessed information tucked in text files
 * WARNING: Dont' do AnnotationConstant.values(). Some of the annotations are not
 * available, depending on whether or not you are reading from input (like xiao's)
 * or trainingDataCandidate. Check TrainingConfig.one of the lists to find out what
 * are available.
 * @author sjonany
 * @see TrainingConfig to find out where these files can be found
 */
public enum AnnotationConstant {
	/*
	 * guide to adding a new annotation constant
	 * 1. Add the constant to this class
	 * 2. Add the annotation file path to TrainingConfig
	 * 3. Add the parsing method in AnnotatedSentenceParser
	 */
	TEXT,
	META,
	ARTICLE_IDS,
	TOKENS,
	STANFORDLEMMA,
	TOKEN_SPANS,
	STANFORDPOS,
	CJ,
	//see fig 1 of http://nlp.stanford.edu/software/stanford-dependencies.shtml
	//this corresponds to xiao's most recent cc processed
	//instead of 'prep' we have 'prep_at', 'prep_on'
	DEPS_STANFORD_CC_PROCESSED,
	DEPS_STANFORD_CC_PROCESSED_SEMANTIC_GRAPH,
	STANFORDNER,
	WIKIFICATION
}


