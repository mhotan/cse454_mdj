package trainingDataGenerator;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import knowledgeBase.Relationship;

/**
 * contains the configurations of the training generator
 * including the locations of the input and output directories
 * @author sjonany
 *
 */
public class TrainingConfig {
	//base directory for everything
	public static final String BASE_DIR = System.getProperty("user.home") + "/CSE_454/";
	//where you put Xiao's preprocessed annotations - sentence.tokens, etc.
	public static final String INPUT_ANNOTATIONS_BASE_DIR = BASE_DIR + "input/corpus";
	//where the training data candidates will be outputted
	public static final String OUTPUT_ANNOTATIONS_BASE_DIR = BASE_DIR + "trainingData/";
	//where the final train.pb will be created
	public static final String TRAINING_DATA_FINAL_OUTPUT_PATH = BASE_DIR + "debug/train.pb";

	//annotations that are available from input files (like the ones provided by xiao ling)
	public static final List<AnnotationConstant> INPUT_CONSTANTS = 
			Collections.unmodifiableList(
					Arrays.asList(	
							AnnotationConstant.TEXT,
							AnnotationConstant.META,
							AnnotationConstant.ARTICLE_IDS,
							AnnotationConstant.TOKENS,
							AnnotationConstant.STANFORDLEMMA,
							AnnotationConstant.TOKEN_SPANS,
							AnnotationConstant.STANFORDPOS,
							AnnotationConstant.CJ,
							AnnotationConstant.DEPS_STANFORD_CC_PROCESSED,
							AnnotationConstant.STANFORDNER,
							AnnotationConstant.WIKIFICATION));

	//annotations that are available from training data candidates
	public static final List<AnnotationConstant> TRAINING_DATA_CONSTANTS = 
			Collections.unmodifiableList(
					Arrays.asList(	
							AnnotationConstant.TEXT,
							AnnotationConstant.META,
							AnnotationConstant.ARTICLE_IDS,
							AnnotationConstant.TOKENS,
							AnnotationConstant.STANFORDLEMMA,
							AnnotationConstant.TOKEN_SPANS,
							AnnotationConstant.STANFORDPOS,
							AnnotationConstant.CJ,
							AnnotationConstant.DEPS_STANFORD_CC_PROCESSED,
							AnnotationConstant.STANFORDNER,
							AnnotationConstant.WIKIFICATION));


	/**
	 * get the file name of an annotation file
	 * Note that this is just a relative path
	 * e.g. sentences.tokens
	 * @param c
	 * @return
	 */
	private static final String ANNOTATION_FILE_PREFIX = "sentences.";
	private static String getRelativePathForAnnotation(AnnotationConstant c){
		String fileEnding = null;
		switch(c){
		case TEXT:
			fileEnding = "text";
			break;
		case ARTICLE_IDS:
			fileEnding = "articleIDs";
			break;
		case META:
			fileEnding = "meta";
			break;
		case TOKENS:
			fileEnding = "tokens.fixed";
			break;
		case STANFORDLEMMA:
			fileEnding = "stanfordlemma";
			break;
		case TOKEN_SPANS:
			fileEnding = "tokenSpans";
			break;
		case STANFORDPOS:
			fileEnding = "stanfordpos";
			break;
		case CJ:
			fileEnding = "cj";
			break;
		case DEPS_STANFORD_CC_PROCESSED:
			fileEnding = "depsStanfordCCProcessed.nodup";
			break;
		case STANFORDNER:
			fileEnding = "stanfordner";
			break;
		case WIKIFICATION:
			fileEnding = "wikification";
			break;
		default:
			throw new IllegalArgumentException(c + " is not a supported annotation.");
		}
		return ANNOTATION_FILE_PREFIX + fileEnding;
	}

	/**
	 * @return the absolute path to the annotation file used for training input, using the 
	 * 	default input base dir
	 * 	e.g. /../../../sentences.stanfordner
	 */
	public static String getAnnotationPathForTrainingInput(AnnotationConstant constant){
		if(!INPUT_CONSTANTS.contains(constant)){
			throw new IllegalArgumentException(constant + " is not a provided annotation for input.");
		}

		return INPUT_ANNOTATIONS_BASE_DIR + getRelativePathForAnnotation(constant);
	}

	/**
	 * @return the absolute path to the annotation file
	 * 	e.g. /../../../sentences.stanfordner
	 */
	public static String getAbsoluteAnnotationPath(String annotationFolderDir, AnnotationConstant constant){
		if(!annotationFolderDir.endsWith("/") && !annotationFolderDir.endsWith("\\")){
			annotationFolderDir += "/";
		}
		return annotationFolderDir + getRelativePathForAnnotation(constant);
	}

	/**
	 * the path to the folder containing sentences + annotations for the given 
	 * relationship and training type
	 * @param r
	 * @param trainType
	 * @return path to folder
	 */
	public static String getAnnotationFolderForTrainingCandidateOutput(Relationship r){
		return OUTPUT_ANNOTATIONS_BASE_DIR +  r.toString() + "/";
	}

	/**
	 * the path to the folder containing sentences + annotations for the given 
	 * relationship and training type
	 * @para baseDir the candidate folder which contains folders, one for a single relationship
	 * @return path to folder
	 */
	public static String getAnnotationFolderForTrainingCandidateOutput(Relationship r, String baseDir){	
		if(!baseDir.endsWith("" + File.separatorChar)){
			baseDir = baseDir + File.separatorChar;
		}
		return baseDir +  r.toString() + "/";
	}


	/**
	 * absolute path to the entity pair mentions file for a given relationship
	 * each line contains an entity pair mention, which uniquely identifies a single mention
	 * from the corpus
	 */
	public static String getAbsoluteAnnotationPathForEntityPairMention(Relationship r){
		return getAnnotationFolderForTrainingCandidateOutput(r) + "entityPairMention.txt";
	}

	/**
	 * absolute path to the entity pair mentions file for a given relationship
	 * each line contains an entity pair mention, which uniquely identifies a single mention
	 * from the corpus
	 * @param baseDir the base directory where training candidates are found
	 * 	in this director are folders, one for a single Relationship
	 */
	public static String getAbsoluteAnnotationPathForEntityPairMention(Relationship r,String baseDir){
		if(!baseDir.endsWith("" + File.separatorChar)){
			baseDir = baseDir + File.separatorChar;
		}
		return baseDir + r.toString() + File.separatorChar + "entityPairMention.txt";
	}


	/**
	 * this is used by TrainingDataCandidateDriver for outputting its results
	 * or by TrainingDataDriver for the input
	 * @param relationship
	 * @param isPositive is this a positive training data?
	 * @return the absolute path to the annotation file
	 */
	public static String getAnnotationPathForTrainingCandidateOutput(Relationship r, AnnotationConstant constant){
		if(!TRAINING_DATA_CONSTANTS.contains(constant)){
			throw new IllegalArgumentException(constant + " is not a provided annotation for training data.");
		}

		String annFolderDir = getAnnotationFolderForTrainingCandidateOutput(r);
		return getAbsoluteAnnotationPath(annFolderDir, constant);
	}
}
