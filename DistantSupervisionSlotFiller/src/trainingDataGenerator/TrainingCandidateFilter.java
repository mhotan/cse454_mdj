package trainingDataGenerator;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import features.extractor.DependencyFeatureExtractor;
import knowledgeBase.Date;
import knowledgeBase.DateKnowledgeBase;
import knowledgeBase.Relationship;
import trainingDataGenerator.AnnotatedSentenceParser.EntityPairMention;
import trainingDataGenerator.AnnotatedSentenceParserAdapter.AnnotationParsingException;

/**
 * heuristics which help filter training data candidates with help of
 * manually predetermined set of keywords
 * used by TrainingCandidateDriver when deciding if a sentence should
 * 	be identified as a training sample for a certain relationship
 * @author sjonany
 */
public class TrainingCandidateFilter {
	//value = if the sentence contains any of these strings, immediately treat it as a training sample
	private Map<Relationship, Set<String>> posKeywordsMap;

	//value = if the sentence contains any of these strings, AND does not contain any pos keywords, discard sentence
	private Map<Relationship, Set<String>> negKeywordsMap;
	private DateKnowledgeBase dateKnowledgeBase;
	private FilterLevel filterLevel;

	//WARNING: don't change the order
	//the filters are cumulative from top to bottom. E.g. If choose dependency, then all filters will be used
	public static enum FilterLevel{
		NONE,
		KEYWORD,
		DATE, 
		DEPENDENCY
	}

	public TrainingCandidateFilter(FilterLevel filterLevel) throws FileNotFoundException{
		this.filterLevel = filterLevel;
		dateKnowledgeBase = DateKnowledgeBase.getInstance();
		posKeywordsMap = new HashMap<Relationship, Set<String>>();
		negKeywordsMap = new HashMap<Relationship, Set<String>>();

		for(Relationship r: Relationship.values()){
			posKeywordsMap.put(r, new HashSet<String>());
		}

		for(Relationship r: Relationship.values()){
			negKeywordsMap.put(r, new HashSet<String>());
		}

		//all lemmas generated by stanford nlp. Although I am not sure why 'loved' is considered a lemma :/
		String[] placeOfDeathPosKeywordArr = new String[]{"body", "murder", "accident", "ill", "death", "kill", 
				"disease", "casualty", "drown", "pass", "perish", "life", "accidentally", 
				"explosion", "bomb", "funeral", "die", "assassination", "dead", "tragically",
				"bombing", "find", "tragic", "suicide"};
		String[] placeOfDeathNegKeywordArr = new String[]{"arrive", "home", "birth", "raise", "birthplace", "origin",
				"visit", "childhood", "loved", "like", "move"};
		Set<String> placeOfDeathPosKeywords = posKeywordsMap.get(Relationship.PLACE_OF_DEATH);
		Set<String> placeOfDeathNegKeywords = negKeywordsMap.get(Relationship.PLACE_OF_DEATH);
		for(String pos : placeOfDeathPosKeywordArr){
			placeOfDeathPosKeywords.add(pos);
		}
		for(String neg : placeOfDeathNegKeywordArr){
			placeOfDeathNegKeywords.add(neg);
		}
	}

	/**
	 * @param annotations pertaining to a single sentence in the corpus
	 * @param the entity pair we want to find relationships of
	 * @return a list of relationships that this sentence is likely to express
	 * 		relationships not listed means that there won't be training samples 
	 * 		for these unlisted relationships based on this sentence.
	 * @throws Exception 
	 */
	public Relationship[] getPossibleRelationships(AnnotatedSentenceParserAdapter annotations, EntityPairMention pair) throws AnnotationParsingException{
		if(filterLevel.equals(FilterLevel.NONE)){
			return Relationship.values();
		}

		List<Relationship> relationships = new ArrayList<Relationship>();

		//If the two entities fail the tests based on the dependency features of the current sentence, reject 
		if(filterLevel.equals(FilterLevel.DEPENDENCY) && !passDependencyTest(annotations, pair)){
			return new Relationship[0];
		}

		for(Relationship r : Relationship.values()){
			if(isPossibleRelationship(annotations, r, pair)){
				relationships.add(r);
			}
		}
		return relationships.toArray(new Relationship[0]);
	}


	private static final int DEPENDENCY_MAX_PATH_LENGTH = 4;
	/**
	 * @return false if dependency rules determine that the two entity pairs are not related.
	 * 	Current heuristic : if pairs' dependency path length > 4, or are not connected by dependency
	 * @throws AnnotationParsingException 
	 */
	private boolean passDependencyTest(AnnotatedSentenceParserAdapter annotations, EntityPairMention pair) throws AnnotationParsingException{
		DependencyFeatureExtractor depExtractor = new DependencyFeatureExtractor(annotations.getUnderlyingAnnotations(), pair);
		int depLength = depExtractor.getPathLengthBetweenEntities();
		if(depLength > DEPENDENCY_MAX_PATH_LENGTH || depLength == -1){
			return false;
		}
		return true;
	}

	/**
	 * @param annotations
	 * @param r
	 * @return false iff we are 100% sure that the sentence can't express the relationship (at least that's the goal)
	 * @throws AnnotationParsingException 
	 */
	private boolean isPossibleRelationship(AnnotatedSentenceParserAdapter annotations, Relationship r, EntityPairMention pair) throws AnnotationParsingException{
		//first layer of heuristic : presence of positive/negative keywords - pretty lenient
		//hack :(. To easily achieve the cumulative filter effect. This way, any filter beyond keyword will also run the keyword filter
		if(filterLevel.ordinal() >= FilterLevel.KEYWORD.ordinal()
				&&
				!passKeywordHeuristic(annotations, r)){
			return false; 
		}

		//second layer : more granular rules
		switch(r){
		case CAUSE_OF_DEATH:
			return isRelatedToCauseOfDeath(annotations, pair);
		case PLACE_OF_BIRTH:
			return isRelatedToPlaceOfBirth(annotations);
		case PLACE_OF_DEATH:
			return isRelatedToPlaceOfDeath(annotations, pair);
		case RELIGION:
			return isRelatedToReligion(annotations);
		default: 
			//anything can become NA, so let's just say yes all the time
			return true;
		}
	}

	/**
	 * first layer of heuristic: keywords
	 * discard sentence iff sentence does not contain any positive keywords, and contain a negative keyword
	 * @return true iff sentence should be included as a training sample for the provided relationship
	 * @throws AnnotationParsingException 
	 */
	private boolean passKeywordHeuristic(AnnotatedSentenceParserAdapter annotations, Relationship r) throws AnnotationParsingException{
		Set<String> posKeywords = posKeywordsMap.get(r);
		Set<String> negKeywords = negKeywordsMap.get(r);

		for(String token : annotations.getStanfordlemma().lemma){
			for(String posWord : posKeywords){
				if(posWord.equalsIgnoreCase(token)){
					return true;
				}
			}
		}

		for(String token : annotations.getStanfordlemma().lemma){
			for(String negWord : negKeywords){
				if(negWord.equalsIgnoreCase(token)){
					return false;
				}
			}
		}

		//neutral, give benefit of doubt
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	//granular rules - return false iff sure that the sentence is NOT related to the relationship
	//                 if not sure, return true to give the benefit of doubt

	private boolean isRelatedToCauseOfDeath(AnnotatedSentenceParserAdapter annotations, EntityPairMention pair) throws AnnotationParsingException{
		if(filterLevel.ordinal() >= FilterLevel.DATE.ordinal()){
			Date deathDate = dateKnowledgeBase.getDeathDate(pair.relInstance.getEntity1());
			Date articleDate = Date.deserializeFromMeta(annotations.getMeta());

			//article can't possibly be talking about a person who has not died at that time
			if(articleDate != null && deathDate != null && deathDate.isAfter(articleDate)){
				return false;
			}
		}

		return true;
	}

	private boolean isRelatedToPlaceOfDeath(AnnotatedSentenceParserAdapter annotations, EntityPairMention pair) throws AnnotationParsingException{
		if(filterLevel.ordinal() >= FilterLevel.DATE.ordinal()){
			Date deathDate = dateKnowledgeBase.getDeathDate(pair.relInstance.getEntity1());
			Date articleDate = Date.deserializeFromMeta(annotations.getMeta());

			//article can't possibly be talking about a person who has not died at that time
			if(articleDate != null && deathDate != null && deathDate.isAfter(articleDate)){
				return false;
			}
		}
		return true;
	}

	private boolean isRelatedToPlaceOfBirth(AnnotatedSentenceParserAdapter annotations) throws AnnotationParsingException{
		//This slot has a lot of noise, so we are going to use a very strong check
		List<String> tokens = annotations.getTokens().tokens;
		for(String token : tokens){
			for(String keyWord : PLACE_OF_BIRTH_VITAL_WORDS){
				if(keyWord.equalsIgnoreCase(token)){
					return true;
				}
			}
		}
		return false;
	}

	private boolean isRelatedToReligion(AnnotatedSentenceParserAdapter annotations){
		return true;
	}


	/////////////////////////////////////////////////////////////////////////
	//handcoded list of rules. Preferably kept in a text file, but since we don't have that many anyways
	public static final List<String> PLACE_OF_BIRTH_VITAL_WORDS = 
			Collections.unmodifiableList(
					Arrays.asList(
							"birth",
							"birthplace",
							"born",
							"childhood",
							"child",
							"raised",
							"grew",
							"origin",
							"home",
							"native"
							));
}
