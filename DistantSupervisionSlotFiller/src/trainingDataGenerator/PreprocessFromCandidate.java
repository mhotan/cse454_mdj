package trainingDataGenerator;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import knowledgeBase.RelationInstance;
import knowledgeBase.Relationship;
import trainingDataGenerator.TrainingCandidateGenerator.EntityPairLocation;
import util.Logger;
import cc.factorie.protobuf.DocumentProtos.Relation;
import cc.factorie.protobuf.DocumentProtos.Relation.RelationMentionRef;
import edu.uw.cs.multir.learning.algorithm.Model;
import edu.uw.cs.multir.learning.data.MILDocument;
import edu.uw.cs.multir.preprocess.Mappings;
import edu.uw.cs.multir.util.SparseBinaryVector;
import features.extractor.OverallFeatureExtractor;
import features.oldstuff.FeatureExtractor;

/**
 * This step is an alternative to TrainingDataDriver + multir's preprocess
 * The pipeline is then TrainingCandidateDriver -> PreprocessFromCandidate -> multir's train
 * input : sample candidates - e.g. candidateDEPENDENCY
 * output: the train file needed by multi'rs Train class, no need to call Preprocess anymore
 * @author sjonany
 *
 */
public class PreprocessFromCandidate {	
	//|NA| / |non-NA| 
	//The higher the ratio, the more likely classifier will say NA -> more false negatives
	//public static final double NA_TO_NON_NA_RATIO = 1.0;
	
	//limit to maximum number of samples for a single relation
	public static void main(String[] args) throws Exception{
		//TODO: add command args for path of training candidate
		//TODO: add command args for preprocess
		if(args.length != 4){
			System.out.println("Usage: NA_TO_NON_NA_RATIO(double) FeatureExtractorArgs candidateDir outDir");
			return;
		}
	
		double NA_TO_NON_NA_RATIO = Double.parseDouble(args[0]);
		FeatureExtractor featureExtractor = OverallFeatureExtractor.getFeatureExtractor(args[1]);
		String candidateDir = args[2];
		String outDir = args[3];
		
		PreprocessWriter preprocessWriter = new PreprocessWriter(outDir);

		//Consume all training data for all relationships other than NA. For NA, we sample a max of |non-NA|*RATIO
		
		long totalNonNASamples = 0;
		//non-NA training data generation
		for(Relationship r : Relationship.values()){
			if(r.equals(Relationship.NA)){
				continue;
			}
			
			Logger.writeLine("Loading all annotations for " + r);
			Map<Integer, Map<AnnotationConstant, String>> annotationCache = initAnnotationCache(r, candidateDir);
			Logger.writeLine("Finished Loading all annotations for " + r);
			
			//for each mention, there is a feature vector
			//thus, it is more logical to undersample based on num mention, not num entity pair
			long mentionCount = 0;
			Logger.writeLine("Generating training data for relationship type : " + r);
			//read the entity pair mentions					
			BufferedReader relationInstanceReader = new BufferedReader(
					new InputStreamReader(new FileInputStream(TrainingConfig.getAbsoluteAnnotationPathForEntityPairMention(r, candidateDir)), "UTF-8"));
			String relInstanceLine  = relationInstanceReader.readLine();
			
			//for each relation instance, we look at all the mentions in the corpus,
			//and output relationship
			while(relInstanceLine != null){
				if(mentionCount %10000 == 0){
					Logger.writeLine("" + mentionCount);
				}
				//parse relInstanceLine
				String[] tokens = relInstanceLine.split("\\|");
				RelationInstance relationInstance = RelationInstance.fromStringDump(tokens[0]);
				List<EntityPairLocation> mentions = new ArrayList<EntityPairLocation>();
				//first token is relation instance, the rest are entity pair locations
				for(int i=1; i<tokens.length; i++){
					mentions.add(EntityPairLocation.deserialize(tokens[i]));
					totalNonNASamples++;
					mentionCount++;
				}
				
				writeRelationToDisk(relationInstance, mentions, annotationCache, featureExtractor, preprocessWriter);
				relInstanceLine = relationInstanceReader.readLine();
			}
			
			relationInstanceReader.close();
		}
		
		//NA training data generation
		long numNAToRead = (long)(totalNonNASamples * NA_TO_NON_NA_RATIO);
		//now for the NA samples, where |NA| =  ratio * (|non-NA|)
		long mentionCount = 0;
		
		Logger.writeLine("Loading all annotations for " + Relationship.NA);
		Map<Integer, Map<AnnotationConstant, String>> annotationCache = initAnnotationCache(Relationship.NA, candidateDir, numNAToRead);
		Logger.writeLine("Finished Loading all annotations for " + Relationship.NA);

		Logger.writeLine("Generating training data for relationship type : " + Relationship.NA);
		//read the entity pair mentions					
		BufferedReader relationInstanceReader = new BufferedReader(
				new InputStreamReader(new FileInputStream(TrainingConfig.getAbsoluteAnnotationPathForEntityPairMention(Relationship.NA, candidateDir)), "UTF-8"));
		String relInstanceLine  = relationInstanceReader.readLine();
		
		//for each relation instance, we look at all the mentions in the corpus,
		//and output relationship
		while(relInstanceLine != null){
			if(mentionCount %10000 == 0){
				Logger.writeLine("" + mentionCount);
			}
			
			//undersample NA
			if(mentionCount> numNAToRead){
				break;
			}
			
			//parse relInstanceLine
			String[] tokens = relInstanceLine.split("\\|");
			RelationInstance relationInstance = RelationInstance.fromStringDump(tokens[0]);
			List<EntityPairLocation> mentions = new ArrayList<EntityPairLocation>();
			//first token is relation instance, the rest are entity pair locations
			for(int i=1; i<tokens.length; i++){
				mentions.add(EntityPairLocation.deserialize(tokens[i]));
				mentionCount++;
				if(mentionCount > numNAToRead){
					break;
				}
			}
			
			writeRelationToDisk(relationInstance, mentions, annotationCache, featureExtractor, preprocessWriter);
			relInstanceLine = relationInstanceReader.readLine();
		}
		
		relationInstanceReader.close();
		preprocessWriter.close();
	}
	
	/**
	 * load all the relevant annotations needed to train r but only barely enough to produce the required number of mentions
	 * useful for generating training data for NA, because there are too many of them, and we are undersampling
	 * @param numMention the number of mentions you need. With this, we don't need to load the entire annotation cache, but 
	 * just enough to satisfy the needs writeRelationToDisk for the selected mentions.
	 * @return annotationCache - key = sentence id, value = annotation related to sentence
	 * @throws Exception 
	 */
	public static Map<Integer, Map<AnnotationConstant, String>> initAnnotationCache(Relationship r, String baseDir, long numMention) throws Exception{
		BufferedReader relationInstanceReader = new BufferedReader(
				new InputStreamReader(new FileInputStream(TrainingConfig.getAbsoluteAnnotationPathForEntityPairMention(r, baseDir)), "UTF-8"));
		String relInstanceLine  = relationInstanceReader.readLine();
		
		//collect the set of sentence Ids that we need to satisfy the inputs for numMention feature vectors 
		TreeSet<Integer> sentenceIds = new TreeSet<Integer>();
		
		long mentionCount = 0;
		while(relInstanceLine != null){
			if(mentionCount > numMention){
				break;
			}
			//parse relInstanceLine
			String[] tokens = relInstanceLine.split("\\|");
			//first token is relation instance, the rest are entity pair locations
			for(int i=1; i<tokens.length; i++){
				Integer id = Integer.parseInt(EntityPairLocation.deserialize(tokens[i]).getGlobalSentenceId());
				sentenceIds.add(id);
				mentionCount++;
			}
			relInstanceLine = relationInstanceReader.readLine();
		}
		relationInstanceReader.close();
		
		Map<Integer, Map<AnnotationConstant, String>> annotCache = new HashMap<Integer, Map<AnnotationConstant, String>>();
		AnnotatedCorpusReader candidates = new AnnotatedCorpusReader(
				TrainingConfig.getAnnotationFolderForTrainingCandidateOutput(r, baseDir)
				,TrainingConfig.TRAINING_DATA_CONSTANTS);

		//go through the candidates, and pick out annotations
		while(candidates.hasNext()){
			Map<AnnotationConstant, String> sentenceAnnot = candidates.next();
			AnnotatedSentenceParserAdapter annotations = new AnnotatedSentenceParserAdapter(sentenceAnnot);
			int sentenceId = Integer.parseInt(annotations.getText().globalSentenceId);
			
			if(sentenceIds.contains(sentenceId)){
				//WARNING: If I don't make this copy, values in hashtable will keep changing value!!
				Map<AnnotationConstant, String> shallowCopy = new HashMap<AnnotationConstant, String>();
				shallowCopy.putAll(sentenceAnnot);
				annotCache.put(sentenceId, shallowCopy);
			}

		}
		
		candidates.close();
		
		return annotCache;
	}
	
	/**
	 * load all the relevant annotations needed to train r to memory
	 * @return annotationCache - key = sentence id, value = annotation related to sentence
	 * @throws Exception 
	 */
	public static Map<Integer, Map<AnnotationConstant, String>> initAnnotationCache(Relationship r, String baseDir) throws Exception{
		Map<Integer, Map<AnnotationConstant, String>> annotCache = new HashMap<Integer, Map<AnnotationConstant, String>>();
		AnnotatedCorpusReader candidates = new AnnotatedCorpusReader(
				TrainingConfig.getAnnotationFolderForTrainingCandidateOutput(r, baseDir)
				,TrainingConfig.TRAINING_DATA_CONSTANTS);

		//go through the candidates, and pick out annotations
		while(candidates.hasNext()){
			Map<AnnotationConstant, String> sentenceAnnot = candidates.next();
			AnnotatedSentenceParserAdapter annotations = new AnnotatedSentenceParserAdapter(sentenceAnnot);
			int sentenceId = Integer.parseInt(annotations.getText().globalSentenceId);
			
			//WARNING: If I don't make this copy, values in hashtable will keep changing value!!
			Map<AnnotationConstant, String> shallowCopy = new HashMap<AnnotationConstant, String>();

			shallowCopy.putAll(sentenceAnnot);
			annotCache.put(sentenceId, shallowCopy);
		}
		
		candidates.close();
		
		return annotCache;
	}
	
	/**
	 * write a single training block to disk
	 * a training block is a relation instance, with a list of <entity pair mention,features>
	 * @param relationInstance
	 * @param mentions
	 * @param out
	 * @param annotationCache - key = sentence id, value = annotation related to sentence
	 * @throws Exception 
	 */
	public static void writeRelationToDisk(RelationInstance relationInstance, List<EntityPairLocation> mentions, 
			Map<Integer, Map<AnnotationConstant,String>> annotationCache, FeatureExtractor featureExtractor, 
			PreprocessWriter prepWriter) throws Exception{
		Relation.Builder relationBuilder = Relation.newBuilder();
		
		relationBuilder.setSourceGuid(relationInstance.getEntity1().getMid());
		relationBuilder.setDestGuid(relationInstance.getEntity2().getMid());
		relationBuilder.setRelType(relationInstance.getRelationship().toString());
		
		
		for(EntityPairLocation location : mentions){
			int sentenceId = Integer.parseInt(location.getGlobalSentenceId());
			if(!annotationCache.containsKey(sentenceId)){
				throw new IllegalArgumentException("Missing annotation for sentenceID = " + sentenceId + ", relation = " + relationInstance);
			}
			
			AnnotatedSentenceParserAdapter annotations = new AnnotatedSentenceParserAdapter(annotationCache.get(sentenceId));
			
			RelationMentionRef.Builder refMention = RelationMentionRef.newBuilder();
			refMention.setFilename(annotations.getMeta().originalFileName);
			//Congle said these id's do not matter
			refMention.setDestId(-123214);
			refMention.setSourceId(-12234314);
			refMention.setSentence(annotations.getText().sentence);
			
			try{
				for(String feature : getFeatures(annotations, relationInstance, location, featureExtractor)){
					refMention.addFeature(feature);
				}
			}catch(Exception e){
				//This is a short hack, just because there are some misformatting in the preprocessed annotations
				//I assume that any exception thrown here is because the annotations are misformatted, which renders this mention unusable
				continue;
			}
			
			relationBuilder.addMention(refMention);
		}
		
		if(relationBuilder.getMentionCount() != 0){
			prepWriter.writeRelation(relationBuilder.build());
		}
	}
	
	
	/**
	 * get the feature vectors corresponding to the given sentence and all its annotation
	 * @param annotations
	 * @return
	 * @throws Exception 
	 */
	private static List<String> getFeatures(AnnotatedSentenceParserAdapter annotations, 
			RelationInstance relInstance, EntityPairLocation location, FeatureExtractor featureExtractor) throws Exception{
		try{
			List<String> features =  featureExtractor.getFeatures(annotations, location.getRange1(), location.getRange2());
			return features;
		}catch(Exception e){
			Logger.writeLine(annotations.toString());
			Logger.writeLine(relInstance.toStringDump());
			Logger.writeLine(location.serialize());
			throw e;
		}
	}
	
	public static class PreprocessWriter{
		private String mappingFile;
		private String modelFile;
		private String outputFile;
		private boolean writeFeatureMapping;
		private boolean writeRelationMapping;
		private DataOutputStream os;
		private MILDocument doc;
		private int count;
		private Mappings m;
		
		public PreprocessWriter(String outDir) throws IOException{
			//from Preprocess
			this.mappingFile = outDir + File.separatorChar + "mapping";
			this.modelFile = outDir + File.separatorChar + "model";
			this.outputFile = outDir + File.separatorChar + "train";
			
			//from ConvertProtobufToMILDocument
			this.writeFeatureMapping = true;
			this.writeRelationMapping = true;
			this.m = new Mappings();
			
			if (!writeFeatureMapping || !writeRelationMapping)
				m.read(mappingFile);
			else
				// ensure that relation NA gets ID 0
				m.getRelationID("NA", true);
			
			this.os = new DataOutputStream
				(new BufferedOutputStream(new FileOutputStream(outputFile)));
		
		    this.doc = new MILDocument();    
		    this.count = 0;	
		}
		
		public void writeRelation(Relation r) throws IOException{
	    	if (++count % 10000 == 0) System.out.println(count);

	    	doc.clear();
	    	
	    	doc.arg1 = r.getSourceGuid();
	    	doc.arg2 = r.getDestGuid();
	    	
	    	// set relations
	    	{
		    	String[] rels = r.getRelType().split(",");
		    	int[] irels = new int[rels.length];
		    	for (int i=0; i < rels.length; i++)
		    		irels[i] = m.getRelationID(rels[i], writeRelationMapping);
		    	Arrays.sort(irels);
		    	// ignore NA and non-mapped relations
		    	int countUnique = 0;
		    	for (int i=0; i < irels.length; i++)
		    		if (irels[i] > 0 && (i == 0 || irels[i-1] != irels[i]))
		    			countUnique++;
		    	doc.Y = new int[countUnique];
		    	int pos = 0;
		    	for (int i=0; i < irels.length; i++)
		    		if (irels[i] > 0 && (i == 0 || irels[i-1] != irels[i]))
		    			doc.Y[pos++] = irels[i];
	    	}
	    	
	    	// set mentions
	    	doc.setCapacity(r.getMentionCount());
	    	doc.numMentions = r.getMentionCount();
	    	
	    	for (int j=0; j < r.getMentionCount(); j++) {
	    		RelationMentionRef rmf = r.getMention(j);
		    	doc.Z[j] = -1;
	    		doc.mentionIDs[j] = j;
	    		SparseBinaryVector sv = doc.features[j] = new SparseBinaryVector();
	    		
	    		int[] fts = new int[rmf.getFeatureCount()];
	    		for (int i=0; i < rmf.getFeatureCount(); i++)
	    			fts[i] = m.getFeatureID(rmf.getFeature(i), writeFeatureMapping);
	    		Arrays.sort(fts);
		    	int countUnique = 0;
		    	for (int i=0; i < fts.length; i++)
		    		if (fts[i] != -1 && (i == 0 || fts[i-1] != fts[i]))
		    			countUnique++;
		    	sv.num = countUnique;
		    	sv.ids = new int[countUnique];
		    	int pos = 0;
		    	for (int i=0; i < fts.length; i++)
		    		if (fts[i] != -1 && (i == 0 || fts[i-1] != fts[i]))
		    			sv.ids[pos++] = fts[i];
	    	}
	    	doc.write(os);
		}
		
		//call this once all the preprocessing is done
		public void close() throws IOException{
			os.close();
			
			Model model = new Model();
			model.numRelations = m.numRelations();
			model.numFeaturesPerRelation = new int[model.numRelations];
			for (int i=0; i < model.numRelations; i++)
				model.numFeaturesPerRelation[i] = m.numFeatures();
			model.write(modelFile);
			
			if (writeFeatureMapping || writeRelationMapping)
				m.write(mappingFile);
			
		}
	}
}
