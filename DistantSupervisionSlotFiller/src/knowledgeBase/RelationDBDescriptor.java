package knowledgeBase;

import java.util.List;
import java.util.Map;

/**
 * This is a kind of "key-value" pair class that 
 * wraps the HashTable of entity relation and informations
 * regarding it
 * 
 * @author antoniusharijanto
 *
 */

public class RelationDBDescriptor {
	// What relationship is described
	private Relationship relationship;
	// eg. /people/person/place_of_birth
	private String mRelationship_str;
	// The freebase string that describes the relation
	private String mFreebase_keyword;
	// The path to the textual file
	private String mFilePath;
	// Indicates whether the relation is two-way or not
	// (eg. a sibling relationship is two-way)
	private boolean mIsTwoWayRelation;	
	
	// The database
	private Map<String, List<String>> mMap_DB;

	/**
	 * @param rel Relationship to be described
	 * @param rel_str String description of the relationship
	 * @param db Map containing the entities satisfying the relationship 
	 */
	public RelationDBDescriptor(Relationship rel, String rel_str,
			Map<String, List<String>> db, String file_path, String freebase_keyword, boolean isTwoWay) {
		mMap_DB = db;
		relationship = rel;
		mRelationship_str = rel_str;
		mFilePath = file_path;
		mFreebase_keyword = freebase_keyword;
		mIsTwoWayRelation = isTwoWay;
	}
	
	public RelationDBDescriptor(Relationship rel, String rel_str,
			Map<String, List<String>> db, String file_path, String freebase_keyword) {
		this(rel, rel_str, db, file_path, freebase_keyword, false);
	}
	
	/**
	 * @return Relationship wrapped within this class
	 */
	public Relationship getRelationship() {
		return relationship;
	}

	/**
	 * @param relationship Relationship wrapped within this class
	 */
	public void setRelationship(Relationship relationship) {
		this.relationship = relationship;
	}

	/**
	 * @return String describing the relationship wrapped within this class
	 */
	public String getRelationship_str() {
		return mRelationship_str;
	}

	/**
	 * @param relationship_str String describing the relationship wrapped within this class
	 */
	public void setRelationship_str(String relationship_str) {
		this.mRelationship_str = relationship_str;
	}

	/**
	 * @return HashTable that describes the relation
	 */
	public Map<String, List<String>> getMap_DB() {
		return mMap_DB;
	}

	/**
	 * @param map_DB HashTable that describes the relation
	 */
	public void setMap_DB(Map<String, List<String>> map_DB) {
		this.mMap_DB = map_DB;
	}
	
	/**
	 * @return The path to the input data for the specified relation
	 */
	public String getFilePath() {
		return mFilePath;
	}

	/**
	 * @param mFilePath The path to the input data for the specified relation
	 */
	public void setFilePath(String mFilePath) {
		this.mFilePath = mFilePath;
	}

	/**
	 * @return return the freebase keyword of the relation (eg. /people/person/place_of_birth)
	 */
	public String getFreebase_keyword() {
		return mFreebase_keyword;
	}

	/**
	 * @param freebase_keyword Freebase relation keyword that connects the 2 entities
	 */
	public void setFreebase_keyword(String freebase_keyword) {
		this.mFreebase_keyword = freebase_keyword;
	}
	

	/**
	 * @return return true if the relation is a two-way relation (eg. sibling)
	 */
	public boolean getIsTwoWayRelation() {
		return mIsTwoWayRelation;
	}

	/**
	 * @param newValue set two-way relation of the relation to be newValue
	 */
	public void setIsTwoWayRelation(boolean newValue) {
		this.mIsTwoWayRelation = newValue;
	}


}