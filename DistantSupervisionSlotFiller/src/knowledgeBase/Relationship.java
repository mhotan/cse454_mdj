package knowledgeBase;

//relationship between two entities
public enum Relationship {
	PLACE_OF_BIRTH,
	//DATE_OF_BIRTH,
	PLACE_OF_DEATH,
	CAUSE_OF_DEATH,
	RELIGION,
	//negative training data
	NA;
}
