package knowledgeBase;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import util.Logger;

/**
 * like the knowledge base, except that instead of storing mid's,
 * this one stores dates
 * 
 * note: because we are pressed for time, and I only need this for the heuristics, 
 * this class is specific to date of death only
 * @author sjonany
 */
public class DateKnowledgeBase {
	private static DateKnowledgeBase instance;
	
	public static DateKnowledgeBase getInstance() throws FileNotFoundException {
		if (instance == null) {
			instance = new DateKnowledgeBase();
		}
		return instance;
	}
	
	//key = mid, val = date of birth of that entity
	private Map<String, Date> deathDateMap;
	
	/**
	 * Private constructor for the singleton
	 * 
	 * @throws FileNotFoundException
	 */
	private DateKnowledgeBase() throws FileNotFoundException {
		Logger.writeLine("Setting up date knowledge base...");
		deathDateMap = new HashMap<String, Date>();
		
		File input_file = new File(KnowledgeBaseConstant.mPath_DateOfDeath);
		if (!input_file.exists()) {
			throw new FileNotFoundException("Relation file not found: "
					+ KnowledgeBaseConstant.mPath_DateOfDeath);
		}

		Scanner scan = new Scanner(input_file);

		int lineCount = 0;
		try {
			while (scan.hasNextLine()) {
				String line = scan.nextLine();
				lineCount++;

				//freebase tokens for date of death and birth is of the form "e1 \t rel \t \t e2"
				//the double \t between rel and e2 is an oddity for these date dumps.
				String[] unparsedTokens = line.trim().split("\t");
				String[] tokens = new String[3];
				tokens[0] = unparsedTokens[0];
				tokens[1] = unparsedTokens[1];
				tokens[2] = unparsedTokens[3];
				if (tokens.length != 3) {
					throw new IllegalArgumentException(
							"Unexpected tokens = " + line
									+ " while init date of death"
									+ " at line = " + lineCount);
				}

				String e1 = tokens[0];
				String freebase_relation_keyword = tokens[1];
				String dateStr = tokens[2];
				
				
				if(dateStr.startsWith("T")){
					//m/05_hst	/people/deceased_person/date_of_death		T16
					//some oddities - start with T
					continue;
				}
				
				if(dateStr.contains("T")){
					///m/082db	/people/deceased_person/date_of_death		1791-12-05T01:00
					//ignore stuff after T
					dateStr = dateStr.substring(0, dateStr.indexOf('T'));
				}

				// Not a valid relation
				if (!freebase_relation_keyword.equals("/people/deceased_person/date_of_death")) {
					throw new IllegalArgumentException(
							"Read a relation of type = "
									+ freebase_relation_keyword
									+ " when init date of death line = "
									+ lineCount);
				}
				
				Date date = null;
				try{
					date = Date.deserializeFromFreebase(dateStr);
				}catch(Exception e){
					Logger.writeLine("Can't parse date : " + dateStr);
					throw e;
				}
				
				deathDateMap.put(e1, date);
			}
		} finally {
			scan.close();
		}
		
		Logger.writeLine("Tuples obtained : " + lineCount);
		Logger.writeLine("Done setting up date knowledge base.");
	}
	
	/**
	 * return the death date of the entity
	 * null if we don't know the death date of this entity
	 */
	public Date getDeathDate(Entity e){
		return deathDateMap.get(e.getMid());
	}
}
