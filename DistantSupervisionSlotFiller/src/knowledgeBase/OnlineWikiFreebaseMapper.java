package knowledgeBase;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import util.Logger;

/**
 * maps wiki articles to freebase mid's by querying freebase website
 * needs internet connection to work
 * @author sjonany
 */
public class OnlineWikiFreebaseMapper implements WikiFreebaseMapper{
	//how long to wait if wiki-freebase query fails before trying again
	private static int RETRY_DELAY_SECONDS = 5;
	
	//<K,V> = <wiki article name, freebase entity>
	private HashMap<String, Entity> mapping;
	
	public OnlineWikiFreebaseMapper(){
		mapping = new HashMap<String, Entity>();
	}
	
	/**
	 * convert wikipedia article name to a freebase entity by looking up online
	 * use this if there is no WEX dump available, or if you want up to date results
	 * This method will keep re-querying if network is down. 
	 * Else, you use the offline implementation of WikiFreebaseMapper
	 * @param wikiArticleName
	 * @return null if no such entry on freebase exists
	 */
	@Override
	public Entity convertToEntity(String wikiArticleName) throws Exception{
		Entity e = mapping.get(wikiArticleName);
		
		if(e == null){
			String guid = null;
			try{
				JSONObject jsonDump = readJsonFromUrl(getUrlQuery(wikiArticleName));
				guid = (String)jsonDump.get("guid");
			}catch(ErrorResponseCodeException ex){
				switch(ex.getResponseCode()){
				case 500:
					//no freebase entity that maps to the chosen wikipedia article
					return null;
				}
				Logger.writeLine("When finding freebase mapping of " + wikiArticleName + " - " + ex);
				return null;
			}catch(IOException ex){
				//most likely internet connection is down. Wait for a while and try again
				Logger.writeLine("Unable to connect to host " + " - " + ex);
				Logger.writeLine("Retrying in " + RETRY_DELAY_SECONDS + " seconds..");
				try {
					Thread.sleep(RETRY_DELAY_SECONDS * 1000);
				} catch (InterruptedException e1) {
					Logger.writeLine("Error when delaying before retry - " + e1);
					return null;
				}
				return convertToEntity(wikiArticleName);
			}catch(Exception ex){
				Logger.writeLine("When finding freebase mapping of " + wikiArticleName + " - " + ex);
				return null;
			}
			
			e = Entity.getEntityFromMid(Entity.convertGuidToMid(guid));
			mapping.put(wikiArticleName, e);
		}
		
		return e;
	}
	
	/**
	 * @param wikiArticleName
	 * @return a url which represents a query to freebase asking for the freebase id which maps to wikiArticleName
	 */
	private static String getUrlQuery(String wikiArticleName){
		//Service call online 
		final String FREEBASE_WIKI_MAPPING_QUERY_PREFIX = "http://ids.freebaseapps.com/get_ids?id=/wikipedia/en/";
		
		return FREEBASE_WIKI_MAPPING_QUERY_PREFIX + wikiArticleName;
	}
	
	/**
	 * @param url
	 * @throws ErrorResponseCodeException if connection succeeded, but there is an error code
	 * @return the json object created by parsing the text dump returned by the url
	 */
	private static JSONObject readJsonFromUrl(String urlStr) throws IOException, JSONException {
		URL url = new URL(urlStr);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		
		if(!String.valueOf(conn.getResponseCode()).startsWith("2")){
		    throw new ErrorResponseCodeException(conn.getResponseCode(), conn.getResponseMessage());
		}
						
		InputStream is = conn.getInputStream();
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));			
			StringBuilder sb = new StringBuilder();
			int cp;
			while ((cp = rd.read()) != -1) {
				sb.append((char) cp);
			}
			
			JSONObject json = new JSONObject(sb.toString());
			return json;
		} finally {
			is.close();
		}
	}
	
	private static class ErrorResponseCodeException extends IOException{
		private static final long serialVersionUID = 1L;
		
		private int responseCode;
		private String responseMessage;
		
		public ErrorResponseCodeException(int respCode, String respMessage){
			this.responseCode = respCode;
			this.responseMessage = respMessage;
		}
		
		public int getResponseCode(){
			return responseCode;
		}
		
		public String getResponseMessage(){
			return responseMessage;
		}
		
		@Override
		public String toString(){
			return "Error code " + getResponseCode() + " : " + getResponseMessage();
		}
	}
}
