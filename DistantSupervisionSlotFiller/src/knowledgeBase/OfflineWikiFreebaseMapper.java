package knowledgeBase;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import trainingDataGenerator.TrainingConfig;
import util.Logger;

/**
 * maps wiki articles to freebase mid's by querying freebase website
 * unlike the online version, this does not internet connection to work,
 * but requires the mapping file to be available locally
 * the mapping file is just a tsv file, with first col = guid, second col = wiki name
 * @see TrainingConfig 
 * @author sjonany
 */
public class OfflineWikiFreebaseMapper implements WikiFreebaseMapper{
	//tsv file from wex original called freebase_names
	//http://wiki.freebase.com/wiki/WEX/Documentation
	//this version is already pruned to only contain those needed by corpus
	private static final String FREEBASE_WIKI_MAPPING_PATH = TrainingConfig.BASE_DIR + "input/freebase_mapping_pruned.tsv";
	
	//<K,V> = <wiki article name, freebase entity>
	private HashMap<String, Entity> mapping;
	
	public OfflineWikiFreebaseMapper() throws Exception{
		mapping = new HashMap<String, Entity>();
		Logger.writeLine("Setting up offline wiki freebase mapper...");
		BufferedReader reader = new BufferedReader(
				new InputStreamReader(
						new FileInputStream(FREEBASE_WIKI_MAPPING_PATH), "UTF-8"));

		String line = reader.readLine();
		while(line != null){
			line = line.trim();
			if(line.length() == 0){
				line = reader.readLine();
				continue;
			}
			String[] tokens = line.split("\t");
			//0 = guid, 1 = wiki article name
			String guid = tokens[0].trim();
			Entity e = Entity.getEntityFromMid(Entity.convertGuidToMid(guid));
			mapping.put(tokens[1].trim(), e);
			line = reader.readLine();
		}
		Logger.writeLine("Done setting up wiki freebase mapper.");
		
		reader.close();
	}
	
	/**
	 * convert wikipedia article name to a freebase entity by referring to the local dump
	 * @param wikiArticleName
	 * @return null if no such entry on freebase exists
	 * @throws FileNotFoundException 
	 * @throws UnsupportedEncodingException 
	 */
	@Override
	public Entity convertToEntity(String wikiArticleName) throws Exception {
		//Xiao's wiki entries replaced all the whitespaces with underscores
		wikiArticleName.replace(' ', '_');
		return mapping.get(wikiArticleName);
		
		//linear search, but decided to load everything up at once 
		/*
		BufferedReader reader = new BufferedReader(
				new InputStreamReader(
						new FileInputStream(TrainingConfig.FREEBASE_WIKI_MAPPING_PATH), "UTF-8"));

		String line = reader.readLine();
		while(line != null){
			String[] tokens = line.split("\t");
			//0 = guid, 1 = wiki article name
			if(tokens[1].trim().equals(wikiArticleName)){
				Entity e = Entity.getEntityFromGuid(tokens[0].trim());
				mapping.put(wikiArticleName, e);
				reader.close();
				return e;
			}
			line = reader.readLine();
		}
		
		//no matching entity found
		mapping.put(wikiArticleName, null);
		reader.close();
		return null;*/
	}
}
