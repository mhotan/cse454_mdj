package knowledgeBase;

import java.util.HashMap;

import util.Logger;

//a single entity uniquely identifiable in freebase
public class Entity {
	//key = mid, value = Entity,
	//An entity of a certain mid is instantiated exactly once
	private static HashMap<String, Entity> midEntityCache = 
			new HashMap<String, Entity>();
	
	private String mid;
	
	/**
	 * @see getEntityFromGuid
	 */
	private Entity(){};
	
	/**
	 * use this in replacement of the constructor
	 * @return the entity corresponding to the given mid
	 */
	public static Entity getEntityFromMid(String mid){
		Entity e = midEntityCache.get(mid);
		if(e == null){
			e = new Entity();
			e.mid = mid;
			midEntityCache.put(mid, e);
		}
		
		return e;
	}
	
	public String getMid(){
		return mid;
	}
	
	@Override
	public String toString(){
		return mid;
	}
	
	@Override
	public int hashCode(){
		return mid.hashCode();
	}
	
	@Override
	public boolean equals(Object o){
		if(!(o instanceof Entity)){
			return false;
		}
		
		Entity other = (Entity) o;
		return this.mid.equals(other.mid);
	}
	
	//code given by xiao to convert guid -> mid
    private static final String MAP = "0123456789bcdfghjklmnpqrstvwxyz_";

    public static String convertGuidToMid(String guid) {
		guid = guid.replace("/guid/", "").replace("#", "");
		if (guid.length() == 32) {
			String str = fromDecimalToOtherBase(32,
					Long.parseLong(guid.replace("9202a8c04000641f8", ""), 16));
			return "/m/0" + str;
		} else {
			Logger.writeLine("length error" + guid.length());
		}
		return null;
    }
    
	private static String fromDecimalToOtherBase(int base, long decimalNumber) {
		String tempVal = decimalNumber == 0 ? "0" : "";
		int mod = 0;
		while (decimalNumber != 0) {
			mod = (int) (decimalNumber % base);
			tempVal = MAP.charAt(mod) + tempVal;
			decimalNumber = decimalNumber / base;
		}
		return tempVal;
	}
	
}
