package knowledgeBase;

import trainingDataGenerator.TrainingConfig;

public class KnowledgeBaseConstant {
	// There are N number of lines to be skipped
	// on top of the relation files
	protected final static int mTopLinesToSkip = 0;
	// Path where the actual database file is located
	// private final static String mPath_PlaceOfBirth =
	// "~/CSE454/Data/people-person-place_of_birth.txt";
	
	protected final static String mPath_PlaceOfBirth = TrainingConfig.BASE_DIR
			+ "input/people-person-place_of_birth.txt";
	protected final static String mPath_PlaceOfDeath = TrainingConfig.BASE_DIR
			+ "input/people-person-place_of_death.txt";
	protected final static String mPath_CauseOfDeath = TrainingConfig.BASE_DIR
			+ "input/people-person-cause_of_death.txt";
	protected final static String mPath_DateOfDeath = TrainingConfig.BASE_DIR
			+ "input/date_of_death.txt";
	protected final static String mPath_Religion = TrainingConfig.BASE_DIR
			+ "input/people-person-religion.txt";
	
	// List all MIDs that are of the entities
	public final static String PATH_CITY_TOWN_MIDS = TrainingConfig.BASE_DIR +
			"input/list_city_town_mid.txt";
	public final static String PATH_STATE_PROVINCE_MIDS = TrainingConfig.BASE_DIR +
			"input/state_province_mid.txt";
	public final static String PATH_COUNTRY_MIDS = TrainingConfig.BASE_DIR +
			"input/country_mid.txt";
	public final static String PATH_RELIGION_MIDS = TrainingConfig.BASE_DIR +
			"input/religion_mid.txt";
	

	//List all MIDs that are causes of death
	public final static String CAUSE_OF_DEATH_MIDS = TrainingConfig.BASE_DIR +
			"input/cause_of_death_mid.txt";
	
	//List all MIDs of people of the relationships we care about
	public final static String PEOPLE_MIDS = TrainingConfig.BASE_DIR +
			"input/people_mid.txt";
	
	protected final static String FREEBASE_KEYWORD_PLACE_OF_BIRTH = "/people/person/place_of_birth";
	protected final static String FREEBASE_KEYWORD_PLACE_OF_DEATH = "/people/deceased_person/place_of_death";
	protected final static String FREEBASE_KEYWORD_CAUSE_OF_DEATH = "/people/deceased_person/cause_of_death";
	protected final static String FREEBASE_KEYWORD_DATE_OF_BIRTH = "/people/person/date_of_birth";
	protected final static String FREEBASE_KEYWORD_RELIGION = "/people/person/religion";
}
