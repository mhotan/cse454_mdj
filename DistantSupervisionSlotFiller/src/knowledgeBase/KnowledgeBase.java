package knowledgeBase;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import util.Logger;

/**
 * the knowledge base of the distant supervision approach contains assertions,
 * current source is freebase
 * 
 * @author denny
 **/
public class KnowledgeBase {
	// Singleton
	private static KnowledgeBase mInstance = null;

	// Mapping for each of the relations
	private Map<String, List<String>> mMap_PlaceOfBirth;
	private Map<String, List<String>> mMap_PlaceOfDeath;
	private Map<String, List<String>> mMap_CauseOfDeath;
	//private Map<String, String> mMap_DateOfBirth;
	private Map<String, List<String>> mMap_Religion;

	private Set<RelationDBDescriptor> mSet_dbMaps;

	/**
	 * KnowledgeBase is initialized as a singleton
	 * 
	 * @return
	 * @throws FileNotFoundException
	 */
	public static KnowledgeBase getInstance() throws FileNotFoundException {
		if (mInstance == null) {
			mInstance = new KnowledgeBase();
		}
		return mInstance;
	}

	/**
	 * Private constructor for the singleton
	 * 
	 * @throws FileNotFoundException
	 */
	private KnowledgeBase() throws FileNotFoundException {
		Logger.writeLine("Setting up knowledge base...");

		// Sets of each of the wrapped relation map
		mSet_dbMaps = new HashSet<RelationDBDescriptor>();

		// Instantiate and initializes each of the relation DBs
		// Initializes the set
		mSet_dbMaps = new HashSet<RelationDBDescriptor>();

		// Place of birth
		mMap_PlaceOfBirth = new HashMap<String, List<String>>();
		mSet_dbMaps.add(new RelationDBDescriptor(Relationship.PLACE_OF_BIRTH,
				"Place of birth", mMap_PlaceOfBirth,
				KnowledgeBaseConstant.mPath_PlaceOfBirth,
				KnowledgeBaseConstant.FREEBASE_KEYWORD_PLACE_OF_BIRTH));

		// Place of death
		mMap_PlaceOfDeath = new HashMap<String, List<String>>();
		mSet_dbMaps.add(new RelationDBDescriptor(Relationship.PLACE_OF_DEATH,
				"Place of death", mMap_PlaceOfDeath,
				KnowledgeBaseConstant.mPath_PlaceOfDeath,
				KnowledgeBaseConstant.FREEBASE_KEYWORD_PLACE_OF_DEATH));

		// Cause of death
		mMap_CauseOfDeath = new HashMap<String, List<String>>();
		mSet_dbMaps.add(new RelationDBDescriptor(Relationship.CAUSE_OF_DEATH,
				"Cause of death", mMap_CauseOfDeath,
				KnowledgeBaseConstant.mPath_CauseOfDeath,
				KnowledgeBaseConstant.FREEBASE_KEYWORD_CAUSE_OF_DEATH));

		// Date of birth
		/*mMap_DateOfBirth = new HashMap<String, List<String>>();
		mSet_dbMaps.add(new RelationDBDescriptor(Relationship.DATE_OF_BIRTH,
				"Date of birth", mMap_DateOfBirth,
				KnowledgeBaseConstant.mPath_DateOfBirth,
				KnowledgeBaseConstant.FREEBASE_KEYWORD_DATE_OF_BIRTH));*/

		// Religion
		mMap_Religion = new HashMap<String, List<String>>();
		mSet_dbMaps.add(new RelationDBDescriptor(Relationship.RELIGION,
				"Religion", mMap_Religion,
				KnowledgeBaseConstant.mPath_Religion,
				KnowledgeBaseConstant.FREEBASE_KEYWORD_RELIGION));

		initialize_mMaps();

		Logger.writeLine("Done setting up knowledge base.");
	}

	/**
	 * Initializes each of the HashTable wrapped inside of the set_dbMaps It
	 * basically loads the text file for each of the relations put on the map
	 * 
	 * @param mSet_dbMaps
	 *            The set where each of the relations is put
	 * @throws FileNotFoundException
	 */
	private void initialize_mMaps() throws FileNotFoundException {
		long tupleRead = 0;
		for (RelationDBDescriptor rdb : mSet_dbMaps) {
			File input_file = new File(rdb.getFilePath());
			if (!input_file.exists()) {
				throw new FileNotFoundException("Relation file not found: "
						+ rdb.getFilePath());
			}

			int topLinesToSkip = KnowledgeBaseConstant.mTopLinesToSkip;

			Scanner scan = new Scanner(input_file);

			int lineCount = 0;
			try {
				while (scan.hasNextLine()) {
					String line = scan.nextLine();
					lineCount++;

					// Skip lines
					if (topLinesToSkip > 0) {
						topLinesToSkip--;
						continue;
					}

					String[] tokens = line.trim().split("\t");

					// Oddity of the FreeBase dump
					// DATE_OF_BIRTH relation has the last entity and relation
					// separated
					// by 2 tabs, while other relations are separated by a
					// single tab
					// if (!((!rdb.getRelationship().equals(
					// Relationship.DATE_OF_BIRTH) && tokens.length == 4) ||
					// (rdb.getRelationship().equals(
					// Relationship.DATE_OF_BIRTH) && tokens.length == 4))) {
					// Fixed by data clean-up instead of trick code
					if (tokens.length != 3) {
						throw new IllegalArgumentException(
								"Unexpected tokens = " + line
										+ " while init freebase "
										+ rdb.getRelationship_str()
										+ " at line = " + lineCount);
					}

					String e1 = tokens[0];
					String freebase_relation_keyword = tokens[1];
					String e2 = tokens[2];

					boolean validRelationString = false;
					for (RelationDBDescriptor rdb2 : mSet_dbMaps) {
						if (rdb2.getFreebase_keyword().equals(
								freebase_relation_keyword)) {
							validRelationString = true;
							break;
						}
					}

					// Not a valid relation
					if (!validRelationString) {
						throw new IllegalArgumentException(
								"Read a relation of type = "
										+ freebase_relation_keyword
										+ " when init " + rdb.getRelationship_str() +" at line = "
										+ lineCount);
					}

					// Joe said we don't have to do this
					// if (rdb.getIsTwoWayRelation()) {
					// rdb.getMap_DB().put(e1, e2);
					// rdb.getMap_DB().put(e2, e1);
					// } else {
					
					if(!rdb.getMap_DB().containsKey(e1)){
						List<String> e2List = new ArrayList<String>();
						rdb.getMap_DB().put(e1, e2List);
					}
					tupleRead++;
					rdb.getMap_DB().get(e1).add(e2);
					// }
				}
			} finally {
				scan.close();
			}
			System.out.println("Tuple read = " + tupleRead);
			tupleRead = 0;
			System.out.printf("Length of Database (%s) = %d\n",
					rdb.getRelationship_str(), rdb.getMap_DB().size());
		}
	}

	/**
	 * 
	 * Check whether a relationship between entities exist or not
	 * 
	 * @param query
	 *            The relationship to be queried
	 * @return Return true if the relation exists, or false if it doesn't
	 */
	public boolean isAssertionTrue(RelationInstance query) {
		// For each possible relations
		for (RelationDBDescriptor rdb : mSet_dbMaps) {
			if (query.getRelationship().equals(rdb.getRelationship())) {
				String e1_mid = query.getEntity1().getMid();
				String e2_mid = query.getEntity2().getMid();

				List<String> e2List = rdb.getMap_DB().get(e1_mid);

				if(e2List == null){
					return false;
				}
				
				for(String e2FROMe1_mid : e2List){
					if (e2_mid.equals(e2FROMe1_mid)) {
						return true;
					}
				}
			}

		}
		return false;
	}

	/**
	 * Given e1, returns a list of e2s that satisfies <e1,r, e2>
	 * 
	 * @param e1
	 *            Th
	 * @param r
	 * @return list of e2s
	 */
	public List<Entity> getSatisfyingEntities(Entity e1, Relationship r) {
		List<Entity> list_of_matching_e2s = new ArrayList<Entity>();
	
		for(RelationDBDescriptor rdb : mSet_dbMaps){
			if (r.equals(rdb.getRelationship())) {
				List<String> e2List = rdb.getMap_DB().get(e1.getMid());
				if(e2List == null){
					continue;
				}
				for(String e2_mid : e2List){
					list_of_matching_e2s.add(Entity.getEntityFromMid(e2_mid));
				}
				break;
			}
		}
		return list_of_matching_e2s;
	}
	
	/**
	 * @param r
	 * @return true iff there exists r<e1, -> in the knowledge base
	 */
	public boolean isFirstArgumentEntity(Entity e1, Relationship r){	
		for (RelationDBDescriptor rdb : mSet_dbMaps) {
			if(rdb.getRelationship().equals(r)){
				return rdb.getMap_DB().containsKey(e1.getMid());
			}
		}
		return false;
	}
}
