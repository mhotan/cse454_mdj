package knowledgeBase;

/**
 * maps wiki articles to freebase mid's
 * @author sjonany
 */
public interface WikiFreebaseMapper {
	/**
	 * convert wikipedia article name to a freebase entity
	 * @param wikiArticleName
	 * @return null if no such entry on freebase exists
	 */
	public Entity convertToEntity(String wikiArticleName) throws Exception;
}
