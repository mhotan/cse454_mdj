package knowledgeBase;

import java.util.ArrayList;
import java.util.List;

import trainingDataGenerator.AnnotatedSentenceParser.Meta;
import util.Logger;

/**
 * represents date in freebase, since they have different formats
 * @author sjonany
 */
public class Date{

	//only year can be negative
	//if month or date is negative, it means they are not set
	//year, however, is always set
	private int year;
	private int month = -1;
	private int date = -1;

	public int getYear(){
		return year;
	}

	public int getMonth(){
		return month;
	}

	public int getDate(){
		return date;
	}

	/**
	 * @param other
	 * @return true only if this date object is sure to be after the provided date
	 * 	E.g. "2012" is after "1995-01-31"
	 * 	If not sure, return false.
	 * 	E.g. "2012" is after "2012-01" is false, because we can't be sure.
	 */
	public boolean isAfter(Date other){
		if(this.year != other.year){
			return this.year > other.year;
		}

		//the years are equal
		if(this.month < 0 || other.month < 0){
			//if months are not set, we can't compare
			return false;
		}

		//months are set 

		if(this.month != other.month){
			return this.month > other.month;
		}

		//all months are equal
		if(this.date <0 || other.date <0 ){
			return false;
		}

		return this.date > other.date;
	}

	/**
	 * @param s a date from freebase dump e.g. the "1998-01" string from
	 * 		a row in the freebase dump
	 * 		/m/0n50zw2	/people/deceased_person/date_of_death		1998-01
	 * @return date representing the string
	 */
	public static Date deserializeFromFreebase(String s){
		boolean isNegativeYear = s.startsWith("-");
		String[] rawTokens = s.split("-");
		//remove the empty tokens
		List<String> tokens = new ArrayList<String>();

		for(String token : rawTokens){
			if(token.trim().length() != 0){
				tokens.add(token);
			}
		}

		Date date = new Date();

		if(tokens.size()>= 1){
			date.year = Integer.parseInt(tokens.get(0));
			if(isNegativeYear){
				date.year *= -1;
			}

			if(tokens.size() >= 2){
				date.month = Integer.parseInt(tokens.get(1));

				if(tokens.size() >= 3){
					date.date = Integer.parseInt(tokens.get(2));
				}
			}
		}

		return date;
	}

	/**
	 * This method does not always work since some meta are unparseable:
	 * By using this command "grep -v $'..._..._[0-9]\{8\}' sentences.meta"
	 * I found only 2 other formats where meta information does not (maybe) provide date
	 * 9850767	7482309	XIN20020120_ARB_0024.LDC2004T17	216	496
	 * 20923131	15516865	REU004-0347.950506.LDC98T25	233	413
	 * @param meta information from a preprocessed corpus sentence
	 * @return publication date of the article the sentence is in, null if meta does not provide such info
	 */
	public static Date deserializeFromMeta(Meta meta){
		try{
			//example: XIN_ENG_20021028.0184.LDC2007T07
			String filename = meta.originalFileName;
			int endDateIndex = filename.indexOf('.') - 1;
			int startDateIndex = endDateIndex;
			for(;filename.charAt(startDateIndex-1) != '_' ; startDateIndex--){}
			//sample:  20021028
			String dateString = filename.substring(startDateIndex, endDateIndex+1);
			int year = Integer.parseInt(dateString.substring(0,4));
			int month = Integer.parseInt(dateString.substring(4, 6));
			int date = Integer.parseInt(dateString.substring(6,8));

			Date dateObj = new Date();
			dateObj.year = year;
			dateObj.month = month;
			dateObj.date = date;
			return dateObj;
		}catch(Exception e){
			//any parsing error -> return null
			return null;
		}
	}

	@Override
	public String toString(){
		return "Year = " + year + " Month = " + month + " Date = " + date;
	}
}
