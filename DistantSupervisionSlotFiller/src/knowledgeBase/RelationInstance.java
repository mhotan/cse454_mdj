package knowledgeBase;

//or a freebase tuple
public class RelationInstance {
	//note: order matters for - it's e1, r, e2 when read in a sentence
	private Entity e1;
	private Entity e2;
	private Relationship r;
	
	public RelationInstance(Entity e1, Entity e2, Relationship r){
		this.e1 = e1;
		this.e2 = e2;
		this.r = r;
	}
	
	public void setRelationship(Relationship r){
		this.r = r;
	}
	
	public Relationship getRelationship(){
		return r;
	}
	
	public Entity getEntity1(){
		return e1;
	}
	
	public Entity getEntity2(){
		return e2;
	}
	
	@Override
	public String toString(){
		return "<" + e1 + "," + e2 + "," + r + ">";
	}
	
	@Override
	public boolean equals(Object o){
		if(!(o instanceof RelationInstance)){
			return false;
		}
		
		RelationInstance other = (RelationInstance) o;
		return other.e1.equals(this.e1) && 
				other.e2.equals(this.e2) &&  
				other.r.equals(this.r);
	}
	
	@Override
	public int hashCode(){
		int code = 37;
		code = 31 * code + e1.hashCode();
		code = 31 * code + e2.hashCode();
		code = 31 * code + r.hashCode();
		
		return code;
	}
	
	//readable serialization and deserialization of this object
	public String toStringDump(){
		return this.getEntity1().getMid() + "\t" + 
				this.getEntity2().getMid() + "\t" + 
				this.getRelationship().name();
	}
	
	public static RelationInstance fromStringDump(String text){
		String[] tokens = text.split("\t");
		return new RelationInstance(Entity.getEntityFromMid(tokens[0]),
				Entity.getEntityFromMid(tokens[1]),
				Relationship.valueOf(tokens[2]));
	}
	
	@Override
	public Object clone(){
		return new RelationInstance(e1,e2,r);
	}
}
