package sf.filler.distantsupervision;

import java.util.Set;

import knowledgeBase.WikiFreebaseMapper;
import classifier.Classifier;
import features.oldstuff.FeatureExtractor;

public abstract class PlaceOfDeath extends PlaceOf {

	protected PlaceOfDeath(Classifier classifier,
			FeatureExtractor featureExtractor,
			WikiFreebaseMapper wikiFreebaseMapper, Set<String> setofPlaceMID,
			String slotName) throws Exception {
		super(classifier, featureExtractor, wikiFreebaseMapper, setofPlaceMID,
				slotName, "PLACE_OF_DEATH");
	}
	
}