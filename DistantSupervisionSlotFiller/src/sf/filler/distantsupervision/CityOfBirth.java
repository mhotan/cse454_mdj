package sf.filler.distantsupervision;

import java.util.Set;

import knowledgeBase.WikiFreebaseMapper;
import classifier.Classifier;
import features.oldstuff.FeatureExtractor;

public class CityOfBirth extends PlaceOfBirth {

	public CityOfBirth(Classifier classifier,
			FeatureExtractor featureExtractor,
			WikiFreebaseMapper wikiFreebaseMapper, Set<String> setofCityMID) throws Exception {
		super(classifier, featureExtractor, wikiFreebaseMapper, setofCityMID, "per:city_of_birth");
	}
	
}
