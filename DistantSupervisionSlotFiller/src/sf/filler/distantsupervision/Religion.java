package sf.filler.distantsupervision;

import java.util.List;
import java.util.Map;
import java.util.Set;

import knowledgeBase.WikiFreebaseMapper;

import sf.SFEntity;
import sf.SFEntity.SingleAnswer;
import trainingDataGenerator.AnnotatedSentenceParserAdapter;
import trainingDataGenerator.AnnotationConstant;
import trainingDataGenerator.AnnotatedSentenceParser.Range;
import trainingDataGenerator.AnnotatedSentenceParserAdapter.AnnotationParsingException;
import benchmark.RangePair;
import classifier.Classifier;
import classifier.Result;
import features.oldstuff.FeatureExtractor;

public class Religion extends PersonFiller {

	// This represents the MID that is religion
	private Set<String> mReligionMID;
	private String RELIGION_RELATION = "RELIGION";

	public Religion(Classifier classifier,
			FeatureExtractor featureExtractor,
			WikiFreebaseMapper wikiFreebaseMapper, Set<String> religionMID) throws Exception {
		super(classifier, featureExtractor, wikiFreebaseMapper, "per:religion");
		this.mReligionMID = religionMID; 
	}

	@Override
	public void predict(SFEntity mention,
			Map<AnnotationConstant, String> annotations,
			Map<RangePair, Result> classifierCache) {

		try {
			AnnotatedSentenceParserAdapter annots = new AnnotatedSentenceParserAdapter(
					annotations);

			Range personNameRange = isQueryCriterionValidated(mention, annots);
			// Proceeds only if the criterion is validated
			if (personNameRange != null) {
				String personName = mention.mentionString;

				// The state/province
				List<Range> rangesReligion;
				rangesReligion = super.getRangeForWhatOnTheSet(
						annotations, mReligionMID);

				for (Range rangeReligion : rangesReligion) {
					if(!personNameRange.isIntersect(rangeReligion)){
						RangePair rangePair = new RangePair(personNameRange,
								rangeReligion);
						Result classification;
						if (classifierCache.containsKey(rangePair)) {
							classification = classifierCache.get(rangePair);
						} else {
							List<String> features = null;
							try {
								features = featureExtractor.getFeatures(annots,
										personNameRange, rangeReligion);
							} catch (AnnotationParsingException e) {
								continue;
							}

							classification = classifier
									.classifySingleSentence(features);
							// cache the new result
							classifierCache.put(rangePair, classification);
						}

						String relationship = classification.getRelationship();

						if (relationship.equals(this.RELIGION_RELATION)) {
							SingleAnswer answer = super.compileAnswer(annots,
									rangeReligion, personName, mention,
									classification.getScore());

							if (super.isNewScoreBetter(answer, mention)) {
								try {
									System.out
									.println("old: "
											+ ((SingleAnswer) mention.answers
													.get(slotName)).reasoning.sentence);
									System.out.println("new: "
											+ answer.reasoning.sentence + "\n");
									System.out.println("\nNew score is better: ");
								} catch (Exception e) {
								}

								mention.answers.put(this.slotName, answer);
							}
						}

					}
				}

			}

		} catch (AnnotationParsingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
