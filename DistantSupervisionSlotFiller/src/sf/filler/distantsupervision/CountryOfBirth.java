package sf.filler.distantsupervision;

import java.util.Set;

import knowledgeBase.WikiFreebaseMapper;
import classifier.Classifier;
import features.oldstuff.FeatureExtractor;

public class CountryOfBirth extends PlaceOfBirth {

	public CountryOfBirth(Classifier classifier,
			FeatureExtractor featureExtractor,
			WikiFreebaseMapper wikiFreebaseMapper, Set<String> setofCountryMID) throws Exception {
		super(classifier, featureExtractor, wikiFreebaseMapper, setofCountryMID, "per:country_of_birth");
	}
	
}
