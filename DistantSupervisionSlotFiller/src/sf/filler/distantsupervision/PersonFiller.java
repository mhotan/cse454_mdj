package sf.filler.distantsupervision;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import knowledgeBase.Entity;
import knowledgeBase.WikiFreebaseMapper;
import sf.SFEntity;
import sf.SFEntity.Answer;
import sf.SFEntity.Reasoning;
import sf.SFEntity.SingleAnswer;
import sf.filler.Filler;
import tackbp.KbEntity.EntityType;
import trainingDataGenerator.AnnotatedSentenceParser.Range;
import trainingDataGenerator.AnnotatedSentenceParser.Wikification;
import trainingDataGenerator.AnnotatedSentenceParserAdapter;
import trainingDataGenerator.AnnotationConstant;
import trainingDataGenerator.AnnotatedSentenceParser.Wikification.WikificationInfo;
import trainingDataGenerator.AnnotatedSentenceParserAdapter.AnnotationParsingException;
import util.FillerHelper;
import benchmark.RangePair;
import classifier.Classifier;
import classifier.Result;
import features.oldstuff.FeatureExtractor;

/**
 * Needs "tokens", "meta",
 * 
 * @author denny
 * 
 */
public abstract class PersonFiller extends Filler {
	// TODO: singleton
	protected Classifier classifier;
	protected FeatureExtractor featureExtractor;
	protected WikiFreebaseMapper wikiFreebaseMapper;

	protected PersonFiller(Classifier classifier,
			FeatureExtractor featureExtractor,
			WikiFreebaseMapper wikiFreebaseMapper, String slotName)
			throws Exception {
		this.classifier = classifier;
		this.featureExtractor = featureExtractor;
		this.wikiFreebaseMapper = wikiFreebaseMapper;
		this.slotName = slotName;
	}

	/**
	 * @param rangePair
	 *            the
	 * @param features
	 * @param classifierCache
	 * @return
	 */
	protected Result classifyFeatures(RangePair rangePair, List<String> features,
			Map<RangePair, Result> classifierCache) {
		Result classification = classifier.classifySingleSentence(features);
		classifierCache.put(rangePair, classification);

		return classification;
	}

	/**
	 * This is a generic checking across all PER slot filler. Checks the
	 * following, return true if any of these are not true: 1. The query doesn't
	 * ignore the slot filler specified in this.slotName 2. The slot is not of a
	 * type PER 3. The person name is mentioned on the sentence
	 * 
	 * @param mention
	 *            information about the slot to be filled
	 * @param annots
	 *            the input sentence annotator
	 * 
	 * @return Range that represents the person name, or null if it is not validated
	 * 
	 * @throws AnnotationParsingException
	 *             thrown if there is an error within annotation parsing
	 */
	protected Range isQueryCriterionValidated(SFEntity mention,
			AnnotatedSentenceParserAdapter annots)
			throws AnnotationParsingException {

		if (mention.ignoredSlots.contains(slotName)
				|| mention.entityType != EntityType.PER) {
			return null;
		}

		// Get the person name
		String person_name = mention.mentionString;
		// Convert it into token
		List<String> nameTokens = new ArrayList<String>(
				Arrays.asList(person_name.split(" ")));
		// Tokenize the entire sentence input into tokens as well
		List<String> tokens = annots.getTokens().tokens;

		// the person name's range
		// null if the Range doesn't exist
		return FillerHelper.getTokenRangeForSubset(tokens,
				nameTokens);
		
	}

	/**
	 * Put the answer into the mention SFEntity
	 * 
	 * @param annots parser adapter
	 * @param answerRange Range identifies the answer
	 * @param personName the name of the person
	 * @param mention query to fill in
	 *
	 * @return return the compiled answer
	 * 
	 * @throws AnnotationParsingException
	 */
	
	public SingleAnswer compileAnswer(AnnotatedSentenceParserAdapter annots,
			Range answerRange, String personName, SFEntity mention, double score)
			throws AnnotationParsingException {
		String answer = FillerHelper.getCombinedTokens(annots, answerRange);

		// Return value
		SFEntity.SingleAnswer ans = new SFEntity.SingleAnswer();

		System.out.println("Found a " + this.slotName + " relation for " + personName
				+ ", place = " + answer);

		String filename = annots.getMeta().originalFileName;
		ans.answer = answer;
		ans.doc = filename;
		ans.score = score;
		Reasoning r = new Reasoning();
		r.e1 = personName;
		r.e2 = answer;
		r.relationship = this.slotName;
		r.sentence = annots.getText().sentence;
		ans.reasoning = r;

		return ans;
	}
	
	/**
	 * Returns whether the new score is better than the oldScore
	 * 
	 * @param newScore
	 * @param query the oldScore is stored in the query
	 * 
	 * @return true/false
	 */
	protected boolean isNewScoreBetter(SingleAnswer newScore, SFEntity query) {
		Answer old_answer = query.answers.get(this.slotName);
		
		if(old_answer == null)
			return true;
		
		//TODO: fix this!
//		if(old_answer instanceof SFEntity.SingleAnswer){
			if(((SFEntity.SingleAnswer) old_answer).score > newScore.score){
				return false;
			}
//		}
		
		return true;
	}

	/**
	 * Return the Range for the occurrence of the substring on the string
	 * 
	 * @param string
	 *            the string to search from
	 * @param substring
	 *            the string to search for
	 * @return Range on where substring occurs on string, null if not exist
	 */
	public Range getRangeForSubstring(String string, String substring) {
		// a b c
		// q a b c d e
		String[] tokens_str = string.trim().split(" ");
		String[] tokens_substr = substring.trim().split(" ");

		// substring to be matched
		if (tokens_substr.length > tokens_str.length) {
			return null;
		}

		for (int i = 0; i <= tokens_str.length - tokens_substr.length; i++) {
			boolean foundMatches = true;
			int j;
			for (j = 0; j < tokens_substr.length; j++) {
				if (!tokens_substr[j].equals(tokens_str[i + j])) {
					foundMatches = false;
					break;
				}
			}
			if (foundMatches) {
				return new Range(i, i + tokens_substr.length);
			}
		}

		return null;
	}

	/**
	 * 
	 * Get the string described by the Range from the sentence
	 * 
	 * @param sentence
	 *            the source string
	 * @param range
	 *            describes where which of the string to be taken
	 * @return the result
	 */
	public String getStringFromRange(String sentence, Range range) {
		String[] tokens = sentence.split(" ");
		String result = "";

		if (range.getStart() > (tokens.length - 1)
				|| range.getEnd() > (tokens.length)) {
			throw new RuntimeException("Invalid range within");
		}

		for (int i = range.getStart(); i < range.getEnd(); i++) {
			result += tokens[i].trim();
			if (i != (range.getEnd() - 1)) {
				result += " ";
			}
		}

		return result;
	}
	
	/**
	 * Given a sentence, returns the Range on which an entity mentioned in the list is located in
	 * the sentence The idea is to use FreeBase data to figure out which part of
	 * the sentence is a place entity
	 * 
	 * @return list of Range where the entities are found
	 * @throws Exception
	 */
	protected List<Range> getRangeForWhatOnTheSet(
			Map<AnnotationConstant, String> annotatedSentence, Set<String> setOfEntities) {
		// Wrap the annotatedSentence
		AnnotatedSentenceParserAdapter annotatedSentenceAdapter = new AnnotatedSentenceParserAdapter(
				annotatedSentence);
		// Get and process the wikification
		Wikification wikiInfo = null;
		// In case there are multiple Place entity
		List<Range> foundRange = new ArrayList<Range>();
		try {
			wikiInfo = annotatedSentenceAdapter.getWikification();
		} catch (AnnotationParsingException e) {
			return foundRange;
		}
		List<WikificationInfo> wikis = wikiInfo.wiki;
		for (WikificationInfo wiki : wikis) {
			// The wikification isn't reliable enough to be taken into
			// consideration
			if (wiki.confidence < 0.5) {
				continue;
			}
			Entity e = null;
			try {
				e = wikiFreebaseMapper.convertToEntity(wiki.wikiArticleName);
			} catch (Exception ex) {
				continue;
			}

			// Convert wiki article's guid to MID
			// Check whether the entity is of type city
			if (e != null && setOfEntities.contains(e.getMid()) && wiki.range != null) {
				foundRange.add(wiki.range);
			}
		}
		return foundRange;
	}


}
