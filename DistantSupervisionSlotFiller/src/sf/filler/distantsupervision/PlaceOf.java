package sf.filler.distantsupervision;

import java.util.List;
import java.util.Map;
import java.util.Set;

import knowledgeBase.WikiFreebaseMapper;
import sf.SFEntity;
import sf.SFEntity.SingleAnswer;
import trainingDataGenerator.AnnotatedSentenceParser.Range;
import trainingDataGenerator.AnnotatedSentenceParserAdapter;
import trainingDataGenerator.AnnotatedSentenceParserAdapter.AnnotationParsingException;
import trainingDataGenerator.AnnotationConstant;
import benchmark.RangePair;
import classifier.Classifier;
import classifier.Result;
import features.oldstuff.FeatureExtractor;

public abstract class PlaceOf extends PersonFiller {

	// This corresponds to either city or province/state or country
	private Set<String> mSetofPlaceMID;
	private String mRelationshipIdentifier;

	protected PlaceOf(Classifier classifier, FeatureExtractor featureExtractor,
			WikiFreebaseMapper wikiFreebaseMapper, Set<String> setofPlaceMID,
			String slotName, String relationshipIdentifier) throws Exception {
		super(classifier, featureExtractor, wikiFreebaseMapper, slotName);
		this.mSetofPlaceMID = setofPlaceMID;
		this.mRelationshipIdentifier = relationshipIdentifier;
	}

	@Override
	public void predict(SFEntity mention,
			Map<AnnotationConstant, String> annotations,
			Map<RangePair, Result> classifierCache) {

		try {

			AnnotatedSentenceParserAdapter annots = new AnnotatedSentenceParserAdapter(
					annotations);

			Range personNameRange = isQueryCriterionValidated(mention, annots);
			// Proceeds only if the criterion is validated
			if (personNameRange != null) {
				String personName = mention.mentionString;

				// The state/province
				List<Range> rangesStateProvince;
				rangesStateProvince = super.getRangeForWhatOnTheSet(
						annotations, mSetofPlaceMID);

				for (Range rangeStateProvince : rangesStateProvince) {
					if(!personNameRange.isIntersect(rangeStateProvince)){
						RangePair rangePair = new RangePair(personNameRange,
								rangeStateProvince);
						Result classification;
						if (classifierCache.containsKey(rangePair)) {
							classification = classifierCache.get(rangePair);
						} else {
							List<String> features = null;
							try {
								features = featureExtractor.getFeatures(annots,
										personNameRange, rangeStateProvince);
							} catch (AnnotationParsingException e) {
								continue;
							}
	
							classification = classifier
									.classifySingleSentence(features);
							// cache the new result
							classifierCache.put(rangePair, classification);
						}
	
						String relationship = classification.getRelationship();
	
						if (relationship.equals(mRelationshipIdentifier)) {
							SingleAnswer answer = super.compileAnswer(annots,
									rangeStateProvince, personName, mention,
									classification.getScore());
	
							if (super.isNewScoreBetter(answer, mention)) {
								try {
									System.out
											.println("old: "
													+ ((SingleAnswer) mention.answers
															.get(slotName)).reasoning.sentence);
									System.out.println("new: "
											+ answer.reasoning.sentence + "\n");
									System.out.println("\nNew score is better: ");
								} catch (Exception e) {
								}
	
								mention.answers.put(this.slotName, answer);
							}
						}
					}
				}
			}

		} catch (AnnotationParsingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
