package sf.filler.distantsupervision;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import knowledgeBase.Entity;
import knowledgeBase.WikiFreebaseMapper;
import sf.SFEntity;
import sf.SFEntity.Answer;
import sf.SFEntity.Reasoning;
import sf.filler.Filler;
import tackbp.KbEntity.EntityType;
import trainingDataGenerator.AnnotatedSentenceParser.Range;
import trainingDataGenerator.AnnotatedSentenceParser.Wikification;
import trainingDataGenerator.AnnotatedSentenceParser.Wikification.WikificationInfo;
import trainingDataGenerator.AnnotatedSentenceParserAdapter;
import trainingDataGenerator.AnnotatedSentenceParserAdapter.AnnotationParsingException;
import trainingDataGenerator.AnnotationConstant;
import util.FillerHelper;
import util.Logger;
import benchmark.RangePair;
import classifier.Classifier;
import classifier.Result;
import features.oldstuff.FeatureExtractor;

public class CauseOfDeathFiller extends Filler{
	private Classifier classifier;
	private FeatureExtractor featureExtractor;
	private WikiFreebaseMapper wikiFreebaseMapper;
	private Set<String> causeOfDeathMID;
	
	public CauseOfDeathFiller(Classifier classifier, 
			FeatureExtractor featureExtractor, WikiFreebaseMapper wikiFreebaseMapper, Set<String> causeOfDeathMID) throws Exception {
		slotName = "per:cause_of_death";
		this.classifier = classifier;
		this.featureExtractor = featureExtractor;
		this.wikiFreebaseMapper = wikiFreebaseMapper;
		this.causeOfDeathMID = causeOfDeathMID;
	}

	
	@Override
	public void predict(SFEntity mention,
			Map<AnnotationConstant, String> annotations,
			Map<RangePair, Result> classifierCache) {
		try{
			AnnotatedSentenceParserAdapter annots = new AnnotatedSentenceParserAdapter(annotations);
			if (mention.ignoredSlots.contains(slotName)
					|| mention.entityType != EntityType.PER) {
				return;
			}
	
			String person_name = mention.mentionString;
			
			List<String> nameTokens = new ArrayList<String>(Arrays.asList(person_name.split(" ")));
			List<String> tokens = annots.getTokens().tokens;
			//the person's name
			Range range1 = FillerHelper.getTokenRangeForSubset(tokens, nameTokens);
			// If the person's name is not mentioned in the sentence, we don't do
			// anything
			if (range1 == null) {
				return;
			}
	
			// Cause of death
			Set<Range> ranges2;
	
			ranges2 = getCausesOfDeath(annotations);
	
			if(ranges2 == null){
				return;
			}
			
			for (Range range2 : ranges2) {
				if(!range1.isIntersect(range2)){
					RangePair rangePair = new RangePair(range1, range2);
					Result classification;
					if(classifierCache.containsKey(rangePair)){
						classification = classifierCache.get(rangePair);
					}else{
						List<String> features = null;
						try{
							features = featureExtractor.getFeatures(annots,
								range1, range2);
						}catch(AnnotationParsingException e){
							continue;
						}
						
						classification = classifier.classifySingleSentence(features);
						//cache the new result
						classifierCache.put(rangePair, classification);
					}
			
					String relationship = classification.getRelationship();
					if (relationship.equals("CAUSE_OF_DEATH")) {
						String answer = FillerHelper.getCombinedTokens(annots, range2);
						System.out.println("Found a cause of death for " + person_name + ", answer = "+ answer);
						// get the filename of this sentence.
						String filename = annots.getMeta().originalFileName;
						SFEntity.SingleAnswer ans = new SFEntity.SingleAnswer();
						
						Answer old_answer = mention.answers.get(this.slotName);
						
						//only override old answer if new score is better
						if(old_answer == null ||((SFEntity.SingleAnswer) old_answer).score < classification.getScore()){	
							ans.score = classification.getScore();
							ans.answer = answer;
							ans.doc = filename;
							ans.reasoning = new Reasoning();
							ans.reasoning.e1 = person_name;
							ans.reasoning.e2 = answer;
							ans.reasoning.relationship = slotName;
							ans.reasoning.sentence = annots.getText().sentence;
							mention.answers.put(slotName, ans);
						}
						return;
					}
				}
			}
		}catch(AnnotationParsingException e){
			Logger.writeLine(e.getMessage());
		}
	}

	/**
	 * return the ranges of the possible causes of death of this sentence
	 * this is slightly tricky because sometimes there are conjunctions involved
	 * see http://nlp.cs.qc.cuny.edu/kbp/2011/TAC_KBP_Slots_V1.1.pdf
	 * one of the correct answer is "heart condition and hemorrhaging"
	 * @param annots
	 * @return
	 */
	private Set<Range> getCausesOfDeath(Map<AnnotationConstant, String> annots){
		// Wrap the annotatedSentence
		AnnotatedSentenceParserAdapter annotatedSentenceAdapter = new AnnotatedSentenceParserAdapter(
				annots);
		// Get and process the wikification
		Wikification wikiInfo = null;
		// In case there are multiple Place entity
		Set<Range> foundRange = new HashSet<Range>();
		try{
			wikiInfo = annotatedSentenceAdapter.getWikification();
		}catch(AnnotationParsingException e){
			return foundRange;
		}
		List<WikificationInfo> wikis = wikiInfo.wiki;
		for (WikificationInfo wiki : wikis) {
			// The wikification isn't reliable enough to be taken into
			// consideration
			if (wiki.confidence < 0.5) {
				continue;
			}
			Entity e = null;		
			try{
				e = wikiFreebaseMapper.convertToEntity(wiki.wikiArticleName);
			}catch(Exception ex){
				continue;
			}
			
			// Convert wiki article's guid to MID
			// Check whether the entity is a known cause of death 
			if (e != null && causeOfDeathMID.contains(e.getMid())) {
				foundRange.add(wiki.range);
			}
		}
		
		//TODO: check if any illnesses can be conjuncted- need to use nlp trees and get all possible subsets of nouns?
		//e.g she passed away due to the A coupled with B
		return foundRange;
	}
}
