package sf.filler.distantsupervision;

import java.util.Set;

import knowledgeBase.WikiFreebaseMapper;
import classifier.Classifier;
import features.oldstuff.FeatureExtractor;

public class CountryOfDeath extends PlaceOfDeath{

	public CountryOfDeath(Classifier classifier,
			FeatureExtractor featureExtractor,
			WikiFreebaseMapper wikiFreebaseMapper, Set<String> setofCountryMID) throws Exception {
		super(classifier, featureExtractor, wikiFreebaseMapper, setofCountryMID, "per:country_of_death");
	}

}
