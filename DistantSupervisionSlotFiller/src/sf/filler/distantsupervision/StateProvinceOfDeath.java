package sf.filler.distantsupervision;

import java.util.Set;

import knowledgeBase.WikiFreebaseMapper;
import classifier.Classifier;
import features.oldstuff.FeatureExtractor;

public class StateProvinceOfDeath extends PlaceOfDeath {

	public StateProvinceOfDeath(Classifier classifier,
			FeatureExtractor featureExtractor,
			WikiFreebaseMapper wikiFreebaseMapper, Set<String> setofStateProvinceMID)
			throws Exception {
		super(classifier, featureExtractor, wikiFreebaseMapper, setofStateProvinceMID,
				"per:stateorprovince_of_death");
	}

}
