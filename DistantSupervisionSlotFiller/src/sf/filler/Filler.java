package sf.filler;

import java.util.Map;

import classifier.Result;

import benchmark.RangePair;

import sf.SFEntity;
import trainingDataGenerator.AnnotationConstant;

/**
 *
 * @author Xiao Ling
 */

public abstract class Filler {
	public String slotName = null;
	public abstract void predict(SFEntity mention, Map<AnnotationConstant, String> annotations, Map<RangePair,Result> classifierCache);
}

