package knowledgeBase;

import org.junit.Assert;
import org.junit.Test;

import trainingDataGenerator.AnnotatedSentenceParser.Meta;

public class DateTest {

	//META test
	@Test
	public void testMetaParsing(){
		Meta meta = new Meta();
		meta.originalFileName = "XIN_ENG_20021028.0184.LDC2007T07";
		Date date = Date.deserializeFromMeta(meta);
		
		Assert.assertEquals(2002, date.getYear());
		Assert.assertEquals(10, date.getMonth());
		Assert.assertEquals(28, date.getDate());
	}
	
	//freebase format tests
	@Test
	public void testYearOnlyPositive4Digits(){
		String testDate =  "1712";
		Date date = Date.deserializeFromFreebase(testDate);

		Assert.assertEquals(1712, date.getYear());
		Assert.assertEquals(-1, date.getMonth());
		Assert.assertEquals(-1, date.getDate());
	}
	
	@Test
	public void testYearOnlyPositive3Digits(){
		String testDate =  "0536";
		Date date = Date.deserializeFromFreebase(testDate);

		Assert.assertEquals(536, date.getYear());
		Assert.assertEquals(-1, date.getMonth());
		Assert.assertEquals(-1, date.getDate());
	}
	
	
	@Test
	public void testYearOnlyNegative(){
		String testDate =  "-0120";
		Date date = Date.deserializeFromFreebase(testDate);

		Assert.assertEquals(-120, date.getYear());
		Assert.assertEquals(-1, date.getMonth());
		Assert.assertEquals(-1, date.getDate());
	}
	
	@Test
	public void testYearMonthOnlyPositive(){
		String testDate =  "1633-09";
		Date date = Date.deserializeFromFreebase(testDate);

		Assert.assertEquals(1633, date.getYear());
		Assert.assertEquals(9, date.getMonth());
		Assert.assertEquals(-1, date.getDate());
	}
	
	@Test
	public void testYearMonthOnlyNegative(){
		String testDate =  "-1633-09";
		Date date = Date.deserializeFromFreebase(testDate);

		Assert.assertEquals(-1633, date.getYear());
		Assert.assertEquals(9, date.getMonth());
		Assert.assertEquals(-1, date.getDate());
	}
	
	@Test
	public void testYearMonthDatePositive(){
		String testDate =  "1633-09-28";
		Date date = Date.deserializeFromFreebase(testDate);

		Assert.assertEquals(1633, date.getYear());
		Assert.assertEquals(9, date.getMonth());
		Assert.assertEquals(28, date.getDate());
	}
	
	@Test
	public void testYearMonthDateNegative(){
		String testDate =  "-1633-09-28";
		Date date = Date.deserializeFromFreebase(testDate);

		Assert.assertEquals(-1633, date.getYear());
		Assert.assertEquals(9, date.getMonth());
		Assert.assertEquals(28, date.getDate());
		
	}

	//comparison tests
	@Test
	public void testEqual(){
		Assert.assertFalse(isAfter("1234","1234"));
		Assert.assertFalse(isAfter("-1234","-1234"));
		Assert.assertFalse(isAfter("1234-12","1234-12"));
		Assert.assertFalse(isAfter("-1234-12","-1234-12"));
		Assert.assertFalse(isAfter("1234-12-23","1234-12-23"));
		Assert.assertFalse(isAfter("-1234-12-23","-1234-12-23"));
	}
	
	@Test
	public void testUnsure(){
		Assert.assertFalse(isAfter("1234", "1234-12"));
		Assert.assertFalse(isAfter("1234", "1234-12-28"));
		Assert.assertFalse(isAfter("1234-12", "1234-12-23"));
	}
	
	@Test
	public void testAfter(){
		Assert.assertTrue(isAfter("1234", "-1234"));
		Assert.assertTrue(isAfter("1", "-2"));
		Assert.assertTrue(isAfter("1234-12", "1234-11"));
		Assert.assertTrue(isAfter("1234-11", "1233-12"));
		Assert.assertTrue(isAfter("1234-12-30", "1000"));
		Assert.assertTrue(isAfter("1234-11-30", "1000-12"));
		Assert.assertTrue(isAfter("1234-23-14", "1234-23-13"));
	}
	
	public static boolean isAfter(String s1, String s2){
		return Date.deserializeFromFreebase(s1).isAfter(Date.deserializeFromFreebase(s2));
	}
}
