package trainingDataGenerator;

import java.util.Scanner;

import knowledgeBase.OfflineWikiFreebaseMapper;
import knowledgeBase.WikiFreebaseMapper;

//utility class to check the mapping of wiki articles to f
public class WikiFreebaseMappingManualDriver {
	public static void main(String[] args) throws Exception{
		Scanner reader = new Scanner(System.in);
		WikiFreebaseMapper mapper = new OfflineWikiFreebaseMapper();
		while(true){
			System.out.print("Wiki article name: ");
			String artName = reader.nextLine().trim();
			System.out.println("Guid = " + mapper.convertToEntity(artName));
		}
	}
}
