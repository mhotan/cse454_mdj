package trainingDataGenerator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import trainingDataGenerator.AnnotatedSentenceParser.Wikification;
import trainingDataGenerator.AnnotatedSentenceParser.Wikification.WikificationInfo;
import util.Logger;

/**
 * downloads all the mapping we need to convert any element in sentences.wikification
 * to freebase entity.
 * @author sjonany
 */
public class WikiMappingDownloader {
	private static double MIN_WIKI_CONF = 0.5;
	//private static String OUTPUT_PATH = "/Users/sjonany/Desktop/CSE 454/wikimapping.txt";
	//stores the sentence ID which has last been SUCCESSFULLY wikified
	private static String LOG_PATH = "/Users/sjonany/Desktop/CSE 454/lineCount.txt";
	//private static String INPUT_PATH = TrainingConfig.getAbsoluteAnnotationPath(TrainingConfig.INPUT_ANNOTATIONS_BASE_DIR,
			//AnnotationConstant.WIKIFICATION);

	//how long to wait if wiki-freebase query fails before trying again
	private static int RETRY_DELAY_SECONDS = 5;
	
	//TODO: in case of frequent failure, time to implement recovery
	//go to the last line number, and rebuild mapping to prevent duplication
	public static void main(String[] args) throws IOException{
		String INPUT_PATH = args[0];
		String OUTPUT_PATH = args[1];
		//<K,V> = <wiki article name, freebase guid>
		HashMap<String,String> mapping = new HashMap<String, String>();
		
		//TODO: if handle recovery from failure, need to append
		PrintWriter mappingWriter = new PrintWriter(new BufferedWriter(new FileWriter(OUTPUT_PATH, true)));
		PrintWriter logWriter = new PrintWriter(new BufferedWriter(new FileWriter(LOG_PATH, true)));
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(INPUT_PATH)));
		String line = reader.readLine();
		int lineCount = 0;
		while(line != null){
			if(lineCount % 10 == 0){
				System.out.println(lineCount);
			}
			Wikification wikiDump = AnnotatedSentenceParser.parseWikification(line);
			List<WikificationInfo> wikiList = wikiDump.wiki;
			for(WikificationInfo wiki : wikiList){
				wiki.wikiArticleName = wiki.wikiArticleName.trim();
				//actually, it's better to output the mapping right away, and write the last line that you have done 
				//mapping on. But if this happens fast, shouldn't be a problem.
				if(wiki.confidence >= MIN_WIKI_CONF && !mapping.containsKey(wiki.wikiArticleName)){
					String guid = convertToGuid(wiki.wikiArticleName);
					if(guid != null){
						mapping.put(wiki.wikiArticleName, guid);
						mappingWriter.write(wiki.wikiArticleName + "\t" + guid + "\n");
						mappingWriter.flush();
					}
				}
			}
			logWriter.write(lineCount + "\n");
			logWriter.flush();
			line = reader.readLine();
			lineCount++;
		}
		reader.close();
		mappingWriter.close();
		logWriter.close();
	};
	
	
	/**
	 * convert wikipedia article name to a freebase entity
	 * This method will keep re-querying if network is down.
	 * @param wikiArticleName
	 * @return null if no such entry on freebase exists
	 */
	public static String convertToGuid(String wikiArticleName){
		String guid = null;
		try{
			JSONObject jsonDump = readJsonFromUrl(getUrlQuery(wikiArticleName));
			guid = (String)jsonDump.get("guid");
		}catch(ErrorResponseCodeException ex){
			switch(ex.getResponseCode()){
			case 500:
				//no freebase entity that maps to the chosen wikipedia article
				return null;
			}
			Logger.writeLine("When finding freebase mapping of " + wikiArticleName + " - " + ex);
			return null;
		}catch(IOException ex){
			//most likely internet connection is down. Wait for a while and try again
			Logger.writeLine("Unable to connect to host " + " - " + ex);
			Logger.writeLine("Retrying in " + RETRY_DELAY_SECONDS + " seconds..");
			try {
				Thread.sleep(RETRY_DELAY_SECONDS * 1000);
			} catch (InterruptedException e1) {
				Logger.writeLine("Error when delaying before retry - " + e1);
				return null;
			}
			return convertToGuid(wikiArticleName);
		}catch(Exception ex){
			Logger.writeLine("When finding freebase mapping of " + wikiArticleName + " - " + ex);
			return null;
		}
		
		return guid;
	}
	
	/**
	 * @param wikiArticleName
	 * @return a url which represents a query to freebase asking for the freebase id which maps to wikiArticleName
	 */
	private static String getUrlQuery(String wikiArticleName){
		//Service call online 
		final String FREEBASE_WIKI_MAPPING_QUERY_PREFIX = "http://ids.freebaseapps.com/get_ids?id=/wikipedia/en/";
		
		return FREEBASE_WIKI_MAPPING_QUERY_PREFIX + wikiArticleName;
	}
	
	/**
	 * @param url
	 * @throws ErrorResponseCodeException if connection succeeded, but there is an error code
	 * @return the json object created by parsing the text dump returned by the url
	 */
	private static JSONObject readJsonFromUrl(String urlStr) throws IOException, JSONException {
		URL url = new URL(urlStr);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		
		if(!String.valueOf(conn.getResponseCode()).startsWith("2")){
		    throw new ErrorResponseCodeException(conn.getResponseCode(), conn.getResponseMessage());
		}
						
		InputStream is = conn.getInputStream();
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));			
			StringBuilder sb = new StringBuilder();
			int cp;
			while ((cp = rd.read()) != -1) {
				sb.append((char) cp);
			}
			
			JSONObject json = new JSONObject(sb.toString());
			return json;
		} finally {
			is.close();
		}
	}
	
	private static class ErrorResponseCodeException extends IOException{
		private static final long serialVersionUID = 1L;
		
		private int responseCode;
		private String responseMessage;
		
		public ErrorResponseCodeException(int respCode, String respMessage){
			this.responseCode = respCode;
			this.responseMessage = respMessage;
		}
		
		public int getResponseCode(){
			return responseCode;
		}
		
		public String getResponseMessage(){
			return responseMessage;
		}
		
		@Override
		public String toString(){
			return "Error code " + getResponseCode() + " : " + getResponseMessage();
		}
	}
}
