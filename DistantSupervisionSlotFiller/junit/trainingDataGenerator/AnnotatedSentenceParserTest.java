package trainingDataGenerator;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

import trainingDataGenerator.AnnotatedSentenceParser.CJ;
import trainingDataGenerator.AnnotatedSentenceParser.Meta;
import trainingDataGenerator.AnnotatedSentenceParser.Range;
import trainingDataGenerator.AnnotatedSentenceParser.Stanfordlemma;
import trainingDataGenerator.AnnotatedSentenceParser.Stanfordpos;
import trainingDataGenerator.AnnotatedSentenceParser.TokenSpans;
import trainingDataGenerator.AnnotatedSentenceParser.Tokens;
import trainingDataGenerator.AnnotatedSentenceParser.Wikification;
import trainingDataGenerator.AnnotatedSentenceParser.Wikification.WikificationInfo;
import edu.stanford.nlp.trees.Tree;

public class AnnotatedSentenceParserTest {

	@Test
	public void testMeta() {
		String text = "5	1	ARTICLE.txt	100	101";
		Meta meta = AnnotatedSentenceParser.parseMeta(text);
		Assert.assertEquals("5", meta.globalSentenceId);
		Assert.assertEquals(1, meta.lineNumber);
		Assert.assertEquals("ARTICLE.txt", meta.originalFileName);
		Assert.assertEquals(100, meta.beginCharOffset);
		Assert.assertEquals(101, meta.endCharOffset);
	}
	
	@Test
	public void testTokens(){
		String text ="5	token1 token2 token3";
		Tokens tokens = AnnotatedSentenceParser.parseTokens(text);
		Assert.assertEquals("5", tokens.globalSentenceId);
		List<String> expTokens = new ArrayList<String>();
		expTokens.add("token1");
		expTokens.add("token2");
		expTokens.add("token3");
		Assert.assertEquals(expTokens, tokens.tokens);
	}
	
	@Test
	public void testStanfordlemma(){
		String text ="5	token1 token2 token3";
		Stanfordlemma lemmas = AnnotatedSentenceParser.parseStanfordlemma(text);
		Assert.assertEquals("5", lemmas.globalSentenceId);
		List<String> lemmaTokens = new ArrayList<String>();
		lemmaTokens.add("token1");
		lemmaTokens.add("token2");
		lemmaTokens.add("token3");
		Assert.assertEquals(lemmaTokens, lemmas.lemma);
	}
	
	@Test
	public void testTokenSpans(){
		String text = "5	1:2 3:4 4:5";
		TokenSpans result = AnnotatedSentenceParser.parseTokenSpans(text);
		Assert.assertEquals("5", result.globalSentenceId);
		List<Range> expRanges = new ArrayList<Range>();
		expRanges.add(new Range(1,2));
		expRanges.add(new Range(3,4));
		expRanges.add(new Range(4,5));
		
		Assert.assertEquals(expRanges, result.tokenSpans);
		
	}
	
	@Test
	public void testStanfordPos(){
		String text = "5	NNP , CD";
		Stanfordpos result = AnnotatedSentenceParser.parseStanfordpos(text);
		Assert.assertEquals("5", result.globalSentenceId);
		List<String> resultTokens = new ArrayList<String>();
		resultTokens.add("NNP");
		resultTokens.add(",");
		resultTokens.add("CD");
		Assert.assertEquals(resultTokens, result.pos);
	}
	
	@Test
	public void testWikification(){
		String text = "5	0 1 Beijing 0.89	5 6 Xinhua_News_Agency 0.75";
		Wikification result = AnnotatedSentenceParser.parseWikification(text);
		Assert.assertEquals("5", result.globalSentenceId);
		Assert.assertEquals(result.wiki.size(), 2);
		List<WikificationInfo> expected = new ArrayList<WikificationInfo>();
		expected.add(new WikificationInfo(new Range(0,1), "Beijing",0.89));
		expected.add(new WikificationInfo(new Range(5,6), "Xinhua_News_Agency",0.75));
	}
	
	@Test
	public void testCJ(){
		String text = "5	S1 NP";
		CJ result = AnnotatedSentenceParser.parseCJ(text);
		Assert.assertEquals("5", result.globalSentenceId);
		Assert.assertEquals(Tree.valueOf("S1 NP"), result.parseTree);
	}
}
