package trainingDataGenerator;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import cc.factorie.protobuf.DocumentProtos.Relation;
import cc.factorie.protobuf.DocumentProtos.Relation.RelationMentionRef;
import edu.uw.cs.multir.learning.algorithm.Model;
import edu.uw.cs.multir.main.Train;
import edu.uw.cs.multir.preprocess.ConvertProtobufToMILDocument;
import edu.uw.cs.multir.preprocess.Mappings;

/**
 * playground for testing Train.java in multir
 * @author sjonany
 *
 */
public class TestMLTraining {
	public static void main(String[] args) throws IOException{
		String trainDir = "/Users/sjonany/Desktop/CSE 454/debug/";
		//String trainDir = "/Users/sjonany/Desktop/CSE 454/debug/";
		//preprocess(trainDir +"train.pb.gz", trainDir );
		Train.train(trainDir);
	   
		boolean b = true;
		if(b) return;
		
		
		String sourceGuid = "/m1232";
		String destGuid = "/m123124";
		String relType = "/location/location/contains";
		
		String[] sentences = new String[]{
				"I like cake.",
				"He likes cake even more!"
		};
		
		String[][]features = new String[][]{
				new String[]{
						"PERSON -> FOOD",
						"In between, cake"
				},
				new String[]{
						"PERSON -> FOOD",
						"even -> more"
				}
		};
		
		
		
		Relation.Builder builder = Relation.newBuilder();
		builder.setSourceGuid(sourceGuid);
		builder.setDestGuid(destGuid);
		builder.setRelType(relType);
		
		int i=0;
		for(String sentence: sentences){
			RelationMentionRef.Builder refMention = RelationMentionRef.newBuilder();
			refMention.setFilename("filename");
			refMention.setDestId(-123214);
			refMention.setSourceId(-12234314);
			refMention.setSentence(sentence);
			for(String feature : features[i]){
				refMention.addFeature(feature);
			}
			
			builder.addMention(refMention);
			i++;
		}
		
		Relation r1 = builder.build();
		
		String sourceGuid2 = "/m13232";
		String destGuid2 = "/m123121224";
		String relType2 = "/people/ethnicity/includes_groups";
		
		String[] sentences2 = new String[]{
				"I hate cake.",
				"He hates cake even more!"
		};
		
		String[][]features2 = new String[][]{
				new String[]{
						"PERSON -> FOOD",
						"In between, cake"
				},
				new String[]{
						"PERSON -> FOOD",
						"hates even -> more"
				}
		};
		
		
		
		Relation.Builder builder2 = Relation.newBuilder();
		builder2.setSourceGuid(sourceGuid2);
		builder2.setDestGuid(destGuid2);
		builder2.setRelType(relType2);
		
		int i2=0;
		for(String sentence: sentences2){
			RelationMentionRef.Builder refMention = RelationMentionRef.newBuilder();
			refMention.setFilename("filename2");
			refMention.setDestId(-12321423);
			refMention.setSourceId(-122343314);
			refMention.setSentence(sentence);
			for(String feature : features2[i2]){
				refMention.addFeature(feature);
			}
			
			builder2.addMention(refMention);
			i2++;
		}
		
		Relation r2 = builder2.build();
	
		FileOutputStream out = new FileOutputStream("/Users/sjonany/Desktop/CSE 454/debug/train.pb", true);
		r1.writeDelimitedTo(out);
		//TODO: add separator?
		r2.writeDelimitedTo(out);	
	}
	
	public static void preprocess(String trainFile, String outDir) 
			throws IOException {
					
				String mappingFile = outDir + File.separatorChar + "mapping";
				String modelFile = outDir + File.separatorChar + "model";
				
				{
					String output1 = outDir + File.separatorChar + "train";
					ConvertProtobufToMILDocument.convert(trainFile, output1, mappingFile, true, true);
				}
				
				{
					Model m = new Model();
					Mappings mappings = new Mappings();	
					mappings.read(mappingFile);
					m.numRelations = mappings.numRelations();
					m.numFeaturesPerRelation = new int[m.numRelations];
					for (int i=0; i < m.numRelations; i++)
						m.numFeaturesPerRelation[i] = mappings.numFeatures();
					m.write(modelFile);
				}		
			}
}
