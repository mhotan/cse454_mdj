package trainingDataGenerator;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * prunes the mapping we have from the freebase wex dump 
 * to just those that exist in sentences.wikification for confidence > THRESHOLD
 * @author sjonany
 */
public class WikiMappingPruner {
	private static double MIN_WIKI_CONF = 0.5;
	//private static String OUTPUT_PATH = "/Users/sjonany/Desktop/CSE 454/wikimapping.txt";
	//stores the sentence ID which has last been SUCCESSFULLY wikified
	//private static String LOG_PATH = "/Users/sjonany/Desktop/CSE 454/lineCount.txt";
	//private static String INPUT_PATH = "/Users/sjonany/Desktop/CSE 454/input/snippet/sentences.wikification";
	//private static String WIKI_MAPPING_PATH = "/Users/sjonany/Desktop/CSE 454/input/wiki_freebase_mapping.tsv";
	//how long to wait if wiki-freebase query fails before trying again
	
	//how many lines to memorize per batch
	//note: there is a total of 8,523,769 batch lines
	private static int LINE_PER_BATCH= 1000000;
	
	//TODO: in case of frequent failure, time to implement recovery
	//go to the last line number, and rebuild mapping to prevent duplication
	public static void main(String[] args) throws IOException{
		if(args.length != 4){
			System.out.println("Usage = INPUT_PATH OUTPUT_PATH LOG_PATH WIKI_MAPPING_PATH");
			return;
		}
		
		String INPUT_PATH = args[0];
		String OUTPUT_PATH = args[1];
		String LOG_PATH = args[2];
		String WIKI_MAPPING_PATH = args[3];
		//<K,V> = <wiki article name, freebase guid>

		//TODO: if handle recovery from failure, need to append
		PrintWriter mappingWriter = new PrintWriter(new BufferedWriter(new FileWriter(OUTPUT_PATH, true)));
		PrintWriter logWriter = new PrintWriter(new BufferedWriter(new FileWriter(LOG_PATH, true)));

		
		HashMap<String,String> mapping = new HashMap<String, String>();
		BufferedReader knowledgeReader = new BufferedReader(
				new InputStreamReader(
						new FileInputStream(WIKI_MAPPING_PATH), "UTF-8"));
		
		//TODO: too lazy to implement recovery. Last time it fails after first batch is processed
		//so skip those lines
		//batch starts at 0, change this line if you want to start at other batches
		final int BATCH_NUM_START= 3;
		
		int curBatch = 0;
		for(; curBatch < BATCH_NUM_START; curBatch++){
			System.out.println("Skipping batch :" + curBatch);
			for(int i=1; i<= LINE_PER_BATCH; i++){
				knowledgeReader.readLine();
			}
		}
		
		//need to divide the freebase mapping file into batches because too big to fit in memory
		//for each batch, take a subset of freebase mapping, write a subset of that subset that will
		//actually be used
		while(true){
			System.out.println("Batch = " + curBatch);
			//set up batch
			mapping.clear();	
			BufferedReader corpusReader = new BufferedReader(
					new InputStreamReader(
							new FileInputStream(INPUT_PATH), "UTF-8"));
			String knowledgeLine = knowledgeReader.readLine();
			int knowledgeCount = 0;
			while(knowledgeLine != null && knowledgeCount < LINE_PER_BATCH){
				String[] tokens = knowledgeLine.split("\t");
				//0 =guid, 1= article 
				String articleName = tokens[1].trim();
				//because sentences.wikification replaces all whitepspaces with '_'
				articleName = articleName.replace(' ', '_');
				mapping.put(articleName, tokens[0].trim());
				knowledgeCount++;
				knowledgeLine = knowledgeReader.readLine();
			}
			
			//process batch
			String corpusLine = corpusReader.readLine();
			int corpusLineCount = 0;
			while(corpusLine != null){
				List<Wiki> wikis = getWiki(corpusLine);
				
				for(Wiki wiki : wikis){
					if(wiki.confidence > MIN_WIKI_CONF){
						if(mapping.containsKey(wiki.articleName)){
							mappingWriter.write(mapping.get(wiki.articleName) + "\t" + wiki.articleName +"\n");
							mappingWriter.flush();
							mapping.remove(wiki.articleName);
						}
					}
				}
				String logLine = curBatch + "\t" + corpusLineCount +"\n";
				logWriter.write(logLine);
				logWriter.flush();
				corpusLine = corpusReader.readLine();
				corpusLineCount++;
			}
			
			//cleanup current batch
			corpusReader.close();
			curBatch++;
			
			if(knowledgeLine == null){
				break;
			}
		}
		knowledgeReader.close();
		mappingWriter.close();
		logWriter.close();
	};
	
	private static List<Wiki> getWiki(String text){
		String[] tokens = text.split("\t");	
		List<Wiki> wikis = new ArrayList<Wiki>();

		for(int i=1; i<tokens.length; i++){
			String[] wikiTokens = tokens[i].split(" ");
			Wiki wiki = new Wiki();
			wiki.articleName = wikiTokens[2];
			wiki.confidence = Double.parseDouble(wikiTokens[3]);
			wikis.add(wiki);
		}

		return wikis;
	}
	
	private static class Wiki{
		public double confidence;
		public String articleName;
	}
}
