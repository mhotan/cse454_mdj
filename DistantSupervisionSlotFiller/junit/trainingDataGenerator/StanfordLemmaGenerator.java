package trainingDataGenerator;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Properties;
import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

/**
 * generate lemma annotation for the corpus because we are not provided one yet
 * @author sjonany
 *
 */
public class StanfordLemmaGenerator {
	public static void main(String[] args) throws Exception{
		if(args.length != 1){
			System.out.println("Usage: outpath");
			return;
		}

		AnnotatedCorpusReader corpus = new AnnotatedCorpusReader(
				TrainingConfig.INPUT_ANNOTATIONS_BASE_DIR, TrainingConfig.INPUT_CONSTANTS);
		PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(args[0], true)));
		Properties props = new Properties();
		props.put("annotators", "tokenize, ssplit, parse, lemma");
		StanfordCoreNLP nlp = new StanfordCoreNLP(props);
		
		int i=0;
		while(corpus.hasNext()){
			if(i % 100000 == 0){
				System.out.println(i);
			}
			i++;
			AnnotatedSentenceParserAdapter annot = new AnnotatedSentenceParserAdapter(corpus.next());
			String sentence = annot.getText().sentence;
			int sentenceId = Integer.parseInt(annot.getText().globalSentenceId);
			
			Annotation document = new Annotation(sentence);

			// run all Annotators on this text
			nlp.annotate(document);

			// these are all the sentences in this document
			// a CoreMap is essentially a Map that uses class objects as keys and has values with custom types

			String lemmaResult = "";
			CoreMap annotatedSentence = document.get(SentencesAnnotation.class).get(0);
			// traversing the words in the current sentence
			// a CoreLabel is a CoreMap with additional token-specific methods
			for (CoreLabel token: annotatedSentence.get(TokensAnnotation.class)) {
				lemmaResult += token.get(LemmaAnnotation.class) + " ";
			}
			
			writer.write(sentenceId + "\t" + lemmaResult + "\n");
		}
		writer.close();
		corpus.close();

	}
}
