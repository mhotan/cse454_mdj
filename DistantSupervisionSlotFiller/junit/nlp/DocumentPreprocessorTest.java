package nlp;

import java.util.List;

import nlp.PreprocessedSentence.SentenceMetaInfo;

import org.junit.Assert;
import org.junit.Test;

import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

public class DocumentPreprocessorTest {

	@Test
	public void preprocessSentenceTest() {
		String text = "Michael William Jackson lives in Ohio," + 
				" and has been a member of the Wildlife Foundation since July 1, 2013, " + 
				"and he is 32 years old, has $32.23 in his bank account, and danced at 5 pm.";
		
		StanfordCoreNLP nlpProcessor = CoreNLP.getInstance();
	    Annotation annotatedDoc = new Annotation(text);
	    nlpProcessor.annotate(annotatedDoc);
	    List<CoreMap> sentences = annotatedDoc.get(SentencesAnnotation.class);
	    
	    SentenceMetaInfo sentenceMetaInfo = new SentenceMetaInfo("id1234", "file", 1234, 0, 100);
		PreprocessedSentence result = DocumentPreprocessor.preprocessSentence(sentences.get(0), sentenceMetaInfo);
		
		Assert.assertEquals(result.getNerTags().toString(), "[PERSON, O, O, LOCATION, O, O, O, O, O, O, O, O, ORGANIZATION, O, DATE, O, O, O, O, DURATION, NUMBER, DURATION, O, O, MONEY, O, O, O, O, O, O, O, O, TIME, O]");
		Assert.assertEquals(result.getChunks().toString(), "[Michael William Jackson, lives, in, Ohio, ,, and, has, been, a, member, of, the, Wildlife Foundation, since, July 1, 2013, ,, and, he, is, 32, years, old, ,, has, $32.23, in, his, bank, account, ,, and, danced, at, 5 pm, .]");
		Assert.assertEquals(result.getTokenSpans().toString(), "[0-23, 24-29, 30-32, 33-37, 37-38, 39-42, 43-46, 47-51, 52-53, 54-60, 61-63, 64-67, 68-87, 88-93, 94-106, 106-107, 108-111, 112-114, 115-117, 118-120, 121-126, 127-130, 130-131, 132-135, 136-142, 143-145, 146-149, 150-154, 155-162, 162-163, 164-167, 168-174, 175-177, 178-182, 182-183]");
	}

}
